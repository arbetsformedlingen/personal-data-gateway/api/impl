# Release Notes - Personal Data Gateway Api

## Upcoming version...

Clients that already have obtained an apiKey prior to this version, must use the base64 encoded value of the apiKey as
the "X_AUTH_KEY" header in authenticated requests.

This is a major version bump and the default webserver context path changes from `/v1` to `/v2`.

If you serve datatypes via your deployed instance of Personal Data Gateway Api, then the context path for those
datatypes will change from `/v1` to `/v2`. Therefore, you must manually update the context path for all rows in
your `datasource` table where column `datatype_url` refers to resources served by your deployed instance of PDG Api.

## v0.3.0

Make sure to first deploy version `v0.2.2`, so that your database schema is up-to-date with the required columns.

You must make sure to populate the following columns before deploying version `v0.3.0`:

- client.org_name
- client.org_nr
- datasource.org_name
- datasource.org_nr

These values are essential for the migration performed by version `v0.3.0`, that puts this information into the "
organization" table.
