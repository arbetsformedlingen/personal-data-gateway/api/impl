# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [4.1.1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v4.1.0...v4.1.1) (2025-02-28)


### Bug Fixes

* add jdk.management module to enable cpu monitoring ([63cbfb1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/63cbfb1dbe52661c9fc1a945f38e4a45a9ed959b))
* **deps:** update dependency org.liquibase:liquibase-core to v4.31.1 ([b2a1258](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/b2a12585d6ce8ad066738dd77cf0281b336d085f))
* **deps:** update logback monorepo to v1.5.17 ([4481d68](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/4481d685eb319186b9f729b92a197eb0558021aa))
* **deps:** update spring boot to v3.4.3 ([b8ee3e0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/b8ee3e0898590ba0da562cb16aedd7ac73b7691c))

## [4.1.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v4.0.2...v4.1.0) (2025-02-19)


### Features

* add jdk.zipfs module needed by elastic apm agent ([67a7365](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/67a736541555e541ab7d7ba48c146aea48e99921))


### Bug Fixes

* **deps:** update dependency jakarta.validation:jakarta.validation-api to v3.1.1 ([b55333b](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/b55333b2f7be3b03a77857a24ef201e8803d265a))

## [4.0.2](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v4.0.1...v4.0.2) (2025-01-31)


### Bug Fixes

* set hikari maximum pool size to 2 ([37c4e22](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/37c4e22a2e0a52c1a18a04a54c861596ecafba9b))

## [4.0.1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v4.0.0...v4.0.1) (2025-01-30)


### Bug Fixes

* use backward compatible changesets ([bd40efc](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/bd40efcef1fc26b0459bb72f9ce12b8a980d4748))

## [4.0.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v3.2.0...v4.0.0) (2025-01-28)


### ⚠ BREAKING CHANGES

* add support for Oracle databases. Flattened all Liquibase migrations into a single one.

### Features

* add support for Oracle databases. Flattened all Liquibase migrations into a single one. ([1c66498](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/1c664985de538ac2f191442dee14ab511aba7358))


### Bug Fixes

* **deps:** update dependency io.swagger.core.v3:swagger-annotations-jakarta to v2.2.28 ([e30c347](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/e30c34772a78cdc49b16c5dd248bd0951283fbe0))
* **deps:** update dependency org.liquibase:liquibase-core to v4.31.0 ([800a99d](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/800a99d68e9498638b1ede28fc843cd4f3907e85))
* **deps:** update dependency org.postgresql:postgresql to v42.7.5 ([f994959](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/f99495999a8cf1d8f4ea12299260bc316f1b251e))
* **deps:** update spring boot to v3.4.2 ([c13947f](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/c13947f62522ada08d08278bc77ff41e39b4778e))
* sonar cube security hotspots ([b42d0a4](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/b42d0a47ca5940195288ff44ea3b33de3bd9bbf2))

## [3.2.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v3.1.3...v3.2.0) (2025-01-09)


### Features

* log http fields, do audit logging only for authenticated requests ([91f8d3e](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/91f8d3e94bec122ab9d359bd3caa84f468181a11))


### Bug Fixes

* **deps:** update logback monorepo to v1.5.15 ([c1ef1f2](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/c1ef1f2ab662b46172a29b757ea243b3479b2858))
* **deps:** update logback monorepo to v1.5.16 ([d918f9d](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/d918f9dc8698ced2631b6a978414ac657d36ab20))
* put SecureRandom in static final field ([d957fbb](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/d957fbbf343d344303f53a02c4062b96f7e2016f))

## [3.1.3](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v3.1.2...v3.1.3) (2024-12-20)


### Bug Fixes

* return service unavailable when not ready ([416d2f6](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/416d2f63def26c9ccc6e0bc4b712968ad3da31e7))

## [3.1.2](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v3.1.1...v3.1.2) (2024-12-20)


### Bug Fixes

* **deps:** update spring boot to v3.4.1 ([6ad8402](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/6ad840239be6ad833b2a68591c2a4141bcccc9cd))

## [3.1.1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v3.1.0...v3.1.1) (2024-12-19)

## [3.1.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v3.0.0...v3.1.0) (2024-12-19)


### Features

* implement audit logging ([48c4513](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/48c45138aa51fa87ae77a5295f6ed633ddde7d2c))

## [3.0.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v2.2.0...v3.0.0) (2024-12-13)


### ⚠ BREAKING CHANGES

* adopt api spec v4

### Features

* adopt api spec v4 ([10d4728](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/10d4728431dbe156ecb6e377a00ec62b0f9e472e))
* rate limit based on profiles, the security role for logged in users and a default profile for anonymous users ([2d031f7](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/2d031f76774d7786e800e4ed4a73595f78950c1a))


### Bug Fixes

* **deps:** update dependency com.fasterxml.jackson.core:jackson-databind to v2.18.2 ([2e004f6](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/2e004f68f242fa3413c838abdabde222323730e9))
* **deps:** update dependency com.fasterxml.jackson.datatype:jackson-datatype-jsr310 to v2.18.2 ([cfc59e0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/cfc59e068a85e71b203b7022ecab3a66454ac882))
* **deps:** update dependency io.swagger.core.v3:swagger-annotations-jakarta to v2.2.26 ([425b6d9](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/425b6d91bd75dd61f6f545f048db56f7d6f51412))
* **deps:** update dependency io.swagger.core.v3:swagger-annotations-jakarta to v2.2.27 ([c2c5e58](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/c2c5e587cee28ac7c1a84ad3796b450d9e8d3880))
* **deps:** update dependency org.projectlombok:lombok to v1.18.36 ([6b3cc1a](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/6b3cc1a869b8701491b09c7ca5de77e10de5b31e))
* NPE when there is no session ([cf44056](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/cf4405628fd1538b1da03d8effad87f2d72e2c91))

## [2.2.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v2.1.0...v2.2.0) (2024-11-13)


### Features

* add buildVersion to the api-info endpoint ([3651cc7](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/3651cc7cc1c7ac08509ea09802ffff7f583ad44a))


### Bug Fixes

* change version info in application.properties ([4e242c7](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/4e242c7844f4672ab066239f8b3ed22d4ac49df8))

## [2.1.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v2.0.0...v2.1.0) (2024-11-12)


### Features

* implement api-info and health endpoints ([a82ce93](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/a82ce937e0aaec6a51640892a1b2141f914e74b6))
* implement simple rate limiting ([4ffcf53](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/4ffcf53622201afcf5b2086a9c30898b2ac4885e))


### Bug Fixes

* **deps:** update dependency org.liquibase:liquibase-core to v4.30.0 ([d7a71d4](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/d7a71d46478bb154c21fbe02686e1e22eeea4442))
* use a default password for h2 connection ([bdcca65](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/bdcca65fb941a4b99cb6b15dd73c57a8337a137d))

## [2.0.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v1.0.0...v2.0.0) (2024-10-30)


### ⚠ BREAKING CHANGES

* adopt pdg api spec v3.0.0
* adopt pdg api spec v3.0.0
* POST /user/{user-id}/sharings now responds with "200 - OK" if a similar (and active) sharing already exists.
* setting api key via patch client, is no longer supported
* clients are expected to use a base64 encoded version of their

### Features

* a default admin client will be created if no active admin client exists (used by api-tests job) ([d88932a](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/d88932abed6d91bfcad1bc35af25b92d9886c57d))
* add 'client_key_reset' table to database schema ([2f1913d](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/2f1913d23b9f6b2d16a6c49b987e85b14b1da193))
* add table sharing_access for logging data access of sharings ([1d74dda](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/1d74dda2d79837271a726c51bd72554d979278cb))
* adopt pdg api spec v3.0.0 ([fb4ff95](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/fb4ff95018bdc4f430dc01eac99e5b9aca21f01f))
* adopt pdg api spec v3.0.0 ([900b61d](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/900b61da41c7edd5118ce927fef93d7e159fb957))
* clients are expected to use a base64 encoded version of their ([5a1d3d1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/5a1d3d1a93a9b396e3ec46692ac31b2c3d4a7d4f))
* implement createSharingAccess and call it from getData in dataController ([b487055](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/b48705506db1218f7c82fb9bff02f877cb789e88))
* implement datasource faq endpoints, add tests ([f67816c](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/f67816c77c0b88a0642499805783f43a4b890ce6))
* implement GET dataAccess and dataAccesses in DataController ([9d3200d](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/9d3200dafba8e42fe3738e78745767812955b505))
* Implement hashing of client api keys in database ([fe408e6](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/fe408e68edc0b64d493e1188cbe5d6e5ed37047a))
* implement self reference in Link header ([9045d39](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/9045d396905fc5d3181350b7502cf6f464ec7d16))
* implementation of post and get clients key-reset endpoints accordingly to api spec v2 ([ddf2f51](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/ddf2f514c7856bc76a0fd38e81f4d546e914fe40))
* log level can now be set via LOG_LEVEL environment variable, defaults to INFO ([bba1ab7](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/bba1ab7264cd9b35c0d74c3fa9ccec504db2bc39))
* log warning when requests are rejected due to bad credentials ([10aec0f](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/10aec0f47d7fc7d27e29248a1b87ac33797647dc))
* order faqs by created ([8c6acc1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/8c6acc1e3917d98d391795829debe574180d66fe))
* POST /user/{user-id}/sharings now responds with "200 - OK" if a similar (and active) sharing already exists. ([02c4d93](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/02c4d935897f24f7b78261ac3cc80a326b95fedc))
* setting api key via patch client, is no longer supported ([6124b6b](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/6124b6b6e50ba453da9aff53dc4e64e8107753b7))
* use API spec 3.1.1 where getDatasourceFaq typo has been fixed ([a34fb0b](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/a34fb0b5315be97ccca92d4460c100d15dd5dfa2))


### Bug Fixes

* add foreign constraint in hibernate-reveng.xml and a replacement of generated annotation in the pom ([82103ca](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/82103ca5acaf96f1d7313a38c92758a63f26e368))
* add request method patterns for datasource faq endpoints to SecurityConfig ([654471d](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/654471dcbcef17737178969b46a309c73db6c2b1))
* client revocation timestamp must not be in the past ([be96214](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/be9621475759def027eb2be8f8ab18de9c6c0484))
* constructor ([8ec92a5](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/8ec92a53626c1543c604c0f50dd4ec9efe8d5bfe))
* controllers now produce more [and better] log messages when encountering failure states ([46caa85](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/46caa855722bb65ca5cef82a44314d6c2e27dffa))
* datasource faq tests ([541d33b](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/541d33b62f260dd0cfa30e3e697672b1db68aa5f))
* datasources/{datasource-id}/users/{user-id}/data now propagates error UNAVAILABLE_FOR_LEGAL_REASONS, for all other errors BAD_GATEWAY is returned to the caller ([fdcf698](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/fdcf698bfa53d0bd38841a2cbed3e8980ebf60d6))
* **deps:** update dependency com.fasterxml.jackson.core:jackson-databind to v2.18.0 ([87e2d22](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/87e2d223bddc683cb336fc93fd47eb5efd31ed1f))
* **deps:** update dependency com.fasterxml.jackson.core:jackson-databind to v2.18.1 ([9a47480](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/9a474801e3e8902aa74372621b52db6d97eb9cb6))
* **deps:** update dependency com.fasterxml.jackson.datatype:jackson-datatype-jsr310 to v2.17.2 ([5e2641f](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/5e2641fab08ae717c3bc72fb7095d3f8defb40ef))
* **deps:** update dependency com.fasterxml.jackson.datatype:jackson-datatype-jsr310 to v2.18.0 ([c02d6da](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/c02d6da12b96af58265dbb6fa57a5843f16bcf0e))
* **deps:** update dependency com.fasterxml.jackson.datatype:jackson-datatype-jsr310 to v2.18.1 ([2b1b3b2](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/2b1b3b2329014924cec6e60efc52d693469c67c2))
* **deps:** update dependency io.swagger.core.v3:swagger-annotations-jakarta to v2.2.23 ([3757a65](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/3757a65a6516e7a422f7cc3cb08c191a88e127c3))
* **deps:** update dependency io.swagger.core.v3:swagger-annotations-jakarta to v2.2.24 ([53c1c76](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/53c1c76dc84f3ef3634799f55c1e4af14e421e02))
* **deps:** update dependency io.swagger.core.v3:swagger-annotations-jakarta to v2.2.25 ([fae4e3b](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/fae4e3ba25b64b06c5fdba63e74d010aafe4575c))
* **deps:** update dependency net.logstash.logback:logstash-logback-encoder to v8 (major) ([a5cf038](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/a5cf03853fdf36fbca31af4e685434dc59a26133))
* **deps:** update dependency org.liquibase:liquibase-core to v4.29.0 ([1667b7e](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/1667b7eb732503064d500305f793b655c0c13203))
* **deps:** update dependency org.liquibase:liquibase-core to v4.29.1 ([61b13cb](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/61b13cb3c1411a449698e7989ca6bcff4d071ead))
* **deps:** update dependency org.liquibase:liquibase-core to v4.29.2 ([890eb73](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/890eb7325901be79418d28236d4e746d474e8132))
* **deps:** update dependency org.postgresql:postgresql to v42.7.4 ([c123d7e](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/c123d7eacf7bbe95c7218a4fa5fe914f0c70694d))
* **deps:** update dependency org.projectlombok:lombok to v1.18.34 ([a8a813c](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/a8a813c6cefd2b80b30920c6dc9110fddadf8243))
* **deps:** update dependency org.springframework:spring-web to v6.1.10 ([a966277](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/a966277d5b9629ca696d3c5c317e2c6c11ac06b1))
* **deps:** update dependency org.springframework:spring-web to v6.1.11 ([0124e44](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/0124e44c7034fbaeff8b0c748c7b167673927b62))
* **deps:** update dependency org.springframework:spring-web to v6.1.12 ([a91b73e](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/a91b73eb7d2ad7cfb0e83fa9f8172ea6b4a57632))
* **deps:** update dependency org.springframework:spring-web to v6.1.13 ([4917474](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/49174743885f81c1f837be5e7d9ad9d8b4edfd8a))
* **deps:** update dependency org.springframework:spring-web to v6.1.14 ([0b3a7c9](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/0b3a7c9e3fef48e1b8b5003c490f26f273638756))
* **deps:** update dependency org.springframework:spring-web to v6.1.9 ([1130c54](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/1130c5478e0d892ca20a19b3856239ca9ec89d82))
* **deps:** update jackson.version to v2.17.2 ([d97c208](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/d97c208dbb8443b0508ea7057343d80437538d28))
* ensure pnr is redacted from log output when masker is used ([7338672](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/73386723afbd6e7f23b3514c689cbf0576bd579e))
* escape hexadecimal key hash in insert command ([61c86c7](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/61c86c748719dc6a5ca80d82b347c1f80fef862c))
* incorrect condition for checking for wildcards in data response content-type, and corrected the response status code to UNAUTHORIZED when data preview could not be fetched due to unknown user id ([3e365b7](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/3e365b7cfce512c748f18037b3666500a675221a))
* let linkHeaders return HttpHeaders instead of the value of the Link header ([7464ac0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/7464ac07f271d97d6278de2b9623a90895af6fba))
* logfilter now outputs all messages with http status lower than 400 as log level 'info', otherwise 'error' ([166eb81](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/166eb81b728b557a0afc5de5df4ec28aa7b0c877))
* order sharings by created in the get sharings response ([8ea9006](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/8ea9006c73ed1e90f2f88076947b4eb48b964092))
* prevent nullpointerexception for public sharings ([ed7a547](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/ed7a547b16defc153273ef933bcd18694f9a107e))
* remove dummy file ([2c6e5a2](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/2c6e5a2252b430a06667f218bb2a789e31072a6f))

## [1.0.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v0.3.1...v1.0.0) (2024-06-10)


### Features

* adopt API spec v1 ([f18e812](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/f18e812e3eabbd9ab466e0171223acc4bc149c30))
* adopt to API spec version v0.15.1 ([8a8ffc2](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/8a8ffc213706f1b27114996ffe3a4174a09fd082))


### Bug Fixes

* add client redirect endpoints to security config ([b1f7cff](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/b1f7cff7e11a40243e2e22ae0bcfd9359855d743))
* **deps:** update dependency org.liquibase:liquibase-core to v4.28.0 ([c877221](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/c8772212ec48fea0efb71f8f20ab77e00236ff27))
* **deps:** update dependency org.springframework:spring-web to v6.1.8 ([90f6b27](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/90f6b27af9012f4f696c8a093724bf95a05a3821))
* use status NOT_FOUND instead of BAD_REQUEST where appropriate, align tests ([2f4fde7](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/2f4fde718746291ac20d98b2372dd87585476176))

## [0.3.1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v0.3.0...v0.3.1) (2024-05-22)


### Features

* align the implementation to v0.13.1 of the API specification ([52e0175](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/52e0175c69aed5844b46c92170f5805de2949b91))
* align with api spec v0.14.0 ([cad6bf2](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/cad6bf21c9219dad225b921e5dc1a34f5a3a284b))


### Bug Fixes

* align to last migration changeSet where not null contraint are added to the updated fields ([e28994f](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/e28994f00e9ba91d4dc4b456108a127614f6dd00))
* **deps:** update dependency io.swagger.core.v3:swagger-annotations-jakarta to v2.2.22 ([2a07d59](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/2a07d59ba0912706390dfa6d5e85217ae0d0a552))
* **deps:** update dependency jakarta.validation:jakarta.validation-api to v3.1.0 ([62b4d27](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/62b4d2709dbef9d5fbe95d1df387febc61ccc8e1))
* **deps:** update dependency org.springframework:spring-web to v6.1.7 ([bfddae3](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/bfddae3b11295c47c522b8890c85e3e9828e649a))
* populate the 'deleted' property in get datatypes FAQ response ([719273f](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/719273f341ceffa31190101e5740829a08832b93))

## [0.3.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v0.2.2...v0.3.0) (2024-05-14)


### ⚠ BREAKING CHANGES

* migrate organization name and number into the 'organization' table from the 'client' and 'datasource' tables, add the foreign key 'organization_id' to the 'client' and 'datasource' tables

### Features

* add constraints to 'organization_id' foreign key and align source code to changes in generated entities and models ([9b28d21](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/9b28d2123eccbbd5fa117bd3938f5b0faccd49d7))
* add exception logging in ControllerExceptionHandler and OrganizationService ([8d86e3f](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/8d86e3f0a2445d5504b296251af3dd4a55b2a954))
* migrate organization name and number into the 'organization' table from the 'client' and 'datasource' tables, add the foreign key 'organization_id' to the 'client' and 'datasource' tables ([018839d](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/018839de55fa3b9d1b5ba30590fa6096ab32fa70))


### Bug Fixes

* omit organization data migration for h2 databases ([050ae90](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/050ae90b992213aedac2035cbdeb74377004e805))

## [0.2.2](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v0.2.1...v0.2.2) (2024-05-08)


### Features

* prepare client and datasource tables to have information about organization name and number, so that we have sufficient information to migrate this information to the 'organization' table ([838a010](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/838a0106650f103abe87b53c2bf40c62562bad57))

## [0.2.1](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v0.2.0...v0.2.1) (2024-05-08)


### Features

* add 'organization_id' to client table ([23fb8f3](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/23fb8f361d7d90a0e6770e679247521173794b9f))

## [0.2.0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/compare/v0.1.0...v0.2.0) (2024-05-07)


### ⚠ BREAKING CHANGES

* added 'organization_id' to datasource endpoints, 'organization_id' is now required in POST datasources request body

### Features

* added 'organization_id' to datasource endpoints, 'organization_id' is now required in POST datasources request body ([b1a6430](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/b1a64307ce534670ee4265a21e76848b69b3f15e))
* added organization endpoints ([37c2bc3](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/37c2bc3705fe27c2dc7b4adb42b49b2dfd095c7e))
* upgrade database schema to support organisations, and added 'deleted' and 'organization_id' columns to datasource table ([d76affb](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/d76affbbe567ae5dcc6fc1b9be84aa6039c73842))
* upgrade to api spec v0.12.3 ([e758d92](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/e758d92eb21c24c26069b1c587d1d56f7ba48a36))


### Bug Fixes

* **deps:** update dependency com.fasterxml.jackson.datatype:jackson-datatype-jsr310 to v2.17.1 ([d828b65](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/d828b6584ffaabaa87ed8b6129f96980944b261b))
* **deps:** update jackson.version to v2.17.1 ([59ae1d3](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/59ae1d3815e7ff7083cf097e24088a52b161e305))

## 0.1.0 (2024-04-29)


### ⚠ BREAKING CHANGES

* adopt spec v0.12.0

### Features

* add datasource controller end service, tests (not ceomplete) ([18bb9a2](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/18bb9a215b85bc4741fbb799ca02175af1e93ed2))
* Adopt API specification 0.7 ([5cc9a39](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/5cc9a39bc82cc6724bd2e99838e141358a009018))
* adopt spec v0.12.0 ([c08e336](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/c08e3368cbf024f997c14b38fdd0d20ade2da996))
* align to version 0.3 of the api for added orgNr, some refactoring ([b7a85b5](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/b7a85b52ef260e7a30ecda8fe528af849550744d))
* code for datatype and schema validation ([2879260](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/287926088e0a18abd16678f11ae6ab2f81b53077))
* context path will be tha major version of the API spec unless the configuration property 'server.servlet.context-path' is defined ([92e4e1b](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/92e4e1b238471ef8efb4d70c54fae230760ab1d8))
* data controller implementation, tests ([438f080](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/438f0809865bbb493d3d0bc48502ddca2499e2c2))
* decouple datatype, add datatype post implementation ([5ea2e3c](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/5ea2e3cb043ce4226f1ba8dc18ed8f2adb549619))
* implement controller, service and corresponding tests ([43b5523](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/43b5523ef04528b745a1f904f0a85a840b53440a))
* implement getDatasourceUserData, add tests ([9aec57d](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/9aec57dec5a8af9895b8dc6a2535ab763dafc22e))
* implemented datasource config endpoints ([f336071](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/f3360711e7c7cf9b268fcec6ad083947ec049a26))
* initial ([048bdf7](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/048bdf77f173d08066d6610472297ecc4af7d086))
* new substitution token '{user-id}' supported in datasource 'path' configuration ([7599769](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/7599769cb2601b447a36e9f3681efcb81c5a5378))
* pass errors from the data source unmodified ([e4ced06](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/e4ced06e7f21316f530e66bfe566b6bdf3d731e7))
* test client controller ([738f918](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/738f9184a035c3da38e9c8b7eba764e0e1f6d644))
* use openapi spec v0.9.0 ([42305a4](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/42305a4c9afbe08c2e77a197f62f76aeb5b19433))
* use v0.8.0 of the API ([ff717b0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/ff717b0421822e3f6b36c669a1635109cfbd50d1))


### Bug Fixes

* add APP_VERSION variable to choose folder inside infra overlay ([48f34f6](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/48f34f61b6e5d4ba91c446b104ba88a3a23a5f0c))
* add request matcher patch datasources for admin role ([851cf9c](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/851cf9c98d30275b9434902863179122c1950258))
* add updated and headers ([7d270b7](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/7d270b7aa68a11c40df2f5001db9cda98ac5673e))
* add value annotations to constructor, make GET /datatypes not requesting auth key ([2fb3f74](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/2fb3f742b7c268696f133e19a12edd0b0a511667))
* aligt ti API v0.2, fix and add tests ([2b37130](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/2b37130aada7cce21ca1548256a32dd05b2127b5))
* allow 'frontend'-role to access 'GET /clients/{client-id}'-endpoint ([ca7ff54](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/ca7ff54096627e5eca4ed930e65a1cc96f42802d))
* check presence before accessing instance via Optional.get() ([af9ba75](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/af9ba750831838c9c9a150d47fcfbf89e43f6c91))
* check presence before accessing instance via Optional.get() ([245a7a2](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/245a7a23f0420bf629aa5dffcb58403b51e9a0fc))
* correct the remote set-url origin to be the $clone_url ([7492edb](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/7492edb65ba8928a2ef4560e803dc7d5cbf321b4))
* db migrate 'client.role' where 'client' to 'consumer' ([4f1e1a6](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/4f1e1a66fd866ebe628c1599107a28e748ee22ff))
* **deps:** pin org.springframework/spring-web:6.1.6 ([7b8b90b](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/7b8b90b2aacdee7ac7b765640efa57f68894afd0))
* **deps:** update dependency co.elastic.logging:logback-ecs-encoder to v1.6.0 ([4f14ce2](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/4f14ce2a31f8452b2e45052c166e3a4635b911a4))
* **deps:** update dependency com.fasterxml.jackson.datatype:jackson-datatype-jsr310 to v2.16.2 ([4fb9d6e](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/4fb9d6e58dc8beea53ec8d1332cd9cba5298afac))
* **deps:** update dependency com.fasterxml.jackson.datatype:jackson-datatype-jsr310 to v2.17.0 ([d7c3dbb](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/d7c3dbb522c26286c8ca84408ebcc415de988f76))
* **deps:** update dependency com.google.guava:guava to v33.1.0-jre ([3641309](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/36413091d7b5abb3efc66f6d0723fe321508cec9))
* **deps:** update dependency io.swagger.core.v3:swagger-annotations-jakarta to v2.2.21 ([17a2e66](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/17a2e66975d16e80c34ed75cbee98a170e11259b))
* **deps:** update dependency org.liquibase:liquibase-core to v4.27.0 ([a0998c3](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/a0998c3ff178edd78e817c7b4f67da04959a12bd))
* **deps:** update dependency org.postgresql:postgresql to v42.7.2 ([6dcdce7](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/6dcdce75fe1b83244aab0177ecfae9331c66eca5))
* **deps:** update dependency org.postgresql:postgresql to v42.7.3 ([b2928f5](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/b2928f55b5cd7da212d8828aa0362387f80902b3))
* **deps:** update dependency org.projectlombok:lombok to v1.18.32 ([b457a90](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/b457a9085751dc7d57a50465d9ebd5ad9cc5f6a9))
* **deps:** update dependency org.springframework.hateoas:spring-hateoas to v2.2.1 ([83d01e5](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/83d01e51957b38e83430cf4bb6d3160d1a12b8cb))
* **deps:** update dependency org.springframework.hateoas:spring-hateoas to v2.2.2 ([b33e2f0](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/b33e2f0c00e8a51d48bce188fc1dbee3aaa53147))
* **deps:** update jackson.version to v2.16.2 ([3c1a9f6](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/3c1a9f6f5384103708c8710023408026588333ed))
* **deps:** update jackson.version to v2.17.0 ([27922f3](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/27922f392f0f7e0bf6a20632d10ee5f7e10f4366))
* Execute permission on super-test.sh. ([d72e1ea](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/d72e1eaeef19e15719ec07d7a6a024fe9483c89a))
* Install bash and run super-test.sh with bash. ([a7a6e81](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/a7a6e8168731b541c5a070d26ef35e1fa01cb2ef))
* merge conflicts ([0428abb](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/0428abba25c8249764c851efff9f587548b79452))
* package name ([05ad183](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/05ad183ece91ab885b8b80e0904a369b7ead9f26))
* PATH must be defined in a separate layer because it depends on the JAVA_HOME value ([49a39eb](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/49a39ebc41760a24ca95b267f731075855376fb5))
* prevent reassignment of 'revoked' in sharings via PATCH request ([eae0a83](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/eae0a83a34b54fa5bd068b48dfe1250d4dd8d82f))
* problem where the Content-Type of data responses included wildcards. ([e2f93f3](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/e2f93f30ef919b8e126357f01fb514bfea683856))
* property 'logotypeUrl' must be provided in request body when calling 'POST /clients' endpoint, otherwise return '400 BAD_REQUEST' ([0beb981](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/0beb981fd375577148d3ebba77ba9133c9d02247))
* property 'name' was incorrectly patched from provided property 'humanFriendlyDescription' ([1a22d40](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/1a22d40debf678efab8a6d998691f7adb4a9b1c8))
* property 'name' was incorrectly patched from provided property 'humanFriendlyName' ([b795ef4](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/b795ef4f4c4e0bb3b7aca760d626395b86aa53c9))
* protect all endpoints as default ([b37cd31](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/b37cd31d950bb0cd24829c238fda1d9212762cff))
* remove request matchers for delete /clients/* and /datatypes/* ([d3c4ec6](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/d3c4ec64e12f6f8414fb7cd8b80d9454e41250bb))
* remove SchemaValidator and DatatypeSchemaExtractor ([919b074](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/919b074838f104ca3cffbf36c88b82dad832fa3d))
* remove some unnecessary ignores ([66a03cf](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/66a03cfff534f7c8d56e74aafbb507cd269d36b1))
* remove the extraction of exception details into the logged message since it sometimes causes StackOverflowException ([267e211](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/267e2116834a9816fa6d37cb0cea0561ed5bcfd2))
* rename placeholder mydata to pdg, remove unused dependencies ([b90251f](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/b90251f3015cca58426a11b3f8e235193ab68b5f))
* return forbidden for revoked sharings ([6ce4921](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/6ce4921ca672f59fb5d50324849e38cb1f40ffde))
* security roles and user sharings datatypeUrl ([78d4456](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/78d445692b8a842e2aec5eee272f47f5fb6ee365))
* test ([c232cf9](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/c232cf9c8b0a6b7391ad29387fabaacc1b41560a))
* use 'pdg-api' as prefix for all custom properties that we define ([97d59ea](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/97d59eae59be88a4018a59be5aba700eab78ad87))
* use personal data gateway openapi-0.1 specification ([ef31a09](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/impl/commit/ef31a09feb801959efb5b6cdb59cc6f2501196c3))
