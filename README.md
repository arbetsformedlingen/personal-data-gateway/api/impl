This repository contains a Java 21 based implementation of the Personal Data Gateway backend component in accordance to this [API Specification](https://gitlab.com/arbetsformedlingen/personal-data-gateway/api/specs).

[[_TOC_]]

# Glossary

This glossary provides definitions for terms related to the Personal Data Gateway.

- **Datatype**: A resource naming and describing data that can be provided by a data source.
- **Datasource**: A resource naming and pointing to a specific data source.
- **Client**: A resource defining a user of the Personal Data Gateway.
- **Role**: Defines the access privileges of a client. Must have one of the roles:
  - **Admin**: Access to onboard and modify new clients, datasources and datatypes.
  - **Frontend**: Access to preview, grant and revoke sharings of data.
  - **Consumer**: Access to shared data by the user.
- **PDG**: Acronym for "Personal Data Gateway".
- **PDG Application**: Frontend application used by an individual to view and share personal data.
- **Sharing request**: Requests for an individual to share data from a specific data source can be made by clients with the consumer role.
- **Sharing**: An individual can accept a sharing request, whereby the frontend appl ication creates a sharing resource.

# Standards compliance

The project aims to be compliant with the following standards:

- [Standard for Public Code](https://standard.publiccode.net/)
- [Semantic Versioning 2.0.0](https://semver.org/)
- [Byggblocksbeskrivning](https://www.digg.se/download/18.23524b0a17e9caeb23da707/1647345431842/informationsutbyte_byggblocksbeskrivning_apihantering_20210129.pdf) (ENA byggblock API-hantering)
- [REST API-profil](https://www.dataportal.se/rest-api-profil) (Sveriges dataportal)

# Technology

The implementation of this API is powered by:

- [Maven](https://maven.apache.org/) for build and dependency management.
- [Spring Boot](https://spring.io/projects/spring-boot) to configure a webserver implementing the REST services.
- [Spring Security](https://spring.io/projects/spring-security) for authentication and access control of the API.
- [Liquibase](https://docs.liquibase.com/home.html) for database migrations.
- [Hibernate Tools](https://hibernate.org/tools/) for reverse engineering and code generation of entities and model files at build time.
- [H2](https://www.h2database.com/html/main.html) acting as a memory database for the Liquibase scripts during the entities and models code generation at build time.
- [OpenAPI Generator](https://openapi-generator.tech/) for automatic code generation for API interfaces and data models at build time.
- [PostgreSQL](https://www.postgresql.org/) for database storage.
- [Mockito framework](https://site.mockito.org/) for mocking in unit tests.
- [Java Code Coverage Library (JaCoCo)](https://www.eclemma.org/jacoco/) for generating test coverage reports.
- [Lombok](https://projectlombok.org/) for boilerplate code generation.
- [Elastic Common Schema (ECS)](https://www.elastic.co/guide/en/ecs/current/ecs-reference.html) for log output  formatting.
- [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) for automatic code quality scanning, linting, building, testing and image publishing. Scans are automatically performed to identify code and dependencies problems, as well as security vulnerabilities.
- [MEND Renovate](https://docs.renovatebot.com/) for automatic dependency housekeeping.

# Specification first

To reduce the amount of code, we opt for a _specification first methodology_, where code generation tools are used to produce source code that are aligned with the specifications.

It is used both when it comes to the endpoints and schemas of the API specification and for the database entities and models. These java source files are code generated at build time and are not checked into source control.

# Security

Spring Security is used for authentication and authorization.

Access control for the three roles **admin**, **frontend** and **consumer** are defined in the **SecurityConfig** class as described by the API specification.

Clients must provide an **X-Auth-Key** as a HTTP request header to access protected endpoints.

# HATEOAS

The endpoints implement a **self** link reference in the payload when applicable, as modelled in the API specification.

# Maven build

The build configuration is found in the **pom.xml** file and the Personal Data Gateway API application is built with

```sh
mvn clean install
```

The build process can be described as follows:

After target directory have been cleaned (deleted), the **maven-antrun-plugin** downloads the OpenAPI specification for the application from GitLab and puts it in the **target/classes/static/openapi.yaml** file. The **openapi-generator-maven-plugin** reades the specification and generates interface- and model files in the **target/generated-sources/openapi/pdg** folder. Next, the **liquibase-maven-plugin** runs migrations and creates an H2 memory database from which the **hibernate-tools-maven** plugin generates entity files in the **target/generated-sources/hibernate-reveng** folder. The **build-helper-maven-plugin** adds the folders for the generated files so that they are included in the compilation. After all the source code has been compiled the **maven-surefire-plugin** compiles the test files and runs all defined unit tests, and with the **jacoco-maven-plugin** code coverage reports are created.

If all tests pass and the requested level of coverage is met, the **spring-boot-maven-plugin** repackages the originally built jar file and creates a standalone fat jar for the application.

# Docker build

The Personal Data Gateway API application is built and deployed as a Docker image.

:warning: The jar file must be compiled before building the docker image.

To make the image smaller and only contain necessary jar libraries, the Docker image is created in two steps.

In the first build step a custom java runtime is being built with **jlink** by first extracting the actively used modules from the application fat jar, where after **jdeps** is used to create a list of active modules as input to **jlink**. The first build step also utilizes the Spring Boot layered jars for more efficient Docker images.

The second build step creates the application docker image based on alpine linux and copies the custom java runtime, dependencies and the application from the first build step.

# CI pipeline

The Personal Data Gateway API is automatically built by the GitLab CI pipeline when source code are committed.

The pipeline stages and jobs are defined in the **.gitlab-ci.yaml** file (an extension of Arbetsförmedlingen Jobtech DevOps ci-pipeline).

The pipeline starts by linting the Dockerfile before compiling the sources and test-sources. Where after unit tests, integration tests and code coverage tests are being run, resulting in a cobertura report.

Next the Spring Boot application is installed and packaged before the Docker image is being built and deployed to our development environment.

Lastly different security related analyses are being performed: code quality scanning, container scanning, maven dependency scanning, secret detection, static application security testing with semgrep, and an integrations test of the API called super tests.

The **super-tests** step uses a Docker executor to install a **PostreSQL** and the newly built **Personal Data Gateway API** in a docker in docker configuration, adds the utilities **curl**, **postgresql-client**, **jq** and **bash**, bootstraps an admin user in the **Personal Data Gateway API** deployment, before it runs the test scripts in **super-test-sh**

The test scipts mimics a real world scenario where **datatype**, **datasource** and a _consumer_ **client** is defined before creating a user **sharing** and letting the _consumer_ **client** retrieve the shared data. After the **sharing** has been revoked another as expected failed request to retrieve data is being made to the _data_ endpoint.

The **jq** utility is being used to parse the responses frfom the API and to validate expected answers.

# System description

The Personal Data Gateway API acts as a gateway to configured API endpoints from where personal data can be fetched as long as the individual admits to share the information digitally.

Before data can be shared from a **datasource**, a corresponding **datatype** has to be created defining the shape of the data provided by the data source. The **datasource** is loosely coupled the **datatype** by a URL reference. (This makes it possible to have data types defined on other servers then the Personal Data Gateway API. Preferably adjacent to the data sources).

Also, data consumers have to be created before they can make requests for users to share any personal data (through a data source).

The creation of data consumers, data sources, (and possibly data types) is done by and administrative account having the **admin** role.

A data consumer can request an individual to share data from a specific API endpoint when de user visits the consumers website. When clicking the link the individual will be redirected to the [Personal Data Gateway Application](https://gitlab.com/arbetsformedlingen/personal-data-gateway/app/impl/-/blob/main/README.md?ref_type=heads) where a request to share data with the consumer are presented. If the individual accepts the sharing request, a **sharing** object is created with a specific uuid and stored in the Personal Data Gateway API database. This uuid are returned to the requesting consumer when the individual are being redirected back to the website of the consumer, and can later be used by the consumer to access data from the shared API.

**Note:** It is not the specific data instance at the time of the sharing that is being shared but read access to the API. Thus, the information can change between accesses.

Data consumers access the shared data through the **/data** endpoint using the uuid of the sharing they received when the user was redirected back from the Personal Data Gateway Application.

Sharing of data from an API to a consumer can be revoked by re-using the request URL for sharing, and by rejecting to share the data. Any active sharing for a datatype/consumer combination will then be revoked.

## Database schema

### FAQ mappings

```mermaid
---
title: Database FAQ mapping tables
---
erDiagram
    Organization {
        UUID id PK
    }
    FAQ {
        UUID id PK
    }
    Datatype {
        UUID id PK
    }
    Organization_FAQ {
        UUID organization_id PK, FK "Organization.id"
        UUID faq_id PK, FK "FAQ.id"
    }
    Datatype_FAQ {
        UUID datatype_id PK, FK "Datatype.id"
        UUID faq_id PK, FK "FAQ.id"
    }
    
    Organization ||--o{ Organization_FAQ : ""
    Datatype ||--o{ Datatype_FAQ : ""
    FAQ ||--o{ Organization_FAQ : ""
    FAQ ||--o{ Datatype_FAQ : ""
```

# Local development

**Note**, for local development you need Git [2.44.0], Maven [3.9.6] and a running PostgreSQL [15.x].

Create an **application-local.properties** in the **src/main/resources** folder with the spring datasource configuration to to PostgeSQL database, and define the base url to the PersonalData Gateway API with the **pdg-api.base-url**. For example:
<pre>
## application-local.properties ###
spring.datasource.url=jdbc:postgresql://127.0.0.1/postgres
spring.datasource.username=[username]
spring.datasource.password=[password]

pdg-api.base-url=http://localhost:8080
</pre>

Build and start the application with:
<pre>
mvn clean install

mvn spring-boot:run -Dspring-boot.run.profiles=local
</pre>
(Use Ctrl-C to stop the application)

Log levels are configured in the **src/main/resources/logback-spring.xml** file.
