package se.jobtechdev.personaldatagateway.api.config;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import se.jobtechdev.personaldatagateway.api.security.RequestMethodPattern;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class SecurityConfigTest {
  static final String ADMIN = "admin";
  static final String FRONTEND = "frontend";
  static final String CONSUMER = "consumer";

  @Test
  public void getMethodPatterns() {
    // Arrange
    final var mockGetClients = mock(RequestMethodPattern.class);
    final var mockPostClient = mock(RequestMethodPattern.class);
    final var mockGetClient = mock(RequestMethodPattern.class);
    final var mockPatchClient = mock(RequestMethodPattern.class);
    final var mockGetClientRedirects = mock(RequestMethodPattern.class);
    final var mockPostClientRedirect = mock(RequestMethodPattern.class);
    final var mockGetClientRedirect = mock(RequestMethodPattern.class);
    final var mockPatchClientRedirect = mock(RequestMethodPattern.class);
    final var mockGetClientKeyResets = mock(RequestMethodPattern.class);
    final var mockPostClientKeyReset = mock(RequestMethodPattern.class);
    final var mockGetData = mock(RequestMethodPattern.class);
    final var mockGetDataAccesses = mock(RequestMethodPattern.class);
    final var mockGetDataAccess = mock(RequestMethodPattern.class);
    final var mockGetDatasources = mock(RequestMethodPattern.class);
    final var mockPostDatasource = mock(RequestMethodPattern.class);
    final var mockGetDatasource = mock(RequestMethodPattern.class);
    final var mockPatchDatasource = mock(RequestMethodPattern.class);
    final var mockGetDatasourceFaqs = mock(RequestMethodPattern.class);
    final var mockPostDatasourceFaq = mock(RequestMethodPattern.class);
    final var mockGetDatasourceFaq = mock(RequestMethodPattern.class);
    final var mockPatchDatasourceFaq = mock(RequestMethodPattern.class);
    final var mockGetDatasourceConfig = mock(RequestMethodPattern.class);
    final var mockPatchDatasourceConfig = mock(RequestMethodPattern.class);
    final var mockGetDatasourceUserData = mock(RequestMethodPattern.class);
    final var mockGetDatatypes = mock(RequestMethodPattern.class);
    final var mockPostDatatype = mock(RequestMethodPattern.class);
    final var mockGetDatatype = mock(RequestMethodPattern.class);
    final var mockPatchDatatype = mock(RequestMethodPattern.class);
    final var mockGetOrganizations = mock(RequestMethodPattern.class);
    final var mockPostOrganization = mock(RequestMethodPattern.class);
    final var mockGetOrganization = mock(RequestMethodPattern.class);
    final var mockPatchOrganization = mock(RequestMethodPattern.class);
    final var mockGetOrganizationFaqs = mock(RequestMethodPattern.class);
    final var mockPostOrganizationFaq = mock(RequestMethodPattern.class);
    final var mockGetOrganizationFaq = mock(RequestMethodPattern.class);
    final var mockPatchOrganizationFaq = mock(RequestMethodPattern.class);
    final var mockGetDatatypeFaqs = mock(RequestMethodPattern.class);
    final var mockPostDatatypeFaq = mock(RequestMethodPattern.class);
    final var mockGetDatatypeFaq = mock(RequestMethodPattern.class);
    final var mockPatchDatatypeFaq = mock(RequestMethodPattern.class);
    final var mockPatternGetError = mock(RequestMethodPattern.class);
    final var mockGetUser = mock(RequestMethodPattern.class);
    final var mockGetUserSharings = mock(RequestMethodPattern.class);
    final var mockPostUserSharing = mock(RequestMethodPattern.class);
    final var mockGetUserSharing = mock(RequestMethodPattern.class);
    final var mockPatchUserSharing = mock(RequestMethodPattern.class);
    final var mockGetApiInfo = mock(RequestMethodPattern.class);
    final var mockGetHealth = mock(RequestMethodPattern.class);

    try (final var staticRequestMethodPattern = mockStatic(RequestMethodPattern.class)) {

      staticRequestMethodPattern
          .when(
              () -> RequestMethodPattern.create(HttpMethod.GET, "/clients", new String[]{ADMIN}))
          .thenReturn(mockGetClients);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(HttpMethod.POST, "/clients", new String[]{ADMIN}))
          .thenReturn(mockPostClient);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.GET, "/clients/*", new String[]{ADMIN, FRONTEND}))
          .thenReturn(mockGetClient);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.PATCH, "/clients/*", new String[]{ADMIN}))
          .thenReturn(mockPatchClient);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.GET, "/clients/*/redirects", new String[]{ADMIN, FRONTEND}))
          .thenReturn(mockGetClientRedirects);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.POST, "/clients/*/redirects", new String[]{ADMIN}))
          .thenReturn(mockPostClientRedirect);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.GET, "/clients/*/redirects/*", new String[]{ADMIN, FRONTEND}))
          .thenReturn(mockGetClientRedirect);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.PATCH, "/clients/*/redirects/*", new String[]{ADMIN}))
          .thenReturn(mockPatchClientRedirect);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.GET, "/clients/*/key-resets", new String[]{ADMIN}))
          .thenReturn(mockGetClientKeyResets);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.POST, "/clients/*/key-resets", new String[]{ADMIN}))
          .thenReturn(mockPostClientKeyReset);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(HttpMethod.GET, "/data/*", new String[]{CONSUMER}))
          .thenReturn(mockGetData);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.GET, "/data/*/accesses", new String[]{FRONTEND}))
          .thenReturn(mockGetDataAccesses);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.GET, "/data/*/accesses/*", new String[]{FRONTEND}))
          .thenReturn(mockGetDataAccess);

      staticRequestMethodPattern
          .when(() -> RequestMethodPattern.create(HttpMethod.GET, "/datasources", new String[]{}))
          .thenReturn(mockGetDatasources);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.POST, "/datasources", new String[]{ADMIN}))
          .thenReturn(mockPostDatasource);

      staticRequestMethodPattern
          .when(
              () -> RequestMethodPattern.create(HttpMethod.GET, "/datasources/*", new String[]{}))
          .thenReturn(mockGetDatasource);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.PATCH, "/datasources/*", new String[]{ADMIN}))
          .thenReturn(mockPatchDatasource);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.GET, "/datasources/*/faqs", new String[]{}))
          .thenReturn(mockGetDatasourceFaqs);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.POST, "/datasources/*/faqs", new String[]{ADMIN}))
          .thenReturn(mockPostDatasourceFaq);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.GET, "/datasources/*/faqs/*", new String[]{}))
          .thenReturn(mockGetDatasourceFaq);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.PATCH, "/datasources/*/faqs/*", new String[]{ADMIN}))
          .thenReturn(mockPatchDatasourceFaq);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.GET, "/datasources/*/config", new String[]{ADMIN}))
          .thenReturn(mockGetDatasourceConfig);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.PATCH, "/datasources/*/config", new String[]{ADMIN}))
          .thenReturn(mockPatchDatasourceConfig);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.GET, "/datasources/*/users/*/data", new String[]{FRONTEND}))
          .thenReturn(mockGetDatasourceUserData);

      staticRequestMethodPattern
          .when(() -> RequestMethodPattern.create(HttpMethod.GET, "/datatypes", new String[]{}))
          .thenReturn(mockGetDatatypes);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.POST, "/datatypes", new String[]{ADMIN}))
          .thenReturn(mockPostDatatype);

      staticRequestMethodPattern
          .when(() -> RequestMethodPattern.create(HttpMethod.GET, "/datatypes/*", new String[]{}))
          .thenReturn(mockGetDatatype);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.PATCH, "/datatypes/*", new String[]{ADMIN}))
          .thenReturn(mockPatchDatatype);

      staticRequestMethodPattern
          .when(
              () -> RequestMethodPattern.create(HttpMethod.GET, "/organizations", new String[]{}))
          .thenReturn(mockGetOrganizations);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.POST, "/organizations", new String[]{ADMIN}))
          .thenReturn(mockPostOrganization);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(HttpMethod.GET, "/organizations/*", new String[]{}))
          .thenReturn(mockGetOrganization);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.PATCH, "/organizations/*", new String[]{ADMIN}))
          .thenReturn(mockPatchOrganization);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.GET, "/organizations/*/faqs", new String[]{}))
          .thenReturn(mockGetOrganizationFaqs);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.POST, "/organizations/*/faqs", new String[]{ADMIN}))
          .thenReturn(mockPostOrganizationFaq);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.GET, "/organizations/*/faqs/*", new String[]{}))
          .thenReturn(mockGetOrganizationFaq);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.PATCH, "/organizations/*/faqs/*", new String[]{ADMIN}))
          .thenReturn(mockPatchOrganizationFaq);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(HttpMethod.GET, "/datatypes/*/faqs", new String[]{}))
          .thenReturn(mockGetDatatypeFaqs);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.POST, "/datatypes/*/faqs", new String[]{ADMIN}))
          .thenReturn(mockPostDatatypeFaq);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.GET, "/datatypes/*/faqs/*", new String[]{}))
          .thenReturn(mockGetDatatypeFaq);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.PATCH, "/datatypes/*/faqs/*", new String[]{ADMIN}))
          .thenReturn(mockPatchDatatypeFaq);

      staticRequestMethodPattern
          .when(() -> RequestMethodPattern.create(HttpMethod.GET, "/error", new String[]{}))
          .thenReturn(mockPatternGetError);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.GET, "/users/*", new String[]{FRONTEND}))
          .thenReturn(mockGetUser);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.GET, "/users/*/sharings", new String[]{FRONTEND}))
          .thenReturn(mockGetUserSharings);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.POST, "/users/*/sharings", new String[]{FRONTEND}))
          .thenReturn(mockPostUserSharing);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.GET, "/users/*/sharings/*", new String[]{FRONTEND}))
          .thenReturn(mockGetUserSharing);

      staticRequestMethodPattern
          .when(
              () ->
                  RequestMethodPattern.create(
                      HttpMethod.PATCH, "/users/*/sharings/*", new String[]{FRONTEND}))
          .thenReturn(mockPatchUserSharing);

      staticRequestMethodPattern
          .when(() -> RequestMethodPattern.create(HttpMethod.GET, "/api-info", new String[]{}))
          .thenReturn(mockGetApiInfo);

      staticRequestMethodPattern
          .when(() -> RequestMethodPattern.create(HttpMethod.GET, "/health", new String[]{}))
          .thenReturn(mockGetHealth);

      // Act
      final var patterns = new SecurityConfig().getMethodPatterns();

      // Assert
      staticRequestMethodPattern.verify(
          () -> RequestMethodPattern.create(any(), any(), any()), times(48));

      staticRequestMethodPattern.verify(
          () -> RequestMethodPattern.create(HttpMethod.GET, "/clients", new String[]{ADMIN}),
          times(1));

      staticRequestMethodPattern.verify(
          () -> RequestMethodPattern.create(HttpMethod.POST, "/clients", new String[]{ADMIN}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.GET, "/clients/*", new String[]{ADMIN, FRONTEND}),
          times(1));

      staticRequestMethodPattern.verify(
          () -> RequestMethodPattern.create(HttpMethod.PATCH, "/clients/*", new String[]{ADMIN}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.GET, "/clients/*/redirects", new String[]{ADMIN, FRONTEND}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.POST, "/clients/*/redirects", new String[]{ADMIN}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.GET, "/clients/*/redirects/*", new String[]{ADMIN, FRONTEND}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.PATCH, "/clients/*/redirects/*", new String[]{ADMIN}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.GET, "/clients/*/key-resets", new String[]{ADMIN}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.POST, "/clients/*/key-resets", new String[]{ADMIN}),
          times(1));

      staticRequestMethodPattern.verify(
          () -> RequestMethodPattern.create(HttpMethod.GET, "/data/*", new String[]{CONSUMER}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.GET, "/data/*/accesses", new String[]{FRONTEND}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.GET, "/data/*/accesses/*", new String[]{FRONTEND}),
          times(1));

      staticRequestMethodPattern.verify(
          () -> RequestMethodPattern.create(HttpMethod.GET, "/datasources", new String[]{}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(HttpMethod.POST, "/datasources", new String[]{ADMIN}),
          times(1));

      staticRequestMethodPattern.verify(
          () -> RequestMethodPattern.create(HttpMethod.GET, "/datasources/*", new String[]{}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.PATCH, "/datasources/*", new String[]{ADMIN}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.GET, "/datasources/*/config", new String[]{ADMIN}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.PATCH, "/datasources/*/config", new String[]{ADMIN}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.GET, "/datasources/*/users/*/data", new String[]{FRONTEND}),
          times(1));

      staticRequestMethodPattern.verify(
          () -> RequestMethodPattern.create(HttpMethod.GET, "/datatypes", new String[]{}),
          times(1));

      staticRequestMethodPattern.verify(
          () -> RequestMethodPattern.create(HttpMethod.POST, "/datatypes", new String[]{ADMIN}),
          times(1));

      staticRequestMethodPattern.verify(
          () -> RequestMethodPattern.create(HttpMethod.GET, "/datatypes/*", new String[]{}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(HttpMethod.PATCH, "/datatypes/*", new String[]{ADMIN}),
          times(1));

      staticRequestMethodPattern.verify(
          () -> RequestMethodPattern.create(HttpMethod.GET, "/organizations", new String[]{}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.POST, "/organizations", new String[]{ADMIN}),
          times(1));

      staticRequestMethodPattern.verify(
          () -> RequestMethodPattern.create(HttpMethod.GET, "/organizations/*", new String[]{}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.PATCH, "/organizations/*", new String[]{ADMIN}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(HttpMethod.GET, "/organizations/*/faqs", new String[]{}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.POST, "/organizations/*/faqs", new String[]{ADMIN}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.GET, "/organizations/*/faqs/*", new String[]{}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.PATCH, "/organizations/*/faqs/*", new String[]{ADMIN}),
          times(1));

      staticRequestMethodPattern.verify(
          () -> RequestMethodPattern.create(HttpMethod.GET, "/datatypes/*/faqs", new String[]{}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.POST, "/datatypes/*/faqs", new String[]{ADMIN}),
          times(1));

      staticRequestMethodPattern.verify(
          () -> RequestMethodPattern.create(HttpMethod.GET, "/datatypes/*/faqs/*", new String[]{}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.PATCH, "/datatypes/*/faqs/*", new String[]{ADMIN}),
          times(1));

      staticRequestMethodPattern.verify(
          () -> RequestMethodPattern.create(HttpMethod.GET, "/error", new String[]{}), times(1));

      staticRequestMethodPattern.verify(
          () -> RequestMethodPattern.create(HttpMethod.GET, "/users/*", new String[]{FRONTEND}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.GET, "/users/*/sharings", new String[]{FRONTEND}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.POST, "/users/*/sharings", new String[]{FRONTEND}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.GET, "/users/*/sharings/*", new String[]{FRONTEND}),
          times(1));

      staticRequestMethodPattern.verify(
          () ->
              RequestMethodPattern.create(
                  HttpMethod.PATCH, "/users/*/sharings/*", new String[]{FRONTEND}),
          times(1));

      staticRequestMethodPattern.verify(
          () -> RequestMethodPattern.create(HttpMethod.GET, "/api-info", new String[]{}),
          times(1));

      staticRequestMethodPattern.verify(
          () -> RequestMethodPattern.create(HttpMethod.GET, "/health", new String[]{}), times(1));

      assertEquals(48, patterns.size());

      assertEquals(
          List.of(
              mockGetClients,
              mockPostClient,
              mockGetClient,
              mockPatchClient,
              mockGetClientRedirects,
              mockPostClientRedirect,
              mockGetClientRedirect,
              mockPatchClientRedirect,
              mockGetClientKeyResets,
              mockPostClientKeyReset,
              mockGetData,
              mockGetDataAccesses,
              mockGetDataAccess,
              mockGetDatasources,
              mockPostDatasource,
              mockGetDatasource,
              mockPatchDatasource,
              mockGetDatasourceFaqs,
              mockPostDatasourceFaq,
              mockGetDatasourceFaq,
              mockPatchDatasourceFaq,
              mockGetDatasourceConfig,
              mockPatchDatasourceConfig,
              mockGetDatasourceUserData,
              mockGetDatatypes,
              mockPostDatatype,
              mockGetDatatype,
              mockPatchDatatype,
              mockGetDatatypeFaqs,
              mockPostDatatypeFaq,
              mockGetDatatypeFaq,
              mockPatchDatatypeFaq,
              mockGetOrganizations,
              mockPostOrganization,
              mockGetOrganization,
              mockPatchOrganization,
              mockGetOrganizationFaqs,
              mockPostOrganizationFaq,
              mockGetOrganizationFaq,
              mockPatchOrganizationFaq,
              mockPatternGetError,
              mockGetUser,
              mockGetUserSharings,
              mockPostUserSharing,
              mockGetUserSharing,
              mockPatchUserSharing,
              mockGetApiInfo,
              mockGetHealth),
          patterns);
    }
  }
}
