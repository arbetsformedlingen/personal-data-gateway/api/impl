package se.jobtechdev.personaldatagateway.api.config;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.AuthorizeHttpRequestsConfigurer;
import org.springframework.security.config.annotation.web.configurers.SessionManagementConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import se.jobtechdev.personaldatagateway.api.security.RequestHeaderAuthenticationProvider;
import se.jobtechdev.personaldatagateway.api.security.RequestMethodPattern;

@ExtendWith(MockitoExtension.class)
public class WebSecurityTest {
  private WebSecurity webSecurity;

  @Mock private RequestHeaderAuthenticationProvider requestHeaderAuthenticationProvider;

  @Mock private SecurityConfig securityConfig;

  @BeforeEach
  void setUp() {
    webSecurity = new WebSecurity(requestHeaderAuthenticationProvider, securityConfig);
  }

  @Test
  public void getAuthorizeHttpRequestsCustomizer() {
    final var mockRegistry =
        mock(AuthorizeHttpRequestsConfigurer.AuthorizationManagerRequestMatcherRegistry.class);
    final var mockAuthorizeHttpRequestsConfigurer =
        mock(AuthorizeHttpRequestsConfigurer.AuthorizedUrl.class);
    when(mockRegistry.requestMatchers(any(), any()))
        .thenReturn(mockAuthorizeHttpRequestsConfigurer);

    final var mockRequestMethodPattern = mock(RequestMethodPattern.class);

    when(securityConfig.getMethodPatterns()).thenReturn(List.of(mockRequestMethodPattern));

    when(mockRequestMethodPattern.method()).thenReturn(HttpMethod.GET);
    when(mockRequestMethodPattern.pattern()).thenReturn("/foo");
    when(mockRequestMethodPattern.roles()).thenReturn(new String[] {"bar"});

    // Act
    final var authorizeHttpRequestsCustomizer = webSecurity.getAuthorizeHttpRequestsCustomizer();
    authorizeHttpRequestsCustomizer.customize(mockRegistry);

    // Assert
    verify(mockRegistry, times(1)).requestMatchers(HttpMethod.GET, "/foo");
    verify(mockAuthorizeHttpRequestsConfigurer, times(1)).hasAnyAuthority("bar");
    verify(mockAuthorizeHttpRequestsConfigurer, times(0)).permitAll();
  }

  @Test
  public void
      getAuthorizeHttpRequestsCustomizer_whenNoMethodPatternHasAnyRole_thenAssignPermitAll() {
    final var mockRegistry =
        mock(AuthorizeHttpRequestsConfigurer.AuthorizationManagerRequestMatcherRegistry.class);
    final var mockAuthorizeHttpRequestsConfigurer =
        mock(AuthorizeHttpRequestsConfigurer.AuthorizedUrl.class);
    when(mockRegistry.requestMatchers(HttpMethod.GET, "/foo"))
        .thenReturn(mockAuthorizeHttpRequestsConfigurer);

    try (final var staticSecurityConfig = mockStatic(WebSecurity.class)) {
      final var mockRequestMethodPattern = mock(RequestMethodPattern.class);
      when(securityConfig.getMethodPatterns()).thenReturn(List.of(mockRequestMethodPattern));

      when(mockRequestMethodPattern.method()).thenReturn(HttpMethod.GET);
      when(mockRequestMethodPattern.pattern()).thenReturn("/foo");
      when(mockRequestMethodPattern.roles()).thenReturn(new String[] {});

      // Act
      final var authorizeHttpRequestsCustomizer = webSecurity.getAuthorizeHttpRequestsCustomizer();
      authorizeHttpRequestsCustomizer.customize(mockRegistry);

      // Assert
      verify(mockRegistry, times(1)).requestMatchers(HttpMethod.GET, "/foo");
      verify(mockAuthorizeHttpRequestsConfigurer, times(0)).hasAnyAuthority(any());
      verify(mockAuthorizeHttpRequestsConfigurer, times(1)).permitAll();
    }
  }

  @Test
  public void
      getAuthorizeHttpRequestsCustomizer_whenNoMethodPatternExists_thenRegistryIsNotPopulated() {
    final var mockRegistry =
        mock(AuthorizeHttpRequestsConfigurer.AuthorizationManagerRequestMatcherRegistry.class);

    try (final var staticSecurityConfig = mockStatic(WebSecurity.class)) {
      when(securityConfig.getMethodPatterns()).thenReturn(List.of());

      // Act
      final var authorizeHttpRequestsCustomizer = webSecurity.getAuthorizeHttpRequestsCustomizer();
      authorizeHttpRequestsCustomizer.customize(mockRegistry);

      // Assert
      verify(mockRegistry, times(0)).requestMatchers(any(), any());
    }
  }

  @Test
  public void getSessionManagementConfigurerCustomizer() {
    // Assemble
    final var mockSessionManagementConfigurer = mock(SessionManagementConfigurer.class);

    // Act
    final var sessionManagementConfigurerCustomizer =
        webSecurity.getSessionManagementConfigurerCustomizer();
    sessionManagementConfigurerCustomizer.customize(mockSessionManagementConfigurer);

    // Assert
    verify(mockSessionManagementConfigurer, times(1))
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
  }

  @Test
  public void filterChain_forSingleRequestMethodPatterns() throws Exception {
    // Assert
    final var mockHttpSecurity = mock(HttpSecurity.class);
    final var constructionProviderManagerArgs = new HashMap<ProviderManager, List<?>>();
    final var constructionOrRequestMatcherArgs = new HashMap<OrRequestMatcher, List<?>>();
    final var constructionAntPathRequestMatcherArgs = new HashMap<AntPathRequestMatcher, List<?>>();

    final var mockRequestMethodPattern1 = mock(RequestMethodPattern.class);

    when(mockRequestMethodPattern1.method()).thenReturn(HttpMethod.GET);
    when(mockRequestMethodPattern1.pattern()).thenReturn("/foo");
    when(mockRequestMethodPattern1.roles()).thenReturn(new String[] {"bar"});

    when(securityConfig.getMethodPatterns()).thenReturn(List.of(mockRequestMethodPattern1));

    try (final var staticCustomizer = mockStatic(Customizer.class);
        final var staticAbstractHttpConfigurer = mockStatic(AbstractHttpConfigurer.class);
        final var constructionRequestHeaderAuthenticationFilter =
            mockConstruction(RequestHeaderAuthenticationFilter.class);
        final var constructionAntPathRequestMatcher =
            mockConstruction(
                AntPathRequestMatcher.class,
                (mock, context) -> {
                  constructionAntPathRequestMatcherArgs.put(mock, context.arguments());
                });
        final var constructionOrRequestMatcher =
            mockConstruction(
                OrRequestMatcher.class,
                (mock, context) -> {
                  constructionOrRequestMatcherArgs.put(mock, context.arguments());
                });
        final var constructionProviderManager =
            mockConstruction(
                ProviderManager.class,
                (mock, context) -> {
                  constructionProviderManagerArgs.put(mock, context.arguments());
                })) {
      final var mockCorsDefaults = mock(Customizer.class);
      staticCustomizer.when(Customizer::withDefaults).thenReturn(mockCorsDefaults);

      final var mockSecurityFilterChain = mock(DefaultSecurityFilterChain.class);
      when(mockHttpSecurity.cors(any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.csrf(any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.sessionManagement(any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.addFilterBefore(any(), any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.authorizeHttpRequests(any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.build()).thenReturn(mockSecurityFilterChain);

      // Act
      final var result = webSecurity.filterChain(mockHttpSecurity);

      // Assert
      assertEquals(mockSecurityFilterChain, result);
      assertEquals(1, constructionProviderManager.constructed().size());

      final var mockProviderManager = constructionProviderManager.constructed().get(0);
      assertEquals(
          Collections.singletonList(requestHeaderAuthenticationProvider),
          constructionProviderManagerArgs.get(mockProviderManager).get(0));

      assertEquals(1, constructionOrRequestMatcher.constructed().size());
      assertEquals(1, constructionAntPathRequestMatcher.constructed().size());

      final var mockOrRequestMatcher = constructionOrRequestMatcher.constructed().get(0);
      assertEquals(1, constructionRequestHeaderAuthenticationFilter.constructed().size());
      final var mockRequestHeaderAuthenticationFilter =
          constructionRequestHeaderAuthenticationFilter.constructed().get(0);
      verify(mockRequestHeaderAuthenticationFilter, times(1))
          .setPrincipalRequestHeader("X-Auth-Key");
      verify(mockRequestHeaderAuthenticationFilter, times(1))
          .setRequiresAuthenticationRequestMatcher(mockOrRequestMatcher);
      verify(mockRequestHeaderAuthenticationFilter, times(1))
          .setAuthenticationManager(mockProviderManager);
    }
  }

  @Test
  public void filterChain_noRequestMethodPatterns() throws Exception {
    // Assert
    final var mockHttpSecurity = mock(HttpSecurity.class);
    final var constructionProviderManagerArgs = new HashMap<ProviderManager, List<?>>();
    final var constructionOrRequestMatcherArgs = new HashMap<OrRequestMatcher, List<?>>();
    final var constructionAntPathRequestMatcherArgs = new HashMap<AntPathRequestMatcher, List<?>>();

    when(securityConfig.getMethodPatterns()).thenReturn(List.of());

    try (final var staticCustomizer = mockStatic(Customizer.class);
        final var staticAbstractHttpConfigurer = mockStatic(AbstractHttpConfigurer.class);
        final var constructionRequestHeaderAuthenticationFilter =
            mockConstruction(RequestHeaderAuthenticationFilter.class);
        final var constructionAntPathRequestMatcher =
            mockConstruction(
                AntPathRequestMatcher.class,
                (mock, context) -> {
                  constructionAntPathRequestMatcherArgs.put(mock, context.arguments());
                });
        final var constructionOrRequestMatcher =
            mockConstruction(
                OrRequestMatcher.class,
                (mock, context) -> {
                  constructionOrRequestMatcherArgs.put(mock, context.arguments());
                });
        final var constructionProviderManager =
            mockConstruction(
                ProviderManager.class,
                (mock, context) -> {
                  constructionProviderManagerArgs.put(mock, context.arguments());
                })) {
      final var mockCorsDefaults = mock(Customizer.class);
      staticCustomizer.when(Customizer::withDefaults).thenReturn(mockCorsDefaults);

      final var mockSecurityFilterChain = mock(DefaultSecurityFilterChain.class);
      when(mockHttpSecurity.cors(any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.csrf(any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.sessionManagement(any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.addFilterBefore(any(), any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.authorizeHttpRequests(any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.build()).thenReturn(mockSecurityFilterChain);

      // Act
      final var result = webSecurity.filterChain(mockHttpSecurity);

      // Assert
      assertEquals(mockSecurityFilterChain, result);
      assertEquals(1, constructionProviderManager.constructed().size());

      final var mockProviderManager = constructionProviderManager.constructed().get(0);
      assertEquals(
          Collections.singletonList(requestHeaderAuthenticationProvider),
          constructionProviderManagerArgs.get(mockProviderManager).get(0));

      assertEquals(1, constructionOrRequestMatcher.constructed().size());
      assertEquals(0, constructionAntPathRequestMatcher.constructed().size());

      final var mockOrRequestMatcher = constructionOrRequestMatcher.constructed().get(0);
      assertEquals(1, constructionRequestHeaderAuthenticationFilter.constructed().size());
      final var mockRequestHeaderAuthenticationFilter =
          constructionRequestHeaderAuthenticationFilter.constructed().get(0);
      verify(mockRequestHeaderAuthenticationFilter, times(1))
          .setPrincipalRequestHeader("X-Auth-Key");
      verify(mockRequestHeaderAuthenticationFilter, times(1))
          .setRequiresAuthenticationRequestMatcher(mockOrRequestMatcher);
      verify(mockRequestHeaderAuthenticationFilter, times(1))
          .setAuthenticationManager(mockProviderManager);
    }
  }

  @Test
  public void filterChain_forMultipleRequestMethodPatterns() throws Exception {
    // Assert
    final var mockHttpSecurity = mock(HttpSecurity.class);
    final var constructionProviderManagerArgs = new HashMap<ProviderManager, List<?>>();
    final var constructionOrRequestMatcherArgs = new HashMap<OrRequestMatcher, List<?>>();
    final var constructionAntPathRequestMatcherArgs = new HashMap<AntPathRequestMatcher, List<?>>();

    final var mockRequestMethodPattern1 = mock(RequestMethodPattern.class);
    final var mockRequestMethodPattern2 = mock(RequestMethodPattern.class);

    when(mockRequestMethodPattern1.method()).thenReturn(HttpMethod.GET);
    when(mockRequestMethodPattern1.pattern()).thenReturn("/foo");
    when(mockRequestMethodPattern1.roles()).thenReturn(new String[] {"bar"});
    when(mockRequestMethodPattern2.method()).thenReturn(HttpMethod.GET);
    when(mockRequestMethodPattern2.pattern()).thenReturn("/hello");
    when(mockRequestMethodPattern2.roles()).thenReturn(new String[] {"world"});

    when(securityConfig.getMethodPatterns())
        .thenReturn(List.of(mockRequestMethodPattern1, mockRequestMethodPattern2));

    try (final var staticCustomizer = mockStatic(Customizer.class);
        final var staticAbstractHttpConfigurer = mockStatic(AbstractHttpConfigurer.class);
        final var constructionRequestHeaderAuthenticationFilter =
            mockConstruction(RequestHeaderAuthenticationFilter.class);
        final var constructionAntPathRequestMatcher =
            mockConstruction(
                AntPathRequestMatcher.class,
                (mock, context) -> {
                  constructionAntPathRequestMatcherArgs.put(mock, context.arguments());
                });
        final var constructionOrRequestMatcher =
            mockConstruction(
                OrRequestMatcher.class,
                (mock, context) -> {
                  constructionOrRequestMatcherArgs.put(mock, context.arguments());
                });
        final var constructionProviderManager =
            mockConstruction(
                ProviderManager.class,
                (mock, context) -> {
                  constructionProviderManagerArgs.put(mock, context.arguments());
                })) {
      final var mockCorsDefaults = mock(Customizer.class);
      staticCustomizer.when(Customizer::withDefaults).thenReturn(mockCorsDefaults);

      final var mockSecurityFilterChain = mock(DefaultSecurityFilterChain.class);
      when(mockHttpSecurity.cors(any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.csrf(any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.sessionManagement(any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.addFilterBefore(any(), any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.authorizeHttpRequests(any())).thenReturn(mockHttpSecurity);
      when(mockHttpSecurity.build()).thenReturn(mockSecurityFilterChain);

      // Act
      final var result = webSecurity.filterChain(mockHttpSecurity);

      // Assert
      assertEquals(mockSecurityFilterChain, result);
      assertEquals(1, constructionProviderManager.constructed().size());

      final var mockProviderManager = constructionProviderManager.constructed().get(0);
      assertEquals(
          Collections.singletonList(requestHeaderAuthenticationProvider),
          constructionProviderManagerArgs.get(mockProviderManager).get(0));

      assertEquals(1, constructionOrRequestMatcher.constructed().size());
      assertEquals(2, constructionAntPathRequestMatcher.constructed().size());

      final var mockOrRequestMatcher = constructionOrRequestMatcher.constructed().get(0);
      assertEquals(1, constructionRequestHeaderAuthenticationFilter.constructed().size());
      final var mockRequestHeaderAuthenticationFilter =
          constructionRequestHeaderAuthenticationFilter.constructed().get(0);
      verify(mockRequestHeaderAuthenticationFilter, times(1))
          .setPrincipalRequestHeader("X-Auth-Key");
      verify(mockRequestHeaderAuthenticationFilter, times(1))
          .setRequiresAuthenticationRequestMatcher(mockOrRequestMatcher);
      verify(mockRequestHeaderAuthenticationFilter, times(1))
          .setAuthenticationManager(mockProviderManager);
    }
  }

  @Test
  public void isRolePopulated_whenRolesIsPopulated_returnTrue() {
    final var mockRequestMethodPattern = mock(RequestMethodPattern.class);
    when(mockRequestMethodPattern.roles()).thenReturn(new String[] {"role"});
    final var lambda = WebSecurity.isRolePopulated();
    assertTrue(lambda.test(mockRequestMethodPattern));
  }

  @Test
  public void isRolePopulated_whenRolesIsNotPopulated_returnFalse() {
    final var mockRequestMethodPattern = mock(RequestMethodPattern.class);
    when(mockRequestMethodPattern.roles()).thenReturn(new String[] {});
    final var lambda = WebSecurity.isRolePopulated();
    assertFalse(lambda.test(mockRequestMethodPattern));
  }
}
