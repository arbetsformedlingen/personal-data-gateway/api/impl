package se.jobtechdev.personaldatagateway.api.config;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class MethodSecurityConfigTest {
  @Test
  public void grantedAuthorityDefaults() {
    // Act
    final var retval = new MethodSecurityConfig().grantedAuthorityDefaults();

    // Assert
    assertEquals("", retval.getRolePrefix());
  }
}
