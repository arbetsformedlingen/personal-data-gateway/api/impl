package se.jobtechdev.personaldatagateway.api.logging;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.UriComponents;

public class UrlFieldExtractorTest {
  @Test
  @Tag("UnitTest")
  public void construct() {
    new UrlFieldExtractor();
  }

  @Test
  @Tag("UnitTest")
  public void extractUrlPath() {
    // Arrange
    final ContentCachingRequestWrapper mockRequest = mock(ContentCachingRequestWrapper.class);
    when(mockRequest.getRequestURI()).thenReturn("example-url-request-uri");

    // Act
    final String urlPath = UrlFieldExtractor.extractUrlPath(mockRequest);

    // Assert
    assertEquals("example-url-request-uri", urlPath);
  }

  @Test
  @Tag("UnitTest")
  public void extractUrlQuery() {
    // Arrange
    final ContentCachingRequestWrapper mockRequest = mock(ContentCachingRequestWrapper.class);
    when(mockRequest.getQueryString()).thenReturn("example-url-query");

    // Act
    final String urlQuery = UrlFieldExtractor.extractUrlQuery(mockRequest);

    // Assert
    assertEquals("example-url-query", urlQuery);
  }

  @Test
  @Tag("UnitTest")
  public void extractUrlFull() {
    // Arrange
    try (MockedStatic<ServletUriComponentsBuilder> staticServletUriComponentsBuilder =
        mockStatic(ServletUriComponentsBuilder.class)) {
      final ContentCachingRequestWrapper mockRequest = mock(ContentCachingRequestWrapper.class);
      when(mockRequest.getQueryString()).thenReturn("example-url-query");
      final ServletUriComponentsBuilder mockServletUriComponentsBuilder =
          mock(ServletUriComponentsBuilder.class);
      staticServletUriComponentsBuilder
          .when(() -> ServletUriComponentsBuilder.fromRequest(mockRequest))
          .thenReturn(mockServletUriComponentsBuilder);
      final UriComponents mockUriComponents = mock(UriComponents.class);
      when(mockServletUriComponentsBuilder.build()).thenReturn(mockUriComponents);
      when(mockUriComponents.toString()).thenReturn("example-url-full");

      // Act
      final String urlFull = UrlFieldExtractor.extractUrlFull(mockRequest);

      // Assert
      assertEquals("example-url-full", urlFull);
    }
  }

  @Test
  @Tag("UnitTest")
  public void extractUrlPort() {
    // Arrange
    try (MockedStatic<ServletUriComponentsBuilder> staticServletUriComponentsBuilder =
        mockStatic(ServletUriComponentsBuilder.class)) {
      final ContentCachingRequestWrapper mockRequest = mock(ContentCachingRequestWrapper.class);
      final ServletUriComponentsBuilder mockServletUriComponentsBuilder =
          mock(ServletUriComponentsBuilder.class);
      staticServletUriComponentsBuilder
          .when(() -> ServletUriComponentsBuilder.fromRequest(mockRequest))
          .thenReturn(mockServletUriComponentsBuilder);
      final UriComponents mockUriComponents = mock(UriComponents.class);
      when(mockServletUriComponentsBuilder.build()).thenReturn(mockUriComponents);
      when(mockUriComponents.getPort()).thenReturn(8080);

      // Act
      final String urlPort = UrlFieldExtractor.extractUrlPort(mockRequest);

      // Assert
      assertEquals("8080", urlPort);
    }
  }

  @Test
  @Tag("UnitTest")
  public void extractUrlScheme() {
    // Arrange
    try (MockedStatic<ServletUriComponentsBuilder> staticServletUriComponentsBuilder =
        mockStatic(ServletUriComponentsBuilder.class)) {
      final ContentCachingRequestWrapper mockRequest = mock(ContentCachingRequestWrapper.class);
      final ServletUriComponentsBuilder mockServletUriComponentsBuilder =
          mock(ServletUriComponentsBuilder.class);
      staticServletUriComponentsBuilder
          .when(() -> ServletUriComponentsBuilder.fromRequest(mockRequest))
          .thenReturn(mockServletUriComponentsBuilder);
      final UriComponents mockUriComponents = mock(UriComponents.class);
      when(mockServletUriComponentsBuilder.build()).thenReturn(mockUriComponents);
      when(mockUriComponents.getScheme()).thenReturn("https");

      // Act
      final String urlScheme = UrlFieldExtractor.extractUrlScheme(mockRequest);

      // Assert
      assertEquals("https", urlScheme);
    }
  }
}
