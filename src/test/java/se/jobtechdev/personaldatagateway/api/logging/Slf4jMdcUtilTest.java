package se.jobtechdev.personaldatagateway.api.logging;

import org.apache.commons.collections4.iterators.IteratorEnumeration;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.slf4j.Logger;
import org.slf4j.MDC;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class Slf4jMdcUtilTest {
  @Test
  public void construct() {
    new Slf4jMdcUtil();
  }

  @Test
  public void activateAuditField_whenTempLogIsNull_thenReturnFalse() {
    // Arrange
    try (final var staticMDC = mockStatic(MDC.class)) {

      staticMDC.when(() -> MDC.get("temp.log")).thenReturn(null);
      staticMDC.when(() -> MDC.get("temp.custom.pdg-api.clientid")).thenReturn(null);

      // Act
      final var result = Slf4jMdcUtil.activateAuditField();

      // Assert
      assertFalse(result);
      staticMDC.verify(() -> MDC.get("temp.log"), times(1));
      staticMDC.verify(() -> MDC.get("temp.custom.pdg-api.clientid"), times(0));
      staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(0));
      staticMDC.verify(() -> MDC.put(eq("audit.log"), anyString()), times(0));
      staticMDC.verify(() -> MDC.put(eq("audit.custom.pdg-api.clientid"), anyString()), times(0));
    }
  }

  @Test
  public void activateAuditField_whenTempLogIsDefined_assignsAuditLog() {
    // Arrange
    try (final var staticMDC = mockStatic(MDC.class)) {

      staticMDC.when(() -> MDC.get("temp.log")).thenReturn("foo");
      staticMDC.when(() -> MDC.get("temp.custom.pdg-api.clientid")).thenReturn(null);

      // Act
      final var result = Slf4jMdcUtil.activateAuditField();

      // Assert
      assertTrue(result);
      staticMDC.verify(() -> MDC.get(anyString()), times(2));
      staticMDC.verify(() -> MDC.get("temp.log"), times(1));
      staticMDC.verify(() -> MDC.get("temp.custom.pdg-api.clientid"), times(1));
      staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(1));
      staticMDC.verify(() -> MDC.put("audit.log", "foo"), times(1));
      staticMDC.verify(() -> MDC.put(eq("audit.custom.pdg-api.clientid"), anyString()), times(0));
    }
  }

  @Test
  public void activateAuditField_whenClientIdIsDefined_assignsAuditClientId() {
    // Arrange
    try (final var staticMDC = mockStatic(MDC.class)) {

      staticMDC.when(() -> MDC.get("temp.log")).thenReturn("foo");
      staticMDC.when(() -> MDC.get("temp.custom.pdg-api.clientid")).thenReturn("foo");

      // Act
      final var result = Slf4jMdcUtil.activateAuditField();

      // Assert
      assertTrue(result);
      staticMDC.verify(() -> MDC.get(anyString()), times(2));
      staticMDC.verify(() -> MDC.get("temp.log"), times(1));
      staticMDC.verify(() -> MDC.get("temp.custom.pdg-api.clientid"), times(1));
      staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(2));
      staticMDC.verify(() -> MDC.put(eq("audit.log"), anyString()), times(1));
      staticMDC.verify(() -> MDC.put("audit.custom.pdg-api.clientid", "foo"), times(1));
    }
  }

  @Test
  public void addHttpFields() {
    // Arrange
    try (MockedStatic<MDC> staticMDC = mockStatic(MDC.class)) {
      final ContentCachingRequestWrapper request = mock(ContentCachingRequestWrapper.class);
      final ContentCachingResponseWrapper response = mock(ContentCachingResponseWrapper.class);
      when(request.getMethod()).thenReturn("METHOD");
      when(response.getContentType()).thenReturn("ContentType");
      when(response.getStatus()).thenReturn(123);

      // Act
      Slf4jMdcUtil.addHttpFields(request, response);

      // Assert
      staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(3));
      staticMDC.verify(() -> MDC.put("http.request.method", "METHOD"));
      staticMDC.verify(() -> MDC.put("http.response.mime_type", "ContentType"));
      staticMDC.verify(() -> MDC.put("http.response.status_code", "123"));
    }
  }

  @Test
  public void addUrlFields() {
    // Arrange
    try (MockedStatic<UrlFieldExtractor> staticUrlFieldExtractor =
             mockStatic(UrlFieldExtractor.class);
         MockedStatic<MDC> staticMDC = mockStatic(MDC.class)) {
      final ContentCachingRequestWrapper request = mock(ContentCachingRequestWrapper.class);
      staticUrlFieldExtractor
          .when(() -> UrlFieldExtractor.extractUrlPath(request))
          .thenReturn("path");
      staticUrlFieldExtractor
          .when(() -> UrlFieldExtractor.extractUrlQuery(request))
          .thenReturn("query");
      staticUrlFieldExtractor
          .when(() -> UrlFieldExtractor.extractUrlFull(request))
          .thenReturn("full");
      staticUrlFieldExtractor
          .when(() -> UrlFieldExtractor.extractUrlPort(request))
          .thenReturn("port");
      staticUrlFieldExtractor
          .when(() -> UrlFieldExtractor.extractUrlScheme(request))
          .thenReturn("scheme");

      // Act
      Slf4jMdcUtil.addUrlFields(request);

      // Assert
      staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(5));
      staticMDC.verify(() -> MDC.put("url.path", "path"));
      staticMDC.verify(() -> MDC.put("url.query", "query"));
      staticMDC.verify(() -> MDC.put("url.full", "full"));
      staticMDC.verify(() -> MDC.put("url.port", "port"));
      staticMDC.verify(() -> MDC.put("url.scheme", "scheme"));
    }
  }

  @Test
  public void addEventFields_defaultCase() {
    // Arrange
    final var mockRequest = mock(ContentCachingRequestWrapper.class);
    final var mockResponse = mock(ContentCachingResponseWrapper.class);
    final var mockLogger = mock(Logger.class);

    final var set = new TreeSet<String>();
    final var mockParameterNames = new IteratorEnumeration<>(set.iterator());
    when(mockRequest.getParameterNames()).thenReturn(mockParameterNames);
    when(mockRequest.getMethod()).thenReturn("DEFAULT");

    when(mockResponse.getStatus()).thenReturn(200);

    try (final var staticMDC = mockStatic(MDC.class);
         final var staticApplicationFieldExtractor = mockStatic(ApplicationFieldExtractor.class)) {

      staticMDC.when(() -> MDC.get("audit.log")).thenReturn("true");

      staticApplicationFieldExtractor
          .when(() -> ApplicationFieldExtractor.extractMessage(mockResponse))
          .thenReturn("mock-application-message");

      // Act
      Slf4jMdcUtil.addEventFields(mockRequest, mockResponse, mockLogger);

      // Assert
      staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(0));
    }
  }

  @Test
  public void addEventFields_whenMethodIsGet() {
    // Arrange
    final var mockRequest = mock(ContentCachingRequestWrapper.class);
    final var mockResponse = mock(ContentCachingResponseWrapper.class);
    final var mockLogger = mock(Logger.class);
    final var set = new TreeSet<String>();
    set.add("foo");
    set.add("hello");
    final var mockParameterNames = new IteratorEnumeration<>(set.iterator());
    final var mockParameterFooValues = new String[]{"bar", "baz"};
    final var mockParameterHelloValues = new String[]{"world"};

    when(mockRequest.getParameterNames()).thenReturn(mockParameterNames);
    when(mockRequest.getParameterValues("foo")).thenReturn(mockParameterFooValues);
    when(mockRequest.getParameterValues("hello")).thenReturn(mockParameterHelloValues);

    when(mockRequest.getMethod()).thenReturn("GET");

    when(mockResponse.getStatus()).thenReturn(200);

    try (final var staticMDC = mockStatic(MDC.class);
         final var staticApplicationFieldExtractor = mockStatic(ApplicationFieldExtractor.class)) {

      staticMDC.when(() -> MDC.get("audit.log")).thenReturn("true");

      staticApplicationFieldExtractor
          .when(() -> ApplicationFieldExtractor.extractMessage(mockResponse))
          .thenReturn("mock-application-message");

      // Act
      Slf4jMdcUtil.addEventFields(mockRequest, mockResponse, mockLogger);

      // Assert
      staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(2));
      staticMDC.verify(() -> MDC.put("event.action", "read"), times(1));
      staticMDC.verify(
          () -> MDC.put("event.search.parameters", "foo=bar,baz;hello=world"), times(1));
    }
  }

  @Test
  public void addEventFields_whenMethodIsGetAndParametersAreEmpty() {
    // Arrange
    final var mockRequest = mock(ContentCachingRequestWrapper.class);
    final var mockResponse = mock(ContentCachingResponseWrapper.class);
    final var mockLogger = mock(Logger.class);
    final var mockParameterNames = new IteratorEnumeration<>(new TreeSet<String>().iterator());

    when(mockRequest.getParameterNames()).thenReturn(mockParameterNames);

    when(mockRequest.getMethod()).thenReturn("GET");

    when(mockResponse.getStatus()).thenReturn(200);

    try (final var staticMDC = mockStatic(MDC.class);
         final var staticApplicationFieldExtractor = mockStatic(ApplicationFieldExtractor.class)) {

      staticMDC.when(() -> MDC.get("audit.log")).thenReturn("true");

      staticApplicationFieldExtractor
          .when(() -> ApplicationFieldExtractor.extractMessage(mockResponse))
          .thenReturn("mock-application-message");

      // Act
      Slf4jMdcUtil.addEventFields(mockRequest, mockResponse, mockLogger);

      // Assert
      staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(1));
      staticMDC.verify(() -> MDC.put("event.action", "read"), times(1));
      staticMDC.verify(() -> MDC.put(eq("event.search.parameters"), anyString()), times(0));
    }
  }

  @Test
  public void addEventFields_whenMethodIsPost() {
    // Arrange
    final var mockRequest = mock(ContentCachingRequestWrapper.class);
    final var mockResponse = mock(ContentCachingResponseWrapper.class);
    final var mockLogger = mock(Logger.class);
    final var set = new TreeSet<String>();
    set.add("foo");
    set.add("hello");
    final var mockParameterNames = new IteratorEnumeration<>(set.iterator());
    final var mockParameterFooValues = new String[]{"bar", "baz"};
    final var mockParameterHelloValues = new String[]{"world"};

    when(mockRequest.getParameterNames()).thenReturn(mockParameterNames);
    when(mockRequest.getParameterValues("foo")).thenReturn(mockParameterFooValues);
    when(mockRequest.getParameterValues("hello")).thenReturn(mockParameterHelloValues);

    when(mockRequest.getMethod()).thenReturn("POST");

    when(mockResponse.getStatus()).thenReturn(200);

    when(mockRequest.getContentAsString()).thenReturn("");

    try (final var staticMDC = mockStatic(MDC.class);
         final var staticApplicationFieldExtractor = mockStatic(ApplicationFieldExtractor.class)) {

      staticMDC.when(() -> MDC.get("audit.log")).thenReturn("true");

      staticApplicationFieldExtractor
          .when(() -> ApplicationFieldExtractor.extractMessage(mockResponse))
          .thenReturn("mock-application-message");

      // Act
      Slf4jMdcUtil.addEventFields(mockRequest, mockResponse, mockLogger);

      // Assert
      staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(2));
      staticMDC.verify(() -> MDC.put("event.action", "create"), times(1));
      staticMDC.verify(
          () -> MDC.put("event.create_parameters", "foo=bar,baz;hello=world"), times(1));
    }
  }

  @Test
  public void addEventFields_whenMethodIsPostAndParametersAreEmpty() {
    // Arrange
    final var mockRequest = mock(ContentCachingRequestWrapper.class);
    final var mockResponse = mock(ContentCachingResponseWrapper.class);
    final var mockLogger = mock(Logger.class);
    final var mockParameterNames = new IteratorEnumeration<>(new TreeSet<String>().iterator());

    when(mockRequest.getParameterNames()).thenReturn(mockParameterNames);

    when(mockRequest.getMethod()).thenReturn("POST");

    when(mockResponse.getStatus()).thenReturn(200);

    when(mockRequest.getContentAsString()).thenReturn("");

    try (final var staticMDC = mockStatic(MDC.class);
         final var staticApplicationFieldExtractor = mockStatic(ApplicationFieldExtractor.class)) {

      staticMDC.when(() -> MDC.get("audit.log")).thenReturn("true");

      staticApplicationFieldExtractor
          .when(() -> ApplicationFieldExtractor.extractMessage(mockResponse))
          .thenReturn("mock-application-message");

      // Act
      Slf4jMdcUtil.addEventFields(mockRequest, mockResponse, mockLogger);

      // Assert
      staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(1));
      staticMDC.verify(() -> MDC.put("event.action", "create"), times(1));
      staticMDC.verify(() -> MDC.put(eq("event.create_parameters"), anyString()), times(0));
    }
  }

  @Test
  public void addEventFields_whenMethodIsPatch() {
    // Arrange
    final var mockRequest = mock(ContentCachingRequestWrapper.class);
    final var mockResponse = mock(ContentCachingResponseWrapper.class);
    final var mockLogger = mock(Logger.class);
    final var set = new TreeSet<String>();
    set.add("foo");
    set.add("hello");
    final var mockParameterNames = new IteratorEnumeration<>(set.iterator());
    final var mockParameterFooValues = new String[]{"bar", "baz"};
    final var mockParameterHelloValues = new String[]{"world"};

    when(mockRequest.getParameterNames()).thenReturn(mockParameterNames);
    when(mockRequest.getParameterValues("foo")).thenReturn(mockParameterFooValues);
    when(mockRequest.getParameterValues("hello")).thenReturn(mockParameterHelloValues);

    when(mockRequest.getMethod()).thenReturn("PATCH");

    when(mockResponse.getStatus()).thenReturn(200);

    when(mockRequest.getContentAsString()).thenReturn("");

    try (final var staticMDC = mockStatic(MDC.class);
         final var staticApplicationFieldExtractor = mockStatic(ApplicationFieldExtractor.class)) {

      staticMDC.when(() -> MDC.get("audit.log")).thenReturn("true");

      staticApplicationFieldExtractor
          .when(() -> ApplicationFieldExtractor.extractMessage(mockResponse))
          .thenReturn("mock-application-message");

      // Act
      Slf4jMdcUtil.addEventFields(mockRequest, mockResponse, mockLogger);

      // Assert
      staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(2));
      staticMDC.verify(() -> MDC.put("event.action", "update"), times(1));
      staticMDC.verify(
          () -> MDC.put("event.update_parameters", "foo=bar,baz;hello=world"), times(1));
    }
  }

  @Test
  public void addEventFields_whenMethodIsPatchAndParametersAreEmpty() {
    // Arrange
    final var mockRequest = mock(ContentCachingRequestWrapper.class);
    final var mockResponse = mock(ContentCachingResponseWrapper.class);
    final var mockLogger = mock(Logger.class);
    final var mockParameterNames = new IteratorEnumeration<>(new TreeSet<String>().iterator());

    when(mockRequest.getParameterNames()).thenReturn(mockParameterNames);

    when(mockRequest.getMethod()).thenReturn("PATCH");

    when(mockResponse.getStatus()).thenReturn(200);

    when(mockRequest.getContentAsString()).thenReturn("");

    try (final var staticMDC = mockStatic(MDC.class);
         final var staticApplicationFieldExtractor = mockStatic(ApplicationFieldExtractor.class)) {

      staticMDC.when(() -> MDC.get("audit.log")).thenReturn("true");

      staticApplicationFieldExtractor
          .when(() -> ApplicationFieldExtractor.extractMessage(mockResponse))
          .thenReturn("mock-application-message");

      // Act
      Slf4jMdcUtil.addEventFields(mockRequest, mockResponse, mockLogger);

      // Assert
      staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(1));
      staticMDC.verify(() -> MDC.put("event.action", "update"), times(1));
      staticMDC.verify(() -> MDC.put(eq("event.update_parameters"), anyString()), times(0));
    }
  }

  @Test
  public void addObjectFields_defaultCase() {
    // Arrange
    final var mockRequest = mock(ContentCachingRequestWrapper.class);
    final var mockLogger = mock(Logger.class);

    when(mockRequest.getMethod()).thenReturn("DEFAULT");
    when(mockRequest.getContentAsString()).thenReturn("");

    // Act
    Slf4jMdcUtil.addObjectFields(mockRequest, mockLogger);

    // Assert
    verify(mockRequest, times(0)).getContentAsString();
  }

  @Test
  public void addObjectFields_whenMethodPost() {
    // Arrange
    final var mockRequest = mock(ContentCachingRequestWrapper.class);
    final var mockLogger = mock(Logger.class);

    when(mockRequest.getMethod()).thenReturn("POST");
    when(mockRequest.getContentAsString()).thenReturn("");

    // Act
    Slf4jMdcUtil.addObjectFields(mockRequest, mockLogger);

    // Assert
    verify(mockRequest, times(1)).getContentAsString();
  }

  @Test
  public void addObjectFields_whenMethodPatch() {
    // Arrange
    final var mockRequest = mock(ContentCachingRequestWrapper.class);
    final var mockLogger = mock(Logger.class);

    when(mockRequest.getMethod()).thenReturn("PATCH");
    when(mockRequest.getContentAsString()).thenReturn("");
    when(mockRequest.getRequestURI()).thenReturn("mock-request-uri");

    try (final var staticMDC = mockStatic(MDC.class)) {

      // Act
      Slf4jMdcUtil.addObjectFields(mockRequest, mockLogger);

      // Assert
      staticMDC.verify(() -> MDC.put("object.id", "mock-request-uri"));
      verify(mockRequest, times(1)).getContentAsString();
    }
  }

  @Test
  public void addObjectBodyField_whenContentCouldBeDecoded_putDecodedContentAsObjectBody() {
    // Arrange
    final var mockRequest = mock(ContentCachingRequestWrapper.class);
    final var mockLogger = mock(Logger.class);

    when(mockRequest.getContentAsString()).thenReturn("mock-content");
    when(mockRequest.getCharacterEncoding()).thenReturn("mock-character-encoding");

    try (final var staticMDC = mockStatic(MDC.class);
         final var staticURLDecoder = mockStatic(URLDecoder.class)) {

      staticURLDecoder
          .when(() -> URLDecoder.decode("mock-content", mockRequest.getCharacterEncoding()))
          .thenReturn("mock-decoded");

      // Act
      Slf4jMdcUtil.addObjectBodyField(mockRequest, mockLogger);

      // Assert
      staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(1));
      staticMDC.verify(() -> MDC.put("object.body", "mock-decoded"));
    }
  }

  @Test
  public void addObjectBodyField_whenContentCouldBeDecodedButIsEmpty_doNotPutObjectBody() {
    // Arrange
    final var mockRequest = mock(ContentCachingRequestWrapper.class);
    final var mockLogger = mock(Logger.class);

    when(mockRequest.getContentAsString()).thenReturn("mock-content");
    when(mockRequest.getCharacterEncoding()).thenReturn("mock-character-encoding");

    try (final var staticMDC = mockStatic(MDC.class);
         final var staticURLDecoder = mockStatic(URLDecoder.class)) {

      staticURLDecoder
          .when(() -> URLDecoder.decode("mock-content", mockRequest.getCharacterEncoding()))
          .thenReturn("");

      // Act
      Slf4jMdcUtil.addObjectBodyField(mockRequest, mockLogger);

      // Assert
      staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(0));
    }
  }

  @Test
  public void addObjectBodyField_whenContentCouldNotBeDecoded_putEncodedContentAsObjectBody() {
    // Arrange
    final var mockRequest = mock(ContentCachingRequestWrapper.class);
    final var mockLogger = mock(Logger.class);

    when(mockRequest.getContentAsString()).thenReturn("mock-content");
    when(mockRequest.getCharacterEncoding()).thenReturn("mock-character-encoding");

    try (final var staticMDC = mockStatic(MDC.class);
         final var staticURLDecoder = mockStatic(URLDecoder.class)) {

      staticURLDecoder
          .when(() -> URLDecoder.decode("mock-content", mockRequest.getCharacterEncoding()))
          .thenThrow(UnsupportedEncodingException.class);

      // Act
      Slf4jMdcUtil.addObjectBodyField(mockRequest, mockLogger);

      // Assert
      staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(1));
      staticMDC.verify(() -> MDC.put("object.body", "mock-content"));
    }
  }

  @Test
  public void addObjectBodyField_whenContentIsEmpty_doNotPutObjectBody() {
    // Arrange
    final var mockRequest = mock(ContentCachingRequestWrapper.class);
    final var mockLogger = mock(Logger.class);

    when(mockRequest.getContentAsString()).thenReturn("");
    when(mockRequest.getCharacterEncoding()).thenReturn("mock-character-encoding");

    try (final var staticMDC = mockStatic(MDC.class)) {

      // Act
      Slf4jMdcUtil.addObjectBodyField(mockRequest, mockLogger);

      // Assert
      staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(0));
    }
  }

  @Test
  public void clearMdc() {
    // Arrange
    try (MockedStatic<MDC> staticMDC = mockStatic(MDC.class)) {
      // Act
      Slf4jMdcUtil.clearMdc();

      // Assert
      staticMDC.verify(MDC::clear, times(1));
    }
  }

  @Test
  public void log_whenResponseStatusIsLessThan400() {
    // Arrange
    final var mockResponse = mock(ContentCachingResponseWrapper.class);
    final var mockLogger = mock(Logger.class);

    when(mockResponse.getStatus()).thenReturn(200);

    try (final var staticMDC = mockStatic(MDC.class);
         final var staticApplicationFieldExtractor = mockStatic(ApplicationFieldExtractor.class)) {

      staticApplicationFieldExtractor
          .when(() -> ApplicationFieldExtractor.extractMessage(mockResponse))
          .thenReturn("mock-application-message");

      // Act
      Slf4jMdcUtil.log(mockResponse, mockLogger);

      // Assert
      staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(1));
      staticMDC.verify(() -> MDC.put("event.outcome", "success"), times(1));

      verify(mockLogger, times(1)).info(anyString());
      verify(mockLogger, times(1)).info("mock-application-message");
      verify(mockLogger, times(0)).error(anyString());
    }
  }

  @Test
  public void log_whenResponseStatusIsGreaterThanOrEqualTo400() {
    // Arrange
    final var mockResponse = mock(ContentCachingResponseWrapper.class);
    final var mockLogger = mock(Logger.class);

    when(mockResponse.getStatus()).thenReturn(518);

    try (final var staticMDC = mockStatic(MDC.class);
         final var staticApplicationFieldExtractor = mockStatic(ApplicationFieldExtractor.class)) {

      staticApplicationFieldExtractor
          .when(() -> ApplicationFieldExtractor.extractMessage(mockResponse))
          .thenReturn("mock-application-message");

      // Act
      Slf4jMdcUtil.log(mockResponse, mockLogger);

      // Assert
      staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(1));
      staticMDC.verify(() -> MDC.put("event.outcome", "failure"), times(1));

      verify(mockLogger, times(0)).info(anyString());
      verify(mockLogger, times(1)).error(anyString());
      verify(mockLogger, times(1)).error("mock-application-message");
    }
  }
}
