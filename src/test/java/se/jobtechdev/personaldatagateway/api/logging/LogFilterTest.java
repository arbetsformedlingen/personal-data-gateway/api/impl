package se.jobtechdev.personaldatagateway.api.logging;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.slf4j.Logger;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;
import se.jobtechdev.personaldatagateway.api.util.LoggerSupplier;

import java.io.IOException;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class LogFilterTest {
  @Test
  @Tag("UnitTest")
  public void doLog_withoutAuditLogging() {
    try (MockedStatic<Slf4jMdcUtil> staticSlf4jMdcUtil = mockStatic(Slf4jMdcUtil.class);
         MockedStatic<LoggerSupplier> staticLoggerSupplier = mockStatic(LoggerSupplier.class)) {
      // Arrange
      final LoggerSupplier mockLoggerSupplier = mock(LoggerSupplier.class);
      staticLoggerSupplier
          .when(() -> LoggerSupplier.forClass(LogFilter.class))
          .thenReturn(mockLoggerSupplier);
      final Logger mockLogger = mock(Logger.class);
      when(mockLoggerSupplier.get()).thenReturn(mockLogger);

      final var request = mock(ContentCachingRequestWrapper.class);
      final var response = mock(ContentCachingResponseWrapper.class);

      staticSlf4jMdcUtil.when(Slf4jMdcUtil::activateAuditField).thenReturn(false);

      // Act
      final LogFilter logFilter = new LogFilter();
      logFilter.doLog(request, response);

      // Assert
      staticSlf4jMdcUtil.verify(Slf4jMdcUtil::activateAuditField, times(1));
      staticSlf4jMdcUtil.verify(() -> Slf4jMdcUtil.addUrlFields(request), times(1));
      staticSlf4jMdcUtil.verify(
          () -> Slf4jMdcUtil.addEventFields(request, response, mockLogger), times(1));
      staticSlf4jMdcUtil.verify(
          () -> Slf4jMdcUtil.addObjectFields(request, mockLogger), times(0));
      verify(mockLogger, times(0)).error(anyString());
      staticSlf4jMdcUtil.verify(Slf4jMdcUtil::clearMdc, times(1));
    }
  }

  @Test
  @Tag("UnitTest")
  public void doLog_withAuditLogging() {
    try (MockedStatic<Slf4jMdcUtil> staticSlf4jMdcUtil = mockStatic(Slf4jMdcUtil.class);
         MockedStatic<LoggerSupplier> staticLoggerSupplier = mockStatic(LoggerSupplier.class)) {
      // Arrange
      final LoggerSupplier mockLoggerSupplier = mock(LoggerSupplier.class);
      staticLoggerSupplier
          .when(() -> LoggerSupplier.forClass(LogFilter.class))
          .thenReturn(mockLoggerSupplier);
      final Logger mockLogger = mock(Logger.class);
      when(mockLoggerSupplier.get()).thenReturn(mockLogger);

      final var request = mock(ContentCachingRequestWrapper.class);
      final var response = mock(ContentCachingResponseWrapper.class);

      staticSlf4jMdcUtil.when(Slf4jMdcUtil::activateAuditField).thenReturn(true);

      // Act
      final LogFilter logFilter = new LogFilter();
      logFilter.doLog(request, response);

      // Assert
      staticSlf4jMdcUtil.verify(Slf4jMdcUtil::activateAuditField, times(1));
      staticSlf4jMdcUtil.verify(() -> Slf4jMdcUtil.addUrlFields(request), times(1));
      staticSlf4jMdcUtil.verify(
          () -> Slf4jMdcUtil.addEventFields(request, response, mockLogger), times(1));
      staticSlf4jMdcUtil.verify(
          () -> Slf4jMdcUtil.addObjectFields(request, mockLogger), times(1));
      verify(mockLogger, times(0)).error(anyString());
      staticSlf4jMdcUtil.verify(Slf4jMdcUtil::clearMdc, times(1));
    }
  }

  @Test
  @Tag("UnitTest")
  public void doLog_whenExceptionIsThrown_simpleErrorLog() {
    try (MockedStatic<Slf4jMdcUtil> staticSlf4jMdcUtil = mockStatic(Slf4jMdcUtil.class);
         MockedStatic<LoggerSupplier> staticLoggerSupplier = mockStatic(LoggerSupplier.class);
         MockedStatic<ApplicationFieldExtractor> staticApplicationFieldExtractor =
             mockStatic(ApplicationFieldExtractor.class);) {
      // Arrange
      final LoggerSupplier mockLoggerSupplier = mock(LoggerSupplier.class);
      staticLoggerSupplier
          .when(() -> LoggerSupplier.forClass(LogFilter.class))
          .thenReturn(mockLoggerSupplier);
      final Logger mockLogger = mock(Logger.class);
      when(mockLoggerSupplier.get()).thenReturn(mockLogger);

      final ContentCachingRequestWrapper request = mock(ContentCachingRequestWrapper.class);
      final ContentCachingResponseWrapper response = mock(ContentCachingResponseWrapper.class);

      staticSlf4jMdcUtil
          .when(() -> Slf4jMdcUtil.addUrlFields(request))
          .thenThrow(new RuntimeException("error"));

      // Act
      final LogFilter logFilter = new LogFilter();
      logFilter.doLog(request, response);

      // Assert
      staticSlf4jMdcUtil.verify(Slf4jMdcUtil::activateAuditField, times(0));
      staticSlf4jMdcUtil.verify(() -> Slf4jMdcUtil.addUrlFields(request), times(1));
      verify(mockLogger, times(0)).info(anyString());
      verify(mockLogger, times(1)).error("Failed to log!");
      staticSlf4jMdcUtil.verify(Slf4jMdcUtil::clearMdc, times(2));
    }
  }

  @Test
  @Tag("UnitTest")
  public void doFilterInternal() throws ServletException, IOException {
    // Arrange
    final var mockRequest = mock(HttpServletRequest.class);
    final var mockResponse = mock(HttpServletResponse.class);
    final var mockFilterChain = mock(FilterChain.class);

    final var constructionContentCachingRequestWrapperArgs = new HashMap<>();
    final var constructionContentCachingResponseWrapperArgs = new HashMap<>();

    try (MockedStatic<LoggerSupplier> staticLoggerSupplier = mockStatic(LoggerSupplier.class);
         final var constructionContentCachingRequestWrapper =
             mockConstruction(
                 ContentCachingRequestWrapper.class,
                 (mock, context) -> {
                   constructionContentCachingRequestWrapperArgs.put(mock, context.arguments());
                 });
         final var constructionContentCachingResponseWrapper =
             mockConstruction(
                 ContentCachingResponseWrapper.class,
                 (mock, context) -> {
                   constructionContentCachingResponseWrapperArgs.put(mock, context.arguments());
                 });
         final var staticSlf4jMdcUtil = mockStatic(Slf4jMdcUtil.class)) {

      final LoggerSupplier mockLoggerSupplier = mock(LoggerSupplier.class);
      staticLoggerSupplier
          .when(() -> LoggerSupplier.forClass(LogFilter.class))
          .thenReturn(mockLoggerSupplier);
      final Logger mockLogger = mock(Logger.class);
      when(mockLoggerSupplier.get()).thenReturn(mockLogger);

      // Act
      final LogFilter logFilter = new LogFilter();
      logFilter.doFilterInternal(mockRequest, mockResponse, mockFilterChain);

      // Assert
      assertEquals(1, constructionContentCachingRequestWrapper.constructed().size());
      final var mockContentCachingRequestWrapper =
          constructionContentCachingRequestWrapper.constructed().getFirst();

      assertEquals(1, constructionContentCachingResponseWrapper.constructed().size());
      final var mockContentCachingResponseWrapper =
          constructionContentCachingResponseWrapper.constructed().getFirst();

      verify(mockFilterChain, times(1))
          .doFilter(mockContentCachingRequestWrapper, mockContentCachingResponseWrapper);

      staticSlf4jMdcUtil.verify(Slf4jMdcUtil::activateAuditField, times(1));
      staticSlf4jMdcUtil.verify(
          () -> Slf4jMdcUtil.addUrlFields(mockContentCachingRequestWrapper), times(1));
      staticSlf4jMdcUtil.verify(
          () ->
              Slf4jMdcUtil.addEventFields(
                  mockContentCachingRequestWrapper, mockContentCachingResponseWrapper, mockLogger),
          times(1));
      staticSlf4jMdcUtil.verify(() -> Slf4jMdcUtil.clearMdc(), times(1));
    }
  }

  @Test
  @Tag("UnitTest")
  public void doFilterInternal_whenExceptionIsThrown_thenLogError()
      throws ServletException, IOException {
    // Arrange
    final var mockRequest = mock(HttpServletRequest.class);
    final var mockResponse = mock(HttpServletResponse.class);
    final var mockFilterChain = mock(FilterChain.class);

    final var constructionContentCachingRequestWrapperArgs = new HashMap<>();
    final var constructionContentCachingResponseWrapperArgs = new HashMap<>();

    try (MockedStatic<LoggerSupplier> staticLoggerSupplier = mockStatic(LoggerSupplier.class);
         final var constructionContentCachingRequestWrapper =
             mockConstruction(
                 ContentCachingRequestWrapper.class,
                 (mock, context) -> {
                   constructionContentCachingRequestWrapperArgs.put(mock, context.arguments());
                 });
         final var constructionContentCachingResponseWrapper =
             mockConstruction(
                 ContentCachingResponseWrapper.class,
                 (mock, context) -> {
                   constructionContentCachingResponseWrapperArgs.put(mock, context.arguments());
                 });
         final var staticSlf4jMdcUtil = mockStatic(Slf4jMdcUtil.class)) {

      final LoggerSupplier mockLoggerSupplier = mock(LoggerSupplier.class);
      staticLoggerSupplier
          .when(() -> LoggerSupplier.forClass(LogFilter.class))
          .thenReturn(mockLoggerSupplier);
      final Logger mockLogger = mock(Logger.class);
      when(mockLoggerSupplier.get()).thenReturn(mockLogger);

      staticSlf4jMdcUtil
          .when(() -> Slf4jMdcUtil.activateAuditField())
          .thenThrow(RuntimeException.class);

      // Act
      final LogFilter logFilter = new LogFilter();
      logFilter.doFilterInternal(mockRequest, mockResponse, mockFilterChain);

      // Assert
      staticSlf4jMdcUtil.verify(() -> Slf4jMdcUtil.clearMdc(), times(2));
      verify(mockLogger, times(1)).error("Failed to log!");
    }
  }
}
