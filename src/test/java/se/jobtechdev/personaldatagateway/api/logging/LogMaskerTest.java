package se.jobtechdev.personaldatagateway.api.logging;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

public class LogMaskerTest {
  @Test
  @Tag("UnitTest")
  public void mask_whenPersonalNumberIsNotPresent_replaceNothing() {
    String masked = LogMasker.mask("{\\\"foo\\\":\\\"bar\\\"}");
    assertEquals("{\\\"foo\\\":\\\"bar\\\"}", masked);
  }

  @Test
  @Tag("UnitTest")
  public void mask_whenPersonalNumberIsPresent_replaceWithRedactedText() {
    assertEquals(
        "Redact personal number REDACTED when present",
        LogMasker.mask("Redact personal number 199010102383 when present"));

    assertEquals(
        "Redact personal number REDACTED when present",
        LogMasker.mask("Redact personal number 9010102383 when present"));

    assertEquals(
        "Redact personal number REDACTED when present",
        LogMasker.mask("Redact personal number 19901010-2383 when present"));

    assertEquals(
        "Redact personal number REDACTED when present",
        LogMasker.mask("Redact personal number 901010-2383 when present"));
  }
}
