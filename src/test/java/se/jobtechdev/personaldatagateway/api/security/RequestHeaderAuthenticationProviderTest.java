package se.jobtechdev.personaldatagateway.api.security;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.MDC;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import se.jobtechdev.personaldatagateway.api.generated.entities.Client;
import se.jobtechdev.personaldatagateway.api.service.ClientService;
import se.jobtechdev.personaldatagateway.api.util.AuthUtil;
import se.jobtechdev.personaldatagateway.api.util.TimeProvider;

@ExtendWith(MockitoExtension.class)
public class RequestHeaderAuthenticationProviderTest {

  private RequestHeaderAuthenticationProvider provider;

  @Mock private ClientService clientService;

  @BeforeEach
  void setUp() {
    provider = new RequestHeaderAuthenticationProvider(clientService);
  }

  @Test
  public void getClientByKey_whenAllChecksPass_returnClient() {
    // Arrange
    final var authKey =
        Base64.getEncoder().encodeToString("authKey".getBytes(StandardCharsets.UTF_8));
    final var mockClient = mock(Client.class);
    final var mockDecodedAuthKey = new byte[] {1, 2, 3};
    final var mockHashedAuthKey = new byte[] {4, 5, 6};

    when(clientService.getClientByKeyHash(mockHashedAuthKey)).thenReturn(Optional.of(mockClient));

    try (final var staticAuthUtil = mockStatic(AuthUtil.class)) {
      staticAuthUtil.when(() -> AuthUtil.decodeAuthKey(authKey)).thenReturn(mockDecodedAuthKey);
      staticAuthUtil
          .when(() -> AuthUtil.hashDecodedAuthKey(mockDecodedAuthKey))
          .thenReturn(mockHashedAuthKey);

      // Act
      final var result = provider.getClientByKey(authKey);

      // Assert
      assertEquals(mockClient, result);
      staticAuthUtil.verify(() -> AuthUtil.assertAuthKeyExists(authKey), times(1));
      staticAuthUtil.verify(() -> AuthUtil.decodeAuthKey(authKey), times(1));
    }
  }

  @Test
  public void getClientByKey_whenClientIsNotFound_throwBadCredentialsException() {
    // Arrange
    final var authKey =
        Base64.getEncoder().encodeToString("authKey".getBytes(StandardCharsets.UTF_8));
    final var mockClient = mock(Client.class);
    final var mockDecodedAuthKey = new byte[] {1, 2, 3};
    final var mockHashedAuthKey = new byte[] {4, 5, 6};

    when(clientService.getClientByKeyHash(mockHashedAuthKey)).thenReturn(Optional.empty());

    try (final var staticAuthUtil = mockStatic(AuthUtil.class)) {
      staticAuthUtil.when(() -> AuthUtil.decodeAuthKey(authKey)).thenReturn(mockDecodedAuthKey);
      staticAuthUtil
          .when(() -> AuthUtil.hashDecodedAuthKey(mockDecodedAuthKey))
          .thenReturn(mockHashedAuthKey);

      // Act
      final var thrown =
          assertThrows(BadCredentialsException.class, () -> provider.getClientByKey(authKey));

      // Assert
      assertEquals(
          "Bad Request Header Credentials, provided authKey does not match any known client",
          thrown.getMessage());
      staticAuthUtil.verify(() -> AuthUtil.assertAuthKeyExists(authKey), times(1));
      staticAuthUtil.verify(() -> AuthUtil.decodeAuthKey(authKey), times(1));
    }
  }

  @Test
  public void authenticate() {
    // Arrange
    final var mockClient = mock(Client.class);
    final var mockClientId = mock(UUID.class);

    when(mockClientId.toString()).thenReturn("mock-client-id");

    final var authKey = "authKey";

    final var mockDecodedAuthKey = new byte[] {1, 2, 3};
    final var mockHashedAuthKey = new byte[] {4, 5, 6};

    when(clientService.getClientByKeyHash(mockHashedAuthKey)).thenReturn(Optional.of(mockClient));
    final var role = "client";
    when(mockClient.getRole()).thenReturn(role);
    when(mockClient.getId()).thenReturn(mockClientId);
    final var mockAuthentication = mock(Authentication.class);
    when(mockAuthentication.getPrincipal()).thenReturn(authKey);

    final var constructionSimpleGrantedAuthorityArgs =
        new HashMap<SimpleGrantedAuthority, List<?>>();
    final var constructionPreAuthenticatedAuthenticationTokenArgs =
        new HashMap<PreAuthenticatedAuthenticationToken, List<?>>();
    try (final var constructionSimpleGrantedAuthority =
            mockConstruction(
                SimpleGrantedAuthority.class,
                (mock, context) -> {
                  constructionSimpleGrantedAuthorityArgs.put(mock, context.arguments());
                });
        final var constructionPreAuthenticatedAuthenticationToken =
            mockConstruction(
                PreAuthenticatedAuthenticationToken.class,
                (mock, context) -> {
                  constructionPreAuthenticatedAuthenticationTokenArgs.put(
                      mock, context.arguments());
                });
        final var staticAuthUtil = mockStatic(AuthUtil.class);
        final var staticMDC = mockStatic(MDC.class)) {
      staticAuthUtil.when(() -> AuthUtil.decodeAuthKey(authKey)).thenReturn(mockDecodedAuthKey);
      staticAuthUtil
          .when(() -> AuthUtil.hashDecodedAuthKey(mockDecodedAuthKey))
          .thenReturn(mockHashedAuthKey);

      // Act
      final var result = provider.authenticate(mockAuthentication);

      // Assert
      assertEquals(1, constructionSimpleGrantedAuthority.constructed().size());
      final var mockSimpleGrantedAuthority =
          constructionSimpleGrantedAuthority.constructed().get(0);
      assertEquals(
          "client", constructionSimpleGrantedAuthorityArgs.get(mockSimpleGrantedAuthority).get(0));
      assertEquals(1, constructionPreAuthenticatedAuthenticationToken.constructed().size());
      final var mockPreAuthenticatedAuthenticationToken =
          constructionPreAuthenticatedAuthenticationToken.constructed().get(0);
      assertEquals(
          "mock-client-id",
          constructionPreAuthenticatedAuthenticationTokenArgs
              .get(mockPreAuthenticatedAuthenticationToken)
              .get(0));
      assertNull(
          constructionPreAuthenticatedAuthenticationTokenArgs
              .get(mockPreAuthenticatedAuthenticationToken)
              .get(1));
      assertEquals(
          List.of(mockSimpleGrantedAuthority),
          constructionPreAuthenticatedAuthenticationTokenArgs
              .get(mockPreAuthenticatedAuthenticationToken)
              .get(2));
      assertEquals(mockPreAuthenticatedAuthenticationToken, result);

      staticAuthUtil.verify(() -> AuthUtil.assertAuthKeyExists(authKey), times(1));
      staticAuthUtil.verify(() -> AuthUtil.decodeAuthKey(authKey), times(1));

      staticMDC.verify(() -> MDC.put(anyString(), anyString()), times(2));
      staticMDC.verify(() -> MDC.put("temp.custom.pdg-api.clientid", "mock-client-id"), times(1));
      staticMDC.verify(() -> MDC.put("temp.log", "true"), times(1));
    }
  }

  @Test
  public void authenticate_whenPrincipalIsNull_logAsWarningAndRethrowException() {
    // Arrange
    final var mockAuthentication = mock(Authentication.class);
    when(mockAuthentication.getPrincipal()).thenReturn(null);

    final var constructionSimpleGrantedAuthorityArgs =
        new HashMap<SimpleGrantedAuthority, List<?>>();
    final var constructionPreAuthenticatedAuthenticationTokenArgs =
        new HashMap<PreAuthenticatedAuthenticationToken, List<?>>();
    try (final var constructionSimpleGrantedAuthority =
            mockConstruction(
                SimpleGrantedAuthority.class,
                (mock, context) -> {
                  constructionSimpleGrantedAuthorityArgs.put(mock, context.arguments());
                });
        final var constructionPreAuthenticatedAuthenticationToken =
            mockConstruction(
                PreAuthenticatedAuthenticationToken.class,
                (mock, context) -> {
                  constructionPreAuthenticatedAuthenticationTokenArgs.put(
                      mock, context.arguments());
                })) {
      // Act
      final var thrown =
          assertThrows(
              BadCredentialsException.class, () -> provider.authenticate(mockAuthentication));

      // Assert
      assertEquals(
          "Bad Request Header Credentials, provided principal is null", thrown.getMessage());
      assertEquals(0, constructionSimpleGrantedAuthority.constructed().size());
      assertEquals(0, constructionPreAuthenticatedAuthenticationToken.constructed().size());
    }
  }

  @Test
  public void supports() {
    // Arrange
    final var mockAuthentication = mock(PreAuthenticatedAuthenticationToken.class);

    // Act
    final var result = provider.supports(mockAuthentication.getClass());

    // Assert
    assertTrue(result);
  }

  @Test
  public void
      supports_whenClassDoesNotEqualPreAuthenticatedAuthenticationTokenClass_thenReturnFalse() {
    // Arrange
    final var mockAuthentication = mock(Object.class);

    // Act
    final var result = provider.supports(mockAuthentication.getClass());

    // Assert
    assertFalse(result);
  }

  @Test
  public void authenticate_whenClientRevokedTimestampIsInThePast_throwBadCredentialsException() {
    // Arrange
    final var mockClient = mock(Client.class);
    final var mockClientRevoked = mock(ZonedDateTime.class);
    final var authKey = "authKey";

    final var mockDecodedAuthKey = new byte[] {1, 2, 3};
    final var mockHashedAuthKey = new byte[] {4, 5, 6};

    when(clientService.getClientByKeyHash(mockHashedAuthKey)).thenReturn(Optional.of(mockClient));
    when(mockClient.getRevoked()).thenReturn(mockClientRevoked);
    final var mockAuthentication = mock(Authentication.class);
    when(mockAuthentication.getPrincipal()).thenReturn(authKey);

    final var mockNow = mock(ZonedDateTime.class);
    when(mockNow.isAfter(mockClientRevoked)).thenReturn(true);

    try (final var staticTimeProvider = mockStatic(TimeProvider.class);
        final var staticAuthUtil = mockStatic(AuthUtil.class)) {
      staticAuthUtil.when(() -> AuthUtil.decodeAuthKey(authKey)).thenReturn(mockDecodedAuthKey);
      staticAuthUtil
          .when(() -> AuthUtil.hashDecodedAuthKey(mockDecodedAuthKey))
          .thenReturn(mockHashedAuthKey);

      staticTimeProvider.when(TimeProvider::now).thenReturn(mockNow);

      // Act
      final var thrown =
          assertThrows(
              BadCredentialsException.class, () -> provider.authenticate(mockAuthentication));

      // Assert
      assertEquals(
          "Bad Request Header Credentials, provided client is revoked", thrown.getMessage());
    }
  }

  @Test
  public void authenticate_whenClientRevokedTimestampIsNotInThePast_continue() {
    // Arrange
    final var mockClient = mock(Client.class);
    final var mockClientId = mock(UUID.class);

    when(mockClient.getId()).thenReturn(mockClientId);
    when(mockClientId.toString()).thenReturn("mock-client-id");

    final var mockClientRevoked = mock(ZonedDateTime.class);
    final var authKey = "authKey";

    final var mockDecodedAuthKey = new byte[] {1, 2, 3};
    final var mockHashedAuthKey = new byte[] {4, 5, 6};

    when(clientService.getClientByKeyHash(mockHashedAuthKey)).thenReturn(Optional.of(mockClient));
    when(mockClient.getRevoked()).thenReturn(mockClientRevoked);
    final var role = "client";
    when(mockClient.getRole()).thenReturn(role);
    final var mockAuthentication = mock(Authentication.class);
    when(mockAuthentication.getPrincipal()).thenReturn(authKey);

    final var mockNow = mock(ZonedDateTime.class);
    when(mockNow.isAfter(mockClientRevoked)).thenReturn(false);

    final var constructionSimpleGrantedAuthorityArgs =
        new HashMap<SimpleGrantedAuthority, List<?>>();
    final var constructionPreAuthenticatedAuthenticationTokenArgs =
        new HashMap<PreAuthenticatedAuthenticationToken, List<?>>();
    try (final var constructionSimpleGrantedAuthority =
            mockConstruction(
                SimpleGrantedAuthority.class,
                (mock, context) -> {
                  constructionSimpleGrantedAuthorityArgs.put(mock, context.arguments());
                });
        final var constructionPreAuthenticatedAuthenticationToken =
            mockConstruction(
                PreAuthenticatedAuthenticationToken.class,
                (mock, context) -> {
                  constructionPreAuthenticatedAuthenticationTokenArgs.put(
                      mock, context.arguments());
                });
        final var staticTimeProvider = mockStatic(TimeProvider.class);
        final var staticAuthUtil = mockStatic(AuthUtil.class)) {
      staticAuthUtil.when(() -> AuthUtil.decodeAuthKey(authKey)).thenReturn(mockDecodedAuthKey);
      staticAuthUtil
          .when(() -> AuthUtil.hashDecodedAuthKey(mockDecodedAuthKey))
          .thenReturn(mockHashedAuthKey);

      staticTimeProvider.when(TimeProvider::now).thenReturn(mockNow);

      // Act
      final var result = provider.authenticate(mockAuthentication);

      // Assert
      assertEquals(1, constructionSimpleGrantedAuthority.constructed().size());
      final var mockSimpleGrantedAuthority =
          constructionSimpleGrantedAuthority.constructed().get(0);
      assertEquals(
          "client", constructionSimpleGrantedAuthorityArgs.get(mockSimpleGrantedAuthority).get(0));
      assertEquals(1, constructionPreAuthenticatedAuthenticationToken.constructed().size());
      final var mockPreAuthenticatedAuthenticationToken =
          constructionPreAuthenticatedAuthenticationToken.constructed().get(0);
      assertEquals(
          "mock-client-id",
          constructionPreAuthenticatedAuthenticationTokenArgs
              .get(mockPreAuthenticatedAuthenticationToken)
              .get(0));
      assertNull(
          constructionPreAuthenticatedAuthenticationTokenArgs
              .get(mockPreAuthenticatedAuthenticationToken)
              .get(1));
      assertEquals(
          List.of(mockSimpleGrantedAuthority),
          constructionPreAuthenticatedAuthenticationTokenArgs
              .get(mockPreAuthenticatedAuthenticationToken)
              .get(2));
      assertEquals(mockPreAuthenticatedAuthenticationToken, result);
    }
  }
}
