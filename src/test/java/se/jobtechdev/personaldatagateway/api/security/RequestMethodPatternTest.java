package se.jobtechdev.personaldatagateway.api.security;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;

public class RequestMethodPatternTest {
  @Test
  public void requestMethodPatternEquals_whenObjectIsTheSame_returnTrue() {
    final var methodPattern =
        RequestMethodPattern.create(HttpMethod.GET, "/foo", new String[] {"bar"});
    assertTrue(methodPattern.equals(methodPattern));
  }

  @Test
  public void requestMethodPatternEquals_whenObjectIsNull_returnFalse() {
    final var methodPattern =
        RequestMethodPattern.create(HttpMethod.GET, "/foo", new String[] {"bar"});
    assertFalse(methodPattern.equals(null));
  }

  @Test
  public void requestMethodPatternEquals_whenObjectClassIsDifferent_returnFalse() {
    final var methodPattern =
        RequestMethodPattern.create(HttpMethod.GET, "/foo", new String[] {"bar"});
    final var otherObject = mock(Object.class);
    assertFalse(methodPattern.equals(otherObject));
  }

  @Test
  public void requestMethodPatternEquals_whenMethodIsDifferent_returnFalse() {
    final var methodPattern1 =
        RequestMethodPattern.create(HttpMethod.GET, "/foo", new String[] {"bar"});
    final var methodPattern2 =
        RequestMethodPattern.create(HttpMethod.POST, "/foo", new String[] {"bar"});
    assertNotEquals(methodPattern1, methodPattern2);
  }

  @Test
  public void requestMethodPatternEquals_whenPatternIsDifferent_returnFalse() {
    final var methodPattern1 =
        RequestMethodPattern.create(HttpMethod.GET, "/foo", new String[] {"bar"});
    final var methodPattern2 =
        RequestMethodPattern.create(HttpMethod.GET, "/baz", new String[] {"bar"});
    assertNotEquals(methodPattern1, methodPattern2);
  }

  @Test
  public void requestMethodPatternEquals_whenRolesIsDifferent_returnFalse() {
    final var methodPattern1 =
        RequestMethodPattern.create(HttpMethod.GET, "/foo", new String[] {"bar"});
    final var methodPattern2 =
        RequestMethodPattern.create(HttpMethod.GET, "/foo", new String[] {"baz"});
    assertNotEquals(methodPattern1, methodPattern2);
  }

  @Test
  public void requestMethodPatternEquals_whenAllPropertiesAreEqual_returnTrue() {
    final var methodPattern1 =
        RequestMethodPattern.create(HttpMethod.GET, "/foo", new String[] {"bar"});
    final var methodPattern2 =
        RequestMethodPattern.create(HttpMethod.GET, "/foo", new String[] {"bar"});
    assertEquals(methodPattern1, methodPattern2);
  }
}
