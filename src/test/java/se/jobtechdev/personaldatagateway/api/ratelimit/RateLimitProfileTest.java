package se.jobtechdev.personaldatagateway.api.ratelimit;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

import io.github.bucket4j.BandwidthBuilder;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.ConsumptionProbe;
import io.github.bucket4j.local.LocalBucket;
import io.github.bucket4j.local.LocalBucketBuilder;
import java.time.Duration;
import java.util.HashMap;
import java.util.function.Function;
import org.junit.jupiter.api.Test;

public class RateLimitProfileTest {

  @Test
  public void rateLimitPolicy() {
    assertEquals("\"foo\";q=1;w=2", RateLimitProfile.rateLimitPolicy("foo", 1, 2));
  }

  @Test
  public void rateLimit() {
    assertEquals("\"foo\";r=1;t=2", RateLimitProfile.rateLimit("foo", 1, 2));
  }

  @Test
  public void createLimitLambda() {
    // Arrange
    final var maxTokens = 5;
    final var refillTokens = 5;
    final var refillPeriod = mock(Duration.class);
    final var mockLimit = mock(BandwidthBuilder.BandwidthBuilderCapacityStage.class);
    final var mockRefillStage = mock(BandwidthBuilder.BandwidthBuilderRefillStage.class);
    when(mockLimit.capacity(maxTokens)).thenReturn(mockRefillStage);

    // Act
    final var limitLambda =
        RateLimitProfile.createLimitLambda(maxTokens, refillTokens, refillPeriod);

    limitLambda.apply(mockLimit);

    // Assert
    verify(mockLimit, times(1)).capacity(maxTokens);
    verify(mockRefillStage, times(1)).refillIntervally(refillTokens, refillPeriod);
  }

  @Test
  public void createQuota() {
    // Arrange
    final var mockRefillPeriod = mock(Duration.class);
    final var mockLocalBucket = mock(LocalBucket.class);
    final var mockLocalBucketBuilder = mock(LocalBucketBuilder.class);
    final var mockLimitLambda = mock(Function.class);

    when(mockRefillPeriod.toSeconds()).thenReturn(3L);

    try (final var staticBucket = mockStatic(Bucket.class);
        final var staticRateLimitProfile = mockStatic(RateLimitProfile.class);
        final var staticDuration = mockStatic(Duration.class)) {
      staticBucket.when(Bucket::builder).thenReturn(mockLocalBucketBuilder);
      staticRateLimitProfile
          .when(() -> RateLimitProfile.createLimitLambda(1, 2, mockRefillPeriod))
          .thenReturn(mockLimitLambda);
      when(mockLocalBucketBuilder.addLimit(mockLimitLambda)).thenReturn(mockLocalBucketBuilder);
      when(mockLocalBucketBuilder.build()).thenReturn(mockLocalBucket);
      staticDuration.when(() -> Duration.ofSeconds(3L)).thenReturn(mockRefillPeriod);

      // Act
      final var rateLimitProfile =
          new RateLimitProfile(new HashMap<>(), "test", 1, 2, mockRefillPeriod.toSeconds());

      final var quota = rateLimitProfile.createQuota("dummy");

      // Assert
      assertEquals(mockLocalBucket, quota);
    }
  }

  @Test
  public void consume_whenConsumedIsTrue_thenQuotaExceededIsFalse() {
    // Arrange
    final var mockRefillPeriod = mock(Duration.class);
    final var mockLocalBucket = mock(LocalBucket.class);
    final var mockLocalBucketBuilder = mock(LocalBucketBuilder.class);
    final var mockLimitLambda = mock(Function.class);
    final var mockConsumptionProbe = mock(ConsumptionProbe.class);

    when(mockRefillPeriod.toSeconds()).thenReturn(3L);
    when(mockLocalBucket.tryConsumeAndReturnRemaining(1)).thenReturn(mockConsumptionProbe);
    when(mockConsumptionProbe.getRemainingTokens()).thenReturn(999L);
    when(mockConsumptionProbe.getNanosToWaitForReset()).thenReturn(123_000_000_000L);
    when(mockConsumptionProbe.isConsumed()).thenReturn(true);

    try (final var staticBucket = mockStatic(Bucket.class);
        final var staticRateLimitProfile = mockStatic(RateLimitProfile.class);
        final var staticDuration = mockStatic(Duration.class)) {
      staticBucket.when(Bucket::builder).thenReturn(mockLocalBucketBuilder);

      staticRateLimitProfile
          .when(() -> RateLimitProfile.createLimitLambda(1, 2, mockRefillPeriod))
          .thenReturn(mockLimitLambda);
      staticRateLimitProfile
          .when(() -> RateLimitProfile.rateLimitPolicy("example-profileName", 2, 3))
          .thenReturn("example-rateLimitPolicy");
      staticRateLimitProfile
          .when(() -> RateLimitProfile.rateLimit("example-profileName", 999, 123))
          .thenReturn("example-rateLimit");

      when(mockLocalBucketBuilder.addLimit(mockLimitLambda)).thenReturn(mockLocalBucketBuilder);
      when(mockLocalBucketBuilder.build()).thenReturn(mockLocalBucket);

      staticDuration.when(() -> Duration.ofSeconds(3L)).thenReturn(mockRefillPeriod);

      // Act
      final var rateLimitProfile =
          new RateLimitProfile(new HashMap<>(), "example-profileName", 1, 2, 3);

      final var quota = rateLimitProfile.consume("example-profileName");

      // Assert
      assertEquals("example-rateLimitPolicy", quota.rateLimitPolicy());
      assertEquals("example-rateLimit", quota.rateLimit());
      assertEquals(false, quota.exceeded());
    }
  }

  @Test
  public void consume_whenConsumedIsFalse_thenQuotaExceededIsTrue() {
    // Arrange
    final var mockRefillPeriod = mock(Duration.class);
    final var mockLocalBucket = mock(LocalBucket.class);
    final var mockLocalBucketBuilder = mock(LocalBucketBuilder.class);
    final var mockLimitLambda = mock(Function.class);
    final var mockConsumptionProbe = mock(ConsumptionProbe.class);

    when(mockRefillPeriod.toSeconds()).thenReturn(3L);
    when(mockLocalBucket.tryConsumeAndReturnRemaining(1)).thenReturn(mockConsumptionProbe);
    when(mockConsumptionProbe.getRemainingTokens()).thenReturn(999L);
    when(mockConsumptionProbe.getNanosToWaitForReset()).thenReturn(123_000_000_000L);
    when(mockConsumptionProbe.isConsumed()).thenReturn(false);

    try (final var staticBucket = mockStatic(Bucket.class);
        final var staticRateLimitProfile = mockStatic(RateLimitProfile.class);
        final var staticDuration = mockStatic(Duration.class)) {
      staticBucket.when(Bucket::builder).thenReturn(mockLocalBucketBuilder);

      staticRateLimitProfile
          .when(() -> RateLimitProfile.createLimitLambda(1, 2, mockRefillPeriod))
          .thenReturn(mockLimitLambda);
      staticRateLimitProfile
          .when(() -> RateLimitProfile.rateLimitPolicy("example-profileName", 2, 3))
          .thenReturn("example-rateLimitPolicy");
      staticRateLimitProfile
          .when(() -> RateLimitProfile.rateLimit("example-profileName", 999, 123))
          .thenReturn("example-rateLimit");

      when(mockLocalBucketBuilder.addLimit(mockLimitLambda)).thenReturn(mockLocalBucketBuilder);
      when(mockLocalBucketBuilder.build()).thenReturn(mockLocalBucket);

      staticDuration.when(() -> Duration.ofSeconds(3L)).thenReturn(mockRefillPeriod);

      // Act
      final var rateLimitProfile =
          new RateLimitProfile(new HashMap<>(), "example-profileName", 1, 2, 3);

      final var quota = rateLimitProfile.consume("example-profileName");

      // Assert
      assertEquals("example-rateLimitPolicy", quota.rateLimitPolicy());
      assertEquals("example-rateLimit", quota.rateLimit());
      assertTrue(quota.exceeded());
    }
  }
}
