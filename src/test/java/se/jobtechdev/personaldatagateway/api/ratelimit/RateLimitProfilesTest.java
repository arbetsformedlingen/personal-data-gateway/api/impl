package se.jobtechdev.personaldatagateway.api.ratelimit;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mockConstruction;

import java.util.HashMap;
import java.util.List;
import org.junit.jupiter.api.Test;

public class RateLimitProfilesTest {
  @Test
  public void getRateLimitProfile() {
    // Arrange
    final var mockConsumerBucketSize = 1;
    final var mockConsumerQuota = 2;
    final var mockConsumerWindow = 3L;
    final var mockFrontendBucketSize = 4;
    final var mockFrontendQuota = 5;
    final var mockFrontendWindow = 6L;
    final var mockAdminBucketSize = 7;
    final var mockAdminQuota = 8;
    final var mockAdminWindow = 9L;
    final var mockDefaultBucketSize = 10;
    final var mockDefaultQuota = 11;
    final var mockDefaultWindow = 12L;

    final var constructionRateLimitProfileArgs = new HashMap<RateLimitProfile, List<?>>();

    try (final var constructionRateLimitProfile =
        mockConstruction(
            RateLimitProfile.class,
            (mock, ctx) -> {
              constructionRateLimitProfileArgs.put(mock, ctx.arguments());
            })) {

      // Act
      final var rateLimitProfiles =
          new RateLimitProfiles(
              mockConsumerBucketSize,
              mockConsumerQuota,
              (int) mockConsumerWindow,
              mockFrontendBucketSize,
              mockFrontendQuota,
              (int) mockFrontendWindow,
              mockAdminBucketSize,
              mockAdminQuota,
              (int) mockAdminWindow,
              mockDefaultBucketSize,
              mockDefaultQuota,
              (int) mockDefaultWindow);

      final var consumerRateLimitProfile = rateLimitProfiles.getRateLimitProfile("consumer");
      final var frontendRateLimitProfile = rateLimitProfiles.getRateLimitProfile("frontend");
      final var adminRateLimitProfile = rateLimitProfiles.getRateLimitProfile("admin");
      final var defaultRateLimitProfile = rateLimitProfiles.getRateLimitProfile("default");
      final var unknownRateLimitProfile = rateLimitProfiles.getRateLimitProfile("unknown");

      assertEquals(4, constructionRateLimitProfile.constructed().size());

      final var mockConsumerRateLimitProfile = constructionRateLimitProfile.constructed().get(0);
      final var mockFrontendRateLimitProfile = constructionRateLimitProfile.constructed().get(1);
      final var mockAdminRateLimitProfile = constructionRateLimitProfile.constructed().get(2);
      final var mockDefaultRateLimitProfile = constructionRateLimitProfile.constructed().get(3);

      assertEquals(
          "consumer", constructionRateLimitProfileArgs.get(mockConsumerRateLimitProfile).get(1));

      assertEquals(
          mockConsumerBucketSize,
          constructionRateLimitProfileArgs.get(mockConsumerRateLimitProfile).get(2));

      assertEquals(
          mockConsumerQuota,
          constructionRateLimitProfileArgs.get(mockConsumerRateLimitProfile).get(3));

      assertEquals(
          mockConsumerWindow,
          constructionRateLimitProfileArgs.get(mockConsumerRateLimitProfile).get(4));

      assertEquals(
          "frontend", constructionRateLimitProfileArgs.get(mockFrontendRateLimitProfile).get(1));

      assertEquals(
          mockFrontendBucketSize,
          constructionRateLimitProfileArgs.get(mockFrontendRateLimitProfile).get(2));

      assertEquals(
          mockFrontendQuota,
          constructionRateLimitProfileArgs.get(mockFrontendRateLimitProfile).get(3));

      assertEquals(
          mockFrontendWindow,
          constructionRateLimitProfileArgs.get(mockFrontendRateLimitProfile).get(4));

      assertEquals("admin", constructionRateLimitProfileArgs.get(mockAdminRateLimitProfile).get(1));

      assertEquals(
          mockAdminBucketSize,
          constructionRateLimitProfileArgs.get(mockAdminRateLimitProfile).get(2));

      assertEquals(
          mockAdminQuota, constructionRateLimitProfileArgs.get(mockAdminRateLimitProfile).get(3));

      assertEquals(
          mockAdminWindow, constructionRateLimitProfileArgs.get(mockAdminRateLimitProfile).get(4));

      assertEquals(
          "default", constructionRateLimitProfileArgs.get(mockDefaultRateLimitProfile).get(1));

      assertEquals(
          mockDefaultBucketSize,
          constructionRateLimitProfileArgs.get(mockDefaultRateLimitProfile).get(2));

      assertEquals(
          mockDefaultQuota,
          constructionRateLimitProfileArgs.get(mockDefaultRateLimitProfile).get(3));

      assertEquals(
          mockDefaultWindow,
          constructionRateLimitProfileArgs.get(mockDefaultRateLimitProfile).get(4));

      assertEquals(constructionRateLimitProfile.constructed().get(0), consumerRateLimitProfile);
      assertEquals(constructionRateLimitProfile.constructed().get(1), frontendRateLimitProfile);
      assertEquals(constructionRateLimitProfile.constructed().get(2), adminRateLimitProfile);
      assertEquals(constructionRateLimitProfile.constructed().get(3), defaultRateLimitProfile);
      assertNull(unknownRateLimitProfile);
    }
  }
}
