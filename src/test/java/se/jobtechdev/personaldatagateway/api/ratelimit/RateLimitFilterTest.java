package se.jobtechdev.personaldatagateway.api.ratelimit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextImpl;

public class RateLimitFilterTest {

  @Test
  public void extractProfileNameFromRequest_whenHttpSessionIsNull_returnDefault() {
    // Arrange
    final var mockHttpServletRequest = mock(HttpServletRequest.class);

    when(mockHttpServletRequest.getSession(false)).thenReturn(null);
    when(mockHttpServletRequest.getRemoteAddr()).thenReturn("example-request-remoteAddress");

    // Act
    final var profileAndKey =
        RateLimitFilter.extractProfileAndKeyFromRequest(mockHttpServletRequest);
    assertEquals("default", profileAndKey.profile());
    assertEquals("example-request-remoteAddress", profileAndKey.key());
  }

  @Test
  public void
      extractProfileNameFromRequest_whenHttpSessionIsNotNullAndNoAuthorityIsFound_returnDefault() {
    // Arrange
    final var mockHttpServletRequest = mock(HttpServletRequest.class);
    final var mockHttpSession = mock(HttpSession.class);
    final var mockSecurityContextImpl = mock(SecurityContextImpl.class);
    final var mockAuthentication = mock(Authentication.class);

    when(mockHttpServletRequest.getSession(false)).thenReturn(mockHttpSession);
    when(mockHttpServletRequest.getRemoteAddr()).thenReturn("example-request-remoteAddress");
    when(mockHttpSession.getAttribute("SPRING_SECURITY_CONTEXT"))
        .thenReturn(mockSecurityContextImpl);
    when(mockSecurityContextImpl.getAuthentication()).thenReturn(mockAuthentication);
    when(mockAuthentication.getAuthorities()).thenReturn(List.of());

    // Act
    final var profileAndKey =
        RateLimitFilter.extractProfileAndKeyFromRequest(mockHttpServletRequest);
    assertEquals("default", profileAndKey.profile());
    assertEquals("example-request-remoteAddress", profileAndKey.key());
  }

  @Test
  public void
      extractProfileNameFromRequest_whenHttpSessionIsNotNullAndAuthorityIsFound_returnAuthority() {
    // Arrange
    final var mockHttpServletRequest = mock(HttpServletRequest.class);
    final var mockHttpSession = mock(HttpSession.class);
    final var mockSecurityContextImpl = mock(SecurityContextImpl.class);
    final var mockAuthentication = mock(Authentication.class);
    final var mockGrantedAuthority = mock(GrantedAuthority.class);

    when(mockHttpServletRequest.getSession(false)).thenReturn(mockHttpSession);
    when(mockHttpServletRequest.getRemoteAddr()).thenReturn("example-request-remoteAddress");
    when(mockHttpSession.getAttribute("SPRING_SECURITY_CONTEXT"))
        .thenReturn(mockSecurityContextImpl);
    when(mockSecurityContextImpl.getAuthentication()).thenReturn(mockAuthentication);
    when(mockAuthentication.getAuthorities())
        .thenReturn((Collection) List.of(mockGrantedAuthority));
    when(mockAuthentication.getPrincipal()).thenReturn("example-client-id");
    when(mockGrantedAuthority.getAuthority()).thenReturn("example-authority");

    // Act
    final var profileAndKey =
        RateLimitFilter.extractProfileAndKeyFromRequest(mockHttpServletRequest);
    assertEquals("example-authority", profileAndKey.profile());
    assertEquals("example-client-id", profileAndKey.key());
  }

  @Test
  public void doFilter_whenQuotaIsExceeded_thenRespondWithStatusCodeTooManyRequests()
      throws ServletException, IOException {
    // Arrange
    final var mockRateLimitProfiles = mock(RateLimitProfiles.class);
    final var mockHttpServletRequest = mock(HttpServletRequest.class);
    final var mockHttpServletResponse = mock(HttpServletResponse.class);
    final var mockFilterChain = mock(FilterChain.class);
    final var mockRateLimitProfile = mock(RateLimitProfile.class);
    final var mockQuota = mock(RateLimitProfile.Quota.class);
    final var mockProfileAndKey = mock(RateLimitFilter.ProfileAndKey.class);

    when(mockHttpServletRequest.getRemoteAddr()).thenReturn("example-request-remoteAddress");
    when(mockRateLimitProfiles.getRateLimitProfile("example-profile"))
        .thenReturn(mockRateLimitProfile);
    when(mockRateLimitProfile.consume("example-key")).thenReturn(mockQuota);
    when(mockQuota.rateLimitPolicy()).thenReturn("example-quota-rateLimitPolicy");
    when(mockQuota.rateLimit()).thenReturn("example-quota-rateLimit");
    when(mockQuota.exceeded()).thenReturn(true);
    when(mockProfileAndKey.profile()).thenReturn("example-profile");
    when(mockProfileAndKey.key()).thenReturn("example-key");

    try (final var mockPrintWriter = mock(PrintWriter.class);
        final var staticRateLimitFilter = mockStatic(RateLimitFilter.class)) {

      when(mockHttpServletResponse.getWriter()).thenReturn(mockPrintWriter);

      staticRateLimitFilter
          .when(() -> RateLimitFilter.extractProfileAndKeyFromRequest(mockHttpServletRequest))
          .thenReturn(mockProfileAndKey);

      // Act
      final var rateLimitFilter = new RateLimitFilter(mockRateLimitProfiles);
      rateLimitFilter.doFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

      // Assert
      verify(mockHttpServletResponse, times(1))
          .setHeader("RateLimit-Policy", "example-quota-rateLimitPolicy");
      verify(mockHttpServletResponse, times(1)).setHeader("RateLimit", "example-quota-rateLimit");
      verify(mockHttpServletResponse, times(1)).setStatus(HttpStatus.TOO_MANY_REQUESTS.value());
      verify(mockHttpServletResponse, times(1)).setContentType("text/plain");
      verify(mockPrintWriter, times(1)).append(HttpStatus.TOO_MANY_REQUESTS.getReasonPhrase());
    }
  }

  @Test
  public void doFilter_whenQuotaIsNotExceeded_thenInvokeFilterChain()
      throws ServletException, IOException {
    // Arrange
    final var mockRateLimitProfiles = mock(RateLimitProfiles.class);
    final var mockHttpServletRequest = mock(HttpServletRequest.class);
    final var mockHttpServletResponse = mock(HttpServletResponse.class);
    final var mockFilterChain = mock(FilterChain.class);
    final var mockRateLimitProfile = mock(RateLimitProfile.class);
    final var mockQuota = mock(RateLimitProfile.Quota.class);
    final var mockProfileAndKey = mock(RateLimitFilter.ProfileAndKey.class);

    when(mockHttpServletRequest.getRemoteAddr()).thenReturn("example-request-remoteAddress");
    when(mockRateLimitProfiles.getRateLimitProfile("example-profile"))
        .thenReturn(mockRateLimitProfile);
    when(mockRateLimitProfile.consume("example-key")).thenReturn(mockQuota);
    when(mockQuota.rateLimitPolicy()).thenReturn("example-quota-rateLimitPolicy");
    when(mockQuota.rateLimit()).thenReturn("example-quota-rateLimit");
    when(mockQuota.exceeded()).thenReturn(false);
    when(mockProfileAndKey.profile()).thenReturn("example-profile");
    when(mockProfileAndKey.key()).thenReturn("example-key");

    try (final var mockPrintWriter = mock(PrintWriter.class);
        final var staticRateLimitFilter = mockStatic(RateLimitFilter.class)) {

      when(mockHttpServletResponse.getWriter()).thenReturn(mockPrintWriter);

      staticRateLimitFilter
          .when(() -> RateLimitFilter.extractProfileAndKeyFromRequest(mockHttpServletRequest))
          .thenReturn(mockProfileAndKey);

      // Act
      final var rateLimitFilter = new RateLimitFilter(mockRateLimitProfiles);
      rateLimitFilter.doFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);

      // Assert
      verify(mockHttpServletResponse, times(1))
          .setHeader("RateLimit-Policy", "example-quota-rateLimitPolicy");
      verify(mockHttpServletResponse, times(1)).setHeader("RateLimit", "example-quota-rateLimit");
      verify(mockFilterChain, times(1)).doFilter(mockHttpServletRequest, mockHttpServletResponse);
    }
  }
}
