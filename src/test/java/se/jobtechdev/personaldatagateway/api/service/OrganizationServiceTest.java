package se.jobtechdev.personaldatagateway.api.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import java.time.ZonedDateTime;
import java.util.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import se.jobtechdev.personaldatagateway.api.exception.ApiException;
import se.jobtechdev.personaldatagateway.api.generated.entities.Organization;
import se.jobtechdev.personaldatagateway.api.generated.model.ErrorResponse;
import se.jobtechdev.personaldatagateway.api.repository.OrganizationRepository;
import se.jobtechdev.personaldatagateway.api.util.ApiExceptionFactory;
import se.jobtechdev.personaldatagateway.api.util.ErrorResponseFactory;
import se.jobtechdev.personaldatagateway.api.util.TimeProvider;
import se.jobtechdev.personaldatagateway.api.util.UuidProvider;

public class OrganizationServiceTest {
  @InjectMocks private OrganizationService organizationService;

  @Mock private OrganizationRepository organizationRepository;

  private AutoCloseable closeable;

  @BeforeEach
  void setUp() {
    closeable = MockitoAnnotations.openMocks(this);
  }

  @AfterEach
  void closeService() throws Exception {
    closeable.close();
  }

  @Test
  public void
      createOrganization_whenCreatingOrganizationWithOrgNrAlreadyInDatabase_thenThrowApiException() {
    try (final var uuidProvider = mockStatic(UuidProvider.class);
        final var timeProvider = mockStatic(TimeProvider.class);
        final var apiExceptionFactory = mockStatic(ApiExceptionFactory.class);
        final var errorResponseFactory = mockStatic(ErrorResponseFactory.class)) {
      // Arrange
      final var uuid = mock(UUID.class);
      when(uuid.toString()).thenReturn("organization-id");
      uuidProvider.when(UuidProvider::uuid).thenReturn(uuid);

      final var created = mock(ZonedDateTime.class);
      timeProvider.when(TimeProvider::now).thenReturn(created);
      when(created.toString()).thenReturn("created");

      final var name = "organization-name";
      final var orgNr = "organization-number";

      final var dataIntegrityViolationException = mock(DataIntegrityViolationException.class);
      when(dataIntegrityViolationException.getMessage()).thenReturn("message");
      final var errorResponse = mock(ErrorResponse.class);
      errorResponseFactory
          .when(() -> ErrorResponseFactory.createErrorResponse(HttpStatus.CONFLICT, "message"))
          .thenReturn(errorResponse);
      final var apiException = mock(ApiException.class);
      apiExceptionFactory
          .when(() -> ApiExceptionFactory.createApiException(errorResponse))
          .thenReturn(apiException);
      when(organizationRepository.save(any(Organization.class)))
          .thenThrow(dataIntegrityViolationException);

      // Act, Assert
      final var thrown =
          assertThrows(
              ApiException.class, () -> organizationService.createOrganization(orgNr, name));

      assertEquals(apiException, thrown);
    }
  }

  @Test
  public void createOrganization() {
    final var organizationArgs = new HashMap<Organization, List<?>>();
    try (MockedStatic<UuidProvider> uuidProvider = mockStatic(UuidProvider.class);
        MockedStatic<TimeProvider> timeProvider = mockStatic(TimeProvider.class);
        final var datatypeMockedConstruction =
            mockConstruction(
                Organization.class,
                (mock, context) -> organizationArgs.put(mock, context.arguments()))) {
      // Arrange
      final var uuid = mock(UUID.class);
      when(uuid.toString()).thenReturn("organization-id");
      uuidProvider.when(UuidProvider::uuid).thenReturn(uuid);

      final var created = mock(ZonedDateTime.class);
      timeProvider.when(TimeProvider::now).thenReturn(created);
      when(created.toString()).thenReturn("created");

      final var name = "organization-name";
      final var orgNr = "organization-number";

      final var savedOrganization = mock(Organization.class);
      when(organizationRepository.save(any(Organization.class))).thenReturn(savedOrganization);

      // Act
      final var result = organizationService.createOrganization(orgNr, name);

      // Assert
      assertEquals(1, datatypeMockedConstruction.constructed().size());
      final var organization = datatypeMockedConstruction.constructed().get(0);
      final var datArgs = organizationArgs.get(organization);
      assertEquals(uuid, datArgs.get(0));
      assertEquals(orgNr, datArgs.get(1));
      assertEquals(name, datArgs.get(2));
      assertEquals(created, datArgs.get(3));
      verify(organizationRepository, times(1)).save(any());
    }
  }

  @Test
  public void getOrganizations() {
    // Arrange
    final var organizations = mock(List.class);
    when(organizationRepository.findAll()).thenReturn(organizations);

    // Act
    final var result = organizationService.getOrganizations();

    // Assert
    assertEquals(organizations, result);
    verify(organizationRepository, times(1)).findAll();
  }

  @Test
  public void getOrganizationById() {
    // Arrange
    final var organizationId = mock(UUID.class);
    final var organization = mock(Organization.class);
    final var optionalOrganization = Optional.of(organization);
    when(organizationRepository.findById(organizationId)).thenReturn(optionalOrganization);

    // Act
    final var result = organizationService.getOrganizationById(organizationId);

    // Assert
    assertEquals(optionalOrganization, result);
    verify(organizationRepository, times(1)).findById(organizationId);
  }

  @Test
  public void getOrganizationByOrgNr() {
    // Arrange
    final var orgNr = "orgNr";
    final var organization = mock(Organization.class);
    final var optionalOrganization = Optional.of(organization);
    when(organizationRepository.findByOrgNr(orgNr)).thenReturn(optionalOrganization);

    // Act
    final var result = organizationService.getOrganizationByOrgNr(orgNr);

    // Assert
    assertEquals(optionalOrganization, result);
    verify(organizationRepository, times(1)).findByOrgNr(orgNr);
  }

  @Test
  public void update() {
    // Arrange
    final var organization = mock(Organization.class);
    when(organizationRepository.save(organization)).thenReturn(organization);

    // Act
    final var result = organizationService.update(organization);

    // Assert
    assertEquals(organization, result);
    verify(organizationRepository, times(1)).save(organization);
  }

  @Test
  public void save() {
    // Arrange
    final var organization = mock(Organization.class);

    // Act
    organizationService.save(organization);

    // Assert
    verify(organizationRepository, times(1)).save(organization);
  }
}
