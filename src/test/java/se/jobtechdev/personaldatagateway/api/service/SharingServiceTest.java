package se.jobtechdev.personaldatagateway.api.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import se.jobtechdev.personaldatagateway.api.generated.entities.*;
import se.jobtechdev.personaldatagateway.api.repository.SharingAccessRepository;
import se.jobtechdev.personaldatagateway.api.repository.SharingRepository;
import se.jobtechdev.personaldatagateway.api.util.TimeProvider;
import se.jobtechdev.personaldatagateway.api.util.UuidProvider;

import java.time.ZonedDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class SharingServiceTest {
  @InjectMocks
  private SharingService sharingService;

  @Mock
  private SharingRepository sharingRepository;

  @Mock
  private SharingAccessRepository sharingAccessRepository;

  private AutoCloseable closeable;

  @BeforeEach
  void setUp() {
    closeable = MockitoAnnotations.openMocks(this);
  }

  @AfterEach
  void closeService() throws Exception {
    closeable.close();
  }

  @Test
  void getSharingById() {
    // Arrange
    final var optional = mock(Optional.class);
    final var sharingId = mock(UUID.class);
    when(sharingRepository.findById(sharingId)).thenReturn(optional);

    // Act
    final var result = sharingService.getSharingById(sharingId);

    // Assert
    assertEquals(optional, result);
  }

  @Test
  void updateSharing() {
    // Arrange
    final var sharing = mock(Sharing.class);
    when(sharingRepository.save(sharing)).thenReturn(sharing);

    // Act
    final var result = sharingService.update(sharing);

    // Assert
    assertEquals(sharing, result);
    verify(sharingRepository, times(1)).save(sharing);
  }

  @Test
  void createSharing_whenPublicIsFalse_thenCreateSharingForSpecificClient() {
    final var sharingArgs = new HashMap<Sharing, List<?>>();
    try (final var timeProvider = mockStatic(TimeProvider.class);
         final var uuidProvider = mockStatic(UuidProvider.class);
         final var sharingConstruction =
             mockConstruction(
                 Sharing.class, (mock, context) -> sharingArgs.put(mock, context.arguments()))) {
      // Arrange
      final var sharingId = mock(UUID.class);
      final var person = mock(Person.class);
      final var client = mock(Client.class);
      final var datasource = mock(Datasource.class);
      final var created = mock(ZonedDateTime.class);

      final var savedSharing = mock(Sharing.class);
      when(sharingRepository.save(any(Sharing.class))).thenReturn(savedSharing);
      uuidProvider.when(UuidProvider::uuid).thenReturn(sharingId);
      timeProvider.when(TimeProvider::now).thenReturn(created);
      // Act
      sharingService.createSharing(person, datasource, false, null, client);

      // Assert
      assertEquals(1, sharingConstruction.constructed().size());
      final var sharing = sharingConstruction.constructed().get(0);
      assertEquals(9, sharingArgs.get(sharing).size());
      assertEquals(sharingId, sharingArgs.get(sharing).get(0));
      assertEquals(client, sharingArgs.get(sharing).get(1));
      assertEquals(datasource, sharingArgs.get(sharing).get(2));
      assertEquals(person, sharingArgs.get(sharing).get(3));
      assertEquals(created, sharingArgs.get(sharing).get(4));
      assertNull(sharingArgs.get(sharing).get(5));
      assertNull(sharingArgs.get(sharing).get(6));
      assertEquals(created, sharingArgs.get(sharing).get(7));
      assertEquals(Set.of(), sharingArgs.get(sharing).get(8));
    }
  }

  @Test
  void createSharing_whenPublicIsTrue_thenCreateSharingWithNoReferencedClient() {
    final var sharingArgs = new HashMap<Sharing, List<?>>();
    try (final var timeProvider = mockStatic(TimeProvider.class);
         final var uuidProvider = mockStatic(UuidProvider.class);
         final var sharingConstruction =
             mockConstruction(
                 Sharing.class, (mock, context) -> sharingArgs.put(mock, context.arguments()))) {
      // Arrange
      final var sharingId = mock(UUID.class);
      final var person = mock(Person.class);
      final var datasource = mock(Datasource.class);
      final var created = mock(ZonedDateTime.class);

      final var savedSharing = mock(Sharing.class);
      when(sharingRepository.save(any(Sharing.class))).thenReturn(savedSharing);
      uuidProvider.when(UuidProvider::uuid).thenReturn(sharingId);
      timeProvider.when(TimeProvider::now).thenReturn(created);
      // Act
      sharingService.createSharing(person, datasource, true, null, null);

      // Assert
      assertEquals(1, sharingConstruction.constructed().size());
      final var sharing = sharingConstruction.constructed().get(0);
      assertEquals(sharingId, sharingArgs.get(sharing).get(0));
      assertNull(sharingArgs.get(sharing).get(1));
      assertEquals(datasource, sharingArgs.get(sharing).get(2));
      assertEquals(person, sharingArgs.get(sharing).get(3));
      assertEquals(created, sharingArgs.get(sharing).get(4));
      assertNull(sharingArgs.get(sharing).get(5));
      assertNull(sharingArgs.get(sharing).get(6));
    }
  }

  @Test
  public void createSharing_whenPublicIsFalseAndClientIsNull_ThrowAssertionError() {
    // Act, Assert
    assertThrows(
        AssertionError.class, () -> sharingService.createSharing(null, null, false, null, null));
  }

  @Test
  public void createSharing_whenPublicIsTrueAndClientIsDefined_ThrowAssertionError() {
    // Arrange
    final var client = mock(Client.class);

    // Act, Assert
    assertThrows(
        AssertionError.class, () -> sharingService.createSharing(null, null, true, null, client));
  }

  @Test
  public void getAllSharingAccessesBySharing() {
    // Arrange
    final var mockSharing = mock(Sharing.class);
    final var mockSharingAccess = mock(SharingAccess.class);

    when(mockSharing.getSharingAccesses()).thenReturn(Set.of(mockSharingAccess));

    // Act
    final var sharingAccesses = sharingService.getAllSharingAccessesBySharing(mockSharing);

    // Assert
    assertEquals(Set.of(mockSharingAccess), sharingAccesses);
  }

  @Test
  public void getSharingAccessById() {
    // Arrange
    final var mockSharingAccessId = mock(UUID.class);
    final var mockSharingAccess = mock(SharingAccess.class);

    when(sharingAccessRepository.findById(mockSharingAccessId))
        .thenReturn(Optional.of(mockSharingAccess));

    // Act
    final var sharingAccess = sharingService.getSharingAccessById(mockSharingAccessId);

    // Assert
    assertEquals(Optional.of(mockSharingAccess), sharingAccess);
  }

  @Test
  public void createSharingAccess() {
    // Arrange
    final var mockSharing = mock(Sharing.class);
    final var mockSharingAccess = mock(SharingAccess.class);
    final var sharingAccessId = mock(UUID.class);

    final var sharingAccessArgs = new HashMap<SharingAccess, List<?>>();
    try (MockedStatic<UuidProvider> uuidProvider = mockStatic(UuidProvider.class);
         MockedStatic<TimeProvider> timeProvider = mockStatic(TimeProvider.class);
         final var sharingAccessMockedConstruction =
             mockConstruction(
                 SharingAccess.class,
                 (mock, context) -> sharingAccessArgs.put(mock, context.arguments()))) {

      when(sharingAccessId.toString()).thenReturn("sharingAccessId");
      uuidProvider.when(UuidProvider::uuid).thenReturn(sharingAccessId);

      final var now = mock(ZonedDateTime.class);
      timeProvider.when(TimeProvider::now).thenReturn(now);
      when(now.toString()).thenReturn("now");

      when(sharingAccessRepository.save(any())).thenReturn(mockSharingAccess);

      // Act
      final var result = sharingService.createSharingAccess(mockSharing);

      // Assert
      assertEquals(1, sharingAccessMockedConstruction.constructed().size());
      final var sharingAccess = sharingAccessMockedConstruction.constructed().get(0);
      final var sharingAccessConstructionArgs = sharingAccessArgs.get(sharingAccess);
      assertEquals(sharingAccessId, sharingAccessConstructionArgs.get(0));
      assertEquals(mockSharing, sharingAccessConstructionArgs.get(1));
      assertEquals(now, sharingAccessConstructionArgs.get(2));
      assertEquals(now, sharingAccessConstructionArgs.get(3));
      assertEquals(now, sharingAccessConstructionArgs.get(4));
      verify(sharingAccessRepository, times(1)).save(sharingAccess);
      assertEquals(Optional.of(mockSharingAccess), result);
    }
  }

  @Test
  public void getActiveSharing() {
    // Arrange
    final var mockUser = mock(Person.class);
    final var mockUserId = "mock-person-id";
    final var mockDatasource = mock(Datasource.class);
    final var mockDatasourceId = mock(UUID.class);
    final var mockClient = mock(Client.class);
    final var mockClientId = mock(UUID.class);
    final var mockSharing = mock(Sharing.class);

    when(mockUser.getId()).thenReturn(mockUserId);
    when(mockDatasource.getId()).thenReturn(mockDatasourceId);
    when(mockClient.getId()).thenReturn(mockClientId);

    when(sharingRepository.findAllActiveSharings(mockUserId, mockDatasourceId, mockClientId))
        .thenReturn(List.of(mockSharing));

    // Act
    final var activeSharings =
        sharingService.getActiveSharings(mockUser, mockDatasource, mockClient);

    final var activeSharing = activeSharings.getFirst();

    // Assert
    verify(sharingRepository, times(1))
        .findAllActiveSharings(mockUserId, mockDatasourceId, mockClientId);

    assertEquals(mockSharing, activeSharing);
  }
}
