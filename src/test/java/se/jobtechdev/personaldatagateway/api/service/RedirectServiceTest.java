package se.jobtechdev.personaldatagateway.api.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import java.net.URL;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import se.jobtechdev.personaldatagateway.api.generated.entities.Client;
import se.jobtechdev.personaldatagateway.api.generated.entities.Redirect;
import se.jobtechdev.personaldatagateway.api.repository.RedirectRepository;
import se.jobtechdev.personaldatagateway.api.util.TimeProvider;
import se.jobtechdev.personaldatagateway.api.util.UuidProvider;

public class RedirectServiceTest {
  @InjectMocks private RedirectService redirectService;

  @Mock private RedirectRepository redirectRepository;

  private AutoCloseable closeable;

  @BeforeEach
  void setUp() {
    closeable = MockitoAnnotations.openMocks(this);
  }

  @AfterEach
  void closeService() throws Exception {
    closeable.close();
  }

  @Test
  public void createRedirect() {
    // Arrange
    final var url = mock(URL.class);
    final var client = mock(Client.class);
    final var redirectConstructionArgs = new HashMap<Redirect, List<?>>();
    try (final var uuidProvider = mockStatic(UuidProvider.class);
        final var timeProvider = mockStatic(TimeProvider.class);
        final var redirectConstruction =
            mockConstruction(
                Redirect.class,
                (mock, ctx) -> {
                  redirectConstructionArgs.put(mock, ctx.arguments());
                })) {
      final var redirectId = mock(UUID.class);
      uuidProvider.when(UuidProvider::uuid).thenReturn(redirectId);
      final var now = mock(ZonedDateTime.class);
      timeProvider.when(TimeProvider::now).thenReturn(now);

      final var savedRedirect = mock(Redirect.class);
      when(redirectRepository.save(any(Redirect.class))).thenReturn(savedRedirect);

      // Act
      final var redirect = redirectService.createRedirect(url, client);

      // Assert
      uuidProvider.verify(UuidProvider::uuid, times(1));
      timeProvider.verify(TimeProvider::now, times(1));

      assertEquals(1, redirectConstruction.constructed().size());
      final var redirectMock = redirectConstruction.constructed().get(0);
      verify(redirectRepository, times(1)).save(redirectMock);
      assertEquals(savedRedirect, redirect);
      assertEquals(redirectId, redirectConstructionArgs.get(redirectMock).get(0));
      assertEquals(client, redirectConstructionArgs.get(redirectMock).get(1));
      assertEquals(url, redirectConstructionArgs.get(redirectMock).get(2));
      assertEquals(now, redirectConstructionArgs.get(redirectMock).get(3));
      assertEquals(now, redirectConstructionArgs.get(redirectMock).get(4));
    }
  }

  @Test
  public void getRedirects() {
    // Arrange
    final var clientId = mock(UUID.class);
    final var redirects = mock(List.class);
    when(redirectRepository.findByClientId(clientId)).thenReturn(redirects);

    // Act
    final var result = redirectService.getRedirects(clientId);

    // Assert
    assertEquals(redirects, result);
    verify(redirectRepository, times(1)).findByClientId(clientId);
  }

  @Test
  public void getRedirectById() {
    // Arrange
    final var redirectId = mock(UUID.class);
    final var optionalRedirect = mock(Optional.class);
    when(redirectRepository.findById(redirectId)).thenReturn(optionalRedirect);

    // Act
    final var result = redirectService.getRedirectById(redirectId);

    // Assert
    assertEquals(optionalRedirect, result);
    verify(redirectRepository, times(1)).findById(redirectId);
  }

  @Test
  public void update() {
    // Arrange
    try (final var timeProvider = mockStatic(TimeProvider.class)) {
      final var updated = mock(ZonedDateTime.class);
      timeProvider.when(TimeProvider::now).thenReturn(updated);

      final var redirect = mock(Redirect.class);
      when(redirectRepository.save(redirect)).thenReturn(redirect);

      // Act
      final var result = redirectService.update(redirect);

      // Asser
      assertEquals(redirect, result);
      verify(redirect, times(1)).setUpdated(updated);
    }
  }
}
