package se.jobtechdev.personaldatagateway.api.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import java.time.ZonedDateTime;
import java.util.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import se.jobtechdev.personaldatagateway.api.generated.entities.Faq;
import se.jobtechdev.personaldatagateway.api.generated.entities.Organization;
import se.jobtechdev.personaldatagateway.api.repository.FaqRepository;
import se.jobtechdev.personaldatagateway.api.util.TimeProvider;
import se.jobtechdev.personaldatagateway.api.util.UuidProvider;

public class FaqServiceTest {
  @InjectMocks private FaqService faqService;

  @Mock private FaqRepository faqRepository;

  private AutoCloseable closeable;

  @BeforeEach
  void setUp() {
    closeable = MockitoAnnotations.openMocks(this);
  }

  @AfterEach
  void closeService() throws Exception {
    closeable.close();
  }

  @Test
  public void getFaqById() {
    // Arrange
    final var faqId = mock(UUID.class);
    final var optionalFaq = mock(Optional.class);
    when(faqRepository.findById(faqId)).thenReturn(optionalFaq);

    // Act
    final var result = faqService.getFaqById(faqId);

    // Assert
    assertEquals(optionalFaq, result);
    verify(faqRepository, times(1)).findById(faqId);
  }

  @Test
  public void update() {
    // Arrange
    try (final var timeProvider = mockStatic(TimeProvider.class)) {
      final var updated = mock(ZonedDateTime.class);
      timeProvider.when(TimeProvider::now).thenReturn(updated);

      final var faq = mock(Faq.class);
      when(faqRepository.save(faq)).thenReturn(faq);

      // Act
      final var result = faqService.update(faq);

      // Asser
      assertEquals(faq, result);
      verify(faq, times(1)).setUpdated(updated);
    }
  }

  @Test
  public void createFaq() {
    final var faqArgs = new HashMap<Faq, List<?>>();
    final var faqs = new ArrayList<Faq>();
    try (final var timeProvider = mockStatic(TimeProvider.class);
        final var uuidProvider = mockStatic(UuidProvider.class);
        final var faqConstruction =
            mockConstruction(
                Faq.class,
                (mock, context) -> {
                  faqs.add(mock);
                  faqArgs.put(mock, context.arguments());
                  when(faqRepository.save(mock)).thenReturn(mock);
                })) {
      // Arrange
      final var faqId = mock(UUID.class);
      uuidProvider.when(UuidProvider::uuid).thenReturn(faqId);
      final var now = mock(ZonedDateTime.class);
      timeProvider.when(TimeProvider::now).thenReturn(now);
      final var lang = "sv";
      final var title = "title";
      final var content = "content";

      // Act
      final var result = faqService.createFaq(lang, title, content);

      // Assert
      assertEquals(1, faqConstruction.constructed().size());
      final var faq = faqConstruction.constructed().get(0);
      assertEquals(result, faq);
      assertEquals(faqId, faqArgs.get(faq).get(0));
      assertEquals(lang, faqArgs.get(faq).get(1));
      assertEquals(title, faqArgs.get(faq).get(2));
      assertEquals(content, faqArgs.get(faq).get(3));
      assertEquals(now, faqArgs.get(faq).get(4));
      assertEquals(now, faqArgs.get(faq).get(5));
    }
  }

  @Test
  public void getFaqs_when_acceptedLanguage_is_null() {
    // Arrange
    final var organization = mock(Organization.class);
    final var faq = mock(Faq.class);
    when(organization.getFaqs()).thenReturn(Set.of(faq));

    // Act
    final var result = faqService.getFaqs((FaqEntity) organization, null);

    // Assert
    assertEquals(Set.of(faq), result);
  }

  @Test
  public void getFaqs_when_acceptedLanguage_is_star() {
    // Arrange
    final var organization = mock(Organization.class);
    final var faq = mock(Faq.class);
    when(organization.getFaqs()).thenReturn(Set.of(faq));

    // Act
    final var result = faqService.getFaqs((FaqEntity) organization, "*");

    // Assert
    assertEquals(Set.of(faq), result);
  }

  @Test
  public void getFaqs_when_acceptedLanguage_is_defined() {
    // Arrange
    final var organization = mock(Organization.class);
    final var faq1 = mock(Faq.class);
    final var faq2 = mock(Faq.class);
    when(faq1.getLang()).thenReturn("not_accepted");
    when(faq2.getLang()).thenReturn("accepted");
    when(organization.getFaqs()).thenReturn(Set.of(faq1, faq2));

    // Act
    final var result = faqService.getFaqs((FaqEntity) organization, "accepted");

    // Assert
    assertEquals(Set.of(faq2), result);
  }
}
