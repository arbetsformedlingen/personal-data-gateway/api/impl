package se.jobtechdev.personaldatagateway.api.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.web.client.HttpClientErrorException;
import se.jobtechdev.personaldatagateway.api.exception.ApiException;
import se.jobtechdev.personaldatagateway.api.generated.entities.Datasource;
import se.jobtechdev.personaldatagateway.api.generated.model.ErrorResponse;
import se.jobtechdev.personaldatagateway.api.util.CustomRestClient;
import se.jobtechdev.personaldatagateway.api.util.DataWrapper;
import se.jobtechdev.personaldatagateway.api.util.ErrorResponseFactory;
import se.jobtechdev.personaldatagateway.api.util.TokenReplacer;

public class DataServiceTest {
  @InjectMocks private DataService dataService;

  private AutoCloseable closeable;

  @BeforeEach
  void setUp() {
    closeable = MockitoAnnotations.openMocks(this);
  }

  @AfterEach
  void closeService() throws Exception {
    closeable.close();
  }

  @Test
  @Tag("Unit")
  public void retrieveData() {
    // Arrange
    final var mockDatasource = mock(Datasource.class);
    final var mockDataWrapper = mock(DataWrapper.class);

    when(mockDatasource.getPath()).thenReturn("path");
    when(mockDatasource.getMethod()).thenReturn("method");
    when(mockDatasource.getHeaders()).thenReturn("headers");

    try (final var staticCustomRestClient = mockStatic(CustomRestClient.class);
        final var staticTokenReplacer = mockStatic(TokenReplacer.class)) {
      staticTokenReplacer
          .when(
              () ->
                  TokenReplacer.replace(
                      "path", Map.of("personnummer", "user-id", "user-id", "user-id")))
          .thenReturn("replaced-path");

      staticCustomRestClient
          .when(
              () ->
                  CustomRestClient.retrieveData(
                      "replaced-path", "method", "headers", "application/json"))
          .thenReturn(mockDataWrapper);

      // Act
      final var data = dataService.retrieveData(mockDatasource, "user-id", "application/json");

      // Assert
      assertEquals(mockDataWrapper, data);
    }
  }

  @Test
  @Tag("Unit")
  public void
      retrieveData_whenRetrievalThrowsHttpClientErrorExceptionWithStatus451_rethrowApiExceptionUnavailableForLegalReasons() {
    // Arrange
    final var mockDatasource = mock(Datasource.class);
    final var mockErrorResponse = mock(ErrorResponse.class);
    final var mockHttpClientErrorException = mock(HttpClientErrorException.class);
    final var mockHttpClientErrorExceptionStatusCode = HttpStatusCode.valueOf(451);

    when(mockDatasource.getPath()).thenReturn("path");
    when(mockDatasource.getMethod()).thenReturn("method");
    when(mockDatasource.getHeaders()).thenReturn("headers");
    when(mockHttpClientErrorException.getMessage())
        .thenReturn("mock-http-client-error-exception-message");
    when(mockHttpClientErrorException.getStatusCode())
        .thenReturn(mockHttpClientErrorExceptionStatusCode);

    final var constructionApiExceptionArgs = new HashMap<ApiException, List<?>>();

    try (final var staticCustomRestClient = mockStatic(CustomRestClient.class);
        final var staticTokenReplacer = mockStatic(TokenReplacer.class);
        final var staticErrorResponseFactory = mockStatic(ErrorResponseFactory.class);
        final var constructionApiException =
            mockConstruction(
                ApiException.class,
                (mock, ctx) -> {
                  constructionApiExceptionArgs.put(mock, ctx.arguments());
                })) {
      staticTokenReplacer
          .when(
              () ->
                  TokenReplacer.replace(
                      "path", Map.of("personnummer", "user-id", "user-id", "user-id")))
          .thenReturn("replaced-path");

      staticCustomRestClient
          .when(
              () ->
                  CustomRestClient.retrieveData(
                      "replaced-path", "method", "headers", "application/json"))
          .thenThrow(mockHttpClientErrorException);

      staticErrorResponseFactory
          .when(
              () ->
                  ErrorResponseFactory.createErrorResponse(
                      HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS,
                      "mock-http-client-error-exception-message"))
          .thenReturn(mockErrorResponse);

      // Act
      final var thrown =
          assertThrows(
              ApiException.class,
              () -> dataService.retrieveData(mockDatasource, "user-id", "application/json"));

      // Assert
      assertEquals(1, constructionApiException.constructed().size());

      final var mockApiException = constructionApiException.constructed().getFirst();

      assertEquals(
          mockErrorResponse, constructionApiExceptionArgs.get(mockApiException).getFirst());

      assertEquals(mockApiException, thrown);
    }
  }

  @Test
  @Tag("Unit")
  public void
      retrieveData_whenRetrievalThrowsHttpClientErrorExceptionNotWithStatus451_rethrowApiExceptionBadGateway() {
    // Arrange
    final var mockDatasource = mock(Datasource.class);
    final var mockErrorResponse = mock(ErrorResponse.class);
    final var mockHttpClientErrorException = mock(HttpClientErrorException.class);
    final var mockHttpClientErrorExceptionStatusCode = HttpStatusCode.valueOf(418);

    when(mockDatasource.getPath()).thenReturn("path");
    when(mockDatasource.getMethod()).thenReturn("method");
    when(mockDatasource.getHeaders()).thenReturn("headers");
    when(mockHttpClientErrorException.getMessage())
        .thenReturn("mock-http-client-error-exception-message");
    when(mockHttpClientErrorException.getStatusCode())
        .thenReturn(mockHttpClientErrorExceptionStatusCode);

    final var constructionApiExceptionArgs = new HashMap<ApiException, List<?>>();

    try (final var staticCustomRestClient = mockStatic(CustomRestClient.class);
        final var staticTokenReplacer = mockStatic(TokenReplacer.class);
        final var staticErrorResponseFactory = mockStatic(ErrorResponseFactory.class);
        final var constructionApiException =
            mockConstruction(
                ApiException.class,
                (mock, ctx) -> {
                  constructionApiExceptionArgs.put(mock, ctx.arguments());
                })) {
      staticTokenReplacer
          .when(
              () ->
                  TokenReplacer.replace(
                      "path", Map.of("personnummer", "user-id", "user-id", "user-id")))
          .thenReturn("replaced-path");

      staticCustomRestClient
          .when(
              () ->
                  CustomRestClient.retrieveData(
                      "replaced-path", "method", "headers", "application/json"))
          .thenThrow(mockHttpClientErrorException);

      staticErrorResponseFactory
          .when(() -> ErrorResponseFactory.createErrorResponse(HttpStatus.BAD_GATEWAY))
          .thenReturn(mockErrorResponse);

      // Act
      final var thrown =
          assertThrows(
              ApiException.class,
              () -> dataService.retrieveData(mockDatasource, "user-id", "application/json"));

      // Assert
      assertEquals(1, constructionApiException.constructed().size());

      final var mockApiException = constructionApiException.constructed().getFirst();

      assertEquals(
          mockErrorResponse, constructionApiExceptionArgs.get(mockApiException).getFirst());

      assertEquals(mockApiException, thrown);
    }
  }

  @Test
  @Tag("Unit")
  public void
      retrieveData_whenRetrievalFailsDueToAGenericRuntimeException_rethrowApiExceptionBadGateway() {
    // Arrange
    final var mockDatasource = mock(Datasource.class);
    final var mockErrorResponse = mock(ErrorResponse.class);

    when(mockDatasource.getPath()).thenReturn("path");
    when(mockDatasource.getMethod()).thenReturn("method");
    when(mockDatasource.getHeaders()).thenReturn("headers");

    final var constructionApiExceptionArgs = new HashMap<ApiException, List<?>>();

    try (final var staticCustomRestClient = mockStatic(CustomRestClient.class);
        final var staticTokenReplacer = mockStatic(TokenReplacer.class);
        final var staticErrorResponseFactory = mockStatic(ErrorResponseFactory.class);
        final var constructionApiException =
            mockConstruction(
                ApiException.class,
                (mock, ctx) -> {
                  constructionApiExceptionArgs.put(mock, ctx.arguments());
                })) {
      staticTokenReplacer
          .when(
              () ->
                  TokenReplacer.replace(
                      "path", Map.of("personnummer", "user-id", "user-id", "user-id")))
          .thenReturn("replaced-path");

      staticCustomRestClient
          .when(
              () ->
                  CustomRestClient.retrieveData(
                      "replaced-path", "method", "headers", "application/json"))
          .thenThrow(RuntimeException.class);

      staticErrorResponseFactory
          .when(() -> ErrorResponseFactory.createErrorResponse(HttpStatus.BAD_GATEWAY))
          .thenReturn(mockErrorResponse);

      // Act
      final var thrown =
          assertThrows(
              ApiException.class,
              () -> dataService.retrieveData(mockDatasource, "user-id", "application/json"));

      // Assert
      assertEquals(1, constructionApiException.constructed().size());

      final var mockApiException = constructionApiException.constructed().getFirst();

      assertEquals(
          mockErrorResponse, constructionApiExceptionArgs.get(mockApiException).getFirst());

      assertEquals(mockApiException, thrown);
    }
  }
}
