package se.jobtechdev.personaldatagateway.api.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import java.nio.charset.StandardCharsets;
import java.time.ZonedDateTime;
import java.util.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import se.jobtechdev.personaldatagateway.api.generated.entities.Client;
import se.jobtechdev.personaldatagateway.api.generated.entities.ClientKeyReset;
import se.jobtechdev.personaldatagateway.api.generated.entities.Organization;
import se.jobtechdev.personaldatagateway.api.repository.ClientKeyResetRepository;
import se.jobtechdev.personaldatagateway.api.repository.ClientRepository;
import se.jobtechdev.personaldatagateway.api.util.KeyProvider;
import se.jobtechdev.personaldatagateway.api.util.MessageDigestProvider;
import se.jobtechdev.personaldatagateway.api.util.TimeProvider;
import se.jobtechdev.personaldatagateway.api.util.UuidProvider;

public class ClientServiceTest {
  @InjectMocks private ClientService clientService;

  @Mock private ClientRepository clientRepository;

  @Mock private ClientKeyResetRepository clientKeyResetRepository;

  private AutoCloseable closeable;

  @BeforeEach
  void setUp() {
    closeable = MockitoAnnotations.openMocks(this);
  }

  @AfterEach
  void closeService() throws Exception {
    closeable.close();
  }

  @Test
  public void getClients() {
    // Arrange
    final var clients = mock(List.class);
    when(clientRepository.findAll()).thenReturn(clients);

    // Act
    final var result = clientService.getClients();

    // Assert
    assertEquals(clients, result);
    verify(clientRepository, times(1)).findAll();
  }

  @Test
  void getClientById() {
    // Arrange
    final var optional = mock(Optional.class);
    final var clientId = mock(UUID.class);
    when(clientRepository.findById(clientId)).thenReturn(optional);

    // Act
    final var result = clientService.getClientById(clientId);

    // Assert
    assertEquals(optional, result);
  }

  @Test
  void getClientByKeyHash() {
    // Arrange
    final var mockClient = mock(Client.class);
    final var mockApiKeyHash = new byte[] {1, 2, 3};
    when(clientRepository.findByKeyHash(mockApiKeyHash)).thenReturn(Optional.of(mockClient));

    // Act
    final var client = clientService.getClientByKeyHash(mockApiKeyHash);

    // Assert
    assertEquals(Optional.of(mockClient), client);
  }

  @Test
  void createClient() {
    final var clientArgs = new HashMap<Client, List<?>>();
    try (MockedStatic<TimeProvider> timeProvider = mockStatic(TimeProvider.class);
        MockedStatic<UuidProvider> uuidProvider = mockStatic(UuidProvider.class);
        MockedStatic<KeyProvider> keyProvider = mockStatic(KeyProvider.class);
        final var clientMockedConstruction =
            mockConstruction(
                Client.class, (mock, context) -> clientArgs.put(mock, context.arguments())); ) {
      // Arrange
      final var organization = mock(Organization.class);
      final var uuid = mock(UUID.class);
      when(uuid.toString()).thenReturn("mock-uuid");
      uuidProvider.when(UuidProvider::uuid).thenReturn(uuid);

      final var key = "mock-key".getBytes(StandardCharsets.UTF_8);
      keyProvider.when(() -> KeyProvider.randomBytes(32)).thenReturn(key);

      final var created = mock(ZonedDateTime.class);
      timeProvider.when(TimeProvider::now).thenReturn(created);
      when(created.toString()).thenReturn("mock-now");

      final var name = "name";
      final var role = "role";

      final var savedClient = mock(Client.class);

      when(clientRepository.save(any(Client.class))).thenReturn(savedClient);

      // Act
      final var result = clientService.createClient(organization, name, role, key);

      // Assert
      assertEquals(1, clientMockedConstruction.constructed().size());
      final var client = clientMockedConstruction.constructed().get(0);
      final var cliArgs = clientArgs.get(client);
      assertEquals(uuid, cliArgs.get(0));

      verify(clientRepository, times(1)).save(client);
      assertEquals(savedClient, result);
    }
  }

  @Test
  void saveClient() {
    // Arrange
    final var client = mock(Client.class);
    when(clientRepository.save(client)).thenReturn(client);

    // Act
    final var result = clientService.saveClient(client);

    // Assert
    assertEquals(client, result);
    verify(clientRepository, times(1)).save(client);
  }

  @Test
  public void delete() {
    // Arrange
    final var client = mock(Client.class);

    // Act
    clientService.delete(client);

    // Assert
    verify(clientRepository, times(1)).delete(client);
  }

  @Test
  @Tag("Unit")
  public void getAllClientKeyResetsByClientId() {
    // Arrange
    final var client = mock(Client.class);
    final var clientId = mock(UUID.class);

    when(client.getId()).thenReturn(clientId);

    // Act
    clientService.getAllClientKeyResetsByClientId(client);

    // Assert
    verify(clientKeyResetRepository, times(1)).findAllByClientId(clientId);
  }

  @Test
  @Tag("Unit")
  public void keyReset() {
    // Arrange
    final var mockBytes = new byte[] {1, 2, 3};
    final var mockHash = new byte[] {4, 5, 6};
    final var client = mock(Client.class);
    final var clientId = mock(UUID.class);
    final var clientKeyResetUuid = mock(UUID.class);
    final var mockNow = mock(ZonedDateTime.class);
    final var savedClientKeyReset = mock(ClientKeyReset.class);

    when(client.getId()).thenReturn(clientId);

    final var clientKeyResetArgs = new HashMap<ClientKeyReset, List<?>>();
    final var clientKeyResetAndKeyArgs = new HashMap<ClientService.ClientKeyResetAndKey, List<?>>();
    try (final var staticKeyProvider = mockStatic(KeyProvider.class);
        final var staticMessageDigestProvider = mockStatic(MessageDigestProvider.class);
        final var staticUuid = mockStatic(UUID.class);
        final var staticZonedDateTime = mockStatic(ZonedDateTime.class);
        final var constructionClientKeyReset =
            mockConstruction(
                ClientKeyReset.class,
                (mock, ctx) -> {
                  clientKeyResetArgs.put(mock, ctx.arguments());
                });
        final var constructionClientKeyResetAndKey =
            mockConstruction(
                ClientService.ClientKeyResetAndKey.class,
                (mock, ctx) -> {
                  clientKeyResetAndKeyArgs.put(mock, ctx.arguments());
                })) {

      staticKeyProvider.when(() -> KeyProvider.randomBytes(32)).thenReturn(mockBytes);
      staticUuid.when(() -> UUID.randomUUID()).thenReturn(clientKeyResetUuid);
      staticZonedDateTime.when(() -> ZonedDateTime.now()).thenReturn(mockNow);
      staticMessageDigestProvider
          .when(() -> MessageDigestProvider.sha256(mockBytes))
          .thenReturn(mockHash);
      when(clientKeyResetRepository.save(any(ClientKeyReset.class)))
          .thenReturn(savedClientKeyReset);

      // Act
      final var clientKeyResetAndKey = clientService.keyReset(client);

      // Assert
      verify(client, times(1)).setKeyHash(mockHash);
      verify(clientRepository, times(1)).save(client);

      assertEquals(1, constructionClientKeyReset.constructed().size());
      final var mockClientKeyReset = constructionClientKeyReset.constructed().getFirst();

      verify(clientKeyResetRepository, times(1)).save(mockClientKeyReset);

      assertEquals(1, constructionClientKeyResetAndKey.constructed().size());
      final var mockClientKeyResetAndKey =
          constructionClientKeyResetAndKey.constructed().getFirst();

      final var mockClientKeyResetAndKeyArgs =
          clientKeyResetAndKeyArgs.get(mockClientKeyResetAndKey);
      assertEquals(savedClientKeyReset, mockClientKeyResetAndKeyArgs.get(0));
      assertEquals(mockBytes, mockClientKeyResetAndKeyArgs.get(1));

      assertEquals(mockClientKeyResetAndKey, clientKeyResetAndKey);
    }
  }

  @Test
  @Tag("Unit")
  public void constructClientKeyResetAndKey() {
    new ClientService.ClientKeyResetAndKey(mock(ClientKeyReset.class), new byte[] {1, 2, 3});
  }
}
