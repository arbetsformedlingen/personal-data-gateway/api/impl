package se.jobtechdev.personaldatagateway.api.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import java.net.MalformedURLException;
import java.net.URI;
import java.time.ZonedDateTime;
import java.util.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import se.jobtechdev.personaldatagateway.api.generated.entities.*;
import se.jobtechdev.personaldatagateway.api.repository.DatasourceRepository;
import se.jobtechdev.personaldatagateway.api.util.TimeProvider;
import se.jobtechdev.personaldatagateway.api.util.UuidProvider;

public class DatasourceServiceTest {
  @InjectMocks private DatasourceService datasourceService;

  @Mock private DatasourceRepository datasourceRepository;

  private AutoCloseable closeable;

  @BeforeEach
  void setUp() {
    closeable = MockitoAnnotations.openMocks(this);
  }

  @AfterEach
  void closeService() throws Exception {
    closeable.close();
  }

  @Test
  public void createDatasource() throws MalformedURLException {
    final var datasourceArgs = new HashMap<Datasource, List<?>>();
    try (MockedStatic<UuidProvider> uuidProvider = mockStatic(UuidProvider.class);
        MockedStatic<TimeProvider> timeProvider = mockStatic(TimeProvider.class);
        final var datasourceMockedConstruction =
            mockConstruction(
                Datasource.class,
                (mock, context) -> datasourceArgs.put(mock, context.arguments()))) {
      // Arrange
      final var uuid = mock(UUID.class);
      when(uuid.toString()).thenReturn("datasource-id");
      uuidProvider.when(UuidProvider::uuid).thenReturn(uuid);

      final var mockOrganization = mock(Organization.class);

      final var created = mock(ZonedDateTime.class);
      timeProvider.when(TimeProvider::now).thenReturn(created);
      when(created.toString()).thenReturn("created");

      String name = "datatype-name";
      String path = "datatype-path";
      String headers = "datatype-headers";
      String method = "datatype-method";

      final var savedDatasource = mock(Datasource.class);
      when(datasourceRepository.save(any(Datasource.class))).thenReturn(savedDatasource);

      // Act
      final var result =
          datasourceService.createDatasource(mockOrganization, name, path, headers, method);

      // Assert
      assertEquals(1, datasourceMockedConstruction.constructed().size());
      final var datasource = datasourceMockedConstruction.constructed().get(0);
      verify(datasource, times(1)).setHeaders(headers);
      verify(datasource, times(1)).setMethod(method);
      final var datArgs = datasourceArgs.get(datasource);
      assertEquals(uuid, datArgs.get(0));
      assertEquals(mockOrganization, datArgs.get(1));
      assertEquals(name, datArgs.get(2));
      assertEquals(
          URI.create("http://example.com/datatypes/6871e92f-5374-4f83-b743-e9966913cbbb").toURL(),
          datArgs.get(3));
      assertEquals(path, datArgs.get(4));
      assertEquals(created, datArgs.get(5));
      assertEquals(created, datArgs.get(6));
      assertEquals(savedDatasource, result.get());
      verify(datasourceRepository, times(1)).save(any());
    }
  }

  @Test
  public void createDatasource_whenDatatypeUrlIsMalformed_throwRuntimeException()
      throws MalformedURLException {
    // Arrange
    final var mockDatatypeUri = mock(URI.class);

    when(mockDatatypeUri.toURL()).thenThrow(MalformedURLException.class);

    try (final var staticUri = mockStatic(URI.class)) {

      staticUri
          .when(
              () -> URI.create("http://example.com/datatypes/6871e92f-5374-4f83-b743-e9966913cbbb"))
          .thenReturn(mockDatatypeUri);

      final var mockOrganization = mock(Organization.class);

      // Act
      assertThrows(
          RuntimeException.class,
          () ->
              datasourceService.createDatasource(
                  mockOrganization,
                  "datatype-name",
                  "datatype-path",
                  "datatype-headers",
                  "datatype-method"));
    }
  }

  @Test
  public void getDatasources() {
    // Arrange
    final var datasources = mock(List.class);
    when(datasourceRepository.findAll()).thenReturn(datasources);

    // Act
    final var result = datasourceService.getDatasources();

    // Assert
    assertEquals(datasources, result);
    verify(datasourceRepository, times(1)).findAll();
  }

  @Test
  public void getDatasourceById() {
    // Arrange
    final var datasourcId = mock(UUID.class);
    final var datasource = mock(Datasource.class);
    final var optionalDatasource = Optional.of(datasource);
    when(datasourceRepository.findById(datasourcId)).thenReturn(optionalDatasource);

    // Act
    final var result = datasourceService.getDatasourceById(datasourcId);

    // Assert
    assertEquals(optionalDatasource, result);
    verify(datasourceRepository, times(1)).findById(datasourcId);
  }

  @Test
  public void update() {
    // Arrange
    final var datasource = mock(Datasource.class);
    when(datasourceRepository.save(datasource)).thenReturn(datasource);

    // Act
    final var result = datasourceService.update(datasource);

    // Assert
    assertEquals(datasource, result);
    verify(datasourceRepository, times(1)).save(datasource);
  }

  @Test
  public void save() {
    // Arrange
    final var datasource = mock(Datasource.class);

    // Act
    datasourceService.save(datasource);

    // Assert
    verify(datasourceRepository, times(1)).save(datasource);
  }
}
