package se.jobtechdev.personaldatagateway.api.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import se.jobtechdev.personaldatagateway.api.generated.entities.Person;
import se.jobtechdev.personaldatagateway.api.repository.PersonRepository;
import se.jobtechdev.personaldatagateway.api.util.TimeProvider;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class PersonServiceTest {
  @InjectMocks
  private PersonService personService;

  @Mock
  private PersonRepository personRepository;

  private AutoCloseable closeable;

  @BeforeEach
  void setUp() {
    closeable = MockitoAnnotations.openMocks(this);
  }

  @AfterEach
  void closeService() throws Exception {
    closeable.close();
  }

  @Test
  void getPersonById() {
    // Arrange
    final var optional = mock(Optional.class);
    final var personId = "person-id";
    when(personRepository.findById(personId)).thenReturn(optional);

    // Act
    final var result = personService.getPersonById(personId);

    // Assert
    assertEquals(optional, result);
  }

  @Test
  void createPerson() {
    final var userArgs = new HashMap<Person, List<?>>();
    try (MockedStatic<TimeProvider> timeProvider = mockStatic(TimeProvider.class);
         final var clientMockedConstruction =
             mockConstruction(
                 Person.class,
                 (mock, context) -> {
                   userArgs.put(mock, context.arguments());
                 })) {
      // Arrange
      final var personId = "person-id";
      final var now = mock(ZonedDateTime.class);
      timeProvider.when(TimeProvider::now).thenReturn(now);
      final var savedPerson = mock(Person.class);
      when(personRepository.save(any(Person.class))).thenReturn(savedPerson);

      // Act
      final var result = personService.createPerson(personId);

      // Assert
      assertEquals(1, clientMockedConstruction.constructed().size());
      final var person = clientMockedConstruction.constructed().get(0);
      assertEquals(personId, userArgs.get(person).get(0));
      assertEquals(now, userArgs.get(person).get(1));
      assertEquals(savedPerson, result);
    }
  }
}
