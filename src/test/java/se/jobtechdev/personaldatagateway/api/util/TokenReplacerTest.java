package se.jobtechdev.personaldatagateway.api.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Map;
import org.junit.jupiter.api.Test;

public class TokenReplacerTest {
  @Test
  public void replaceToken() {
    final var tokenReplacements = Map.of("a", "abc", "b", "123");
    final var text = "foo/{a}/bar/{b}/baz";

    final var result = TokenReplacer.replace(text, tokenReplacements);
    assertEquals("foo/abc/bar/123/baz", result);
  }

  @Test
  public void replaceToken_whenTextEndsWithAToken() {
    final var tokenReplacements = Map.of("a", "abc", "b", "123");
    final var text = "foo/{a}/bar/{b}";

    final var result = TokenReplacer.replace(text, tokenReplacements);
    assertEquals("foo/abc/bar/123", result);
  }
}
