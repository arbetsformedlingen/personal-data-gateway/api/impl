package se.jobtechdev.personaldatagateway.api.util;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import se.jobtechdev.personaldatagateway.api.exception.ApiException;
import se.jobtechdev.personaldatagateway.api.generated.model.ErrorResponse;

import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class ControllerUtilTest {
  @Test
  public void throwApiExceptionOnAbsentValue() {
    // Arrange
    final var mockHttpStatus = mock(HttpStatus.class);
    final var mockLogMessageOnFailure = "mock-log-message-on-failure";

    // Act
    assertThrows(ApiException.class, () -> ControllerUtil.throwApiExceptionOnAbsentValue(Optional.empty(), mockHttpStatus, mockLogMessageOnFailure));

    assertThrows(ApiException.class, () -> ControllerUtil.throwApiExceptionOnAbsentValue(Optional.ofNullable(null), mockHttpStatus, mockLogMessageOnFailure));

    assertThrows(ApiException.class, () -> ControllerUtil.throwApiExceptionOnAbsentValue(null, mockHttpStatus, mockLogMessageOnFailure));

    assertEquals("123", ControllerUtil.throwApiExceptionOnAbsentValue(Optional.of("123"), mockHttpStatus, mockLogMessageOnFailure));
  }

  @Test
  public void
  throwApiExceptionOnAbsentValue_whenValueIsEmptyAndHttpStatusIsEqualOrGreaterThan500_invokeErrorLoggerAndThrowApiException() {
    // Arrange
    final var mockErrorResponse = mock(ErrorResponse.class);
    final var mockErrorLogMessage = "mock-example-log-message";

    final var constructionApiExceptionArgs = new HashMap<ApiException, List<?>>();
    try (final var staticErrorResponseFactory = mockStatic(ErrorResponseFactory.class);
         final var constructionApiException =
             mockConstruction(
                 ApiException.class,
                 (mock, context) -> {
                   constructionApiExceptionArgs.put(mock, context.arguments());

                   final var errorResponse = mock(ErrorResponse.class);
                   when(errorResponse.getStatus()).thenReturn(501);
                   when(mock.getErrorResponse()).thenReturn(errorResponse);
                 })) {

      staticErrorResponseFactory
          .when(() -> ErrorResponseFactory.createErrorResponse(HttpStatus.NOT_IMPLEMENTED))
          .thenReturn(mockErrorResponse);

      // Act
      assertThrows(
          ApiException.class,
          () ->
              ControllerUtil.throwApiExceptionOnAbsentValue(
                  Optional.empty(), HttpStatus.NOT_IMPLEMENTED, mockErrorLogMessage));

      // Assert
      assertEquals(1, constructionApiException.constructed().size());
      final var mockApiException = constructionApiException.constructed().get(0);
      staticErrorResponseFactory.verify(
          () -> ErrorResponseFactory.createErrorResponse(HttpStatus.NOT_IMPLEMENTED), times(1));
      assertEquals(mockErrorResponse, constructionApiExceptionArgs.get(mockApiException).get(0));
    }
  }

  @Test
  public void
  throwApiExceptionOnAbsentValue_whenValueIsEmptyAndHttpStatusIsLessThan500_invokeWarnLoggerAndThrowApiException() {
    // Arrange
    final var mockErrorResponse = mock(ErrorResponse.class);
    final var mockErrorLogMessage = "mock-example-log-message";

    final var constructionApiExceptionArgs = new HashMap<ApiException, List<?>>();
    try (final var staticErrorResponseFactory = mockStatic(ErrorResponseFactory.class);
         final var constructionApiException =
             mockConstruction(
                 ApiException.class,
                 (mock, context) -> {
                   constructionApiExceptionArgs.put(mock, context.arguments());

                   final var errorResponse = mock(ErrorResponse.class);
                   when(errorResponse.getStatus()).thenReturn(418);
                   when(mock.getErrorResponse()).thenReturn(errorResponse);
                 })) {

      staticErrorResponseFactory
          .when(() -> ErrorResponseFactory.createErrorResponse(HttpStatus.I_AM_A_TEAPOT))
          .thenReturn(mockErrorResponse);

      // Act
      assertThrows(
          ApiException.class,
          () ->
              ControllerUtil.throwApiExceptionOnAbsentValue(
                  Optional.empty(), HttpStatus.I_AM_A_TEAPOT, mockErrorLogMessage));

      // Assert
      assertEquals(1, constructionApiException.constructed().size());
      final var mockApiException = constructionApiException.constructed().get(0);
      staticErrorResponseFactory.verify(
          () -> ErrorResponseFactory.createErrorResponse(HttpStatus.I_AM_A_TEAPOT), times(1));
      assertEquals(mockErrorResponse, constructionApiExceptionArgs.get(mockApiException).get(0));
    }
  }


  @Test
  public void earlyExit() {
    // Arrange
    final var mockValue = mock(Object.class);
    final var mockCheck = mock(Function.class);
    final var mockHttpStatus = mock(HttpStatus.class);
    final var mockLogMessageOnFailure = "mock-log-message-on-failure";

    when(mockCheck.apply(mockValue)).thenReturn(false);

    // Act
    final var value =
        ControllerUtil.earlyExit(mockValue, mockCheck, mockHttpStatus, mockLogMessageOnFailure);

    // Assert
    assertEquals(mockValue, value);
  }

  @Test
  public void
  earlyExitWithLogMessage_whenCheckIsTrueAndApiExceptionStatusIsEqualOrGreaterThan500_invokeErrorLoggerAndRethrowApiException() {
    // Arrange
    final var mockFunction = mock(Function.class);
    final var mockErrorResponse = mock(ErrorResponse.class);
    final var mockErrorLogMessage = "mock-example-log-message";

    when(mockFunction.apply("example-value")).thenReturn(true);

    final var constructionApiExceptionArgs = new HashMap<ApiException, List<?>>();
    try (final var staticErrorResponseFactory = mockStatic(ErrorResponseFactory.class);
         final var constructionApiException =
             mockConstruction(
                 ApiException.class,
                 (mock, context) -> {
                   constructionApiExceptionArgs.put(mock, context.arguments());

                   final var errorResponse = mock(ErrorResponse.class);
                   when(errorResponse.getStatus()).thenReturn(501);
                   when(mock.getErrorResponse()).thenReturn(errorResponse);
                 })) {

      staticErrorResponseFactory
          .when(() -> ErrorResponseFactory.createErrorResponse(HttpStatus.NOT_IMPLEMENTED))
          .thenReturn(mockErrorResponse);

      // Act
      assertThrows(
          ApiException.class,
          () ->
              ControllerUtil.earlyExit(
                  "example-value", mockFunction, HttpStatus.NOT_IMPLEMENTED, mockErrorLogMessage));

      // Assert
      assertEquals(1, constructionApiException.constructed().size());
      final var mockApiException = constructionApiException.constructed().get(0);
      staticErrorResponseFactory.verify(
          () -> ErrorResponseFactory.createErrorResponse(HttpStatus.NOT_IMPLEMENTED), times(1));
      assertEquals(mockErrorResponse, constructionApiExceptionArgs.get(mockApiException).get(0));
    }
  }

  @Test
  public void
  earlyExitWithLogMessage_whenCheckIsTrueAndApiExceptionStatusIsLessThan500_invokeWarnLoggerAndRethrowApiException() {
    // Arrange
    final var mockFunction = mock(Function.class);
    final var mockErrorResponse = mock(ErrorResponse.class);
    final var mockErrorLogMessage = "mock-example-log-message";

    when(mockFunction.apply("example-value")).thenReturn(true);

    final var constructionApiExceptionArgs = new HashMap<ApiException, List<?>>();
    try (final var staticErrorResponseFactory = mockStatic(ErrorResponseFactory.class);
         final var constructionApiException =
             mockConstruction(
                 ApiException.class,
                 (mock, context) -> {
                   constructionApiExceptionArgs.put(mock, context.arguments());

                   final var errorResponse = mock(ErrorResponse.class);
                   when(errorResponse.getStatus()).thenReturn(418);
                   when(mock.getErrorResponse()).thenReturn(errorResponse);
                 })) {

      staticErrorResponseFactory
          .when(() -> ErrorResponseFactory.createErrorResponse(HttpStatus.I_AM_A_TEAPOT))
          .thenReturn(mockErrorResponse);

      // Act
      assertThrows(
          ApiException.class,
          () ->
              ControllerUtil.earlyExit(
                  "example-value", mockFunction, HttpStatus.I_AM_A_TEAPOT, mockErrorLogMessage));

      // Assert
      assertEquals(1, constructionApiException.constructed().size());
      final var mockApiException = constructionApiException.constructed().get(0);
      staticErrorResponseFactory.verify(
          () -> ErrorResponseFactory.createErrorResponse(HttpStatus.I_AM_A_TEAPOT), times(1));
      assertEquals(mockErrorResponse, constructionApiExceptionArgs.get(mockApiException).get(0));
    }
  }

  @Test
  public void
  earlyExitWithLogMessage_whenCheckThrowsRuntimeException_invokeErrorLoggerAndThrowApiException() {
    // Arrange
    final var mockFunction = mock(Function.class);
    final var mockErrorResponse = mock(ErrorResponse.class);
    final var mockErrorLogMessage = "mock-example-log-message";
    final var mockRuntimeException = mock(RuntimeException.class);

    when(mockFunction.apply("example-value")).thenThrow(mockRuntimeException);

    final var constructionApiExceptionArgs = new HashMap<ApiException, List<?>>();
    try (final var staticErrorResponseFactory = mockStatic(ErrorResponseFactory.class);
         final var constructionApiException =
             mockConstruction(
                 ApiException.class,
                 (mock, context) -> constructionApiExceptionArgs.put(mock, context.arguments()))) {

      staticErrorResponseFactory
          .when(() -> ErrorResponseFactory.createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR))
          .thenReturn(mockErrorResponse);

      // Act
      assertThrows(
          ApiException.class,
          () ->
              ControllerUtil.earlyExit(
                  "example-value",
                  mockFunction,
                  HttpStatus.INTERNAL_SERVER_ERROR,
                  mockErrorLogMessage));

      // Assert
      assertEquals(1, constructionApiException.constructed().size());
      final var mockApiException = constructionApiException.constructed().get(0);
      staticErrorResponseFactory.verify(
          () -> ErrorResponseFactory.createErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR),
          times(1));
      assertEquals(mockErrorResponse, constructionApiExceptionArgs.get(mockApiException).get(0));
    }
  }

  @Test
  public void preventReassignment_whenCurrentAndNewValueIsNull_doesNotThrowException() {
    // Act
    assertDoesNotThrow(() -> ControllerUtil.preventReassignment(null, null, "Foo"));
  }

  @Test
  public void preventReassignment_whenCurrentIsDefined_doesNotThrowException() {
    final var currentValue = mock(ZonedDateTime.class);
    // Act
    assertDoesNotThrow(() -> ControllerUtil.preventReassignment(currentValue, null, "Foo"));
  }

  @Test
  public void preventReassignment_whenNewValueIsDefined_doesNotThrowException() {
    final var newValue = mock(ZonedDateTime.class);
    // Act
    assertDoesNotThrow(() -> ControllerUtil.preventReassignment(null, newValue, "Foo"));
  }

  @Test
  public void
  preventReassignment_whenCurrentAndNewValueRefersToTheSameValue_doesNotThrowException() {
    final var value = mock(ZonedDateTime.class);
    // Act
    assertDoesNotThrow(() -> ControllerUtil.preventReassignment(value, value, "Foo"));
  }

  @Test
  public void
  preventReassignment_whenCurrentAndNewValueIsDefinedButRefersToDifferentValues_throwException() {
    final var currentValue = mock(ZonedDateTime.class);
    final var newValue = mock(ZonedDateTime.class);
    // Act
    assertThrows(
        ApiException.class,
        () -> ControllerUtil.preventReassignment(currentValue, newValue, "Foo"));
  }

  @Test
  public void assignNonNull() {
    final var input = new Foo("new value");
    final var target = new Foo("current value");
    final var result = ControllerUtil.assignNonNull(input, target);
    assertEquals("new value", result.getBar());
  }

  @Test
  public void assignNonNull_whenAnInputFieldIsNull_doNotInvokeTargetFieldSetter() {
    final var input = new Foo(null);
    final var target = new Foo("current value");
    final var result = ControllerUtil.assignNonNull(input, target);
    assertEquals("current value", result.getBar());
  }

  @Test
  public void assignNonNull_whenAnInputFieldEqualsTargetField_doNotInvokeTargetFieldSetter() {
    final var input = new Foo("same value");
    final var target = new Foo("same value");
    final var result = ControllerUtil.assignNonNull(input, target);
    assertEquals("same value", result.getBar());
  }

  @Test
  public void assignNonNull_whenAnInputAndTargetClassesAreIncompatible_throwRuntimeException() {
    final var input = new Foo("hello");
    final var target = new Bar();

    final var thrown =
        assertThrows(RuntimeException.class, () -> ControllerUtil.assignNonNull(input, target));

    assertEquals("Failed to assign non-null values to target instance!", thrown.getMessage());
  }

  @Test
  public void assignNonNullWithWhitelist() {
    final var input = new Foo("new value");
    final var target = new Foo("current value");
    final var result = ControllerUtil.assignNonNull(input, target, List.of("bar"), List.of());
    assertEquals("new value", result.getBar());
  }

  @Test
  public void assignNonNullWithWhitelist_whenAnInputFieldIsNull_doNotInvokeTargetFieldSetter() {
    final var input = new Foo(null);
    final var target = new Foo("current value");
    final var result = ControllerUtil.assignNonNull(input, target, List.of("bar"), List.of());
    assertEquals("current value", result.getBar());
  }

  @Test
  public void
  assignNonNullWithWhitelist_whenAnInputFieldEqualsTargetField_doNotInvokeTargetFieldSetter() {
    final var input = new Foo("same value");
    final var target = new Foo("same value");
    final var result = ControllerUtil.assignNonNull(input, target, List.of("bar"), List.of());
    assertEquals("same value", result.getBar());
  }

  @Test
  public void
  assignNonNullWithWhitelist_whenAnInputAndTargetClassesAreIncompatible_throwRuntimeException() {
    final var input = new Foo("new value");
    final var target = new Bar();

    final var thrown =
        assertThrows(
            RuntimeException.class,
            () -> ControllerUtil.assignNonNull(input, target, List.of("bar"), List.of()));

    assertEquals("Failed to assign non-null values to target instance!", thrown.getMessage());
  }

  @Test
  public void assignNonNullWithWhitelist_whenNothingIsWhitelisted_thenValuesAreNotChanged() {
    final var input = new Foo("new vlaue");
    final var target = new Foo("current value");
    final var result = ControllerUtil.assignNonNull(input, target, List.of(), List.of());
    assertEquals("current value", result.getBar());
  }

  @Test
  public void
  assignNonNullWithWhitelist_whenValueIsWhitelistedAndRestrictedReassignmentAndCurrentValueIsNotNull_throwApiExceptionConflict() {
    final var input = new Foo("new value");
    final var target = new Foo("current value");
    final var thrown =
        assertThrows(
            ApiException.class,
            () -> ControllerUtil.assignNonNull(input, target, List.of("bar"), List.of("bar")));
    assertEquals(409, thrown.getErrorResponse().getStatus());
    assertEquals("Conflict", thrown.getErrorResponse().getError());
    assertEquals(
        "bar already exist and can not be overwritten with new value",
        thrown.getErrorResponse().getMessage());
  }

  @Test
  public void
  assignNonNullWithWhitelist_whenValueIsWhitelistedAndRestrictedReassignmentAndCurrentValueIsNull_thenValueIsChanged() {
    final var input = new Foo("new value");
    final var target = new Foo(null);
    final var result = ControllerUtil.assignNonNull(input, target, List.of("bar"), List.of("bar"));
    assertEquals("new value", result.getBar());
  }

  private static class Foo {
    private String bar;

    Foo(String bar) {
      this.bar = bar;
    }

    public String getBar() {
      return bar;
    }

    public void setBar(String bar) {
      this.bar = bar;
    }
  }

  private static class Bar {
  }
}
