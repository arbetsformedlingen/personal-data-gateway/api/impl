package se.jobtechdev.personaldatagateway.api.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import org.junit.jupiter.api.Test;
import org.springframework.http.*;
import org.springframework.web.client.RestClient;

public class CustomRestClientTest {
  @Test
  public void
      headerConsumerCreateFromHeaders_whenAcceptIsInvoked_thenItAddsAllToThePassedHttpHeadersObject() {
    // Arrange
    final var mockHeaders = Map.of("example-header-key", "example-header-value");
    final var mockHttpHeaders = mock(HttpHeaders.class);

    // Act
    final var consumer = CustomRestClient.HeaderConsumer.createFromHeaders(mockHeaders);
    consumer.accept(mockHttpHeaders);

    // Assert
    verify(mockHttpHeaders, times(1)).add("example-header-key", "example-header-value");
  }

  @Test
  public void retrieveData() {
    final var mockResponseEntity = mock(ResponseEntity.class);
    final var mockHttpStatus = mock(HttpStatus.class);
    final var mockHttpStatusCode = HttpStatusCode.valueOf(200);

    when(mockResponseEntity.getStatusCode()).thenReturn(mockHttpStatusCode);

    final var constructionDataWrapperArgs = new HashMap<DataWrapper, List<?>>();

    try (final var staticHeaderExtractor = mockStatic(HeaderExtractor.class);
        final var staticRestClient = mockStatic(RestClient.class);
        final var staticHttpMethod = mockStatic(HttpMethod.class);
        final var staticCustomRestClientHeaderConsumer =
            mockStatic(CustomRestClient.HeaderConsumer.class);
        final var constructionDataWrapper =
            mockConstruction(
                DataWrapper.class,
                (mock, ctx) -> {
                  constructionDataWrapperArgs.put(mock, ctx.arguments());
                });
        final var staticHttpStatus = mockStatic(HttpStatus.class)) {
      final var mockHeadersMap = mock(Map.class);
      staticHeaderExtractor
          .when(() -> HeaderExtractor.extractHeader("example-headers"))
          .thenReturn(mockHeadersMap);

      final var mockHeaderConsumer = mock(Consumer.class);
      staticCustomRestClientHeaderConsumer
          .when(() -> CustomRestClient.HeaderConsumer.createFromHeaders(any()))
          .thenReturn(mockHeaderConsumer);

      final var mockRestClientBuilder = mock(RestClient.Builder.class);
      staticRestClient.when(() -> RestClient.builder()).thenReturn(mockRestClientBuilder);

      when(mockRestClientBuilder.baseUrl("example-path")).thenReturn(mockRestClientBuilder);
      when(mockRestClientBuilder.defaultHeaders(mockHeaderConsumer))
          .thenReturn(mockRestClientBuilder);

      final var mockRestClient = mock(RestClient.class);
      when(mockRestClientBuilder.build()).thenReturn(mockRestClient);

      final var mockHttpMethod = mock(HttpMethod.class);
      staticHttpMethod.when(() -> HttpMethod.valueOf("example-method")).thenReturn(mockHttpMethod);

      final var mockRestClientRequestBodyUriSpec = mock(RestClient.RequestBodyUriSpec.class);
      when(mockRestClient.method(mockHttpMethod)).thenReturn(mockRestClientRequestBodyUriSpec);

      final var mockRestClientResponseSpec = mock(RestClient.ResponseSpec.class);
      when(mockRestClientRequestBodyUriSpec.retrieve()).thenReturn(mockRestClientResponseSpec);

      when(mockRestClientResponseSpec.toEntity(byte[].class)).thenReturn(mockResponseEntity);

      final var mockHeaders = mock(HttpHeaders.class);
      when(mockResponseEntity.getHeaders()).thenReturn(mockHeaders);
      final var mockResponseEntityBody = new byte[] {1, 2, 3};
      when(mockResponseEntity.getBody()).thenReturn(mockResponseEntityBody);

      when(mockHeaders.getContentType()).thenReturn(MediaType.TEXT_PLAIN);

      staticHttpStatus.when(() -> HttpStatus.valueOf(200)).thenReturn(mockHttpStatus);

      // Act
      final var result =
          CustomRestClient.retrieveData("example-path", "example-method", "example-headers", null);

      // Assert
      assertEquals(1, constructionDataWrapper.constructed().size());
      final var mockDataWrapper = constructionDataWrapper.constructed().getFirst();
      assertEquals(mockDataWrapper, result);

      assertEquals(mockResponseEntityBody, constructionDataWrapperArgs.get(mockDataWrapper).get(0));
      assertEquals(MediaType.TEXT_PLAIN, constructionDataWrapperArgs.get(mockDataWrapper).get(1));
      assertEquals(mockHttpStatus, constructionDataWrapperArgs.get(mockDataWrapper).get(2));

      staticHeaderExtractor.verify(
          () -> HeaderExtractor.extractHeader("example-headers"), times(1));
      staticRestClient.verify(() -> RestClient.builder(), times(1));
      verify(mockRestClientBuilder, times(1)).baseUrl("example-path");
      verify(mockRestClientBuilder, times(1)).defaultHeaders(mockHeaderConsumer);
      verify(mockRestClientBuilder, times(1)).build();
      verify(mockHeadersMap, times(0)).put(eq(HttpHeaders.ACCEPT), any());
      staticHttpMethod.verify(() -> HttpMethod.valueOf("example-method"), times(1));
      verify(mockRestClient, times(1)).method(mockHttpMethod);
      verify(mockRestClientRequestBodyUriSpec, times(1)).retrieve();
      verify(mockRestClientResponseSpec, times(1)).toEntity(byte[].class);
      verify(mockResponseEntity, times(1)).getHeaders();
      verify(mockHeaders, times(1)).getContentType();
      verify(mockResponseEntity, times(1)).getBody();
    }
  }

  @Test
  public void retrieveData_withAcceptHeader() {
    final var mockResponseEntity = mock(ResponseEntity.class);
    final var mockHttpStatus = mock(HttpStatus.class);
    final var mockHttpStatusCode = HttpStatusCode.valueOf(200);

    when(mockResponseEntity.getStatusCode()).thenReturn(mockHttpStatusCode);

    final var constructionDataWrapperArgs = new HashMap<DataWrapper, List<?>>();

    try (final var staticHeaderExtractor = mockStatic(HeaderExtractor.class);
        final var staticRestClient = mockStatic(RestClient.class);
        final var staticHttpMethod = mockStatic(HttpMethod.class);
        final var staticCustomRestClientHeaderConsumer =
            mockStatic(CustomRestClient.HeaderConsumer.class);
        final var constructionDataWrapper =
            mockConstruction(
                DataWrapper.class,
                (mock, ctx) -> {
                  constructionDataWrapperArgs.put(mock, ctx.arguments());
                });
        final var staticHttpStatus = mockStatic(HttpStatus.class)) {
      final var mockHeadersMap = mock(Map.class);
      staticHeaderExtractor
          .when(() -> HeaderExtractor.extractHeader("example-headers"))
          .thenReturn(mockHeadersMap);

      final var mockHeaderConsumer = mock(Consumer.class);
      staticCustomRestClientHeaderConsumer
          .when(() -> CustomRestClient.HeaderConsumer.createFromHeaders(any()))
          .thenReturn(mockHeaderConsumer);

      final var mockRestClientBuilder = mock(RestClient.Builder.class);
      staticRestClient.when(() -> RestClient.builder()).thenReturn(mockRestClientBuilder);

      when(mockRestClientBuilder.baseUrl("example-path")).thenReturn(mockRestClientBuilder);
      when(mockRestClientBuilder.defaultHeaders(mockHeaderConsumer))
          .thenReturn(mockRestClientBuilder);

      final var mockRestClient = mock(RestClient.class);
      when(mockRestClientBuilder.build()).thenReturn(mockRestClient);

      final var mockHttpMethod = mock(HttpMethod.class);
      staticHttpMethod.when(() -> HttpMethod.valueOf("example-method")).thenReturn(mockHttpMethod);

      final var mockRestClientRequestBodyUriSpec = mock(RestClient.RequestBodyUriSpec.class);
      when(mockRestClient.method(mockHttpMethod)).thenReturn(mockRestClientRequestBodyUriSpec);

      final var mockRestClientResponseSpec = mock(RestClient.ResponseSpec.class);
      when(mockRestClientRequestBodyUriSpec.retrieve()).thenReturn(mockRestClientResponseSpec);

      when(mockRestClientResponseSpec.toEntity(byte[].class)).thenReturn(mockResponseEntity);

      final var mockHeaders = mock(HttpHeaders.class);
      when(mockResponseEntity.getHeaders()).thenReturn(mockHeaders);

      final var mockResponseEntityBody = new byte[] {1, 2, 3};
      when(mockResponseEntity.getBody()).thenReturn(mockResponseEntityBody);

      when(mockHeaders.getContentType()).thenReturn(MediaType.TEXT_PLAIN);

      staticHttpStatus.when(() -> HttpStatus.valueOf(200)).thenReturn(mockHttpStatus);

      // Act
      final var result =
          CustomRestClient.retrieveData(
              "example-path", "example-method", "example-headers", "text/plain");

      // Assert
      assertEquals(1, constructionDataWrapper.constructed().size());
      final var mockDataWrapper = constructionDataWrapper.constructed().getFirst();
      assertEquals(mockDataWrapper, result);

      assertEquals(mockResponseEntityBody, constructionDataWrapperArgs.get(mockDataWrapper).get(0));
      assertEquals(MediaType.TEXT_PLAIN, constructionDataWrapperArgs.get(mockDataWrapper).get(1));
      assertEquals(mockHttpStatus, constructionDataWrapperArgs.get(mockDataWrapper).get(2));

      staticHeaderExtractor.verify(
          () -> HeaderExtractor.extractHeader("example-headers"), times(1));
      staticRestClient.verify(() -> RestClient.builder(), times(1));
      verify(mockRestClientBuilder, times(1)).baseUrl("example-path");
      verify(mockRestClientBuilder, times(1)).defaultHeaders(mockHeaderConsumer);
      verify(mockRestClientBuilder, times(1)).build();
      verify(mockHeadersMap, times(1)).put(HttpHeaders.ACCEPT, "text/plain");
      staticHttpMethod.verify(() -> HttpMethod.valueOf("example-method"), times(1));
      verify(mockRestClient, times(1)).method(mockHttpMethod);
      verify(mockRestClientRequestBodyUriSpec, times(1)).retrieve();
      verify(mockRestClientResponseSpec, times(1)).toEntity(byte[].class);
      verify(mockResponseEntity, times(1)).getHeaders();
      verify(mockHeaders, times(1)).getContentType();
      verify(mockResponseEntity, times(1)).getBody();
    }
  }

  @Test
  public void retrieveData_whenResponseContentTypeIsUndefined_assignContentTypeAsNull() {
    final var mockResponseEntity = mock(ResponseEntity.class);
    final var mockHttpStatus = mock(HttpStatus.class);
    final var mockHttpStatusCode = HttpStatusCode.valueOf(200);

    when(mockResponseEntity.getStatusCode()).thenReturn(mockHttpStatusCode);

    final var constructionDataWrapperArgs = new HashMap<DataWrapper, List<?>>();

    try (final var staticHeaderExtractor = mockStatic(HeaderExtractor.class);
        final var staticRestClient = mockStatic(RestClient.class);
        final var staticHttpMethod = mockStatic(HttpMethod.class);
        final var staticCustomRestClientHeaderConsumer =
            mockStatic(CustomRestClient.HeaderConsumer.class);
        final var constructionDataWrapper =
            mockConstruction(
                DataWrapper.class,
                (mock, ctx) -> {
                  constructionDataWrapperArgs.put(mock, ctx.arguments());
                });
        final var staticHttpStatus = mockStatic(HttpStatus.class)) {
      final var mockHeadersMap = mock(Map.class);
      staticHeaderExtractor
          .when(() -> HeaderExtractor.extractHeader("example-headers"))
          .thenReturn(mockHeadersMap);

      final var mockHeaderConsumer = mock(Consumer.class);
      staticCustomRestClientHeaderConsumer
          .when(() -> CustomRestClient.HeaderConsumer.createFromHeaders(any()))
          .thenReturn(mockHeaderConsumer);

      final var mockRestClientBuilder = mock(RestClient.Builder.class);
      staticRestClient.when(() -> RestClient.builder()).thenReturn(mockRestClientBuilder);

      when(mockRestClientBuilder.baseUrl("example-path")).thenReturn(mockRestClientBuilder);
      when(mockRestClientBuilder.defaultHeaders(mockHeaderConsumer))
          .thenReturn(mockRestClientBuilder);

      final var mockRestClient = mock(RestClient.class);
      when(mockRestClientBuilder.build()).thenReturn(mockRestClient);

      final var mockHttpMethod = mock(HttpMethod.class);
      staticHttpMethod.when(() -> HttpMethod.valueOf("example-method")).thenReturn(mockHttpMethod);

      final var mockRestClientRequestBodyUriSpec = mock(RestClient.RequestBodyUriSpec.class);
      when(mockRestClient.method(mockHttpMethod)).thenReturn(mockRestClientRequestBodyUriSpec);

      final var mockRestClientResponseSpec = mock(RestClient.ResponseSpec.class);
      when(mockRestClientRequestBodyUriSpec.retrieve()).thenReturn(mockRestClientResponseSpec);

      final var mockResponseEntityBody = new byte[] {1, 2, 3};
      when(mockResponseEntity.getBody()).thenReturn(mockResponseEntityBody);

      when(mockRestClientResponseSpec.toEntity(byte[].class)).thenReturn(mockResponseEntity);

      final var mockHeaders = mock(HttpHeaders.class);
      when(mockResponseEntity.getHeaders()).thenReturn(mockHeaders);

      when(mockHeaders.getContentType()).thenReturn(null);

      staticHttpStatus.when(() -> HttpStatus.valueOf(200)).thenReturn(mockHttpStatus);

      // Act
      final var result =
          CustomRestClient.retrieveData("example-path", "example-method", "example-headers", null);

      // Assert
      assertEquals(1, constructionDataWrapper.constructed().size());
      final var mockDataWrapper = constructionDataWrapper.constructed().getFirst();
      assertEquals(mockDataWrapper, result);

      assertEquals(mockResponseEntityBody, constructionDataWrapperArgs.get(mockDataWrapper).get(0));
      assertNull(constructionDataWrapperArgs.get(mockDataWrapper).get(1));
      assertEquals(mockHttpStatus, constructionDataWrapperArgs.get(mockDataWrapper).get(2));

      staticHeaderExtractor.verify(
          () -> HeaderExtractor.extractHeader("example-headers"), times(1));
      staticRestClient.verify(() -> RestClient.builder(), times(1));
      verify(mockRestClientBuilder, times(1)).baseUrl("example-path");
      verify(mockRestClientBuilder, times(1)).defaultHeaders(mockHeaderConsumer);
      verify(mockRestClientBuilder, times(1)).build();
      verify(mockHeadersMap, times(0)).put(eq(HttpHeaders.ACCEPT), any());
      staticHttpMethod.verify(() -> HttpMethod.valueOf("example-method"), times(1));
      verify(mockRestClient, times(1)).method(mockHttpMethod);
      verify(mockRestClientRequestBodyUriSpec, times(1)).retrieve();
      verify(mockRestClientResponseSpec, times(1)).toEntity(byte[].class);
      verify(mockResponseEntity, times(1)).getHeaders();
      verify(mockHeaders, times(1)).getContentType();
      verify(mockResponseEntity, times(1)).getBody();
    }
  }

  @Test
  public void retrieveData_whenResponseContentTypeContainWildcard_assignContentTypeAsNull() {
    final var mockResponseEntity = mock(ResponseEntity.class);
    final var mockHttpStatus = mock(HttpStatus.class);
    final var mockHttpStatusCode = HttpStatusCode.valueOf(200);

    when(mockResponseEntity.getStatusCode()).thenReturn(mockHttpStatusCode);

    final var constructionDataWrapperArgs = new HashMap<DataWrapper, List<?>>();

    try (final var staticHeaderExtractor = mockStatic(HeaderExtractor.class);
        final var staticRestClient = mockStatic(RestClient.class);
        final var staticHttpMethod = mockStatic(HttpMethod.class);
        final var staticCustomRestClientHeaderConsumer =
            mockStatic(CustomRestClient.HeaderConsumer.class);
        final var constructionDataWrapper =
            mockConstruction(
                DataWrapper.class,
                (mock, ctx) -> {
                  constructionDataWrapperArgs.put(mock, ctx.arguments());
                });
        final var staticHttpStatus = mockStatic(HttpStatus.class)) {
      final var mockHeadersMap = mock(Map.class);
      staticHeaderExtractor
          .when(() -> HeaderExtractor.extractHeader("example-headers"))
          .thenReturn(mockHeadersMap);

      final var mockHeaderConsumer = mock(Consumer.class);
      staticCustomRestClientHeaderConsumer
          .when(() -> CustomRestClient.HeaderConsumer.createFromHeaders(any()))
          .thenReturn(mockHeaderConsumer);

      final var mockRestClientBuilder = mock(RestClient.Builder.class);
      staticRestClient.when(() -> RestClient.builder()).thenReturn(mockRestClientBuilder);

      when(mockRestClientBuilder.baseUrl("example-path")).thenReturn(mockRestClientBuilder);
      when(mockRestClientBuilder.defaultHeaders(mockHeaderConsumer))
          .thenReturn(mockRestClientBuilder);

      final var mockRestClient = mock(RestClient.class);
      when(mockRestClientBuilder.build()).thenReturn(mockRestClient);

      final var mockHttpMethod = mock(HttpMethod.class);
      staticHttpMethod.when(() -> HttpMethod.valueOf("example-method")).thenReturn(mockHttpMethod);

      final var mockRestClientRequestBodyUriSpec = mock(RestClient.RequestBodyUriSpec.class);
      when(mockRestClient.method(mockHttpMethod)).thenReturn(mockRestClientRequestBodyUriSpec);

      final var mockRestClientResponseSpec = mock(RestClient.ResponseSpec.class);
      when(mockRestClientRequestBodyUriSpec.retrieve()).thenReturn(mockRestClientResponseSpec);

      final var mockResponseEntityBody = new byte[] {1, 2, 3};
      when(mockResponseEntity.getBody()).thenReturn(mockResponseEntityBody);

      when(mockRestClientResponseSpec.toEntity(byte[].class)).thenReturn(mockResponseEntity);

      final var mockHeaders = mock(HttpHeaders.class);
      when(mockResponseEntity.getHeaders()).thenReturn(mockHeaders);

      when(mockHeaders.getContentType()).thenReturn(MediaType.ALL);

      staticHttpStatus.when(() -> HttpStatus.valueOf(200)).thenReturn(mockHttpStatus);

      // Act
      final var result =
          CustomRestClient.retrieveData("example-path", "example-method", "example-headers", null);

      // Assert
      assertEquals(1, constructionDataWrapper.constructed().size());
      final var mockDataWrapper = constructionDataWrapper.constructed().getFirst();
      assertEquals(mockDataWrapper, result);

      assertEquals(mockResponseEntityBody, constructionDataWrapperArgs.get(mockDataWrapper).get(0));
      assertNull(constructionDataWrapperArgs.get(mockDataWrapper).get(1));
      assertEquals(mockHttpStatus, constructionDataWrapperArgs.get(mockDataWrapper).get(2));

      staticHeaderExtractor.verify(
          () -> HeaderExtractor.extractHeader("example-headers"), times(1));
      staticRestClient.verify(() -> RestClient.builder(), times(1));
      verify(mockRestClientBuilder, times(1)).baseUrl("example-path");
      verify(mockRestClientBuilder, times(1)).defaultHeaders(mockHeaderConsumer);
      verify(mockRestClientBuilder, times(1)).build();
      verify(mockHeadersMap, times(0)).put(eq(HttpHeaders.ACCEPT), any());
      staticHttpMethod.verify(() -> HttpMethod.valueOf("example-method"), times(1));
      verify(mockRestClient, times(1)).method(mockHttpMethod);
      verify(mockRestClientRequestBodyUriSpec, times(1)).retrieve();
      verify(mockRestClientResponseSpec, times(1)).toEntity(byte[].class);
      verify(mockResponseEntity, times(1)).getHeaders();
      verify(mockHeaders, times(1)).getContentType();
      verify(mockResponseEntity, times(1)).getBody();
    }
  }
}
