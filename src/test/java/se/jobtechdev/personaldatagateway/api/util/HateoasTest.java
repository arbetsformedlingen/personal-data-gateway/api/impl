package se.jobtechdev.personaldatagateway.api.util;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.web.context.request.NativeWebRequest;
import se.jobtechdev.personaldatagateway.api.generated.model.Link;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class HateoasTest {
  @Test
  public void extractRequestUri() {
    // Arrange
    final var request = mock(NativeWebRequest.class);
    when(request.getDescription(false)).thenReturn("uri=/foo?bar=baz");

    // Act
    final var optionalUri = Hateoas.extractRequestUri(request, "https://example.com");

    assertTrue(optionalUri.isPresent());
    final var uri = optionalUri.get();

    // Assert
    assertEquals("https://example.com/foo?bar=baz", uri);
  }

  @Test
  public void extractRequestUri_whenUriIsNotPresent_returnOptionalEmpty() {
    // Arrange
    final var request = mock(NativeWebRequest.class);
    when(request.getDescription(false)).thenReturn("foo=bar");

    // Act
    final var optionalUri = Hateoas.extractRequestUri(request, "https://example.com");

    assertTrue(optionalUri.isEmpty());
  }

  @Test
  public void extractQueryParametersFromParameterMap() {
    // Arrange
    Map<String, String[]> parameterMap = Map.of("foo", new String[]{"bar", "baz"});

    // Act
    final var queryParams = Hateoas.extractQueryParametersFromParameterMap(parameterMap);

    // Assert
    assertEquals("?foo=bar,baz", queryParams);
  }

  @Test
  public void extractQueryParametersFromParameterMap_whenParameterMapIsEmpty_returnEmptyString() {
    // Arrange
    Map<String, String[]> parameterMap = Map.of();

    // Act
    final var queryParams = Hateoas.extractQueryParametersFromParameterMap(parameterMap);

    // Assert
    assertTrue(queryParams.isEmpty());
  }

  @Test
  public void linkMap() {
    // Arrange
    final var linkConstructionArgs = new HashMap<Link, List<?>>();
    try (final var linkConstruction =
             mockConstruction(
                 Link.class,
                 (mock, context) -> {
                   linkConstructionArgs.put(mock, context.arguments());
                 });
         final var staticUrlFactory = mockStatic(UrlFactory.class)) {
      final var request = mock(NativeWebRequest.class);
      when(request.getDescription(false)).thenReturn("uri=/foo");
      when(request.getParameterMap()).thenReturn(Map.of("foo", new String[]{"bar", "baz"}));
      final var mockURL = mock(URL.class);
      staticUrlFactory
          .when(() -> UrlFactory.createURL("https://example.com/foo?foo=bar,baz"))
          .thenReturn(mockURL);

      // Act
      final var result = Hateoas.linkMap(request, "https://example.com");

      // Assert
      assertEquals(1, linkConstruction.constructed().size());
      final var mockLink = linkConstruction.constructed().get(0);

      assertEquals(1, linkConstructionArgs.get(mockLink).size());
      assertEquals(mockURL, linkConstructionArgs.get(mockLink).get(0));

      assertEquals(1, result.size());
      assertEquals(mockLink, result.get("self"));
      staticUrlFactory.verify(
          () -> UrlFactory.createURL("https://example.com/foo?foo=bar,baz"), times(1));
    }
  }

  @Test
  public void linkMap_whenRequestUriIsNotPresent_thenReturnAnEmptyMap() {
    // Arrange
    final var linkConstructionArgs = new HashMap<Link, List<?>>();
    try (final var linkConstruction =
             mockConstruction(
                 Link.class,
                 (mock, context) -> {
                   linkConstructionArgs.put(mock, context.arguments());
                 })) {
      final var request = mock(NativeWebRequest.class);
      when(request.getDescription(false)).thenReturn("");
      when(request.getParameterMap()).thenReturn(Map.of("foo", new String[]{"bar", "baz"}));

      // Act
      final var result = Hateoas.linkMap(request, "https://example.com");

      // Assert
      assertEquals(0, result.size());
      assertEquals(0, linkConstruction.constructed().size());
    }
  }

  @Test
  public void linkHeader() {
    // Arrange
    try (final var staticUrlFactory = mockStatic(UrlFactory.class)) {
      final var request = mock(NativeWebRequest.class);
      when(request.getDescription(false)).thenReturn("uri=/foo");
      when(request.getParameterMap()).thenReturn(Map.of("foo", new String[]{"bar", "baz"}));
      final var mockURL = mock(URL.class);
      staticUrlFactory
          .when(() -> UrlFactory.createURL("https://example.com/foo?foo=bar,baz"))
          .thenReturn(mockURL);
      final var expectedResult = new HttpHeaders();
      expectedResult.add("Link", "<https://example.com/foo?foo=bar,baz>; rel=\"self\"");

      // Act
      final var result = Hateoas.linkHeader(request, "https://example.com");

      // Assert
      assertEquals(expectedResult, result);
    }
  }

  @Test
  public void linkHeader_whenRequestUriIsNotPresent_thenReturnAnEmptyString() {
    // Arrange
    try (final var staticUrlFactory = mockStatic(UrlFactory.class)) {
      final var request = mock(NativeWebRequest.class);
      when(request.getDescription(false)).thenReturn("");
      when(request.getParameterMap()).thenReturn(Map.of("foo", new String[]{"bar", "baz"}));
      final var mockURL = mock(URL.class);
      staticUrlFactory
          .when(() -> UrlFactory.createURL("https://example.com/foo?foo=bar,baz"))
          .thenReturn(mockURL);

      final var expectedResult = new HttpHeaders();

      // Act
      final var result = Hateoas.linkHeader(request, "https://example.com");

      // Assert
      assertEquals(expectedResult, result);
    }
  }
}
