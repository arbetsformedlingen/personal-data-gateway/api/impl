package se.jobtechdev.personaldatagateway.api.util;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.Base64;
import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.BadCredentialsException;

public class AuthUtilTest {
  @Test
  public void assertAuthKeyExists_whenAuthKeyIsNull_throwBadCredentialsException() {
    final var thrown =
        assertThrows(BadCredentialsException.class, () -> AuthUtil.assertAuthKeyExists(null));
    assertEquals("Bad Request Header Credentials, provided authKey is null", thrown.getMessage());
  }

  @Test
  public void assertAuthKeyExists_whenAuthKeyIsEmpty_throwBadCredentialsException() {
    final var thrown =
        assertThrows(BadCredentialsException.class, () -> AuthUtil.assertAuthKeyExists(""));
    assertEquals("Bad Request Header Credentials, provided authKey is empty", thrown.getMessage());
  }

  @Test
  public void assertAuthKeyExists() {
    assertDoesNotThrow(() -> AuthUtil.assertAuthKeyExists("abc"));
  }

  @Test
  public void decodeAuthKey_whenAuthKeyIsNotAValidBase64Scheme_throwBadCredentialsException() {
    final var mockAuthKey = "abc";
    final var mockDecodedAuthKey = new byte[] {1, 2, 3};
    final var mockBase64Decoder = mock(Base64.Decoder.class);

    when(mockBase64Decoder.decode(mockAuthKey)).thenThrow(RuntimeException.class);

    try (final var staticBase64 = mockStatic(Base64.class)) {
      staticBase64.when(Base64::getDecoder).thenReturn(mockBase64Decoder);

      final var thrown =
          assertThrows(BadCredentialsException.class, () -> AuthUtil.decodeAuthKey(mockAuthKey));

      assertEquals(
          "Bad Request Header Credentials, provided authKey could not be decoded",
          thrown.getMessage());
    }
  }

  @Test
  public void decodeAuthKey() {
    final var mockAuthKey = "abc";
    final var mockDecodedAuthKey = new byte[] {1, 2, 3};
    final var mockBase64Decoder = mock(Base64.Decoder.class);

    when(mockBase64Decoder.decode(mockAuthKey)).thenReturn(mockDecodedAuthKey);

    try (final var staticBase64 = mockStatic(Base64.class)) {
      staticBase64.when(Base64::getDecoder).thenReturn(mockBase64Decoder);

      final var decodedAuthKey = AuthUtil.decodeAuthKey(mockAuthKey);

      assertEquals(mockDecodedAuthKey, decodedAuthKey);
    }
  }

  @Test
  public void hashDecodedAuthKey() {
    final var mockDecodedAuthKey = new byte[] {1, 2, 3};
    final var mockHash = new byte[] {4, 5, 6};

    try (final var staticMessageDigestProvider = mockStatic(MessageDigestProvider.class)) {
      staticMessageDigestProvider
          .when(() -> MessageDigestProvider.sha256(mockDecodedAuthKey))
          .thenReturn(mockHash);

      final var hash = AuthUtil.hashDecodedAuthKey(mockDecodedAuthKey);

      assertEquals(mockHash, hash);
    }
  }

  @Test
  public void hashDecodedAuthKey_whenHashingFailed_throwBadCredentialsException() {
    final var mockDecodedAuthKey = new byte[] {1, 2, 3};

    try (final var staticMessageDigestProvider = mockStatic(MessageDigestProvider.class)) {
      staticMessageDigestProvider
          .when(() -> MessageDigestProvider.sha256(mockDecodedAuthKey))
          .thenThrow(RuntimeException.class);

      final var thrown =
          assertThrows(
              BadCredentialsException.class, () -> AuthUtil.hashDecodedAuthKey(mockDecodedAuthKey));

      assertEquals(
          "Bad Request Header Credentials, base64 decoded authKey could not be hashed",
          thrown.getMessage());
    }
  }
}
