package se.jobtechdev.personaldatagateway.api.util;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;

public class UrlFactoryTest {
  @Test
  @Tag("UnitTest")
  public void createURL_whenPassedNull_returnsNull() {
    assertNull(UrlFactory.createURL(null));
  }

  @Test
  @Tag("UnitTest")
  public void
  createURL_whenPassedAnInvalidUrl_throwsRuntimeExceptionWithCauseMalformedURLException() {
    final var thrown =
        assertThrows(RuntimeException.class, () -> UrlFactory.createURL("invalid://example.com"));
    assertEquals(MalformedURLException.class, thrown.getCause().getClass());
  }

  @Test
  @Tag("IntegrationTest")
  public void createURL() throws MalformedURLException {
    URL url = UrlFactory.createURL("http://example.url:8080/foo?bar=baz");
    assertEquals("http", url.getProtocol());
    assertEquals("example.url", url.getHost());
    assertEquals(8080, url.getPort());
    assertEquals("/foo", url.getPath());
    assertEquals("bar=baz", url.getQuery());
  }

  @Test
  @Tag("IntegrationTest")
  public void createURL_whenInvalidUrlIsProvided_thenThrowIllegalArgumentException() {
    final var thrown =
        assertThrows(IllegalArgumentException.class, () -> UrlFactory.createURL("invalid-url"));
    assertEquals("URI is not absolute", thrown.getMessage());
  }
}
