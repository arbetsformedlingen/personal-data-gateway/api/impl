package se.jobtechdev.personaldatagateway.api.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockConstruction;

import java.util.HashMap;
import java.util.List;
import org.junit.jupiter.api.Test;
import se.jobtechdev.personaldatagateway.api.exception.ApiException;
import se.jobtechdev.personaldatagateway.api.generated.model.ErrorResponse;

public class ApiExceptionFactoryTest {
  @Test
  public void createApiException() {
    // Arrange
    final var errorResponse = mock(ErrorResponse.class);

    final var apiExceptionArgs = new HashMap<ApiException, List<?>>();
    try (final var apiExceptionConstructor =
        mockConstruction(
            ApiException.class, (mock, ctx) -> apiExceptionArgs.put(mock, ctx.arguments()))) {
      // Act
      final var result = ApiExceptionFactory.createApiException(errorResponse);

      // Assert
      assertEquals(1, apiExceptionConstructor.constructed().size());
      final var mockApiException = apiExceptionConstructor.constructed().getFirst();
      assertEquals(errorResponse, apiExceptionArgs.get(mockApiException).getFirst());
    }
  }
}
