package se.jobtechdev.personaldatagateway.api.util;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.web.context.request.NativeWebRequest;
import se.jobtechdev.personaldatagateway.api.generated.entities.*;
import se.jobtechdev.personaldatagateway.api.generated.model.*;

import java.net.URL;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class ResponseFactoryTest {
  @Test
  public void createGetDataAccessResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockLinkMap = mock(Map.class);
    final var mockSharing = mock(Sharing.class);
    final var mockSharingId = mock(UUID.class);
    final var mockSharingAccess = mock(SharingAccess.class);
    final var mockSharingAccessId = mock(UUID.class);
    final var mockSharingAccessCreated = mock(ZonedDateTime.class);
    final var mockSharingAccessUpdated = mock(ZonedDateTime.class);
    final var mockSharingAccessAccessed = mock(ZonedDateTime.class);

    when(mockSharing.getId()).thenReturn(mockSharingId);
    when(mockSharingAccess.getId()).thenReturn(mockSharingAccessId);
    when(mockSharingAccess.getSharing()).thenReturn(mockSharing);
    when(mockSharingAccess.getCreated()).thenReturn(mockSharingAccessCreated);
    when(mockSharingAccess.getUpdated()).thenReturn(mockSharingAccessUpdated);
    when(mockSharingAccess.getAccessed()).thenReturn(mockSharingAccessAccessed);

    final var constructionGetDataAccessResponseArgs = new HashMap<GetDataAccessResponse, List<?>>();

    try (final var constructionGetDataAccessResponse =
             mockConstruction(
                 GetDataAccessResponse.class,
                 (mock, ctx) -> constructionGetDataAccessResponseArgs.put(mock, ctx.arguments()));
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createGetDataAccessResponse(mockRequest, mockBaseUrl, mockSharingAccess);

      // Assert
      assertEquals(1, constructionGetDataAccessResponse.constructed().size());

      final var mockGetDataAccessResponse =
          constructionGetDataAccessResponse.constructed().getFirst();

      assertEquals(6, constructionGetDataAccessResponseArgs.get(mockGetDataAccessResponse).size());

      assertEquals(
          mockLinkMap, constructionGetDataAccessResponseArgs.get(mockGetDataAccessResponse).get(0));
      assertEquals(
          mockSharingAccessId,
          constructionGetDataAccessResponseArgs.get(mockGetDataAccessResponse).get(1));
      assertEquals(
          mockSharingId,
          constructionGetDataAccessResponseArgs.get(mockGetDataAccessResponse).get(2));
      assertEquals(
          mockSharingAccessCreated,
          constructionGetDataAccessResponseArgs.get(mockGetDataAccessResponse).get(3));
      assertEquals(
          mockSharingAccessUpdated,
          constructionGetDataAccessResponseArgs.get(mockGetDataAccessResponse).get(4));
      assertEquals(
          mockSharingAccessAccessed,
          constructionGetDataAccessResponseArgs.get(mockGetDataAccessResponse).get(5));

      assertEquals(mockGetDataAccessResponse, response);
    }
  }

  @Test
  public void createPostOrganizationResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockLinkMap = mock(Map.class);
    final var mockOrganization = mock(Organization.class);
    final var mockOrganizationId = mock(UUID.class);
    final var mockOrganizationOrgNr = "mock-organization-org-nr";
    final var mockOrganizationName = "mock-organization-name";
    final var mockOrganizationCreated = mock(ZonedDateTime.class);
    final var mockOrganizationUpdated = mock(ZonedDateTime.class);
    final var mockOrganizationDeleted = mock(ZonedDateTime.class);

    when(mockOrganization.getId()).thenReturn(mockOrganizationId);
    when(mockOrganization.getOrgNr()).thenReturn(mockOrganizationOrgNr);
    when(mockOrganization.getName()).thenReturn(mockOrganizationName);
    when(mockOrganization.getCreated()).thenReturn(mockOrganizationCreated);
    when(mockOrganization.getUpdated()).thenReturn(mockOrganizationUpdated);
    when(mockOrganization.getDeleted()).thenReturn(mockOrganizationDeleted);

    final var constructionPostOrganizationResponseArgs =
        new HashMap<PostOrganizationResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionPostOrganizationResponse =
             mockConstruction(
                 PostOrganizationResponse.class,
                 (mock, ctx) ->
                     constructionPostOrganizationResponseArgs.put(mock, ctx.arguments()))) {
      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createPostOrganizationResponse(
              mockRequest, mockBaseUrl, mockOrganization);

      // Assert
      staticHateoas.verify(() -> Hateoas.linkMap(mockRequest, mockBaseUrl), times(1));

      assertEquals(1, constructionPostOrganizationResponse.constructed().size());

      final var mockPostOrganizationResponse =
          constructionPostOrganizationResponse.constructed().getFirst();

      assertEquals(
          mockLinkMap,
          constructionPostOrganizationResponseArgs.get(mockPostOrganizationResponse).get(0));
      assertEquals(
          mockOrganizationId,
          constructionPostOrganizationResponseArgs.get(mockPostOrganizationResponse).get(1));
      assertEquals(
          mockOrganizationOrgNr,
          constructionPostOrganizationResponseArgs.get(mockPostOrganizationResponse).get(2));
      assertEquals(
          mockOrganizationName,
          constructionPostOrganizationResponseArgs.get(mockPostOrganizationResponse).get(3));
      assertEquals(
          mockOrganizationCreated,
          constructionPostOrganizationResponseArgs.get(mockPostOrganizationResponse).get(4));
      assertEquals(
          mockOrganizationUpdated,
          constructionPostOrganizationResponseArgs.get(mockPostOrganizationResponse).get(5));

      verify(mockPostOrganizationResponse, times(1)).setDeleted(mockOrganization.getDeleted());

      assertEquals(mockPostOrganizationResponse, response);
    }
  }

  @Test
  public void createGetOrganizationResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockLinkMap = mock(Map.class);
    final var mockOrganization = mock(Organization.class);
    final var mockOrganizationId = mock(UUID.class);
    final var mockOrganizationOrgNr = "mock-organization-org-nr";
    final var mockOrganizationName = "mock-organization-name";
    final var mockOrganizationCreated = mock(ZonedDateTime.class);
    final var mockOrganizationUpdated = mock(ZonedDateTime.class);
    final var mockOrganizationDeleted = mock(ZonedDateTime.class);

    when(mockOrganization.getId()).thenReturn(mockOrganizationId);
    when(mockOrganization.getOrgNr()).thenReturn(mockOrganizationOrgNr);
    when(mockOrganization.getName()).thenReturn(mockOrganizationName);
    when(mockOrganization.getCreated()).thenReturn(mockOrganizationCreated);
    when(mockOrganization.getUpdated()).thenReturn(mockOrganizationUpdated);
    when(mockOrganization.getDeleted()).thenReturn(mockOrganizationDeleted);

    final var constructionGetOrganizationResponseArgs =
        new HashMap<GetOrganizationResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionPostOrganizationResponse =
             mockConstruction(
                 GetOrganizationResponse.class,
                 (mock, ctx) ->
                     constructionGetOrganizationResponseArgs.put(mock, ctx.arguments()))) {
      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createGetOrganizationResponse(mockRequest, mockBaseUrl, mockOrganization);

      // Assert
      staticHateoas.verify(() -> Hateoas.linkMap(mockRequest, mockBaseUrl), times(1));

      assertEquals(1, constructionPostOrganizationResponse.constructed().size());

      final var mockGetOrganizationResponse =
          constructionPostOrganizationResponse.constructed().getFirst();

      assertEquals(
          mockLinkMap,
          constructionGetOrganizationResponseArgs.get(mockGetOrganizationResponse).get(0));
      assertEquals(
          mockOrganizationId,
          constructionGetOrganizationResponseArgs.get(mockGetOrganizationResponse).get(1));
      assertEquals(
          mockOrganizationOrgNr,
          constructionGetOrganizationResponseArgs.get(mockGetOrganizationResponse).get(2));
      assertEquals(
          mockOrganizationName,
          constructionGetOrganizationResponseArgs.get(mockGetOrganizationResponse).get(3));
      assertEquals(
          mockOrganizationCreated,
          constructionGetOrganizationResponseArgs.get(mockGetOrganizationResponse).get(4));
      assertEquals(
          mockOrganizationUpdated,
          constructionGetOrganizationResponseArgs.get(mockGetOrganizationResponse).get(5));

      verify(mockGetOrganizationResponse, times(1)).setDeleted(mockOrganization.getDeleted());

      assertEquals(mockGetOrganizationResponse, response);
    }
  }

  @Test
  public void createPatchOrganizationResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockLinkMap = mock(Map.class);
    final var mockOrganization = mock(Organization.class);
    final var mockOrganizationId = mock(UUID.class);
    final var mockOrganizationOrgNr = "mock-organization-org-nr";
    final var mockOrganizationName = "mock-organization-name";
    final var mockOrganizationCreated = mock(ZonedDateTime.class);
    final var mockOrganizationUpdated = mock(ZonedDateTime.class);
    final var mockOrganizationDeleted = mock(ZonedDateTime.class);

    when(mockOrganization.getId()).thenReturn(mockOrganizationId);
    when(mockOrganization.getOrgNr()).thenReturn(mockOrganizationOrgNr);
    when(mockOrganization.getName()).thenReturn(mockOrganizationName);
    when(mockOrganization.getCreated()).thenReturn(mockOrganizationCreated);
    when(mockOrganization.getUpdated()).thenReturn(mockOrganizationUpdated);
    when(mockOrganization.getDeleted()).thenReturn(mockOrganizationDeleted);

    final var constructionPatchOrganizationResponseArgs =
        new HashMap<PatchOrganizationResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionPatchOrganizationResponse =
             mockConstruction(
                 PatchOrganizationResponse.class,
                 (mock, ctx) ->
                     constructionPatchOrganizationResponseArgs.put(mock, ctx.arguments()))) {
      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createPatchOrganizationResponse(
              mockRequest, mockBaseUrl, mockOrganization);

      // Assert
      staticHateoas.verify(() -> Hateoas.linkMap(mockRequest, mockBaseUrl), times(1));

      assertEquals(1, constructionPatchOrganizationResponse.constructed().size());

      final var mockPatchOrganizationResponse =
          constructionPatchOrganizationResponse.constructed().getFirst();

      assertEquals(
          mockLinkMap,
          constructionPatchOrganizationResponseArgs.get(mockPatchOrganizationResponse).get(0));
      assertEquals(
          mockOrganizationId,
          constructionPatchOrganizationResponseArgs.get(mockPatchOrganizationResponse).get(1));
      assertEquals(
          mockOrganizationOrgNr,
          constructionPatchOrganizationResponseArgs.get(mockPatchOrganizationResponse).get(2));
      assertEquals(
          mockOrganizationName,
          constructionPatchOrganizationResponseArgs.get(mockPatchOrganizationResponse).get(3));
      assertEquals(
          mockOrganizationCreated,
          constructionPatchOrganizationResponseArgs.get(mockPatchOrganizationResponse).get(4));
      assertEquals(
          mockOrganizationUpdated,
          constructionPatchOrganizationResponseArgs.get(mockPatchOrganizationResponse).get(5));

      verify(mockPatchOrganizationResponse, times(1)).setDeleted(mockOrganization.getDeleted());

      assertEquals(mockPatchOrganizationResponse, response);
    }
  }

  @Test
  public void createPostOrganizationFaqResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockLinkMap = mock(Map.class);
    final var mockOrganization = mock(Organization.class);
    final var mockOrganizationId = mock(UUID.class);
    final var mockFaq = mock(Faq.class);
    final var mockFaqId = mock(UUID.class);
    final var mockFaqLang = "mock-faq-lang";
    final var mockFaqTitle = "mock-faq-title";
    final var mockFaqContent = "mock-faq-content";
    final var mockFaqCreated = mock(ZonedDateTime.class);
    final var mockFaqUpdated = mock(ZonedDateTime.class);
    final var mockFaqDeleted = mock(ZonedDateTime.class);

    when(mockFaq.getId()).thenReturn(mockFaqId);
    when(mockFaq.getLang()).thenReturn(mockFaqLang);
    when(mockFaq.getTitle()).thenReturn(mockFaqTitle);
    when(mockFaq.getContent()).thenReturn(mockFaqContent);
    when(mockFaq.getCreated()).thenReturn(mockFaqCreated);
    when(mockFaq.getUpdated()).thenReturn(mockFaqUpdated);
    when(mockFaq.getDeleted()).thenReturn(mockFaqDeleted);

    when(mockOrganization.getId()).thenReturn(mockOrganizationId);

    final var constructionPostOrganizationFaqResponseArgs =
        new HashMap<PostOrganizationFaqResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionPostOrganizationFaqResponse =
             mockConstruction(
                 PostOrganizationFaqResponse.class,
                 (mock, ctx) ->
                     constructionPostOrganizationFaqResponseArgs.put(mock, ctx.arguments()))) {
      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createPostOrganizationFaqResponse(
              mockRequest, mockBaseUrl, mockOrganizationId, mockFaq);

      // Assert
      staticHateoas.verify(() -> Hateoas.linkMap(mockRequest, mockBaseUrl), times(1));

      assertEquals(1, constructionPostOrganizationFaqResponse.constructed().size());

      final var mockPostOrganizationFaqResponse =
          constructionPostOrganizationFaqResponse.constructed().getFirst();

      assertEquals(
          mockLinkMap,
          constructionPostOrganizationFaqResponseArgs.get(mockPostOrganizationFaqResponse).get(0));
      assertEquals(
          mockFaqId,
          constructionPostOrganizationFaqResponseArgs.get(mockPostOrganizationFaqResponse).get(1));
      assertEquals(
          mockOrganizationId,
          constructionPostOrganizationFaqResponseArgs.get(mockPostOrganizationFaqResponse).get(2));
      assertEquals(
          mockFaqLang,
          constructionPostOrganizationFaqResponseArgs.get(mockPostOrganizationFaqResponse).get(3));
      assertEquals(
          mockFaqTitle,
          constructionPostOrganizationFaqResponseArgs.get(mockPostOrganizationFaqResponse).get(4));
      assertEquals(
          mockFaqContent,
          constructionPostOrganizationFaqResponseArgs.get(mockPostOrganizationFaqResponse).get(5));
      assertEquals(
          mockFaqCreated,
          constructionPostOrganizationFaqResponseArgs.get(mockPostOrganizationFaqResponse).get(6));
      assertEquals(
          mockFaqUpdated,
          constructionPostOrganizationFaqResponseArgs.get(mockPostOrganizationFaqResponse).get(7));

      verify(mockPostOrganizationFaqResponse, times(1)).setDeleted(mockFaq.getDeleted());

      assertEquals(mockPostOrganizationFaqResponse, response);
    }
  }

  @Test
  public void createGetOrganizationFaqResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockLinkMap = mock(Map.class);
    final var mockOrganization = mock(Organization.class);
    final var mockOrganizationId = mock(UUID.class);
    final var mockFaq = mock(Faq.class);
    final var mockFaqId = mock(UUID.class);
    final var mockFaqLang = "mock-faq-lang";
    final var mockFaqTitle = "mock-faq-title";
    final var mockFaqContent = "mock-faq-content";
    final var mockFaqCreated = mock(ZonedDateTime.class);
    final var mockFaqUpdated = mock(ZonedDateTime.class);
    final var mockFaqDeleted = mock(ZonedDateTime.class);

    when(mockFaq.getId()).thenReturn(mockFaqId);
    when(mockFaq.getLang()).thenReturn(mockFaqLang);
    when(mockFaq.getTitle()).thenReturn(mockFaqTitle);
    when(mockFaq.getContent()).thenReturn(mockFaqContent);
    when(mockFaq.getCreated()).thenReturn(mockFaqCreated);
    when(mockFaq.getUpdated()).thenReturn(mockFaqUpdated);
    when(mockFaq.getDeleted()).thenReturn(mockFaqDeleted);

    when(mockOrganization.getId()).thenReturn(mockOrganizationId);

    final var constructionGetOrganizationFaqResponseArgs =
        new HashMap<GetOrganizationFaqResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionGetOrganizationFaqResponse =
             mockConstruction(
                 GetOrganizationFaqResponse.class,
                 (mock, ctx) ->
                     constructionGetOrganizationFaqResponseArgs.put(mock, ctx.arguments()))) {
      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createGetOrganizationFaqResponse(
              mockRequest, mockBaseUrl, mockOrganizationId, mockFaq);

      // Assert
      staticHateoas.verify(() -> Hateoas.linkMap(mockRequest, mockBaseUrl), times(1));

      assertEquals(1, constructionGetOrganizationFaqResponse.constructed().size());

      final var mockGetOrganizationFaqResponse =
          constructionGetOrganizationFaqResponse.constructed().getFirst();

      assertEquals(
          mockLinkMap,
          constructionGetOrganizationFaqResponseArgs.get(mockGetOrganizationFaqResponse).get(0));
      assertEquals(
          mockFaqId,
          constructionGetOrganizationFaqResponseArgs.get(mockGetOrganizationFaqResponse).get(1));
      assertEquals(
          mockOrganizationId,
          constructionGetOrganizationFaqResponseArgs.get(mockGetOrganizationFaqResponse).get(2));
      assertEquals(
          mockFaqLang,
          constructionGetOrganizationFaqResponseArgs.get(mockGetOrganizationFaqResponse).get(3));
      assertEquals(
          mockFaqTitle,
          constructionGetOrganizationFaqResponseArgs.get(mockGetOrganizationFaqResponse).get(4));
      assertEquals(
          mockFaqContent,
          constructionGetOrganizationFaqResponseArgs.get(mockGetOrganizationFaqResponse).get(5));
      assertEquals(
          mockFaqCreated,
          constructionGetOrganizationFaqResponseArgs.get(mockGetOrganizationFaqResponse).get(6));
      assertEquals(
          mockFaqUpdated,
          constructionGetOrganizationFaqResponseArgs.get(mockGetOrganizationFaqResponse).get(7));

      verify(mockGetOrganizationFaqResponse, times(1)).setDeleted(mockFaq.getDeleted());

      assertEquals(mockGetOrganizationFaqResponse, response);
    }
  }

  @Test
  public void createPatchOrganizationFaqResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockLinkMap = mock(Map.class);
    final var mockOrganization = mock(Organization.class);
    final var mockOrganizationId = mock(UUID.class);
    final var mockFaq = mock(Faq.class);
    final var mockFaqId = mock(UUID.class);
    final var mockFaqLang = "mock-faq-lang";
    final var mockFaqTitle = "mock-faq-title";
    final var mockFaqContent = "mock-faq-content";
    final var mockFaqCreated = mock(ZonedDateTime.class);
    final var mockFaqUpdated = mock(ZonedDateTime.class);
    final var mockFaqDeleted = mock(ZonedDateTime.class);

    when(mockFaq.getId()).thenReturn(mockFaqId);
    when(mockFaq.getLang()).thenReturn(mockFaqLang);
    when(mockFaq.getTitle()).thenReturn(mockFaqTitle);
    when(mockFaq.getContent()).thenReturn(mockFaqContent);
    when(mockFaq.getCreated()).thenReturn(mockFaqCreated);
    when(mockFaq.getUpdated()).thenReturn(mockFaqUpdated);
    when(mockFaq.getDeleted()).thenReturn(mockFaqDeleted);

    when(mockOrganization.getId()).thenReturn(mockOrganizationId);

    final var constructionPatchOrganizationFaqResponseArgs =
        new HashMap<PatchOrganizationFaqResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionPatchOrganizationFaqResponse =
             mockConstruction(
                 PatchOrganizationFaqResponse.class,
                 (mock, ctx) ->
                     constructionPatchOrganizationFaqResponseArgs.put(mock, ctx.arguments()))) {
      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createPatchOrganizationFaqResponse(
              mockRequest, mockBaseUrl, mockOrganizationId, mockFaq);

      // Assert
      staticHateoas.verify(() -> Hateoas.linkMap(mockRequest, mockBaseUrl), times(1));

      assertEquals(1, constructionPatchOrganizationFaqResponse.constructed().size());

      final var mockPatchOrganizationFaqResponse =
          constructionPatchOrganizationFaqResponse.constructed().getFirst();

      assertEquals(
          mockLinkMap,
          constructionPatchOrganizationFaqResponseArgs
              .get(mockPatchOrganizationFaqResponse)
              .get(0));
      assertEquals(
          mockFaqId,
          constructionPatchOrganizationFaqResponseArgs
              .get(mockPatchOrganizationFaqResponse)
              .get(1));
      assertEquals(
          mockOrganizationId,
          constructionPatchOrganizationFaqResponseArgs
              .get(mockPatchOrganizationFaqResponse)
              .get(2));
      assertEquals(
          mockFaqLang,
          constructionPatchOrganizationFaqResponseArgs
              .get(mockPatchOrganizationFaqResponse)
              .get(3));
      assertEquals(
          mockFaqTitle,
          constructionPatchOrganizationFaqResponseArgs
              .get(mockPatchOrganizationFaqResponse)
              .get(4));
      assertEquals(
          mockFaqContent,
          constructionPatchOrganizationFaqResponseArgs
              .get(mockPatchOrganizationFaqResponse)
              .get(5));
      assertEquals(
          mockFaqCreated,
          constructionPatchOrganizationFaqResponseArgs
              .get(mockPatchOrganizationFaqResponse)
              .get(6));
      assertEquals(
          mockFaqUpdated,
          constructionPatchOrganizationFaqResponseArgs
              .get(mockPatchOrganizationFaqResponse)
              .get(7));

      verify(mockPatchOrganizationFaqResponse, times(1)).setDeleted(mockFaq.getDeleted());

      assertEquals(mockPatchOrganizationFaqResponse, response);
    }
  }

  @Test
  public void createPostClientRedirectResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockLinkMap = mock(Map.class);
    final var mockOrganization = mock(Organization.class);
    final var mockOrganizationId = mock(UUID.class);
    final var mockClient = mock(Client.class);
    final var mockClientId = mock(UUID.class);
    final var mockClientName = "mock-client-name";
    final var mockClientRole = "mock-client-role";
    final var mockClientCreated = mock(ZonedDateTime.class);
    final var mockClientUpdated = mock(ZonedDateTime.class);
    final var mockClientRevoked = mock(ZonedDateTime.class);
    final var mockRole = mock(Role.class);
    final var mockRedirect = mock(Redirect.class);
    final var mockRedirectId = mock(UUID.class);
    final var mockRedirectUrl = mock(URL.class);
    final var mockRedirectCreated = mock(ZonedDateTime.class);
    final var mockRedirectUpdated = mock(ZonedDateTime.class);
    final var mockRedirectDeleted = mock(ZonedDateTime.class);

    when(mockClient.getId()).thenReturn(mockClientId);
    when(mockClient.getOrganization()).thenReturn(mockOrganization);
    when(mockClient.getName()).thenReturn(mockClientName);
    when(mockClient.getRole()).thenReturn(mockClientRole);
    when(mockClient.getCreated()).thenReturn(mockClientCreated);
    when(mockClient.getUpdated()).thenReturn(mockClientUpdated);
    when(mockClient.getRevoked()).thenReturn(mockClientRevoked);
    when(mockOrganization.getId()).thenReturn(mockOrganizationId);
    when(mockRedirect.getId()).thenReturn(mockRedirectId);
    when(mockRedirect.getRedirectUrl()).thenReturn(mockRedirectUrl);
    when(mockRedirect.getClient()).thenReturn(mockClient);
    when(mockRedirect.getCreated()).thenReturn(mockRedirectCreated);
    when(mockRedirect.getUpdated()).thenReturn(mockRedirectUpdated);
    when(mockRedirect.getDeleted()).thenReturn(mockRedirectDeleted);

    final var constructionPostClientRedirectResponseArgs =
        new HashMap<PostClientRedirectResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionPostClientRedirectResponse =
             mockConstruction(
                 PostClientRedirectResponse.class,
                 (mock, ctx) ->
                     constructionPostClientRedirectResponseArgs.put(mock, ctx.arguments()));
         final var staticRole = mockStatic(Role.class)) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      staticRole.when(() -> Role.fromValue(mockClientRole)).thenReturn(mockRole);

      // Act
      final var response =
          ResponseFactory.createPostClientRedirectResponse(mockRequest, mockBaseUrl, mockRedirect);

      // Assert
      staticHateoas.verify(() -> Hateoas.linkMap(mockRequest, mockBaseUrl), times(1));

      assertEquals(1, constructionPostClientRedirectResponse.constructed().size());

      final var mockPostClientRedirectResponse =
          constructionPostClientRedirectResponse.constructed().getFirst();

      assertEquals(
          mockLinkMap,
          constructionPostClientRedirectResponseArgs.get(mockPostClientRedirectResponse).get(0));
      assertEquals(
          mockRedirectId,
          constructionPostClientRedirectResponseArgs.get(mockPostClientRedirectResponse).get(1));
      assertEquals(
          mockClientId,
          constructionPostClientRedirectResponseArgs.get(mockPostClientRedirectResponse).get(2));
      assertEquals(
          mockRedirectUrl,
          constructionPostClientRedirectResponseArgs.get(mockPostClientRedirectResponse).get(3));
      assertEquals(
          mockRedirectCreated,
          constructionPostClientRedirectResponseArgs.get(mockPostClientRedirectResponse).get(4));
      assertEquals(
          mockRedirectUpdated,
          constructionPostClientRedirectResponseArgs.get(mockPostClientRedirectResponse).get(5));

      verify(mockPostClientRedirectResponse, times(1)).setDeleted(mockRedirectDeleted);

      assertEquals(mockPostClientRedirectResponse, response);
    }
  }

  @Test
  public void createGetClientRedirectResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockLinkMap = mock(Map.class);
    final var mockOrganization = mock(Organization.class);
    final var mockOrganizationId = mock(UUID.class);
    final var mockClient = mock(Client.class);
    final var mockClientId = mock(UUID.class);
    final var mockClientName = "mock-client-name";
    final var mockClientRole = "mock-client-role";
    final var mockClientCreated = mock(ZonedDateTime.class);
    final var mockClientUpdated = mock(ZonedDateTime.class);
    final var mockClientRevoked = mock(ZonedDateTime.class);
    final var mockRole = mock(Role.class);
    final var mockRedirect = mock(Redirect.class);
    final var mockRedirectId = mock(UUID.class);
    final var mockRedirectUrl = mock(URL.class);
    final var mockRedirectCreated = mock(ZonedDateTime.class);
    final var mockRedirectUpdated = mock(ZonedDateTime.class);
    final var mockRedirectDeleted = mock(ZonedDateTime.class);

    when(mockClient.getId()).thenReturn(mockClientId);
    when(mockClient.getOrganization()).thenReturn(mockOrganization);
    when(mockClient.getName()).thenReturn(mockClientName);
    when(mockClient.getRole()).thenReturn(mockClientRole);
    when(mockClient.getCreated()).thenReturn(mockClientCreated);
    when(mockClient.getUpdated()).thenReturn(mockClientUpdated);
    when(mockClient.getRevoked()).thenReturn(mockClientRevoked);
    when(mockOrganization.getId()).thenReturn(mockOrganizationId);
    when(mockRedirect.getId()).thenReturn(mockRedirectId);
    when(mockRedirect.getRedirectUrl()).thenReturn(mockRedirectUrl);
    when(mockRedirect.getClient()).thenReturn(mockClient);
    when(mockRedirect.getCreated()).thenReturn(mockRedirectCreated);
    when(mockRedirect.getUpdated()).thenReturn(mockRedirectUpdated);
    when(mockRedirect.getDeleted()).thenReturn(mockRedirectDeleted);

    final var constructionGetClientRedirectResponseArgs =
        new HashMap<GetClientRedirectResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionGetClientRedirectResponse =
             mockConstruction(
                 GetClientRedirectResponse.class,
                 (mock, ctx) ->
                     constructionGetClientRedirectResponseArgs.put(mock, ctx.arguments()));
         final var staticRole = mockStatic(Role.class)) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      staticRole.when(() -> Role.fromValue(mockClientRole)).thenReturn(mockRole);

      // Act
      final var response =
          ResponseFactory.createGetClientRedirectResponse(mockRequest, mockBaseUrl, mockRedirect);

      // Assert
      staticHateoas.verify(() -> Hateoas.linkMap(mockRequest, mockBaseUrl), times(1));

      assertEquals(1, constructionGetClientRedirectResponse.constructed().size());

      final var mockGetClientRedirectResponse =
          constructionGetClientRedirectResponse.constructed().getFirst();

      assertEquals(
          mockLinkMap,
          constructionGetClientRedirectResponseArgs.get(mockGetClientRedirectResponse).get(0));
      assertEquals(
          mockRedirectId,
          constructionGetClientRedirectResponseArgs.get(mockGetClientRedirectResponse).get(1));
      assertEquals(
          mockClientId,
          constructionGetClientRedirectResponseArgs.get(mockGetClientRedirectResponse).get(2));
      assertEquals(
          mockRedirectUrl,
          constructionGetClientRedirectResponseArgs.get(mockGetClientRedirectResponse).get(3));
      assertEquals(
          mockRedirectCreated,
          constructionGetClientRedirectResponseArgs.get(mockGetClientRedirectResponse).get(4));
      assertEquals(
          mockRedirectUpdated,
          constructionGetClientRedirectResponseArgs.get(mockGetClientRedirectResponse).get(5));

      verify(mockGetClientRedirectResponse, times(1)).setDeleted(mockRedirectDeleted);

      assertEquals(mockGetClientRedirectResponse, response);
    }
  }

  @Test
  public void createPatchClientRedirectResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockLinkMap = mock(Map.class);
    final var mockOrganization = mock(Organization.class);
    final var mockOrganizationId = mock(UUID.class);
    final var mockClient = mock(Client.class);
    final var mockClientId = mock(UUID.class);
    final var mockClientName = "mock-client-name";
    final var mockClientRole = "mock-client-role";
    final var mockClientCreated = mock(ZonedDateTime.class);
    final var mockClientUpdated = mock(ZonedDateTime.class);
    final var mockClientRevoked = mock(ZonedDateTime.class);
    final var mockRole = mock(Role.class);
    final var mockRedirect = mock(Redirect.class);
    final var mockRedirectId = mock(UUID.class);
    final var mockRedirectUrl = mock(URL.class);
    final var mockRedirectCreated = mock(ZonedDateTime.class);
    final var mockRedirectUpdated = mock(ZonedDateTime.class);
    final var mockRedirectDeleted = mock(ZonedDateTime.class);

    when(mockClient.getId()).thenReturn(mockClientId);
    when(mockClient.getOrganization()).thenReturn(mockOrganization);
    when(mockClient.getName()).thenReturn(mockClientName);
    when(mockClient.getRole()).thenReturn(mockClientRole);
    when(mockClient.getCreated()).thenReturn(mockClientCreated);
    when(mockClient.getUpdated()).thenReturn(mockClientUpdated);
    when(mockClient.getRevoked()).thenReturn(mockClientRevoked);
    when(mockOrganization.getId()).thenReturn(mockOrganizationId);
    when(mockRedirect.getId()).thenReturn(mockRedirectId);
    when(mockRedirect.getRedirectUrl()).thenReturn(mockRedirectUrl);
    when(mockRedirect.getClient()).thenReturn(mockClient);
    when(mockRedirect.getCreated()).thenReturn(mockRedirectCreated);
    when(mockRedirect.getUpdated()).thenReturn(mockRedirectUpdated);
    when(mockRedirect.getDeleted()).thenReturn(mockRedirectDeleted);

    final var constructionPatchClientRedirectResponseArgs =
        new HashMap<PatchClientRedirectResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionPatchClientRedirectResponse =
             mockConstruction(
                 PatchClientRedirectResponse.class,
                 (mock, ctx) ->
                     constructionPatchClientRedirectResponseArgs.put(mock, ctx.arguments()));
         final var staticRole = mockStatic(Role.class)) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      staticRole.when(() -> Role.fromValue(mockClientRole)).thenReturn(mockRole);

      // Act
      final var response =
          ResponseFactory.createPatchClientRedirectResponse(mockRequest, mockBaseUrl, mockRedirect);

      // Assert
      staticHateoas.verify(() -> Hateoas.linkMap(mockRequest, mockBaseUrl), times(1));

      assertEquals(1, constructionPatchClientRedirectResponse.constructed().size());

      final var mockPatchClientRedirectResponse =
          constructionPatchClientRedirectResponse.constructed().getFirst();

      assertEquals(
          mockLinkMap,
          constructionPatchClientRedirectResponseArgs.get(mockPatchClientRedirectResponse).get(0));
      assertEquals(
          mockRedirectId,
          constructionPatchClientRedirectResponseArgs.get(mockPatchClientRedirectResponse).get(1));
      assertEquals(
          mockClientId,
          constructionPatchClientRedirectResponseArgs.get(mockPatchClientRedirectResponse).get(2));
      assertEquals(
          mockRedirectUrl,
          constructionPatchClientRedirectResponseArgs.get(mockPatchClientRedirectResponse).get(3));
      assertEquals(
          mockRedirectCreated,
          constructionPatchClientRedirectResponseArgs.get(mockPatchClientRedirectResponse).get(4));
      assertEquals(
          mockRedirectUpdated,
          constructionPatchClientRedirectResponseArgs.get(mockPatchClientRedirectResponse).get(5));

      verify(mockPatchClientRedirectResponse, times(1)).setDeleted(mockRedirectDeleted);

      assertEquals(mockPatchClientRedirectResponse, response);
    }
  }

  @Test
  public void createGetClientKeyResetResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockLinkMap = mock(Map.class);
    final var mockClient = mock(Client.class);
    final var mockClientId = mock(UUID.class);
    final var mockClientKeyReset = mock(ClientKeyReset.class);
    final var mockClientKeyResetId = mock(UUID.class);
    final var mockClientKeyResetCreated = mock(ZonedDateTime.class);
    final var mockClientKeyResetUpdated = mock(ZonedDateTime.class);

    when(mockClient.getId()).thenReturn(mockClientId);
    when(mockClientKeyReset.getId()).thenReturn(mockClientKeyResetId);
    when(mockClientKeyReset.getClient()).thenReturn(mockClient);
    when(mockClientKeyReset.getCreated()).thenReturn(mockClientKeyResetCreated);
    when(mockClientKeyReset.getUpdated()).thenReturn(mockClientKeyResetUpdated);

    final var constructionGetClientKeyResetResponseArgs =
        new HashMap<GetClientKeyResetResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionGetClientKeyResetResponse =
             mockConstruction(
                 GetClientKeyResetResponse.class,
                 (mock, ctx) ->
                     constructionGetClientKeyResetResponseArgs.put(mock, ctx.arguments()))) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createGetClientKeyResetResponse(
              mockRequest, mockBaseUrl, mockClientKeyReset);

      // Assert
      staticHateoas.verify(() -> Hateoas.linkMap(mockRequest, mockBaseUrl), times(1));

      assertEquals(1, constructionGetClientKeyResetResponse.constructed().size());

      final var mockGetClientKeyResetResponse =
          constructionGetClientKeyResetResponse.constructed().getFirst();

      assertEquals(
          mockLinkMap,
          constructionGetClientKeyResetResponseArgs.get(mockGetClientKeyResetResponse).get(0));
      assertEquals(
          mockClientKeyResetId,
          constructionGetClientKeyResetResponseArgs.get(mockGetClientKeyResetResponse).get(1));
      assertEquals(
          mockClientId,
          constructionGetClientKeyResetResponseArgs.get(mockGetClientKeyResetResponse).get(2));
      assertEquals(
          mockClientKeyResetCreated,
          constructionGetClientKeyResetResponseArgs.get(mockGetClientKeyResetResponse).get(3));
      assertEquals(
          mockClientKeyResetUpdated,
          constructionGetClientKeyResetResponseArgs.get(mockGetClientKeyResetResponse).get(4));

      assertEquals(mockGetClientKeyResetResponse, response);
    }
  }

  @Test
  public void createPostClientKeyResetResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockApiKey = "mock-api-key";
    final var mockLinkMap = mock(Map.class);
    final var mockClient = mock(Client.class);
    final var mockClientId = mock(UUID.class);
    final var mockClientKeyReset = mock(ClientKeyReset.class);
    final var mockClientKeyResetId = mock(UUID.class);
    final var mockClientKeyResetCreated = mock(ZonedDateTime.class);
    final var mockClientKeyResetUpdated = mock(ZonedDateTime.class);

    when(mockClient.getId()).thenReturn(mockClientId);
    when(mockClientKeyReset.getId()).thenReturn(mockClientKeyResetId);
    when(mockClientKeyReset.getClient()).thenReturn(mockClient);
    when(mockClientKeyReset.getCreated()).thenReturn(mockClientKeyResetCreated);
    when(mockClientKeyReset.getUpdated()).thenReturn(mockClientKeyResetUpdated);

    final var constructionPostClientKeyResetResponseArgs =
        new HashMap<PostClientKeyResetResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionPostClientKeyResetResponse =
             mockConstruction(
                 PostClientKeyResetResponse.class,
                 (mock, ctx) ->
                     constructionPostClientKeyResetResponseArgs.put(mock, ctx.arguments()))) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createPostClientKeyResetResponse(
              mockRequest, mockBaseUrl, mockClientKeyReset, mockApiKey);

      // Assert
      staticHateoas.verify(() -> Hateoas.linkMap(mockRequest, mockBaseUrl), times(1));

      assertEquals(1, constructionPostClientKeyResetResponse.constructed().size());

      final var mockPostClientKeyResetResponse =
          constructionPostClientKeyResetResponse.constructed().getFirst();

      assertEquals(
          mockLinkMap,
          constructionPostClientKeyResetResponseArgs.get(mockPostClientKeyResetResponse).get(0));
      assertEquals(
          mockClientKeyResetId,
          constructionPostClientKeyResetResponseArgs.get(mockPostClientKeyResetResponse).get(1));
      assertEquals(
          mockClientId,
          constructionPostClientKeyResetResponseArgs.get(mockPostClientKeyResetResponse).get(2));
      assertEquals(
          mockClientKeyResetCreated,
          constructionPostClientKeyResetResponseArgs.get(mockPostClientKeyResetResponse).get(3));
      assertEquals(
          mockClientKeyResetUpdated,
          constructionPostClientKeyResetResponseArgs.get(mockPostClientKeyResetResponse).get(4));
      assertEquals(
          mockApiKey,
          constructionPostClientKeyResetResponseArgs.get(mockPostClientKeyResetResponse).get(5));

      assertEquals(mockPostClientKeyResetResponse, response);
    }
  }

  @Test
  public void createPostClientResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockApiKey = "mock-api-key";
    final var mockLinkMap = mock(Map.class);
    final var mockClient = mock(Client.class);
    final var mockClientId = mock(UUID.class);
    final var mockClientRole = "mock-client-role";
    final var mockClientRoleEnum = mock(Role.class);
    final var mockClientName = "mock-client-name";
    final var mockClientCreated = mock(ZonedDateTime.class);
    final var mockClientUpdated = mock(ZonedDateTime.class);
    final var mockClientRevoked = mock(ZonedDateTime.class);
    final var mockOrganization = mock(Organization.class);
    final var mockOrganizationId = mock(UUID.class);

    when(mockClient.getId()).thenReturn(mockClientId);
    when(mockClient.getOrganization()).thenReturn(mockOrganization);
    when(mockClient.getRole()).thenReturn(mockClientRole);
    when(mockClient.getName()).thenReturn(mockClientName);
    when(mockClient.getCreated()).thenReturn(mockClientCreated);
    when(mockClient.getUpdated()).thenReturn(mockClientUpdated);
    when(mockClient.getRevoked()).thenReturn(mockClientRevoked);
    when(mockOrganization.getId()).thenReturn(mockOrganizationId);

    final var constructionPostClientResponseArgs = new HashMap<PostClientResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionPostClientResponse =
             mockConstruction(
                 PostClientResponse.class,
                 (mock, ctx) -> constructionPostClientResponseArgs.put(mock, ctx.arguments()));
         final var staticRole = mockStatic(Role.class)) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      staticRole.when(() -> Role.fromValue(mockClientRole)).thenReturn(mockClientRoleEnum);

      // Act
      final var response =
          ResponseFactory.createPostClientResponse(
              mockRequest, mockBaseUrl, mockClient, mockApiKey);

      // Assert
      staticHateoas.verify(() -> Hateoas.linkMap(mockRequest, mockBaseUrl), times(1));

      assertEquals(1, constructionPostClientResponse.constructed().size());

      final var mockPostClientResponse = constructionPostClientResponse.constructed().getFirst();

      assertEquals(
          mockLinkMap, constructionPostClientResponseArgs.get(mockPostClientResponse).get(0));
      assertEquals(
          mockClientId, constructionPostClientResponseArgs.get(mockPostClientResponse).get(1));
      assertEquals(
          mockOrganizationId,
          constructionPostClientResponseArgs.get(mockPostClientResponse).get(2));
      assertEquals(
          mockClientName, constructionPostClientResponseArgs.get(mockPostClientResponse).get(3));
      assertEquals(
          mockClientRoleEnum,
          constructionPostClientResponseArgs.get(mockPostClientResponse).get(4));
      assertEquals(
          mockClientCreated, constructionPostClientResponseArgs.get(mockPostClientResponse).get(5));
      assertEquals(
          mockClientUpdated, constructionPostClientResponseArgs.get(mockPostClientResponse).get(6));
      assertEquals(
          mockApiKey, constructionPostClientResponseArgs.get(mockPostClientResponse).get(7));

      verify(mockPostClientResponse, times(1)).setRevoked(mockClientRevoked);

      assertEquals(mockPostClientResponse, response);
    }
  }

  @Test
  public void createGetClientResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockLinkMap = mock(Map.class);
    final var mockClient = mock(Client.class);
    final var mockClientId = mock(UUID.class);
    final var mockClientRole = "mock-client-role";
    final var mockClientRoleEnum = mock(Role.class);
    final var mockClientName = "mock-client-name";
    final var mockClientCreated = mock(ZonedDateTime.class);
    final var mockClientUpdated = mock(ZonedDateTime.class);
    final var mockClientRevoked = mock(ZonedDateTime.class);
    final var mockOrganization = mock(Organization.class);
    final var mockOrganizationId = mock(UUID.class);

    when(mockClient.getId()).thenReturn(mockClientId);
    when(mockClient.getOrganization()).thenReturn(mockOrganization);
    when(mockClient.getRole()).thenReturn(mockClientRole);
    when(mockClient.getName()).thenReturn(mockClientName);
    when(mockClient.getCreated()).thenReturn(mockClientCreated);
    when(mockClient.getUpdated()).thenReturn(mockClientUpdated);
    when(mockClient.getRevoked()).thenReturn(mockClientRevoked);
    when(mockOrganization.getId()).thenReturn(mockOrganizationId);

    final var constructionGetClientResponseArgs = new HashMap<GetClientResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionGetClientResponse =
             mockConstruction(
                 GetClientResponse.class,
                 (mock, ctx) -> constructionGetClientResponseArgs.put(mock, ctx.arguments()));
         final var staticRole = mockStatic(Role.class)) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      staticRole.when(() -> Role.fromValue(mockClientRole)).thenReturn(mockClientRoleEnum);

      // Act
      final var response =
          ResponseFactory.createGetClientResponse(mockRequest, mockBaseUrl, mockClient);

      // Assert
      staticHateoas.verify(() -> Hateoas.linkMap(mockRequest, mockBaseUrl), times(1));

      assertEquals(1, constructionGetClientResponse.constructed().size());

      final var mockGetClientResponse = constructionGetClientResponse.constructed().getFirst();

      assertEquals(
          mockLinkMap, constructionGetClientResponseArgs.get(mockGetClientResponse).get(0));
      assertEquals(
          mockClientId, constructionGetClientResponseArgs.get(mockGetClientResponse).get(1));
      assertEquals(
          mockOrganizationId, constructionGetClientResponseArgs.get(mockGetClientResponse).get(2));
      assertEquals(
          mockClientName, constructionGetClientResponseArgs.get(mockGetClientResponse).get(3));
      assertEquals(
          mockClientRoleEnum, constructionGetClientResponseArgs.get(mockGetClientResponse).get(4));
      assertEquals(
          mockClientCreated, constructionGetClientResponseArgs.get(mockGetClientResponse).get(5));
      assertEquals(
          mockClientUpdated, constructionGetClientResponseArgs.get(mockGetClientResponse).get(6));

      verify(mockGetClientResponse, times(1)).setRevoked(mockClientRevoked);

      assertEquals(mockGetClientResponse, response);
    }
  }

  @Test
  public void createPatchClientResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockLinkMap = mock(Map.class);
    final var mockClient = mock(Client.class);
    final var mockClientId = mock(UUID.class);
    final var mockClientName = "mock-client-name";
    final var mockClientCreated = mock(ZonedDateTime.class);
    final var mockClientUpdated = mock(ZonedDateTime.class);
    final var mockClientRevoked = mock(ZonedDateTime.class);
    final var mockOrganization = mock(Organization.class);
    final var mockOrganizationId = mock(UUID.class);

    when(mockClient.getId()).thenReturn(mockClientId);
    when(mockClient.getOrganization()).thenReturn(mockOrganization);
    when(mockClient.getName()).thenReturn(mockClientName);
    when(mockClient.getCreated()).thenReturn(mockClientCreated);
    when(mockClient.getUpdated()).thenReturn(mockClientUpdated);
    when(mockClient.getRevoked()).thenReturn(mockClientRevoked);
    when(mockOrganization.getId()).thenReturn(mockOrganizationId);

    final var constructionPatchClientResponseArgs = new HashMap<PatchClientResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionPatchClientResponse =
             mockConstruction(
                 PatchClientResponse.class,
                 (mock, ctx) -> constructionPatchClientResponseArgs.put(mock, ctx.arguments()));
         final var staticRole = mockStatic(Role.class)) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createPatchClientResponse(mockRequest, mockBaseUrl, mockClient);

      // Assert
      staticHateoas.verify(() -> Hateoas.linkMap(mockRequest, mockBaseUrl), times(1));

      assertEquals(1, constructionPatchClientResponse.constructed().size());

      final var mockPatchClientResponse = constructionPatchClientResponse.constructed().getFirst();

      assertEquals(
          mockLinkMap, constructionPatchClientResponseArgs.get(mockPatchClientResponse).get(0));
      assertEquals(
          mockClientId, constructionPatchClientResponseArgs.get(mockPatchClientResponse).get(1));
      assertEquals(
          mockOrganizationId,
          constructionPatchClientResponseArgs.get(mockPatchClientResponse).get(2));
      assertEquals(
          mockClientName, constructionPatchClientResponseArgs.get(mockPatchClientResponse).get(3));
      assertEquals(
          mockClientCreated,
          constructionPatchClientResponseArgs.get(mockPatchClientResponse).get(4));
      assertEquals(
          mockClientUpdated,
          constructionPatchClientResponseArgs.get(mockPatchClientResponse).get(5));

      verify(mockPatchClientResponse, times(1)).setRevoked(mockClientRevoked);

      assertEquals(mockPatchClientResponse, response);
    }
  }

  @Test
  @Tag("Unit")
  public void createPostDatasourceResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockDatasource = mock(Datasource.class);
    final var mockDatasourceId = mock(UUID.class);
    final var mockDatasourceName = "mock-datasource-name";
    final var mockDatasourceCreated = mock(ZonedDateTime.class);
    final var mockDatasourceUpdated = mock(ZonedDateTime.class);
    final var mockDatasourceDeleted = mock(ZonedDateTime.class);
    final var mockLinkMap = mock(Map.class);
    final var mockOrganization = mock(Organization.class);
    final var mockOrganizationId = mock(UUID.class);

    when(mockDatasource.getId()).thenReturn(mockDatasourceId);
    when(mockDatasource.getOrganization()).thenReturn(mockOrganization);
    when(mockDatasource.getName()).thenReturn(mockDatasourceName);
    when(mockDatasource.getCreated()).thenReturn(mockDatasourceCreated);
    when(mockDatasource.getUpdated()).thenReturn(mockDatasourceUpdated);
    when(mockDatasource.getDeleted()).thenReturn(mockDatasourceDeleted);
    when(mockOrganization.getId()).thenReturn(mockOrganizationId);

    final var constructionPostDatasourceResponseArgs =
        new HashMap<PostDatasourceResponse, List<?>>();

    try (final var constructionPostDatasourceResponse =
             mockConstruction(
                 PostDatasourceResponse.class,
                 (mock, ctx) -> constructionPostDatasourceResponseArgs.put(mock, ctx.arguments()));
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createPostDatasourceResponse(mockRequest, mockBaseUrl, mockDatasource);

      // Assert
      assertEquals(1, constructionPostDatasourceResponse.constructed().size());

      final var mockPostDatasourceResponse =
          constructionPostDatasourceResponse.constructed().getFirst();

      assertEquals(
          6, constructionPostDatasourceResponseArgs.get(mockPostDatasourceResponse).size());

      assertEquals(
          mockLinkMap,
          constructionPostDatasourceResponseArgs.get(mockPostDatasourceResponse).get(0));
      assertEquals(
          mockDatasourceId,
          constructionPostDatasourceResponseArgs.get(mockPostDatasourceResponse).get(1));
      assertEquals(
          mockOrganizationId,
          constructionPostDatasourceResponseArgs.get(mockPostDatasourceResponse).get(2));
      assertEquals(
          mockDatasourceName,
          constructionPostDatasourceResponseArgs.get(mockPostDatasourceResponse).get(3));
      assertEquals(
          mockDatasourceCreated,
          constructionPostDatasourceResponseArgs.get(mockPostDatasourceResponse).get(4));
      assertEquals(
          mockDatasourceUpdated,
          constructionPostDatasourceResponseArgs.get(mockPostDatasourceResponse).get(5));

      verify(mockPostDatasourceResponse, times(1)).setDeleted(mockDatasourceDeleted);

      assertEquals(mockPostDatasourceResponse, response);
    }
  }

  @Test
  @Tag("Unit")
  public void createGetDatasourceResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockDatasource = mock(Datasource.class);
    final var mockDatasourceId = mock(UUID.class);
    final var mockDatasourceName = "mock-datasource-name";
    final var mockDatasourceCreated = mock(ZonedDateTime.class);
    final var mockDatasourceUpdated = mock(ZonedDateTime.class);
    final var mockDatasourceDeleted = mock(ZonedDateTime.class);
    final var mockLinkMap = mock(Map.class);
    final var mockOrganization = mock(Organization.class);
    final var mockOrganizationId = mock(UUID.class);

    when(mockDatasource.getId()).thenReturn(mockDatasourceId);
    when(mockDatasource.getOrganization()).thenReturn(mockOrganization);
    when(mockDatasource.getName()).thenReturn(mockDatasourceName);
    when(mockDatasource.getCreated()).thenReturn(mockDatasourceCreated);
    when(mockDatasource.getUpdated()).thenReturn(mockDatasourceUpdated);
    when(mockDatasource.getDeleted()).thenReturn(mockDatasourceDeleted);
    when(mockOrganization.getId()).thenReturn(mockOrganizationId);

    final var constructionGetDatasourceResponseArgs = new HashMap<GetDatasourceResponse, List<?>>();

    try (final var constructionGetDatasourceResponse =
             mockConstruction(
                 GetDatasourceResponse.class,
                 (mock, ctx) -> constructionGetDatasourceResponseArgs.put(mock, ctx.arguments()));
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createGetDatasourceResponse(mockRequest, mockBaseUrl, mockDatasource);

      // Assert
      assertEquals(1, constructionGetDatasourceResponse.constructed().size());

      final var mockGetDatasourceResponse =
          constructionGetDatasourceResponse.constructed().getFirst();

      assertEquals(6, constructionGetDatasourceResponseArgs.get(mockGetDatasourceResponse).size());

      assertEquals(
          mockLinkMap, constructionGetDatasourceResponseArgs.get(mockGetDatasourceResponse).get(0));
      assertEquals(
          mockDatasourceId,
          constructionGetDatasourceResponseArgs.get(mockGetDatasourceResponse).get(1));
      assertEquals(
          mockOrganizationId,
          constructionGetDatasourceResponseArgs.get(mockGetDatasourceResponse).get(2));
      assertEquals(
          mockDatasourceName,
          constructionGetDatasourceResponseArgs.get(mockGetDatasourceResponse).get(3));
      assertEquals(
          mockDatasourceCreated,
          constructionGetDatasourceResponseArgs.get(mockGetDatasourceResponse).get(4));
      assertEquals(
          mockDatasourceUpdated,
          constructionGetDatasourceResponseArgs.get(mockGetDatasourceResponse).get(5));

      verify(mockGetDatasourceResponse, times(1)).setDeleted(mockDatasourceDeleted);

      assertEquals(mockGetDatasourceResponse, response);
    }
  }

  @Test
  @Tag("Unit")
  public void createPatchDatasourceResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockDatasource = mock(Datasource.class);
    final var mockDatasourceId = mock(UUID.class);
    final var mockDatasourceName = "mock-datasource-name";
    final var mockDatasourceCreated = mock(ZonedDateTime.class);
    final var mockDatasourceUpdated = mock(ZonedDateTime.class);
    final var mockDatasourceDeleted = mock(ZonedDateTime.class);
    final var mockLinkMap = mock(Map.class);
    final var mockOrganization = mock(Organization.class);
    final var mockOrganizationId = mock(UUID.class);

    when(mockDatasource.getId()).thenReturn(mockDatasourceId);
    when(mockDatasource.getOrganization()).thenReturn(mockOrganization);
    when(mockDatasource.getName()).thenReturn(mockDatasourceName);
    when(mockDatasource.getCreated()).thenReturn(mockDatasourceCreated);
    when(mockDatasource.getUpdated()).thenReturn(mockDatasourceUpdated);
    when(mockDatasource.getDeleted()).thenReturn(mockDatasourceDeleted);
    when(mockOrganization.getId()).thenReturn(mockOrganizationId);

    final var constructionPatchDatasourceResponseArgs =
        new HashMap<PatchDatasourceResponse, List<?>>();

    try (final var constructionPatchDatasourceResponse =
             mockConstruction(
                 PatchDatasourceResponse.class,
                 (mock, ctx) -> constructionPatchDatasourceResponseArgs.put(mock, ctx.arguments()));
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createPatchDatasourceResponse(mockRequest, mockBaseUrl, mockDatasource);

      // Assert
      assertEquals(1, constructionPatchDatasourceResponse.constructed().size());

      final var mockPatchDatasourceResponse =
          constructionPatchDatasourceResponse.constructed().getFirst();

      assertEquals(
          6, constructionPatchDatasourceResponseArgs.get(mockPatchDatasourceResponse).size());

      assertEquals(
          mockLinkMap,
          constructionPatchDatasourceResponseArgs.get(mockPatchDatasourceResponse).get(0));
      assertEquals(
          mockDatasourceId,
          constructionPatchDatasourceResponseArgs.get(mockPatchDatasourceResponse).get(1));
      assertEquals(
          mockOrganizationId,
          constructionPatchDatasourceResponseArgs.get(mockPatchDatasourceResponse).get(2));
      assertEquals(
          mockDatasourceName,
          constructionPatchDatasourceResponseArgs.get(mockPatchDatasourceResponse).get(3));
      assertEquals(
          mockDatasourceCreated,
          constructionPatchDatasourceResponseArgs.get(mockPatchDatasourceResponse).get(4));
      assertEquals(
          mockDatasourceUpdated,
          constructionPatchDatasourceResponseArgs.get(mockPatchDatasourceResponse).get(5));

      verify(mockPatchDatasourceResponse, times(1)).setDeleted(mockDatasourceDeleted);

      assertEquals(mockPatchDatasourceResponse, response);
    }
  }

  @Test
  @Tag("Unit")
  public void createGetDatasourceConfigResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockDatasource = mock(Datasource.class);
    final var mockDatasourceId = mock(UUID.class);
    final var mockDatasourcePath = "mock-datasource-path";
    final var mockDatasourceMethod = "mock-datasource-method";
    final var mockDatasourceCreated = mock(ZonedDateTime.class);
    final var mockDatasourceUpdated = mock(ZonedDateTime.class);
    final var mockDatasourceDeleted = mock(ZonedDateTime.class);
    final var mockLinkMap = mock(Map.class);

    when(mockDatasource.getId()).thenReturn(mockDatasourceId);
    when(mockDatasource.getPath()).thenReturn(mockDatasourcePath);
    when(mockDatasource.getMethod()).thenReturn(mockDatasourceMethod);
    when(mockDatasource.getCreated()).thenReturn(mockDatasourceCreated);
    when(mockDatasource.getUpdated()).thenReturn(mockDatasourceUpdated);
    when(mockDatasource.getDeleted()).thenReturn(mockDatasourceDeleted);

    final var constructionGetDatasourceConfigResponseArgs =
        new HashMap<GetDatasourceConfigResponse, List<?>>();

    try (final var constructionGetDatasourceConfigResponse =
             mockConstruction(
                 GetDatasourceConfigResponse.class,
                 (mock, ctx) ->
                     constructionGetDatasourceConfigResponseArgs.put(mock, ctx.arguments()));
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createGetDatasourceConfigResponse(
              mockRequest, mockBaseUrl, mockDatasource);

      // Assert
      assertEquals(1, constructionGetDatasourceConfigResponse.constructed().size());

      final var mockGetDatasourceConfigResponse =
          constructionGetDatasourceConfigResponse.constructed().getFirst();

      assertEquals(
          6,
          constructionGetDatasourceConfigResponseArgs.get(mockGetDatasourceConfigResponse).size());

      assertEquals(
          mockLinkMap,
          constructionGetDatasourceConfigResponseArgs.get(mockGetDatasourceConfigResponse).get(0));
      assertEquals(
          mockDatasourceId,
          constructionGetDatasourceConfigResponseArgs.get(mockGetDatasourceConfigResponse).get(1));
      assertEquals(
          mockDatasourcePath,
          constructionGetDatasourceConfigResponseArgs.get(mockGetDatasourceConfigResponse).get(2));
      assertEquals(
          mockDatasourceMethod,
          constructionGetDatasourceConfigResponseArgs.get(mockGetDatasourceConfigResponse).get(3));
      assertEquals(
          mockDatasourceCreated,
          constructionGetDatasourceConfigResponseArgs.get(mockGetDatasourceConfigResponse).get(4));
      assertEquals(
          mockDatasourceUpdated,
          constructionGetDatasourceConfigResponseArgs.get(mockGetDatasourceConfigResponse).get(5));

      verify(mockGetDatasourceConfigResponse, times(1)).setDeleted(mockDatasourceDeleted);

      assertEquals(mockGetDatasourceConfigResponse, response);
    }
  }

  @Test
  @Tag("Unit")
  public void createPatchDatasourceConfigResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockDatasource = mock(Datasource.class);
    final var mockDatasourceId = mock(UUID.class);
    final var mockDatasourcePath = "mock-datasource-path";
    final var mockDatasourceMethod = "mock-datasource-method";
    final var mockDatasourceCreated = mock(ZonedDateTime.class);
    final var mockDatasourceUpdated = mock(ZonedDateTime.class);
    final var mockDatasourceDeleted = mock(ZonedDateTime.class);
    final var mockLinkMap = mock(Map.class);

    when(mockDatasource.getId()).thenReturn(mockDatasourceId);
    when(mockDatasource.getPath()).thenReturn(mockDatasourcePath);
    when(mockDatasource.getMethod()).thenReturn(mockDatasourceMethod);
    when(mockDatasource.getCreated()).thenReturn(mockDatasourceCreated);
    when(mockDatasource.getUpdated()).thenReturn(mockDatasourceUpdated);
    when(mockDatasource.getDeleted()).thenReturn(mockDatasourceDeleted);

    final var constructionPatchDatasourceConfigResponseArgs =
        new HashMap<PatchDatasourceConfigResponse, List<?>>();

    try (final var constructionPatchDatasourceConfigResponse =
             mockConstruction(
                 PatchDatasourceConfigResponse.class,
                 (mock, ctx) ->
                     constructionPatchDatasourceConfigResponseArgs.put(mock, ctx.arguments()));
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createPatchDatasourceConfigResponse(
              mockRequest, mockBaseUrl, mockDatasource);

      // Assert
      assertEquals(1, constructionPatchDatasourceConfigResponse.constructed().size());

      final var mockPatchDatasourceConfigResponse =
          constructionPatchDatasourceConfigResponse.constructed().getFirst();

      assertEquals(
          6,
          constructionPatchDatasourceConfigResponseArgs
              .get(mockPatchDatasourceConfigResponse)
              .size());

      assertEquals(
          mockLinkMap,
          constructionPatchDatasourceConfigResponseArgs
              .get(mockPatchDatasourceConfigResponse)
              .get(0));
      assertEquals(
          mockDatasourceId,
          constructionPatchDatasourceConfigResponseArgs
              .get(mockPatchDatasourceConfigResponse)
              .get(1));
      assertEquals(
          mockDatasourcePath,
          constructionPatchDatasourceConfigResponseArgs
              .get(mockPatchDatasourceConfigResponse)
              .get(2));
      assertEquals(
          mockDatasourceMethod,
          constructionPatchDatasourceConfigResponseArgs
              .get(mockPatchDatasourceConfigResponse)
              .get(3));
      assertEquals(
          mockDatasourceCreated,
          constructionPatchDatasourceConfigResponseArgs
              .get(mockPatchDatasourceConfigResponse)
              .get(4));
      assertEquals(
          mockDatasourceUpdated,
          constructionPatchDatasourceConfigResponseArgs
              .get(mockPatchDatasourceConfigResponse)
              .get(5));

      verify(mockPatchDatasourceConfigResponse, times(1)).setDeleted(mockDatasourceDeleted);

      assertEquals(mockPatchDatasourceConfigResponse, response);
    }
  }

  @Test
  public void createPostDatasourceFaqResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockDatasource = mock(Datasource.class);
    final var mockDatasourceId = mock(UUID.class);
    final var mockLinkMap = mock(Map.class);
    final var mockFaq = mock(Faq.class);
    final var mockFaqId = mock(UUID.class);
    final var mockFaqLang = "mock-faq-lang";
    final var mockFaqTitle = "mock-faq-title";
    final var mockFaqContent = "mock-faq-content";
    final var mockFaqCreated = mock(ZonedDateTime.class);
    final var mockFaqUpdated = mock(ZonedDateTime.class);
    final var mockFaqDeleted = mock(ZonedDateTime.class);

    when(mockDatasource.getId()).thenReturn(mockDatasourceId);
    when(mockFaq.getId()).thenReturn(mockFaqId);
    when(mockFaq.getLang()).thenReturn(mockFaqLang);
    when(mockFaq.getTitle()).thenReturn(mockFaqTitle);
    when(mockFaq.getContent()).thenReturn(mockFaqContent);
    when(mockFaq.getCreated()).thenReturn(mockFaqCreated);
    when(mockFaq.getUpdated()).thenReturn(mockFaqUpdated);
    when(mockFaq.getDeleted()).thenReturn(mockFaqDeleted);

    final var constructionPostDatasourceFaqResponseArgs =
        new HashMap<PostDatasourceFaqResponse, List<?>>();

    try (final var constructionPostDatasourceFaqResponse =
             mockConstruction(
                 PostDatasourceFaqResponse.class,
                 (mock, ctx) ->
                     constructionPostDatasourceFaqResponseArgs.put(mock, ctx.arguments()));
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createPostDatasourceFaqResponse(
              mockRequest, mockBaseUrl, mockDatasource, mockFaq);

      // Assert
      assertEquals(1, constructionPostDatasourceFaqResponse.constructed().size());

      final var mockPostDatasourceFaqResponse =
          constructionPostDatasourceFaqResponse.constructed().getFirst();

      assertEquals(
          8, constructionPostDatasourceFaqResponseArgs.get(mockPostDatasourceFaqResponse).size());

      assertEquals(
          mockLinkMap,
          constructionPostDatasourceFaqResponseArgs.get(mockPostDatasourceFaqResponse).get(0));
      assertEquals(
          mockFaqId,
          constructionPostDatasourceFaqResponseArgs.get(mockPostDatasourceFaqResponse).get(1));
      assertEquals(
          mockDatasourceId,
          constructionPostDatasourceFaqResponseArgs.get(mockPostDatasourceFaqResponse).get(2));
      assertEquals(
          mockFaqLang,
          constructionPostDatasourceFaqResponseArgs.get(mockPostDatasourceFaqResponse).get(3));
      assertEquals(
          mockFaqTitle,
          constructionPostDatasourceFaqResponseArgs.get(mockPostDatasourceFaqResponse).get(4));
      assertEquals(
          mockFaqContent,
          constructionPostDatasourceFaqResponseArgs.get(mockPostDatasourceFaqResponse).get(5));
      assertEquals(
          mockFaqCreated,
          constructionPostDatasourceFaqResponseArgs.get(mockPostDatasourceFaqResponse).get(6));
      assertEquals(
          mockFaqUpdated,
          constructionPostDatasourceFaqResponseArgs.get(mockPostDatasourceFaqResponse).get(7));

      verify(mockPostDatasourceFaqResponse, times(1)).setDeleted(mockFaq.getDeleted());

      assertEquals(mockPostDatasourceFaqResponse, response);
    }
  }

  @Test
  public void createGetDatasourceFaqResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockDatasource = mock(Datasource.class);
    final var mockDatasourceId = mock(UUID.class);
    final var mockLinkMap = mock(Map.class);
    final var mockFaq = mock(Faq.class);
    final var mockFaqId = mock(UUID.class);
    final var mockFaqLang = "mock-faq-lang";
    final var mockFaqTitle = "mock-faq-title";
    final var mockFaqContent = "mock-faq-content";
    final var mockFaqCreated = mock(ZonedDateTime.class);
    final var mockFaqUpdated = mock(ZonedDateTime.class);
    final var mockFaqDeleted = mock(ZonedDateTime.class);

    when(mockDatasource.getId()).thenReturn(mockDatasourceId);
    when(mockFaq.getId()).thenReturn(mockFaqId);
    when(mockFaq.getLang()).thenReturn(mockFaqLang);
    when(mockFaq.getTitle()).thenReturn(mockFaqTitle);
    when(mockFaq.getContent()).thenReturn(mockFaqContent);
    when(mockFaq.getCreated()).thenReturn(mockFaqCreated);
    when(mockFaq.getUpdated()).thenReturn(mockFaqUpdated);
    when(mockFaq.getDeleted()).thenReturn(mockFaqDeleted);

    final var constructionGetDatasourceFaqResponseArgs =
        new HashMap<GetDatasourceFaqResponse, List<?>>();

    try (final var constructionGetDatasourceFaqResponse =
             mockConstruction(
                 GetDatasourceFaqResponse.class,
                 (mock, ctx) ->
                     constructionGetDatasourceFaqResponseArgs.put(mock, ctx.arguments()));
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createGetDatasourceFaqResponse(
              mockRequest, mockBaseUrl, mockDatasourceId, mockFaq);

      // Assert
      assertEquals(1, constructionGetDatasourceFaqResponse.constructed().size());

      final var mockGetDatasourceFaqResponse =
          constructionGetDatasourceFaqResponse.constructed().getFirst();

      assertEquals(
          8, constructionGetDatasourceFaqResponseArgs.get(mockGetDatasourceFaqResponse).size());

      assertEquals(
          mockLinkMap,
          constructionGetDatasourceFaqResponseArgs.get(mockGetDatasourceFaqResponse).get(0));
      assertEquals(
          mockFaqId,
          constructionGetDatasourceFaqResponseArgs.get(mockGetDatasourceFaqResponse).get(1));
      assertEquals(
          mockDatasourceId,
          constructionGetDatasourceFaqResponseArgs.get(mockGetDatasourceFaqResponse).get(2));
      assertEquals(
          mockFaqLang,
          constructionGetDatasourceFaqResponseArgs.get(mockGetDatasourceFaqResponse).get(3));
      assertEquals(
          mockFaqTitle,
          constructionGetDatasourceFaqResponseArgs.get(mockGetDatasourceFaqResponse).get(4));
      assertEquals(
          mockFaqContent,
          constructionGetDatasourceFaqResponseArgs.get(mockGetDatasourceFaqResponse).get(5));
      assertEquals(
          mockFaqCreated,
          constructionGetDatasourceFaqResponseArgs.get(mockGetDatasourceFaqResponse).get(6));
      assertEquals(
          mockFaqUpdated,
          constructionGetDatasourceFaqResponseArgs.get(mockGetDatasourceFaqResponse).get(7));

      verify(mockGetDatasourceFaqResponse, times(1)).setDeleted(mockFaq.getDeleted());

      assertEquals(mockGetDatasourceFaqResponse, response);
    }
  }

  @Test
  public void createPatchDatasourceFaqResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockDatasource = mock(Datasource.class);
    final var mockDatasourceId = mock(UUID.class);
    final var mockLinkMap = mock(Map.class);
    final var mockFaq = mock(Faq.class);
    final var mockFaqId = mock(UUID.class);
    final var mockFaqLang = "mock-faq-lang";
    final var mockFaqTitle = "mock-faq-title";
    final var mockFaqContent = "mock-faq-content";
    final var mockFaqCreated = mock(ZonedDateTime.class);
    final var mockFaqUpdated = mock(ZonedDateTime.class);
    final var mockFaqDeleted = mock(ZonedDateTime.class);

    when(mockDatasource.getId()).thenReturn(mockDatasourceId);
    when(mockFaq.getId()).thenReturn(mockFaqId);
    when(mockFaq.getLang()).thenReturn(mockFaqLang);
    when(mockFaq.getTitle()).thenReturn(mockFaqTitle);
    when(mockFaq.getContent()).thenReturn(mockFaqContent);
    when(mockFaq.getCreated()).thenReturn(mockFaqCreated);
    when(mockFaq.getUpdated()).thenReturn(mockFaqUpdated);
    when(mockFaq.getDeleted()).thenReturn(mockFaqDeleted);

    final var constructionPatchDatasourceFaqResponseArgs =
        new HashMap<PatchDatasourceFaqResponse, List<?>>();

    try (final var constructionGetDatasourceFaqResponse =
             mockConstruction(
                 PatchDatasourceFaqResponse.class,
                 (mock, ctx) ->
                     constructionPatchDatasourceFaqResponseArgs.put(mock, ctx.arguments()));
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createPatchDatasourceFaqResponse(
              mockRequest, mockBaseUrl, mockDatasourceId, mockFaq);

      // Assert
      assertEquals(1, constructionGetDatasourceFaqResponse.constructed().size());

      final var mockPatchDatasourceFaqResponse =
          constructionGetDatasourceFaqResponse.constructed().getFirst();

      assertEquals(
          8, constructionPatchDatasourceFaqResponseArgs.get(mockPatchDatasourceFaqResponse).size());

      assertEquals(
          mockLinkMap,
          constructionPatchDatasourceFaqResponseArgs.get(mockPatchDatasourceFaqResponse).get(0));
      assertEquals(
          mockFaqId,
          constructionPatchDatasourceFaqResponseArgs.get(mockPatchDatasourceFaqResponse).get(1));
      assertEquals(
          mockDatasourceId,
          constructionPatchDatasourceFaqResponseArgs.get(mockPatchDatasourceFaqResponse).get(2));
      assertEquals(
          mockFaqLang,
          constructionPatchDatasourceFaqResponseArgs.get(mockPatchDatasourceFaqResponse).get(3));
      assertEquals(
          mockFaqTitle,
          constructionPatchDatasourceFaqResponseArgs.get(mockPatchDatasourceFaqResponse).get(4));
      assertEquals(
          mockFaqContent,
          constructionPatchDatasourceFaqResponseArgs.get(mockPatchDatasourceFaqResponse).get(5));
      assertEquals(
          mockFaqCreated,
          constructionPatchDatasourceFaqResponseArgs.get(mockPatchDatasourceFaqResponse).get(6));
      assertEquals(
          mockFaqUpdated,
          constructionPatchDatasourceFaqResponseArgs.get(mockPatchDatasourceFaqResponse).get(7));

      verify(mockPatchDatasourceFaqResponse, times(1)).setDeleted(mockFaq.getDeleted());

      assertEquals(mockPatchDatasourceFaqResponse, response);
    }
  }

  @Test
  public void createPostUserSharingResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockSharing = mock(Sharing.class);
    final var mockSharingId = mock(UUID.class);
    final var mockSharingDatasource = mock(Datasource.class);
    final var mockSharingDatasourceId = mock(UUID.class);
    final var mockSharingPerson = mock(Person.class);
    final var mockSharingUserId = "mock-sharing-Person-id";
    final var mockSharingClient = mock(Client.class);
    final var mockSharingClientId = mock(UUID.class);
    final var mockSharingPublic = false;
    final var mockSharingCreated = mock(ZonedDateTime.class);
    final var mockSharingUpdated = mock(ZonedDateTime.class);
    final var mockSharingRevoked = mock(ZonedDateTime.class);
    final var mockSharingExpires = mock(ZonedDateTime.class);
    final var mockLinkMap = mock(Map.class);

    when(mockSharing.getClient()).thenReturn(mockSharingClient);
    when(mockSharingClient.getId()).thenReturn(mockSharingClientId);
    when(mockSharing.getId()).thenReturn(mockSharingId);
    when(mockSharing.getDatasource()).thenReturn(mockSharingDatasource);
    when(mockSharingDatasource.getId()).thenReturn(mockSharingDatasourceId);
    when(mockSharing.getPerson()).thenReturn(mockSharingPerson);
    when(mockSharingPerson.getId()).thenReturn(mockSharingUserId);
    when(mockSharing.getCreated()).thenReturn(mockSharingCreated);
    when(mockSharing.getUpdated()).thenReturn(mockSharingUpdated);
    when(mockSharing.getRevoked()).thenReturn(mockSharingRevoked);
    when(mockSharing.getExpires()).thenReturn(mockSharingExpires);

    final var constructionPostUserSharingResponseArgs =
        new HashMap<PostUserSharingResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionPostUserSharingResponse =
             mockConstruction(
                 PostUserSharingResponse.class,
                 (mock, ctx) ->
                     constructionPostUserSharingResponseArgs.put(mock, ctx.arguments()))) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createPostUserSharingResponse(mockRequest, mockBaseUrl, mockSharing);

      // Assert
      assertEquals(1, constructionPostUserSharingResponse.constructed().size());

      final var mockPostUserSharingResponse =
          constructionPostUserSharingResponse.constructed().getFirst();

      assertEquals(
          7, constructionPostUserSharingResponseArgs.get(mockPostUserSharingResponse).size());

      assertEquals(
          mockLinkMap,
          constructionPostUserSharingResponseArgs.get(mockPostUserSharingResponse).get(0));
      assertEquals(
          mockSharingId,
          constructionPostUserSharingResponseArgs.get(mockPostUserSharingResponse).get(1));
      assertEquals(
          mockSharingDatasourceId,
          constructionPostUserSharingResponseArgs.get(mockPostUserSharingResponse).get(2));
      assertEquals(
          mockSharingUserId,
          constructionPostUserSharingResponseArgs.get(mockPostUserSharingResponse).get(3));
      assertEquals(
          mockSharingPublic,
          constructionPostUserSharingResponseArgs.get(mockPostUserSharingResponse).get(4));
      assertEquals(
          mockSharingCreated,
          constructionPostUserSharingResponseArgs.get(mockPostUserSharingResponse).get(5));
      assertEquals(
          mockSharingUpdated,
          constructionPostUserSharingResponseArgs.get(mockPostUserSharingResponse).get(6));

      verify(response, times(1)).clientId(mockSharingClientId);
      verify(response, times(1)).setRevoked(mockSharingRevoked);
      verify(response, times(1)).setExpires(mockSharingExpires);

      assertEquals(mockPostUserSharingResponse, response);
    }
  }

  @Test
  public void createPostUserSharingResponse_whenClientIsNull_thenPublicIsTrue() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockSharing = mock(Sharing.class);
    final var mockSharingId = mock(UUID.class);
    final var mockSharingDatasource = mock(Datasource.class);
    final var mockSharingDatasourceId = mock(UUID.class);
    final var mockSharingUser = mock(Person.class);
    final var mockSharingUserId = "mock-sharing-Person-id";
    final var mockSharingCreated = mock(ZonedDateTime.class);
    final var mockSharingUpdated = mock(ZonedDateTime.class);
    final var mockSharingRevoked = mock(ZonedDateTime.class);
    final var mockSharingExpires = mock(ZonedDateTime.class);
    final var mockLinkMap = mock(Map.class);

    when(mockSharing.getClient()).thenReturn(null);
    when(mockSharing.getId()).thenReturn(mockSharingId);
    when(mockSharing.getDatasource()).thenReturn(mockSharingDatasource);
    when(mockSharingDatasource.getId()).thenReturn(mockSharingDatasourceId);
    when(mockSharing.getPerson()).thenReturn(mockSharingUser);
    when(mockSharingUser.getId()).thenReturn(mockSharingUserId);
    when(mockSharing.getCreated()).thenReturn(mockSharingCreated);
    when(mockSharing.getUpdated()).thenReturn(mockSharingUpdated);
    when(mockSharing.getRevoked()).thenReturn(mockSharingRevoked);
    when(mockSharing.getExpires()).thenReturn(mockSharingExpires);

    final var constructionPostUserSharingResponseArgs =
        new HashMap<PostUserSharingResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionPostUserSharingResponse =
             mockConstruction(
                 PostUserSharingResponse.class,
                 (mock, ctx) ->
                     constructionPostUserSharingResponseArgs.put(mock, ctx.arguments()))) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createPostUserSharingResponse(mockRequest, mockBaseUrl, mockSharing);

      // Assert
      assertEquals(1, constructionPostUserSharingResponse.constructed().size());

      final var mockPostUserSharingResponse =
          constructionPostUserSharingResponse.constructed().getFirst();

      assertEquals(
          7, constructionPostUserSharingResponseArgs.get(mockPostUserSharingResponse).size());

      assertEquals(
          mockLinkMap,
          constructionPostUserSharingResponseArgs.get(mockPostUserSharingResponse).get(0));
      assertEquals(
          mockSharingId,
          constructionPostUserSharingResponseArgs.get(mockPostUserSharingResponse).get(1));
      assertEquals(
          mockSharingDatasourceId,
          constructionPostUserSharingResponseArgs.get(mockPostUserSharingResponse).get(2));
      assertEquals(
          mockSharingUserId,
          constructionPostUserSharingResponseArgs.get(mockPostUserSharingResponse).get(3));
      assertEquals(
          true,
          constructionPostUserSharingResponseArgs.get(mockPostUserSharingResponse).get(4));
      assertEquals(
          mockSharingCreated,
          constructionPostUserSharingResponseArgs.get(mockPostUserSharingResponse).get(5));
      assertEquals(
          mockSharingUpdated,
          constructionPostUserSharingResponseArgs.get(mockPostUserSharingResponse).get(6));

      verify(response, times(1)).setRevoked(mockSharingRevoked);
      verify(response, times(1)).setExpires(mockSharingExpires);

      assertEquals(mockPostUserSharingResponse, response);
    }
  }

  @Test
  public void createGetUserSharingResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockSharing = mock(Sharing.class);
    final var mockSharingId = mock(UUID.class);
    final var mockSharingDatasource = mock(Datasource.class);
    final var mockSharingDatasourceId = mock(UUID.class);
    final var mockSharingPerson = mock(Person.class);
    final var mockSharingUserId = "mock-sharing-Person-id";
    final var mockSharingClient = mock(Client.class);
    final var mockSharingClientId = mock(UUID.class);
    final var mockSharingPublic = false;
    final var mockSharingCreated = mock(ZonedDateTime.class);
    final var mockSharingUpdated = mock(ZonedDateTime.class);
    final var mockSharingRevoked = mock(ZonedDateTime.class);
    final var mockSharingExpires = mock(ZonedDateTime.class);
    final var mockLinkMap = mock(Map.class);

    when(mockSharing.getClient()).thenReturn(mockSharingClient);
    when(mockSharingClient.getId()).thenReturn(mockSharingClientId);
    when(mockSharing.getId()).thenReturn(mockSharingId);
    when(mockSharing.getDatasource()).thenReturn(mockSharingDatasource);
    when(mockSharingDatasource.getId()).thenReturn(mockSharingDatasourceId);
    when(mockSharing.getPerson()).thenReturn(mockSharingPerson);
    when(mockSharingPerson.getId()).thenReturn(mockSharingUserId);
    when(mockSharing.getCreated()).thenReturn(mockSharingCreated);
    when(mockSharing.getUpdated()).thenReturn(mockSharingUpdated);
    when(mockSharing.getRevoked()).thenReturn(mockSharingRevoked);
    when(mockSharing.getExpires()).thenReturn(mockSharingExpires);

    final var constructionGetUserSharingResponseArgs =
        new HashMap<GetUserSharingResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionGetUserSharingResponse =
             mockConstruction(
                 GetUserSharingResponse.class,
                 (mock, ctx) -> constructionGetUserSharingResponseArgs.put(mock, ctx.arguments()))) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createGetUserSharingResponse(mockRequest, mockBaseUrl, mockSharing);

      // Assert
      assertEquals(1, constructionGetUserSharingResponse.constructed().size());

      final var mockGetUserSharingResponse =
          constructionGetUserSharingResponse.constructed().getFirst();

      assertEquals(
          7, constructionGetUserSharingResponseArgs.get(mockGetUserSharingResponse).size());

      assertEquals(
          mockLinkMap,
          constructionGetUserSharingResponseArgs.get(mockGetUserSharingResponse).get(0));
      assertEquals(
          mockSharingId,
          constructionGetUserSharingResponseArgs.get(mockGetUserSharingResponse).get(1));
      assertEquals(
          mockSharingDatasourceId,
          constructionGetUserSharingResponseArgs.get(mockGetUserSharingResponse).get(2));
      assertEquals(
          mockSharingUserId,
          constructionGetUserSharingResponseArgs.get(mockGetUserSharingResponse).get(3));
      assertEquals(
          mockSharingPublic,
          constructionGetUserSharingResponseArgs.get(mockGetUserSharingResponse).get(4));
      assertEquals(
          mockSharingCreated,
          constructionGetUserSharingResponseArgs.get(mockGetUserSharingResponse).get(5));
      assertEquals(
          mockSharingUpdated,
          constructionGetUserSharingResponseArgs.get(mockGetUserSharingResponse).get(6));

      verify(response, times(1)).clientId(mockSharingClientId);
      verify(response, times(1)).setRevoked(mockSharingRevoked);
      verify(response, times(1)).setExpires(mockSharingExpires);

      assertEquals(mockGetUserSharingResponse, response);
    }
  }

  @Test
  public void createGetUserSharingResponse_whenClientIsNull_thenPublicIsTrue() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockSharing = mock(Sharing.class);
    final var mockSharingId = mock(UUID.class);
    final var mockSharingDatasource = mock(Datasource.class);
    final var mockSharingDatasourceId = mock(UUID.class);
    final var mockSharingUser = mock(Person.class);
    final var mockSharingUserId = "mock-sharing-Person-id";
    final var mockSharingCreated = mock(ZonedDateTime.class);
    final var mockSharingUpdated = mock(ZonedDateTime.class);
    final var mockSharingRevoked = mock(ZonedDateTime.class);
    final var mockSharingExpires = mock(ZonedDateTime.class);
    final var mockLinkMap = mock(Map.class);

    when(mockSharing.getClient()).thenReturn(null);
    when(mockSharing.getId()).thenReturn(mockSharingId);
    when(mockSharing.getDatasource()).thenReturn(mockSharingDatasource);
    when(mockSharingDatasource.getId()).thenReturn(mockSharingDatasourceId);
    when(mockSharing.getPerson()).thenReturn(mockSharingUser);
    when(mockSharingUser.getId()).thenReturn(mockSharingUserId);
    when(mockSharing.getCreated()).thenReturn(mockSharingCreated);
    when(mockSharing.getUpdated()).thenReturn(mockSharingUpdated);
    when(mockSharing.getRevoked()).thenReturn(mockSharingRevoked);
    when(mockSharing.getExpires()).thenReturn(mockSharingExpires);

    final var constructionGetUserSharingResponseArgs =
        new HashMap<GetUserSharingResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionGetUserSharingResponse =
             mockConstruction(
                 GetUserSharingResponse.class,
                 (mock, ctx) -> constructionGetUserSharingResponseArgs.put(mock, ctx.arguments()))) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createGetUserSharingResponse(mockRequest, mockBaseUrl, mockSharing);

      // Assert
      assertEquals(1, constructionGetUserSharingResponse.constructed().size());

      final var mockGetUserSharingResponse =
          constructionGetUserSharingResponse.constructed().getFirst();

      assertEquals(
          7, constructionGetUserSharingResponseArgs.get(mockGetUserSharingResponse).size());

      assertEquals(
          mockLinkMap,
          constructionGetUserSharingResponseArgs.get(mockGetUserSharingResponse).get(0));
      assertEquals(
          mockSharingId,
          constructionGetUserSharingResponseArgs.get(mockGetUserSharingResponse).get(1));
      assertEquals(
          mockSharingDatasourceId,
          constructionGetUserSharingResponseArgs.get(mockGetUserSharingResponse).get(2));
      assertEquals(
          mockSharingUserId,
          constructionGetUserSharingResponseArgs.get(mockGetUserSharingResponse).get(3));
      assertEquals(
          true,
          constructionGetUserSharingResponseArgs.get(mockGetUserSharingResponse).get(4));
      assertEquals(
          mockSharingCreated,
          constructionGetUserSharingResponseArgs.get(mockGetUserSharingResponse).get(5));
      assertEquals(
          mockSharingUpdated,
          constructionGetUserSharingResponseArgs.get(mockGetUserSharingResponse).get(6));

      verify(response, times(1)).setRevoked(mockSharingRevoked);
      verify(response, times(1)).setExpires(mockSharingExpires);

      assertEquals(mockGetUserSharingResponse, response);
    }
  }

  @Test
  public void createPatchUserSharingResponse() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockSharing = mock(Sharing.class);
    final var mockSharingId = mock(UUID.class);
    final var mockSharingDatasource = mock(Datasource.class);
    final var mockSharingDatasourceId = mock(UUID.class);
    final var mockSharingPerson = mock(Person.class);
    final var mockSharingUserId = "mock-sharing-Person-id";
    final var mockSharingClient = mock(Client.class);
    final var mockSharingClientId = mock(UUID.class);
    final var mockSharingPublic = false;
    final var mockSharingCreated = mock(ZonedDateTime.class);
    final var mockSharingUpdated = mock(ZonedDateTime.class);
    final var mockSharingRevoked = mock(ZonedDateTime.class);
    final var mockSharingExpires = mock(ZonedDateTime.class);
    final var mockLinkMap = mock(Map.class);

    when(mockSharing.getClient()).thenReturn(mockSharingClient);
    when(mockSharingClient.getId()).thenReturn(mockSharingClientId);
    when(mockSharing.getId()).thenReturn(mockSharingId);
    when(mockSharing.getDatasource()).thenReturn(mockSharingDatasource);
    when(mockSharingDatasource.getId()).thenReturn(mockSharingDatasourceId);
    when(mockSharing.getPerson()).thenReturn(mockSharingPerson);
    when(mockSharingPerson.getId()).thenReturn(mockSharingUserId);
    when(mockSharing.getCreated()).thenReturn(mockSharingCreated);
    when(mockSharing.getUpdated()).thenReturn(mockSharingUpdated);
    when(mockSharing.getRevoked()).thenReturn(mockSharingRevoked);
    when(mockSharing.getExpires()).thenReturn(mockSharingExpires);

    final var constructionPatchUserSharingResponseArgs =
        new HashMap<PatchUserSharingResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionPatchUserSharingResponse =
             mockConstruction(
                 PatchUserSharingResponse.class,
                 (mock, ctx) ->
                     constructionPatchUserSharingResponseArgs.put(mock, ctx.arguments()))) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createPatchUserSharingResponse(mockRequest, mockBaseUrl, mockSharing);

      // Assert
      assertEquals(1, constructionPatchUserSharingResponse.constructed().size());

      final var mockPatchUserSharingResponse =
          constructionPatchUserSharingResponse.constructed().getFirst();

      assertEquals(
          7, constructionPatchUserSharingResponseArgs.get(mockPatchUserSharingResponse).size());

      assertEquals(
          mockLinkMap,
          constructionPatchUserSharingResponseArgs.get(mockPatchUserSharingResponse).get(0));
      assertEquals(
          mockSharingId,
          constructionPatchUserSharingResponseArgs.get(mockPatchUserSharingResponse).get(1));
      assertEquals(
          mockSharingDatasourceId,
          constructionPatchUserSharingResponseArgs.get(mockPatchUserSharingResponse).get(2));
      assertEquals(
          mockSharingUserId,
          constructionPatchUserSharingResponseArgs.get(mockPatchUserSharingResponse).get(3));
      assertEquals(
          mockSharingPublic,
          constructionPatchUserSharingResponseArgs.get(mockPatchUserSharingResponse).get(4));
      assertEquals(
          mockSharingCreated,
          constructionPatchUserSharingResponseArgs.get(mockPatchUserSharingResponse).get(5));
      assertEquals(
          mockSharingUpdated,
          constructionPatchUserSharingResponseArgs.get(mockPatchUserSharingResponse).get(6));

      verify(response, times(1)).clientId(mockSharingClientId);
      verify(response, times(1)).setRevoked(mockSharingRevoked);
      verify(response, times(1)).setExpires(mockSharingExpires);

      assertEquals(mockPatchUserSharingResponse, response);
    }
  }

  @Test
  public void createPatchUserSharingResponse_whenClientIsNull_thenPublicIsTrue() {
    // Arrange
    final var mockRequest = mock(NativeWebRequest.class);
    final var mockBaseUrl = "mock-base-url";
    final var mockSharing = mock(Sharing.class);
    final var mockSharingId = mock(UUID.class);
    final var mockSharingDatasource = mock(Datasource.class);
    final var mockSharingDatasourceId = mock(UUID.class);
    final var mockSharingUser = mock(Person.class);
    final var mockSharingUserId = "mock-sharing-Person-id";
    final var mockSharingCreated = mock(ZonedDateTime.class);
    final var mockSharingUpdated = mock(ZonedDateTime.class);
    final var mockSharingRevoked = mock(ZonedDateTime.class);
    final var mockSharingExpires = mock(ZonedDateTime.class);
    final var mockLinkMap = mock(Map.class);

    when(mockSharing.getClient()).thenReturn(null);
    when(mockSharing.getId()).thenReturn(mockSharingId);
    when(mockSharing.getDatasource()).thenReturn(mockSharingDatasource);
    when(mockSharingDatasource.getId()).thenReturn(mockSharingDatasourceId);
    when(mockSharing.getPerson()).thenReturn(mockSharingUser);
    when(mockSharingUser.getId()).thenReturn(mockSharingUserId);
    when(mockSharing.getCreated()).thenReturn(mockSharingCreated);
    when(mockSharing.getUpdated()).thenReturn(mockSharingUpdated);
    when(mockSharing.getRevoked()).thenReturn(mockSharingRevoked);
    when(mockSharing.getExpires()).thenReturn(mockSharingExpires);

    final var constructionPatchUserSharingResponseArgs =
        new HashMap<PatchUserSharingResponse, List<?>>();

    try (final var staticHateoas = mockStatic(Hateoas.class);
         final var constructionPatchUserSharingResponse =
             mockConstruction(
                 PatchUserSharingResponse.class,
                 (mock, ctx) ->
                     constructionPatchUserSharingResponseArgs.put(mock, ctx.arguments()))) {

      staticHateoas.when(() -> Hateoas.linkMap(mockRequest, mockBaseUrl)).thenReturn(mockLinkMap);

      // Act
      final var response =
          ResponseFactory.createPatchUserSharingResponse(mockRequest, mockBaseUrl, mockSharing);

      // Assert
      assertEquals(1, constructionPatchUserSharingResponse.constructed().size());

      final var mockPatchUserSharingResponse =
          constructionPatchUserSharingResponse.constructed().getFirst();

      assertEquals(
          7, constructionPatchUserSharingResponseArgs.get(mockPatchUserSharingResponse).size());

      assertEquals(
          mockLinkMap,
          constructionPatchUserSharingResponseArgs.get(mockPatchUserSharingResponse).get(0));
      assertEquals(
          mockSharingId,
          constructionPatchUserSharingResponseArgs.get(mockPatchUserSharingResponse).get(1));
      assertEquals(
          mockSharingDatasourceId,
          constructionPatchUserSharingResponseArgs.get(mockPatchUserSharingResponse).get(2));
      assertEquals(
          mockSharingUserId,
          constructionPatchUserSharingResponseArgs.get(mockPatchUserSharingResponse).get(3));
      assertEquals(
          true,
          constructionPatchUserSharingResponseArgs.get(mockPatchUserSharingResponse).get(4));
      assertEquals(
          mockSharingCreated,
          constructionPatchUserSharingResponseArgs.get(mockPatchUserSharingResponse).get(5));
      assertEquals(
          mockSharingUpdated,
          constructionPatchUserSharingResponseArgs.get(mockPatchUserSharingResponse).get(6));

      verify(response, times(1)).setRevoked(mockSharingRevoked);
      verify(response, times(1)).setExpires(mockSharingExpires);

      assertEquals(mockPatchUserSharingResponse, response);
    }
  }

  @Test
  public void getApiInfoResponse() {
    // Arrange
    final var mockApiName = "mockApiName";
    final var mockApiVersion = "mockApiVersion";
    final var mockBuildVersion = "mockBuildVersion";
    final var mockApiReleased = mock(LocalDate.class);
    final var mockApiDocumentation = mock(URL.class);
    final var mockApiStatus = mock(ApiStatus.class);

    final var constructionGetApiInfoResponseArgs = new HashMap<GetApiInfoResponse, List<?>>();

    try (final var constructionGetApiInfoResponse =
             mockConstruction(
                 GetApiInfoResponse.class,
                 (mock, ctx) -> constructionGetApiInfoResponseArgs.put(mock, ctx.arguments()))) {

      // Act
      final var response =
          ResponseFactory.getApiInfoResponse(
              mockApiName,
              mockApiVersion,
              mockBuildVersion,
              mockApiReleased,
              mockApiDocumentation,
              mockApiStatus);

      assertEquals(1, constructionGetApiInfoResponse.constructed().size());

      final var mockGetApiInfoResponse = constructionGetApiInfoResponse.constructed().getFirst();

      assertEquals(6, constructionGetApiInfoResponseArgs.get(mockGetApiInfoResponse).size());

      assertEquals(
          mockApiName, constructionGetApiInfoResponseArgs.get(mockGetApiInfoResponse).get(0));

      assertEquals(
          mockApiVersion, constructionGetApiInfoResponseArgs.get(mockGetApiInfoResponse).get(1));

      assertEquals(
          mockBuildVersion, constructionGetApiInfoResponseArgs.get(mockGetApiInfoResponse).get(2));

      assertEquals(
          mockApiReleased, constructionGetApiInfoResponseArgs.get(mockGetApiInfoResponse).get(3));

      assertEquals(
          mockApiDocumentation,
          constructionGetApiInfoResponseArgs.get(mockGetApiInfoResponse).get(4));

      assertEquals(
          mockApiStatus, constructionGetApiInfoResponseArgs.get(mockGetApiInfoResponse).get(5));
    }
  }

  @Test
  public void getHealthResponse() {
    // Arrange
    final var constructionGetHealthResponseArgs = new HashMap<GetHealthResponse, List<?>>();
    final var mockStatus = mock(Status.class);
    final var mockHealthChecks = mock(HealthChecks.class);

    try (final var constructionGetHealthResponse =
             mockConstruction(
                 GetHealthResponse.class,
                 (mock, ctx) -> constructionGetHealthResponseArgs.put(mock, ctx.arguments()))) {

      // Act
      final var response = ResponseFactory.getHealthResponse(mockStatus, mockHealthChecks);

      assertEquals(1, constructionGetHealthResponse.constructed().size());

      final var mockGetHealthResponse = constructionGetHealthResponse.constructed().getFirst();

      assertEquals(2, constructionGetHealthResponseArgs.get(mockGetHealthResponse).size());

      assertEquals(mockStatus, constructionGetHealthResponseArgs.get(mockGetHealthResponse).get(0));

      assertEquals(
          mockHealthChecks, constructionGetHealthResponseArgs.get(mockGetHealthResponse).get(1));
    }
  }
}
