package se.jobtechdev.personaldatagateway.api.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerSupplierTest {

  private static class Dummy {}

  @Test
  public void forClass() {
    try (MockedStatic<LoggerFactory> staticMock = mockStatic(LoggerFactory.class)) {

      // Arrange
      Logger mockLogger = mock(Logger.class);
      staticMock.when(() -> LoggerFactory.getLogger(Dummy.class.getName())).thenReturn(mockLogger);

      // Act
      Logger logger = LoggerSupplier.forClass(Dummy.class).get();

      // Assert
      assertEquals(mockLogger, logger);
    }
  }
}
