package se.jobtechdev.personaldatagateway.api.util;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HeaderExtractorTest {
  @Test
  public void extractHeader_whenHeaderIsNullString_returnEmptyMap() {
    final var headerMap = HeaderExtractor.extractHeader(null);
    assertEquals(Map.of(), headerMap);
  }

  @Test
  public void extractHeader_whenHeaderIsInvalidFormat_omitFromResultMap() {
    final var headerMap = HeaderExtractor.extractHeader("header1=v1,v2&header2v3&header3=v4,v5,v6");
    assertEquals(Map.of("header1", "v1,v2", "header3", "v4,v5,v6"), headerMap);
  }

  @Test
  public void extractHeader() {
    final var headerString = "header1=v1,v2&header2=v3&header3=v4,v5,v6";
    final var headerMap = HeaderExtractor.extractHeader(headerString);
    assertEquals(Map.of("header1", "v1,v2", "header2", "v3", "header3", "v4,v5,v6"), headerMap);
  }
}
