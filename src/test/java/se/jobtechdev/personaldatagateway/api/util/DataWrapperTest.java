package se.jobtechdev.personaldatagateway.api.util;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.mock;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

public class DataWrapperTest {
  @Test
  @Tag("Unit")
  public void construct() {
    final var body = new byte[] {1, 2, 3};
    final var contentType = mock(MediaType.class);
    final var httpStatus = mock(HttpStatus.class);
    assertDoesNotThrow(() -> new DataWrapper(body, contentType, httpStatus));
  }
}
