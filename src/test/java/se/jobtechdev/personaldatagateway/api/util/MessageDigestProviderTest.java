package se.jobtechdev.personaldatagateway.api.util;

import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class MessageDigestProviderTest {
  @Test
  void sha256_missingProvider() {
    try (final var staticMessageDigest = mockStatic(MessageDigest.class);) {
      // Arrange
      staticMessageDigest
          .when(() -> MessageDigest.getInstance("SHA-256"))
          .thenThrow(NoSuchAlgorithmException.class);

      // Act
      assertThrows(
          Exception.class,
          () -> MessageDigestProvider.sha256("key".getBytes(StandardCharsets.UTF_8)));
    }
  }

  @Test
  void sha256() {
    try (final var staticMessageDigest = mockStatic(MessageDigest.class);) {
      // Arrange
      final var sha256 = mock(MessageDigest.class);
      staticMessageDigest.when(() -> MessageDigest.getInstance("SHA-256")).thenReturn(sha256);
      final var hash = "hash".getBytes(StandardCharsets.UTF_8);
      when(sha256.digest(any())).thenReturn(hash);

      // Act
      final var result = MessageDigestProvider.sha256("key".getBytes(StandardCharsets.UTF_8));

      // Assert
      assertEquals(result, hash);
    }
  }
}
