package se.jobtechdev.personaldatagateway.api.util;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

import java.util.UUID;
import org.junit.jupiter.api.Test;

public class UuidProviderTest {
  @Test
  public void test() {
    assertNotNull(UuidProvider.uuid());
  }

  @Test
  public void generateInequalityCheckingLambda_whenUuidsAreDifferent_returnTrue() {
    final var mockUuid = mock(UUID.class);
    final var otherUuid = mock(UUID.class);

    final var lambda = UuidProvider.generateInequalityCheckingLambda(mockUuid);

    assertTrue(lambda.apply(otherUuid));
  }

  @Test
  public void generateInequalityCheckingLambda_whenUuidsAreEqual_returnFalse() {
    final var mockUuid = mock(UUID.class);

    final var lambda = UuidProvider.generateInequalityCheckingLambda(mockUuid);

    assertFalse(lambda.apply(mockUuid));
  }
}
