package se.jobtechdev.personaldatagateway.api.util;

import org.junit.jupiter.api.Test;

import java.util.Base64;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class KeyProviderTest {
  @Test
  public void randomBytes() {
    // Act
    final var result = KeyProvider.randomBytes(32);

    // Assert
    assertEquals(result.length, 32);
  }

  @Test
  public void b64EncodedKey() {
    // Arrange
    final var bytes = new byte[32];

    try (final var staticBase64 = mockStatic(Base64.class)) {
      final var mockBase64Encoder = mock(Base64.Encoder.class);
      staticBase64.when(Base64::getEncoder).thenReturn(mockBase64Encoder);
      when(mockBase64Encoder.withoutPadding()).thenReturn(mockBase64Encoder);
      when(mockBase64Encoder.encodeToString(bytes)).thenReturn("example-encoded");
      // Act
      final var result = KeyProvider.b64EncodedKey(bytes);

      // Assert
      assertEquals("example-encoded", result);
      staticBase64.verify(Base64::getEncoder, times(1));
      verify(mockBase64Encoder, times(1)).withoutPadding();
      verify(mockBase64Encoder, times(1)).encodeToString(bytes);
    }
  }
}
