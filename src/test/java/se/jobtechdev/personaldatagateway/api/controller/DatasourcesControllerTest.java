package se.jobtechdev.personaldatagateway.api.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import se.jobtechdev.personaldatagateway.api.generated.entities.Datasource;
import se.jobtechdev.personaldatagateway.api.generated.entities.Faq;
import se.jobtechdev.personaldatagateway.api.generated.entities.Organization;
import se.jobtechdev.personaldatagateway.api.generated.model.*;
import se.jobtechdev.personaldatagateway.api.service.DataService;
import se.jobtechdev.personaldatagateway.api.service.DatasourceService;
import se.jobtechdev.personaldatagateway.api.service.FaqService;
import se.jobtechdev.personaldatagateway.api.service.OrganizationService;
import se.jobtechdev.personaldatagateway.api.util.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static se.jobtechdev.personaldatagateway.api.util.ControllerUtil.throwApiExceptionOnAbsentValue;

@ExtendWith(MockitoExtension.class)
public class DatasourcesControllerTest {
  private final String mockBaseUrl = "mock-base-url";
  private final HttpHeaders mockLinkHeader = mock(HttpHeaders.class);
  private DatasourcesController datasourcesController;
  @Mock
  private DatasourceService datasourceService;

  @Mock
  private FaqService faqService;

  @Mock
  private OrganizationService organizationService;

  @Mock
  private DataService dataService;

  @Mock
  private NativeWebRequest request;

  @BeforeEach
  void setUp() {
    datasourcesController =
        new DatasourcesController(
            mockBaseUrl, request, datasourceService, faqService, organizationService, dataService);
  }

  @Test
  void postDatasource() {
    // Arrange
    final var mockOrganization = mock(Organization.class);
    final var mockOrganizationId = mock(UUID.class);
    final var mockDatasource = mock(Datasource.class);
    final var mockPostDatasourceRequest = mock(PostDatasourceRequest.class);
    final var mockPostDatasourceResponse = mock(PostDatasourceResponse.class);
    final var mockPostDatasourceRequestName = "mock-post-datasource-request-name";
    final var mockPostDatasourcePath = "mock-post-datasource-request-path";
    final var mockPostDatasourceHeaders = "mock-post-datasource-request-headers";
    final var mockPostDatasourceMethod = "mock-post-datasource-request-method";
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockPostDatasourceRequest.getOrganizationId()).thenReturn(mockOrganizationId);
    when(mockPostDatasourceRequest.getName()).thenReturn(mockPostDatasourceRequestName);
    when(mockPostDatasourceRequest.getPath()).thenReturn(mockPostDatasourcePath);
    when(mockPostDatasourceRequest.getHeaders()).thenReturn(mockPostDatasourceHeaders);
    when(mockPostDatasourceRequest.getMethod()).thenReturn(mockPostDatasourceMethod);
    when(mockOrganizationId.toString()).thenReturn("mock-organization-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockPostDatasourceResponse))
        .thenReturn(mockResponseEntity);

    when(organizationService.getOrganizationById(mockOrganizationId))
        .thenReturn(Optional.of(mockOrganization));

    when(datasourceService.createDatasource(
        mockOrganization,
        mockPostDatasourceRequestName,
        mockPostDatasourcePath,
        mockPostDatasourceHeaders,
        mockPostDatasourceMethod))
        .thenReturn(Optional.of(mockDatasource));

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.CREATED))
          .thenReturn(mockResponseEntityBodyBuilder);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createPostDatasourceResponse(
                      request, mockBaseUrl, mockDatasource))
          .thenReturn(mockPostDatasourceResponse);

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockOrganization)),
                      eq(HttpStatus.BAD_REQUEST),
                      eq(
                          "Failed to handle POST datasource request - Could not find organization"
                              + " with id: mock-organization-id")))
          .thenReturn(mockOrganization);

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockDatasource)),
                      eq(HttpStatus.BAD_REQUEST),
                      eq(
                          "Failed to handle POST datasource request - Attempt to create datasource"
                              + " resulted in an empty result")))
          .thenReturn(mockDatasource);

      // Act
      final var responseEntity = datasourcesController.postDatasource(mockPostDatasourceRequest);

      // Assert
      assertEquals(mockResponseEntity, responseEntity);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  public void getDatasources() {
    final var mockDatasource = mock(Datasource.class);
    final var mockGetDatasourceResponse = mock(GetDatasourceResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(List.of(mockGetDatasourceResponse)))
        .thenReturn(mockResponseEntity);

    when(datasourceService.getDatasources()).thenReturn(List.of(mockDatasource));

    try (final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);
      // Arrange
      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createGetDatasourceResponse(request, mockBaseUrl, mockDatasource))
          .thenReturn(mockGetDatasourceResponse);

      // Act
      final var responseEntity = datasourcesController.getDatasources();

      // Assert
      assertEquals(mockResponseEntity, responseEntity);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  void getDatasource() {
    final var mockDatasource = mock(Datasource.class);
    final var mockDatasourceId = mock(UUID.class);
    final var mockGetDatasourceResponse = mock(GetDatasourceResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockDatasourceId.toString()).thenReturn("mock-datasource-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockGetDatasourceResponse))
        .thenReturn(mockResponseEntity);

    when(datasourceService.getDatasourceById(mockDatasourceId))
        .thenReturn(Optional.of(mockDatasource));

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      // Arrange
      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockDatasource)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle GET datasource - Could not find datasource with id:"
                              + " mock-datasource-id")))
          .thenReturn(mockDatasource);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createGetDatasourceResponse(request, mockBaseUrl, mockDatasource))
          .thenReturn(mockGetDatasourceResponse);

      // Act
      final var result = datasourcesController.getDatasource(mockDatasourceId);

      // Assert
      assertEquals(mockResponseEntity, result);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  void patchDatasource() {
    // Arrange
    final var mockDatasource = mock(Datasource.class);
    final var mockDatasourceId = mock(UUID.class);
    final var mockAssignedDatasource = mock(Datasource.class);
    final var mockUpdatedDatasource = mock(Datasource.class);
    final var mockPatchDatasourceRequest = mock(PatchDatasourceRequest.class);
    final var mockPatchDatasourceResponse = mock(PatchDatasourceResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockDatasourceId.toString()).thenReturn("mock-datasource-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockPatchDatasourceResponse))
        .thenReturn(mockResponseEntity);

    when(datasourceService.getDatasourceById(mockDatasourceId))
        .thenReturn(Optional.of(mockDatasource));

    when(datasourceService.update(mockAssignedDatasource)).thenReturn(mockUpdatedDatasource);

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.assignNonNull(
                      mockPatchDatasourceRequest, mockDatasource, List.of("deleted")))
          .thenReturn(mockAssignedDatasource);

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockDatasource)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle PATCH datasource - Could not find datasource with id:"
                              + " mock-datasource-id")))
          .thenReturn(mockDatasource);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createPatchDatasourceResponse(
                      request, mockBaseUrl, mockUpdatedDatasource))
          .thenReturn(mockPatchDatasourceResponse);

      // Act
      final var result =
          datasourcesController.patchDatasource(mockDatasourceId, mockPatchDatasourceRequest);

      // Assert
      assertEquals(mockResponseEntity, result);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  public void getDatasourceUserData() {
    // Arrange
    final var mockRequestAcceptHeader = "mock-request-accept-header";
    final var mockDatasource = mock(Datasource.class);
    final var mockDatasourceId = mock(UUID.class);
    final var mockUserId = "mock-user-id";
    final var mockBytes = new byte[]{1, 2, 3};
    final var mockDataWrapper = mock(DataWrapper.class);
    final var mockDataWrapperStatus = mock(HttpStatus.class);
    final var mockDataWrapperContentType = mock(MediaType.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockDatasourceId.toString()).thenReturn("mock-datasource-id");
    when(request.getHeader(HttpHeaders.ACCEPT)).thenReturn(mockRequestAcceptHeader);
    when(mockDataWrapper.contentType()).thenReturn(mockDataWrapperContentType);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockDataWrapper.body()).thenReturn(mockBytes);
    when(mockDataWrapper.status()).thenReturn(mockDataWrapperStatus);

    when(mockResponseEntityBodyBuilder.body(mockBytes)).thenReturn(mockResponseEntity);

    when(datasourceService.getDatasourceById(mockDatasourceId))
        .thenReturn(Optional.of(mockDatasource));

    when(dataService.retrieveData(mockDatasource, mockUserId, mockRequestAcceptHeader))
        .thenReturn(mockDataWrapper);

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockDatasource)),
                      eq(HttpStatus.NOT_FOUND),
                      eq("Could not find datasource by id: mock-datasource-id")))
          .thenReturn(mockDatasource);

      staticResponseEntity
          .when(() -> ResponseEntity.status(mockDataWrapperStatus))
          .thenReturn(mockResponseEntityBodyBuilder);

      when(mockResponseEntityBodyBuilder.contentType(mockDataWrapperContentType))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response =
          datasourcesController.getDatasourceUserData(mockDatasourceId, mockUserId);

      // Assert
      verify(mockResponseEntityBodyBuilder, times(1)).contentType(mockDataWrapperContentType);

      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  public void getDatasourceConfig() {
    // Arrange
    final var mockGetDatasourceConfigResponse = mock(GetDatasourceConfigResponse.class);
    final var mockDatasource = mock(Datasource.class);
    final var mockDatasourceId = mock(UUID.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockDatasourceId.toString()).thenReturn("mock-datasource-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockGetDatasourceConfigResponse))
        .thenReturn(mockResponseEntity);

    when(datasourceService.getDatasourceById(mockDatasourceId))
        .thenReturn(Optional.of(mockDatasource));

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockDatasource)),
                      eq(HttpStatus.NOT_FOUND),
                      eq("Could not find datasource by id: mock-datasource-id")))
          .thenReturn(mockDatasource);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createGetDatasourceConfigResponse(
                      request, mockBaseUrl, mockDatasource))
          .thenReturn(mockGetDatasourceConfigResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response = datasourcesController.getDatasourceConfig(mockDatasourceId);

      // Assert
      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  public void patchDatasourceConfig() {
    // Arrange
    final var mockPatchDatasourceConfigResponse = mock(PatchDatasourceConfigResponse.class);
    final var mockDatasource = mock(Datasource.class);
    final var mockDatasourceId = mock(UUID.class);
    final var mockAssignedDatasource = mock(Datasource.class);
    final var mockUpdatedDatasource = mock(Datasource.class);
    final var mockUpdatedDatasourceHeaders = "mock-updated-datasource-headers";
    final var mockPatchDatasourceConfigRequest = mock(PatchDatasourceConfigRequest.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockDatasourceId.toString()).thenReturn("mock-datasource-id");
    when(mockUpdatedDatasource.getHeaders()).thenReturn(mockUpdatedDatasourceHeaders);

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockPatchDatasourceConfigResponse))
        .thenReturn(mockResponseEntity);

    when(datasourceService.getDatasourceById(mockDatasourceId))
        .thenReturn(Optional.of(mockDatasource));

    when(datasourceService.update(mockAssignedDatasource)).thenReturn(mockUpdatedDatasource);

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createPatchDatasourceConfigResponse(
                      request, mockBaseUrl, mockUpdatedDatasource))
          .thenReturn(mockPatchDatasourceConfigResponse);

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockDatasource)),
                      eq(HttpStatus.NOT_FOUND),
                      eq("Could not find datasource by id: mock-datasource-id")))
          .thenReturn(mockDatasource);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.assignNonNull(
                      mockPatchDatasourceConfigRequest, mockDatasource, List.of("deleted")))
          .thenReturn(mockAssignedDatasource);

      // Act
      final var response =
          datasourcesController.patchDatasourceConfig(
              mockDatasourceId, mockPatchDatasourceConfigRequest);

      // Assert
      verify(mockPatchDatasourceConfigResponse, times(1)).setHeaders(mockUpdatedDatasourceHeaders);

      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  public void postDatasourceFaq() {
    // Arrange
    final var mockDatasource = mock(Datasource.class);
    final var mockDatasourceId = mock(UUID.class);
    final var mockPostDatasourceFaqRequestLang = "mock-post-datasource-faq-request-lang";
    final var mockPostDatasourceFaqRequestTitle = "mock-post-datasource-faq-request-title";
    final var mockPostDatasourceFaqRequestContent = "mock-post-datasource-faq-request-content";
    final var mockPostDatasourceFaqRequest = mock(PostDatasourceFaqRequest.class);
    final var mockPostDatasourceFaqResponse = mock(PostDatasourceFaqResponse.class);
    final var mockFaq = mock(Faq.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockDatasourceId.toString()).thenReturn("mock-datasource-id");
    when(mockPostDatasourceFaqRequest.getLang()).thenReturn(mockPostDatasourceFaqRequestLang);
    when(mockPostDatasourceFaqRequest.getTitle()).thenReturn(mockPostDatasourceFaqRequestTitle);
    when(mockPostDatasourceFaqRequest.getContent()).thenReturn(mockPostDatasourceFaqRequestContent);

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockPostDatasourceFaqResponse))
        .thenReturn(mockResponseEntity);

    when(datasourceService.getDatasourceById(mockDatasourceId))
        .thenReturn(Optional.of(mockDatasource));
    when(faqService.createFaq(
        mockPostDatasourceFaqRequestLang,
        mockPostDatasourceFaqRequestTitle,
        mockPostDatasourceFaqRequestContent))
        .thenReturn(mockFaq);

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class)) {

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockDatasource)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle POST datasource faq request - Could not find datasource"
                              + " with id: mock-datasource-id")))
          .thenReturn(mockDatasource);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createPostDatasourceFaqResponse(
                      request, mockBaseUrl, mockDatasource, mockFaq))
          .thenReturn(mockPostDatasourceFaqResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response =
          datasourcesController.postDatasourceFaq(mockDatasourceId, mockPostDatasourceFaqRequest);

      // Assert
      assertEquals(mockResponseEntity, response);
    }
  }

  @Test
  public void getDatasourceFaqs() {
    // Arrange
    final var mockDatasource = mock(Datasource.class);
    final var mockDatasourceId = mock(UUID.class);
    final var mockAcceptLanguage = "mock-accept-language";
    final var mockFaq = mock(Faq.class);
    final var mockGetDatasourceFaqResponse = mock(GetDatasourceFaqResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockDatasourceId.toString()).thenReturn("mock-datasource-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(List.of(mockGetDatasourceFaqResponse)))
        .thenReturn(mockResponseEntity);

    when(datasourceService.getDatasourceById(mockDatasourceId))
        .thenReturn(Optional.of(mockDatasource));
    when(faqService.getFaqs(mockDatasource, mockAcceptLanguage)).thenReturn(Set.of(mockFaq));

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticUuidProvider = mockStatic(UuidProvider.class)) {

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockDatasource)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle GET datasource faqs request - Could not find datasource"
                              + " with id: mock-datasource-id")))
          .thenReturn(mockDatasource);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createGetDatasourceFaqResponse(
                      request, mockBaseUrl, mockDatasourceId, mockFaq))
          .thenReturn(mockGetDatasourceFaqResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      final Function<UUID, Boolean> mockInequalityCheckingLambda = (uuid) -> true;
      staticUuidProvider
          .when(() -> UuidProvider.generateInequalityCheckingLambda(mockDatasourceId))
          .thenReturn(mockInequalityCheckingLambda);

      // Act
      final var response =
          datasourcesController.getDatasourceFaqs(mockDatasourceId, mockAcceptLanguage);

      // Assert
      assertEquals(mockResponseEntity, response);
    }
  }

  @Test
  public void getDatasourceFaq() {
    // Arrange
    final var mockDatasource = mock(Datasource.class);
    final var mockDatasourceId = mock(UUID.class);
    final var mockFaq = mock(Faq.class);
    final var mockFaqId = mock(UUID.class);
    final var mockGetDatasourceFaqResponse = mock(GetDatasourceFaqResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockFaqId.toString()).thenReturn("mock-faq-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockGetDatasourceFaqResponse))
        .thenReturn(mockResponseEntity);

    when(faqService.getFaqById(mockFaqId)).thenReturn(Optional.of(mockFaq));

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticUuidProvider = mockStatic(UuidProvider.class)) {

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockFaq)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle GET datasource faq request - Could not find faq with"
                              + " id: mock-faq-id")))
          .thenReturn(mockFaq);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createGetDatasourceFaqResponse(
                      request, mockBaseUrl, mockDatasourceId, mockFaq))
          .thenReturn(mockGetDatasourceFaqResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      final Function<UUID, Boolean> mockInequalityCheckingLambda = (uuid) -> true;
      staticUuidProvider
          .when(() -> UuidProvider.generateInequalityCheckingLambda(mockDatasourceId))
          .thenReturn(mockInequalityCheckingLambda);

      // Act
      final var response = datasourcesController.getDatasourceFaq(mockDatasourceId, mockFaqId);

      // Assert
      assertEquals(mockResponseEntity, response);
    }
  }

  @Test
  public void patchDatasourceFaq() {
    // Arrange
    final var mockDatasourceId = mock(UUID.class);
    final var mockFaq = mock(Faq.class);
    final var mockFaqId = mock(UUID.class);
    final var mockPatchDatasourceFaqRequest = mock(PatchDatasourceFaqRequest.class);
    final var mockPatchDatasourceFaqResponse = mock(PatchDatasourceFaqResponse.class);
    final var mockAssignedFaq = mock(Faq.class);
    final var mockUpdatedFaq = mock(Faq.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockFaqId.toString()).thenReturn("mock-faq-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockPatchDatasourceFaqResponse))
        .thenReturn(mockResponseEntity);

    when(faqService.getFaqById(mockFaqId)).thenReturn(Optional.of(mockFaq));
    when(faqService.update(mockAssignedFaq)).thenReturn(mockUpdatedFaq);

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticUuidProvider = mockStatic(UuidProvider.class)) {

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockFaq)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle PATCH datasource faq request - Could not find faq with"
                              + " id: mock-faq-id")))
          .thenReturn(mockFaq);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.assignNonNull(
                      mockPatchDatasourceFaqRequest, mockFaq, List.of("deleted")))
          .thenReturn(mockAssignedFaq);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createPatchDatasourceFaqResponse(
                      request, mockBaseUrl, mockDatasourceId, mockUpdatedFaq))
          .thenReturn(mockPatchDatasourceFaqResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      final Function<UUID, Boolean> mockInequalityCheckingLambda = (uuid) -> true;
      staticUuidProvider
          .when(() -> UuidProvider.generateInequalityCheckingLambda(mockDatasourceId))
          .thenReturn(mockInequalityCheckingLambda);

      // Act
      final var response =
          datasourcesController.patchDatasourceFaq(
              mockDatasourceId, mockFaqId, mockPatchDatasourceFaqRequest);

      // Assert
      assertEquals(mockResponseEntity, response);
    }
  }
}
