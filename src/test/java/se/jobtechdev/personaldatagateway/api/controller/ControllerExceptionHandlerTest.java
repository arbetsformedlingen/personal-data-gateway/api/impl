package se.jobtechdev.personaldatagateway.api.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import se.jobtechdev.personaldatagateway.api.exception.ApiException;
import se.jobtechdev.personaldatagateway.api.generated.model.ErrorResponse;
import se.jobtechdev.personaldatagateway.api.util.ErrorResponseFactory;
import se.jobtechdev.personaldatagateway.api.util.ResponseEntityFactory;

@ExtendWith(MockitoExtension.class)
public class ControllerExceptionHandlerTest {
  private ControllerExceptionHandler controllerExceptionHandler;

  @BeforeEach
  void setUp() {
    controllerExceptionHandler = new ControllerExceptionHandler();
  }

  @Test
  public void test() {
    final var logger = LoggerFactory.getLogger(ControllerExceptionHandlerTest.class);
    final var errorResponse = new ErrorResponse(HttpStatus.NOT_FOUND.value(), "Error text");
    errorResponse.setMessage("Error message");
    final var apiException = new ApiException(errorResponse);
    logger.info("Logger ApiException = " + apiException);
  }

  @Test
  public void exception_withApiException() throws JsonProcessingException {
    // Arrange
    final var mockHttpServletRequest = mock(HttpServletRequest.class);
    final var mockApiException = mock(ApiException.class);
    final var mockErrorResponse = mock(ErrorResponse.class);
    final var mockResponseEntity = mock(ResponseEntity.class);
    final var mockResponseEntityBodyBuilder1 = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntityBodyBuilder2 = mock(ResponseEntity.BodyBuilder.class);
    when(mockApiException.getErrorResponse()).thenReturn(mockErrorResponse);
    when(mockErrorResponse.getStatus()).thenReturn(418);
    when(mockResponseEntityBodyBuilder1.contentType(any()))
        .thenReturn(mockResponseEntityBodyBuilder2);
    when(mockResponseEntityBodyBuilder2.body("result")).thenReturn(mockResponseEntity);

    try (final var staticErrorResponseFactory = mockStatic(ErrorResponseFactory.class);
        final var staticResponseEntity = mockStatic(ResponseEntity.class);
        final var objectMapperConstruction =
            mockConstruction(
                ObjectMapper.class,
                (mock, context) -> {
                  when(mock.writeValueAsString(mockErrorResponse)).thenReturn("result");
                })) {
      staticResponseEntity
          .when(() -> ResponseEntity.status(418))
          .thenReturn(mockResponseEntityBodyBuilder1);
      // Act
      final var result =
          controllerExceptionHandler.exception(mockHttpServletRequest, mockApiException);

      // Assert
      final var objectmapper = objectMapperConstruction.constructed().get(0);
      verify(objectmapper, times(1)).writeValueAsString(mockErrorResponse);
      assertEquals(mockResponseEntity, result);
    }
  }

  @Test
  public void exception_withApiException_whenObjectMapperThrowsJsonProcessingException()
      throws JsonProcessingException {
    // Arrange
    final var mockHttpServletRequest = mock(HttpServletRequest.class);
    final var mockApiException = mock(ApiException.class);
    final var mockErrorResponse = mock(ErrorResponse.class);
    final var mockResponseEntity = mock(ResponseEntity.class);
    final var mockResponseEntityBodyBuilder1 = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntityBodyBuilder2 = mock(ResponseEntity.BodyBuilder.class);
    when(mockApiException.getErrorResponse()).thenReturn(mockErrorResponse);
    when(mockErrorResponse.getStatus()).thenReturn(418);
    when(mockResponseEntityBodyBuilder1.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder1);

    when(mockResponseEntityBodyBuilder2.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder2);
    when(mockResponseEntityBodyBuilder2.body(
            "{\"status\":500,\"error\":\"Internal Server Error\",\"message\":\"Unexpected exception"
                + " occurred\"}"))
        .thenReturn(mockResponseEntity);

    try (final var staticResponseEntity = mockStatic(ResponseEntity.class);
        final var objectMapperConstruction =
            mockConstruction(
                ObjectMapper.class,
                (mock, context) -> {
                  when(mock.writeValueAsString(mockErrorResponse))
                      .thenThrow(JsonProcessingException.class);
                })) {
      staticResponseEntity
          .when(() -> ResponseEntity.status(418))
          .thenReturn(mockResponseEntityBodyBuilder1);
      staticResponseEntity
          .when(() -> ResponseEntity.status(500))
          .thenReturn(mockResponseEntityBodyBuilder2);

      // Act, Assert
      final var result =
          controllerExceptionHandler.exception(mockHttpServletRequest, mockApiException);

      final var objectmapper = objectMapperConstruction.constructed().get(0);
      verify(objectmapper, times(1)).writeValueAsString(mockErrorResponse);
      assertEquals(mockResponseEntity, result);
    }
  }

  @Test
  public void exception_withException() {
    // Arrange
    final var mockHttpServletRequest = mock(HttpServletRequest.class);
    final var mockException = mock(Exception.class);
    final var mockErrorResponse = mock(ErrorResponse.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    try (final var staticErrorResponseFactory = mockStatic(ErrorResponseFactory.class);
        final var staticResponseEntityFactory = mockStatic(ResponseEntityFactory.class)) {
      staticErrorResponseFactory
          .when(
              () ->
                  ErrorResponseFactory.createErrorResponse(
                      HttpStatus.INTERNAL_SERVER_ERROR, "Unexpected exception occurred"))
          .thenReturn(mockErrorResponse);
      staticResponseEntityFactory
          .when(() -> ResponseEntityFactory.create(mockErrorResponse))
          .thenReturn(mockResponseEntity);

      // Act
      final var result =
          controllerExceptionHandler.exception(mockHttpServletRequest, mockException);

      // Assert
      staticErrorResponseFactory.verify(
          () ->
              ErrorResponseFactory.createErrorResponse(
                  HttpStatus.INTERNAL_SERVER_ERROR, "Unexpected exception occurred"),
          times(1));
      staticResponseEntityFactory.verify(
          () -> ResponseEntityFactory.create(mockErrorResponse), times(1));
      assertEquals(mockResponseEntity, result);
    }
  }

  @Test
  public void errorHandler() {
    // Arrange
    final var mockErrorResponse = mock(ErrorResponse.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    try (final var staticErrorResponseFactory = mockStatic(ErrorResponseFactory.class);
        final var staticResponseEntityFactory = mockStatic(ResponseEntityFactory.class)) {
      staticErrorResponseFactory
          .when(() -> ErrorResponseFactory.createErrorResponse(HttpStatus.NOT_FOUND, "Not found"))
          .thenReturn(mockErrorResponse);
      staticResponseEntityFactory
          .when(() -> ResponseEntityFactory.create(mockErrorResponse))
          .thenReturn(mockResponseEntity);

      // Act
      final var result = controllerExceptionHandler.errorHandler();

      // Assert
      staticErrorResponseFactory.verify(
          () -> ErrorResponseFactory.createErrorResponse(HttpStatus.NOT_FOUND, "Not found"),
          times(1));
      staticResponseEntityFactory.verify(
          () -> ResponseEntityFactory.create(mockErrorResponse), times(1));
      assertEquals(mockResponseEntity, result);
    }
  }
}
