package se.jobtechdev.personaldatagateway.api.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import se.jobtechdev.personaldatagateway.api.Main;
import se.jobtechdev.personaldatagateway.api.generated.model.CheckStatus;
import se.jobtechdev.personaldatagateway.api.generated.model.GetHealthResponse;
import se.jobtechdev.personaldatagateway.api.generated.model.HealthChecks;
import se.jobtechdev.personaldatagateway.api.generated.model.Status;
import se.jobtechdev.personaldatagateway.api.util.ResponseFactory;

public class HealthControllerTest {

  private HealthController mockHealthController;

  @BeforeEach
  void setUp() {
    mockHealthController = new HealthController();
  }

  @Test
  public void getHealth_whenNotStarted_thenReturnServiceNotAvailable() {
    // Arrange
    final var checkStatusArgs = new HashMap<CheckStatus, List<?>>();
    final var healthChecksArgs = new HashMap<HealthChecks, List<?>>();
    final var mockGetHealthResponse = mock(GetHealthResponse.class);
    final var mockResponseEntity = mock(ResponseEntity.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    try (final var staticResponseFactory = mockStatic(ResponseFactory.class);
        final var staticMain = mockStatic(Main.class);
        final var staticResponseEntity = mockStatic(ResponseEntity.class);
        final var checkStatusMockedConstruction =
            mockConstruction(
                CheckStatus.class,
                (mock, context) -> checkStatusArgs.put(mock, context.arguments()));
        final var healthChecksMockedConstruction =
            mockConstruction(
                HealthChecks.class,
                (mock, context) -> healthChecksArgs.put(mock, context.arguments()))) {

      staticResponseFactory
          .when(() -> ResponseFactory.getHealthResponse(any(), any()))
          .thenReturn(mockGetHealthResponse);
      staticMain.when(() -> Main.isStarted()).thenReturn(false);
      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE))
          .thenReturn(mockResponseEntityBodyBuilder);
      when(mockResponseEntityBodyBuilder.contentType(any()))
          .thenReturn(mockResponseEntityBodyBuilder);
      when(mockResponseEntityBodyBuilder.body(any())).thenReturn(mockResponseEntity);

      // Act
      final var result = mockHealthController.health();

      // Assert
      assertEquals(mockResponseEntity, result);
      assertEquals(2, checkStatusMockedConstruction.constructed().size());
      final var liveness = checkStatusMockedConstruction.constructed().get(0);
      final var readiness = checkStatusMockedConstruction.constructed().get(1);
      final var livenessArgs = checkStatusArgs.get(liveness);
      assertEquals(Status.UP, livenessArgs.get(0));
      final var readinessArgs = checkStatusArgs.get(readiness);
      assertEquals(Status.DOWN, readinessArgs.get(0));
      assertEquals(1, healthChecksMockedConstruction.constructed().size());
      final var checks = healthChecksMockedConstruction.constructed().get(0);
      final var checksArgs = healthChecksArgs.get(checks);
      assertEquals(liveness, checksArgs.get(0));
      assertEquals(readiness, checksArgs.get(1));
    }
  }

  @Test
  public void getHealth_whenStarted_thenReturnOk() {
    // Arrange
    final var checkStatusArgs = new HashMap<CheckStatus, List<?>>();
    final var healthChecksArgs = new HashMap<HealthChecks, List<?>>();
    final var mockGetHealthResponse = mock(GetHealthResponse.class);
    final var mockResponseEntity = mock(ResponseEntity.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    try (final var staticResponseFactory = mockStatic(ResponseFactory.class);
        final var staticMain = mockStatic(Main.class);
        final var staticResponseEntity = mockStatic(ResponseEntity.class);
        final var checkStatusMockedConstruction =
            mockConstruction(
                CheckStatus.class,
                (mock, context) -> checkStatusArgs.put(mock, context.arguments()));
        final var healthChecksMockedConstruction =
            mockConstruction(
                HealthChecks.class,
                (mock, context) -> healthChecksArgs.put(mock, context.arguments()))) {

      staticResponseFactory
          .when(() -> ResponseFactory.getHealthResponse(any(), any()))
          .thenReturn(mockGetHealthResponse);
      staticMain.when(() -> Main.isStarted()).thenReturn(true);
      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);
      when(mockResponseEntityBodyBuilder.contentType(any()))
          .thenReturn(mockResponseEntityBodyBuilder);
      when(mockResponseEntityBodyBuilder.body(any())).thenReturn(mockResponseEntity);

      // Act
      final var result = mockHealthController.health();

      // Assert
      assertEquals(mockResponseEntity, result);
      assertEquals(2, checkStatusMockedConstruction.constructed().size());
      final var liveness = checkStatusMockedConstruction.constructed().get(0);
      final var readiness = checkStatusMockedConstruction.constructed().get(1);
      final var livenessArgs = checkStatusArgs.get(liveness);
      assertEquals(Status.UP, livenessArgs.get(0));
      final var readinessArgs = checkStatusArgs.get(readiness);
      assertEquals(Status.UP, readinessArgs.get(0));
      assertEquals(1, healthChecksMockedConstruction.constructed().size());
      final var checks = healthChecksMockedConstruction.constructed().get(0);
      final var checksArgs = healthChecksArgs.get(checks);
      assertEquals(liveness, checksArgs.get(0));
      assertEquals(readiness, checksArgs.get(1));
    }
  }
}
