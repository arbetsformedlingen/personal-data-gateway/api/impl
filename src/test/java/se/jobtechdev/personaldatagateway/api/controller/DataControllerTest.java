package se.jobtechdev.personaldatagateway.api.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import se.jobtechdev.personaldatagateway.api.generated.entities.*;
import se.jobtechdev.personaldatagateway.api.generated.model.GetDataAccessResponse;
import se.jobtechdev.personaldatagateway.api.service.ClientService;
import se.jobtechdev.personaldatagateway.api.service.DataService;
import se.jobtechdev.personaldatagateway.api.service.SharingService;
import se.jobtechdev.personaldatagateway.api.util.*;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static se.jobtechdev.personaldatagateway.api.util.ControllerUtil.throwApiExceptionOnAbsentValue;

@ExtendWith(MockitoExtension.class)
public class DataControllerTest {
  private final String mockBaseUrl = "mock-base-url";
  private final HttpHeaders mockLinkHeader = mock(HttpHeaders.class);
  private DataController dataController;
  @Mock
  private NativeWebRequest request;

  @Mock
  private ClientService clientService;

  @Mock
  private SharingService sharingService;

  @Mock
  private DataService dataService;

  @BeforeEach
  void setUp() {
    dataController =
        new DataController(mockBaseUrl, request, clientService, sharingService, dataService);
  }

  @Test
  public void getData() {
    // Arrange
    final var authKey = "abc";
    final var decodedAuthKey = new byte[]{1, 2, 3};
    final var authKeyHash = new byte[]{4, 5, 6};
    final var mockDataWrapper = mock(DataWrapper.class);
    final var mockHttpStatus = mock(HttpStatus.class);
    final var mockMediaType = mock(MediaType.class);
    final var mockBytes = new byte[]{1, 2, 3};
    final var statusId = mock(UUID.class);
    final var client = mock(Client.class);
    final var clientId = mock(UUID.class);
    final var sharing = mock(Sharing.class);
    final var sharingRevoked = mock(ZonedDateTime.class);
    final var sharingExpires = mock(ZonedDateTime.class);
    final var sharingId = mock(UUID.class);
    final var person = mock(Person.class);
    final var datasource = mock(Datasource.class);
    final var mockResponseEntity = mock(ResponseEntity.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final Function<Client, Boolean> mockClientInequalityCheck = mock(Function.class);

    when(statusId.toString()).thenReturn("mock-status-id");
    when(request.getHeader("X-Auth-Key")).thenReturn(authKey);
    when(request.getHeader(HttpHeaders.ACCEPT)).thenReturn("application/json");
    when(clientService.getClientByKeyHash(authKeyHash)).thenReturn(Optional.of(client));
    when(client.getId()).thenReturn(clientId);
    when(clientId.toString()).thenReturn("mock-client-id");
    when(sharingService.getSharingById(statusId)).thenReturn(Optional.of(sharing));
    when(sharing.getClient()).thenReturn(client);
    when(sharing.getRevoked()).thenReturn(sharingRevoked);
    when(sharing.getExpires()).thenReturn(sharingExpires);
    when(sharing.getId()).thenReturn(sharingId);
    when(sharingId.toString()).thenReturn("mock-sharing-id");
    when(sharing.getPerson()).thenReturn(person);
    when(person.getId()).thenReturn("person-id");
    when(sharing.getDatasource()).thenReturn(datasource);
    when(mockDataWrapper.body()).thenReturn(mockBytes);
    when(mockDataWrapper.contentType()).thenReturn(mockMediaType);
    when(mockDataWrapper.status()).thenReturn(mockHttpStatus);

    when(mockResponseEntityBodyBuilder.contentType(mockMediaType))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockBytes)).thenReturn(mockResponseEntity);

    when(dataService.retrieveData(datasource, "person-id", "application/json"))
        .thenReturn(mockDataWrapper);

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticDataController = mockStatic(DataController.class);
         final var staticAuthUtil = mockStatic(AuthUtil.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticResponseEntity
          .when(() -> ResponseEntity.status(mockHttpStatus))
          .thenReturn(mockResponseEntityBodyBuilder);

      staticAuthUtil.when(() -> AuthUtil.decodeAuthKey(authKey)).thenReturn(decodedAuthKey);

      staticAuthUtil
          .when(() -> AuthUtil.hashDecodedAuthKey(decodedAuthKey))
          .thenReturn(authKeyHash);

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(client)),
                      eq(HttpStatus.FORBIDDEN),
                      eq(
                          "Failed to handle GET data request - Could not find any client associated"
                              + " with the provided auth key")))
          .thenReturn(client);

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(sharing)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle GET data request - Could not find sharing by id:"
                              + " mock-status-id")))
          .thenReturn(sharing);

      staticDataController
          .when(() -> DataController.clientInequalityCheck(client))
          .thenReturn(mockClientInequalityCheck);

      // Act
      final var result = dataController.getData(statusId);

      // Assert
      staticControllerUtil.verify(
          () ->
              ControllerUtil.earlyExit(
                  client,
                  mockClientInequalityCheck,
                  HttpStatus.FORBIDDEN,
                  "Failed to handle GET data request - Authenticated client"
                      + " [clientId:mock-client-id] does not match sharing's client"
                      + " [clientId:mock-client-id]"),
          times(1));

      staticControllerUtil.verify(
          () ->
              ControllerUtil.earlyExit(
                  eq(sharingRevoked),
                  any(),
                  eq(HttpStatus.FORBIDDEN),
                  eq(
                      "Failed to handle GET data request - The requested sharing [mock-sharing-id]"
                          + " has been revoked")),
          times(1));

      staticControllerUtil.verify(
          () ->
              ControllerUtil.earlyExit(
                  eq(sharingExpires),
                  any(),
                  eq(HttpStatus.FORBIDDEN),
                  eq(
                      "Failed to handle GET data request - The requested sharing [mock-sharing-id]"
                          + " has expired")),
          times(1));

      assertNotNull(result);
      verify(request, times(1)).getHeader("X-Auth-Key");
      verify(clientService, times(1)).getClientByKeyHash(authKeyHash);
      verify(sharingService, times(1)).getSharingById(statusId);
      verify(sharing, times(1)).getClient();
      verify(sharing, times(1)).getRevoked();
      verify(sharing, times(1)).getExpires();
      verify(sharing, times(1)).getPerson();
      verify(sharing, times(1)).getDatasource();

      verify(dataService, times(1)).retrieveData(datasource, "person-id", "application/json");

      staticResponseEntity.verify(() -> ResponseEntity.status(mockHttpStatus), times(1));
      verify(mockResponseEntityBodyBuilder, times(1)).contentType(mockMediaType);
      verify(mockResponseEntityBodyBuilder, times(1)).body(mockBytes);
      verify(sharingService, times(1)).createSharingAccess(sharing);
    }
  }

  @Test
  public void getDataAccess() {
    // Arrange
    final var mockDataId = mock(UUID.class);
    final var mockAccessId = mock(UUID.class);
    final var mockSharingAccess = mock(SharingAccess.class);
    final var mockSharingAccessId = mock(UUID.class);
    final var mockSharingAccessSharing = mock(Sharing.class);
    final var mockGetDataAccessResponse = mock(GetDataAccessResponse.class);
    final Function<UUID, Boolean> mockInequalityCheckingLambda = mock(Function.class);
    final var mockResponseEntity = mock(ResponseEntity.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);

    when(mockDataId.toString()).thenReturn("mock-data-id");
    when(mockAccessId.toString()).thenReturn("mock-access-id");
    when(mockSharingAccess.getSharing()).thenReturn(mockSharingAccessSharing);
    when(mockSharingAccessSharing.getId()).thenReturn(mockSharingAccessId);

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockGetDataAccessResponse))
        .thenReturn(mockResponseEntity);

    when(sharingService.getSharingAccessById(mockAccessId))
        .thenReturn(Optional.of(mockSharingAccess));

    try (final var staticUuidProvider = mockStatic(UuidProvider.class);
         final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticUuidProvider
          .when(() -> UuidProvider.generateInequalityCheckingLambda(mockDataId))
          .thenReturn(mockInequalityCheckingLambda);

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockSharingAccess)),
                      eq(HttpStatus.NOT_FOUND),
                      eq("Could not find data access by id: mock-access-id")))
          .thenReturn(mockSharingAccess);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.earlyExit(
                      eq(mockSharingAccessId),
                      any(),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Could not find data access by dataId: mock-data-id and accessId:"
                              + " mock-access-id")))
          .thenReturn(mockSharingAccess);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createGetDataAccessResponse(
                      request, mockBaseUrl, mockSharingAccess))
          .thenReturn(mockGetDataAccessResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var result = dataController.getDataAccess(mockDataId, mockAccessId);

      // Assert
      staticControllerUtil.verify(
          () ->
              throwApiExceptionOnAbsentValue(
                  eq(Optional.of(mockSharingAccess)),
                  eq(HttpStatus.NOT_FOUND),
                  eq("Could not find data access by id: mock-access-id")),
          times(1));

      staticControllerUtil.verify(
          () ->
              ControllerUtil.earlyExit(
                  eq(mockSharingAccessId),
                  eq(mockInequalityCheckingLambda),
                  eq(HttpStatus.NOT_FOUND),
                  eq(
                      "Could not find data access by dataId: mock-data-id and accessId:"
                          + " mock-access-id")),
          times(1));

      assertEquals(mockResponseEntity, result);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  public void getDataAccesses() {
    // Arrange
    final var mockDataId = mock(UUID.class);
    final var mockSharing = mock(Sharing.class);
    final var mockSharingAccess = mock(SharingAccess.class);
    final var mockGetDataAccessResponse = mock(GetDataAccessResponse.class);
    final var mockResponseEntity = mock(ResponseEntity.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);

    when(mockDataId.toString()).thenReturn("mock-data-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(List.of(mockGetDataAccessResponse)))
        .thenReturn(mockResponseEntity);

    when(sharingService.getSharingById(mockDataId)).thenReturn(Optional.of(mockSharing));
    when(sharingService.getAllSharingAccessesBySharing(mockSharing))
        .thenReturn(Set.of(mockSharingAccess));

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockSharing)),
                      eq(HttpStatus.NOT_FOUND),
                      eq("Could not find sharing by id: mock-data-id")))
          .thenReturn(mockSharing);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createGetDataAccessResponse(
                      request, mockBaseUrl, mockSharingAccess))
          .thenReturn(mockGetDataAccessResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var result = dataController.getDataAccesses(mockDataId);

      // Assert
      assertEquals(mockResponseEntity, result);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  public void clientInequalityCheck_whenTheClientsAreDifferent_returnTrue() {
    // Arrange
    final var client1 = mock(Client.class);
    final var client2 = mock(Client.class);

    // Act & Assert
    final var lambda = DataController.clientInequalityCheck(client1);
    assertTrue(lambda.apply(client2));
  }

  @Test
  public void clientInequalityCheck_whenTheClientsAreSame_returnFalse() {
    // Arrange
    final var client = mock(Client.class);

    // Act & Assert
    final var lambda = DataController.clientInequalityCheck(client);
    assertFalse(lambda.apply(client));
  }

  @Test
  public void clientInequalityCheck_whenTheClientIsNull_returnTrue() {
    // Arrange
    final var client = mock(Client.class);

    // Act & Assert
    final var lambda = DataController.clientInequalityCheck(null);
    assertTrue(lambda.apply(client));
  }

  @Test
  public void clientInequalityCheck_whenThePassedInClientIsNull_returnFalse() {
    // Arrange
    final var client = mock(Client.class);

    // Act & Assert
    final var lambda = DataController.clientInequalityCheck(client);
    assertFalse(lambda.apply(null));
  }

  @Test
  public void isTimestampNullOrPassedCheck_whenTimestampIsPassed_returnTrue() {
    // Arrange
    final var past = mock(ZonedDateTime.class);
    final var now = mock(ZonedDateTime.class);

    when(past.isBefore(now)).thenReturn(true);

    try (final var staticTimeProvider = mockStatic(TimeProvider.class)) {

      staticTimeProvider.when(TimeProvider::now).thenReturn(now);

      // Act & Assert
      final var lambda = DataController.isTimestampNullOrPassedCheck();
      assertTrue(lambda.apply(past));
    }
  }

  @Test
  public void isTimestampNullOrPassedCheck_whenTimestampNotPassed_returnFalse() {
    // Arrange
    final var future = mock(ZonedDateTime.class);
    final var now = mock(ZonedDateTime.class);

    when(future.isBefore(now)).thenReturn(false);

    try (final var staticTimeProvider = mockStatic(TimeProvider.class)) {

      staticTimeProvider.when(TimeProvider::now).thenReturn(now);

      // Act & Assert
      final var lambda = DataController.isTimestampNullOrPassedCheck();
      assertFalse(lambda.apply(future));
    }
  }

  @Test
  public void isTimestampNullOrPassedCheck_whenTimestampIsNull_returnFalse() {
    // Arrange
    final var now = mock(ZonedDateTime.class);

    try (final var staticTimeProvider = mockStatic(TimeProvider.class)) {

      staticTimeProvider.when(TimeProvider::now).thenReturn(now);

      // Act & Assert
      final var lambda = DataController.isTimestampNullOrPassedCheck();
      assertFalse(lambda.apply(null));
    }
  }
}
