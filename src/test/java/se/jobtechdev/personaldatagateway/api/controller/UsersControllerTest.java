package se.jobtechdev.personaldatagateway.api.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import se.jobtechdev.personaldatagateway.api.generated.entities.Client;
import se.jobtechdev.personaldatagateway.api.generated.entities.Datasource;
import se.jobtechdev.personaldatagateway.api.generated.entities.Person;
import se.jobtechdev.personaldatagateway.api.generated.entities.Sharing;
import se.jobtechdev.personaldatagateway.api.generated.model.*;
import se.jobtechdev.personaldatagateway.api.service.ClientService;
import se.jobtechdev.personaldatagateway.api.service.DatasourceService;
import se.jobtechdev.personaldatagateway.api.service.PersonService;
import se.jobtechdev.personaldatagateway.api.service.SharingService;
import se.jobtechdev.personaldatagateway.api.util.ControllerUtil;
import se.jobtechdev.personaldatagateway.api.util.Hateoas;
import se.jobtechdev.personaldatagateway.api.util.ResponseFactory;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UsersControllerTest {
  private final String mockBaseUrl = "https://example.com/foo?bar=baz";
  private final HttpHeaders mockLinkHeader = mock(HttpHeaders.class);
  private UsersController usersController;
  @Mock
  private NativeWebRequest request;

  @Mock
  private SharingService sharingService;

  @Mock
  private PersonService personService;

  @Mock
  private DatasourceService datasourceService;

  @Mock
  private ClientService clientService;

  @BeforeEach
  void setUp() {
    usersController =
        new UsersController(
            mockBaseUrl, request, sharingService, personService, datasourceService, clientService);
  }

  @Test
  @Tag("Unit")
  public void postUserSharing_forSpecificClient_whenActiveSharingExists_reuseExistingSharing() {
    // Arrange
    final var mockUserId = "mock-user-id";
    final var mockPerson = mock(Person.class);
    final var mockPostUserSharingRequest = mock(PostUserSharingRequest.class);
    final var mockPostUserSharingRequestPublic = false;
    final var mockPostUserSharingRequestExpires = mock(ZonedDateTime.class);
    final var mockPostUserSharingRequestClientId = mock(UUID.class);
    final var mockPostUserSharingRequestDatasourceId = mock(UUID.class);
    final var mockDatasource = mock(Datasource.class);
    final var mockClient = mock(Client.class);
    final var mockActiveSharing = mock(Sharing.class);
    final var mockPostUserSharingResponse = mock(PostUserSharingResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockPostUserSharingRequest.getPublic()).thenReturn(mockPostUserSharingRequestPublic);
    when(mockPostUserSharingRequest.getExpires()).thenReturn(mockPostUserSharingRequestExpires);
    when(mockPostUserSharingRequest.getClientId()).thenReturn(mockPostUserSharingRequestClientId);
    when(mockPostUserSharingRequest.getDatasourceId())
        .thenReturn(mockPostUserSharingRequestDatasourceId);
    when(mockPostUserSharingRequestDatasourceId.toString())
        .thenReturn("mock-post-user-sharing-request-datasource-id");
    when(mockPostUserSharingRequestClientId.toString())
        .thenReturn("mock-post-user-sharing-request-client-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockPostUserSharingResponse))
        .thenReturn(mockResponseEntity);

    when(personService.getPersonById(mockUserId)).thenReturn(Optional.empty());
    when(personService.createPerson(mockUserId)).thenReturn(mockPerson);
    when(datasourceService.getDatasourceById(mockPostUserSharingRequestDatasourceId))
        .thenReturn(Optional.of(mockDatasource));
    when(clientService.getClientById(mockPostUserSharingRequestClientId))
        .thenReturn(Optional.of(mockClient));
    when(sharingService.getActiveSharings(mockPerson, mockDatasource, mockClient))
        .thenReturn(List.of(mockActiveSharing));

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockDatasource)),
                      eq(HttpStatus.BAD_REQUEST),
                      eq(
                          "Could not find datasource by datasourceId:"
                              + " mock-post-user-sharing-request-datasource-id")))
          .thenReturn(mockDatasource);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockClient)),
                      eq(HttpStatus.BAD_REQUEST),
                      eq(
                          "Could not find client by clientId:"
                              + " mock-post-user-sharing-request-client-id")))
          .thenReturn(mockClient);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createPostUserSharingResponse(
                      request, mockBaseUrl, mockActiveSharing))
          .thenReturn(mockPostUserSharingResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response = usersController.postUserSharing(mockUserId, mockPostUserSharingRequest);

      // Assert
      staticControllerUtil.verify(
          () ->
              ControllerUtil.earlyExit(
                  eq(mockPostUserSharingRequestClientId),
                  any(),
                  eq(HttpStatus.UNPROCESSABLE_ENTITY),
                  eq("The \"public\" and \"clientId\" properties are mutually exclusive")),
          times(0));

      staticControllerUtil.verify(
          () ->
              ControllerUtil.earlyExit(
                  eq(mockPostUserSharingRequestClientId),
                  any(),
                  eq(HttpStatus.UNPROCESSABLE_ENTITY),
                  eq("One of \"public\" or \"clientId\" properties must be defined")),
          times(1));

      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  @Tag("Unit")
  public void postUserSharing_forSpecificClient_whenNoActiveSharingExists_createNewSharing() {
    // Arrange
    final var mockUserId = "mock-user-id";
    final var mockPerson = mock(Person.class);
    final var mockPostUserSharingRequest = mock(PostUserSharingRequest.class);
    final var mockPostUserSharingRequestPublic = false;
    final var mockPostUserSharingRequestExpires = mock(ZonedDateTime.class);
    final var mockPostUserSharingRequestClientId = mock(UUID.class);
    final var mockPostUserSharingRequestDatasourceId = mock(UUID.class);
    final var mockDatasource = mock(Datasource.class);
    final var mockClient = mock(Client.class);
    final var mockActiveSharing = mock(Sharing.class);
    final var mockPostUserSharingResponse = mock(PostUserSharingResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockPostUserSharingRequest.getPublic()).thenReturn(mockPostUserSharingRequestPublic);
    when(mockPostUserSharingRequest.getExpires()).thenReturn(mockPostUserSharingRequestExpires);
    when(mockPostUserSharingRequest.getClientId()).thenReturn(mockPostUserSharingRequestClientId);
    when(mockPostUserSharingRequest.getDatasourceId())
        .thenReturn(mockPostUserSharingRequestDatasourceId);
    when(mockPostUserSharingRequestDatasourceId.toString())
        .thenReturn("mock-post-user-sharing-request-datasource-id");
    when(mockPostUserSharingRequestClientId.toString())
        .thenReturn("mock-post-user-sharing-request-client-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockPostUserSharingResponse))
        .thenReturn(mockResponseEntity);

    when(personService.getPersonById(mockUserId)).thenReturn(Optional.empty());
    when(personService.createPerson(mockUserId)).thenReturn(mockPerson);
    when(datasourceService.getDatasourceById(mockPostUserSharingRequestDatasourceId))
        .thenReturn(Optional.of(mockDatasource));
    when(clientService.getClientById(mockPostUserSharingRequestClientId))
        .thenReturn(Optional.of(mockClient));
    when(sharingService.getActiveSharings(mockPerson, mockDatasource, mockClient))
        .thenReturn(List.of());
    when(sharingService.createSharing(
        mockPerson,
        mockDatasource,
        mockPostUserSharingRequestPublic,
        mockPostUserSharingRequestExpires,
        mockClient))
        .thenReturn(mockActiveSharing);

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockDatasource)),
                      eq(HttpStatus.BAD_REQUEST),
                      eq(
                          "Could not find datasource by datasourceId:"
                              + " mock-post-user-sharing-request-datasource-id")))
          .thenReturn(mockDatasource);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockClient)),
                      eq(HttpStatus.BAD_REQUEST),
                      eq(
                          "Could not find client by clientId:"
                              + " mock-post-user-sharing-request-client-id")))
          .thenReturn(mockClient);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createPostUserSharingResponse(
                      request, mockBaseUrl, mockActiveSharing))
          .thenReturn(mockPostUserSharingResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.CREATED))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response = usersController.postUserSharing(mockUserId, mockPostUserSharingRequest);

      // Assert
      staticControllerUtil.verify(
          () ->
              ControllerUtil.earlyExit(
                  eq(mockPostUserSharingRequestClientId),
                  any(),
                  eq(HttpStatus.UNPROCESSABLE_ENTITY),
                  eq("The \"public\" and \"clientId\" properties are mutually exclusive")),
          times(0));

      staticControllerUtil.verify(
          () ->
              ControllerUtil.earlyExit(
                  eq(mockPostUserSharingRequestClientId),
                  any(),
                  eq(HttpStatus.UNPROCESSABLE_ENTITY),
                  eq("One of \"public\" or \"clientId\" properties must be defined")),
          times(1));

      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  @Tag("Unit")
  public void postUserSharing_forPublic_whenNoActiveSharingExists_createNewSharing() {
    // Arrange
    final var mockUserId = "mock-user-id";
    final var mockPerson = mock(Person.class);
    final var mockPostUserSharingRequest = mock(PostUserSharingRequest.class);
    final var mockPostUserSharingRequestPublic = true;
    final var mockPostUserSharingRequestExpires = mock(ZonedDateTime.class);
    final var mockPostUserSharingRequestClientId = mock(UUID.class);
    final var mockPostUserSharingRequestDatasourceId = mock(UUID.class);
    final var mockDatasource = mock(Datasource.class);
    final var mockActiveSharing = mock(Sharing.class);
    final var mockPostUserSharingResponse = mock(PostUserSharingResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockPostUserSharingRequest.getPublic()).thenReturn(mockPostUserSharingRequestPublic);
    when(mockPostUserSharingRequest.getExpires()).thenReturn(mockPostUserSharingRequestExpires);
    when(mockPostUserSharingRequest.getClientId()).thenReturn(mockPostUserSharingRequestClientId);
    when(mockPostUserSharingRequest.getDatasourceId())
        .thenReturn(mockPostUserSharingRequestDatasourceId);
    when(mockPostUserSharingRequestDatasourceId.toString())
        .thenReturn("mock-post-user-sharing-request-datasource-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockPostUserSharingResponse))
        .thenReturn(mockResponseEntity);

    when(personService.getPersonById(mockUserId)).thenReturn(Optional.empty());
    when(personService.createPerson(mockUserId)).thenReturn(mockPerson);
    when(datasourceService.getDatasourceById(mockPostUserSharingRequestDatasourceId))
        .thenReturn(Optional.of(mockDatasource));
    when(sharingService.getActiveSharings(mockPerson, mockDatasource, null)).thenReturn(List.of());
    when(sharingService.createSharing(
        mockPerson, mockDatasource, true, mockPostUserSharingRequestExpires, null))
        .thenReturn(mockActiveSharing);

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockDatasource)),
                      eq(HttpStatus.BAD_REQUEST),
                      eq(
                          "Could not find datasource by datasourceId:"
                              + " mock-post-user-sharing-request-datasource-id")))
          .thenReturn(mockDatasource);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createPostUserSharingResponse(
                      request, mockBaseUrl, mockActiveSharing))
          .thenReturn(mockPostUserSharingResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.CREATED))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response = usersController.postUserSharing(mockUserId, mockPostUserSharingRequest);

      // Assert
      staticControllerUtil.verify(
          () ->
              ControllerUtil.earlyExit(
                  eq(mockPostUserSharingRequestClientId),
                  any(),
                  eq(HttpStatus.UNPROCESSABLE_ENTITY),
                  eq("The \"public\" and \"clientId\" properties are mutually exclusive")),
          times(1));

      staticControllerUtil.verify(
          () ->
              ControllerUtil.earlyExit(
                  eq(mockPostUserSharingRequestClientId),
                  any(),
                  eq(HttpStatus.UNPROCESSABLE_ENTITY),
                  eq("One of \"public\" or \"clientId\" properties must be defined")),
          times(0));

      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  @Tag("Unit")
  public void postUserSharing_forPublic_whenActiveSharingExists_reuseExistingSharing() {
    // Arrange
    final var mockUserId = "mock-user-id";
    final var mockPerson = mock(Person.class);
    final var mockPostUserSharingRequest = mock(PostUserSharingRequest.class);
    final var mockPostUserSharingRequestPublic = true;
    final var mockPostUserSharingRequestExpires = mock(ZonedDateTime.class);
    final var mockPostUserSharingRequestClientId = mock(UUID.class);
    final var mockPostUserSharingRequestDatasourceId = mock(UUID.class);
    final var mockDatasource = mock(Datasource.class);
    final var mockActiveSharing = mock(Sharing.class);
    final var mockPostUserSharingResponse = mock(PostUserSharingResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockPostUserSharingRequest.getPublic()).thenReturn(mockPostUserSharingRequestPublic);
    when(mockPostUserSharingRequest.getExpires()).thenReturn(mockPostUserSharingRequestExpires);
    when(mockPostUserSharingRequest.getClientId()).thenReturn(mockPostUserSharingRequestClientId);
    when(mockPostUserSharingRequest.getDatasourceId())
        .thenReturn(mockPostUserSharingRequestDatasourceId);
    when(mockPostUserSharingRequestDatasourceId.toString())
        .thenReturn("mock-post-user-sharing-request-datasource-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockPostUserSharingResponse))
        .thenReturn(mockResponseEntity);

    when(personService.getPersonById(mockUserId)).thenReturn(Optional.empty());
    when(personService.createPerson(mockUserId)).thenReturn(mockPerson);
    when(datasourceService.getDatasourceById(mockPostUserSharingRequestDatasourceId))
        .thenReturn(Optional.of(mockDatasource));
    when(sharingService.getActiveSharings(mockPerson, mockDatasource, null))
        .thenReturn(List.of(mockActiveSharing));

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockDatasource)),
                      eq(HttpStatus.BAD_REQUEST),
                      eq(
                          "Could not find datasource by datasourceId:"
                              + " mock-post-user-sharing-request-datasource-id")))
          .thenReturn(mockDatasource);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createPostUserSharingResponse(
                      request, mockBaseUrl, mockActiveSharing))
          .thenReturn(mockPostUserSharingResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response = usersController.postUserSharing(mockUserId, mockPostUserSharingRequest);

      // Assert
      staticControllerUtil.verify(
          () ->
              ControllerUtil.earlyExit(
                  eq(mockPostUserSharingRequestClientId),
                  any(),
                  eq(HttpStatus.UNPROCESSABLE_ENTITY),
                  eq("The \"public\" and \"clientId\" properties are mutually exclusive")),
          times(1));

      staticControllerUtil.verify(
          () ->
              ControllerUtil.earlyExit(
                  eq(mockPostUserSharingRequestClientId),
                  any(),
                  eq(HttpStatus.UNPROCESSABLE_ENTITY),
                  eq("One of \"public\" or \"clientId\" properties must be defined")),
          times(0));

      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  @Tag("Unit")
  public void getUserSharing() {
    // Arrange
    final var mockUserId = "mock-user-id";
    final var mockSharingId = mock(UUID.class);
    final var mockSharing = mock(Sharing.class);
    final var mockGetUserSharingResponse = mock(GetUserSharingResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockSharingId.toString()).thenReturn("mock-sharing-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockGetUserSharingResponse))
        .thenReturn(mockResponseEntity);

    when(sharingService.getSharingById(mockSharingId)).thenReturn(Optional.of(mockSharing));

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockSharing)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle GET user sharing request - Could not find sharing with"
                              + " id: mock-sharing-id")))
          .thenReturn(mockSharing);

      staticResponseFactory
          .when(
              () -> ResponseFactory.createGetUserSharingResponse(request, mockBaseUrl, mockSharing))
          .thenReturn(mockGetUserSharingResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response = usersController.getUserSharing(mockUserId, mockSharingId);

      // Assert
      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  @Tag("Unit")
  public void getUserSharings() {
    // Arrange
    final var mockUserId = "mock-user-id";
    final var mockPerson = mock(Person.class);
    final var mockSharing = mock(Sharing.class);
    final var mockGetUserSharingResponse = mock(GetUserSharingResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockPerson.getSharings()).thenReturn(Set.of(mockSharing));

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(List.of(mockGetUserSharingResponse)))
        .thenReturn(mockResponseEntity);

    when(personService.getPersonById(mockUserId)).thenReturn(Optional.of(mockPerson));

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockPerson)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle GET user sharings request - Could not find user with"
                              + " id: mock-user-id")))
          .thenReturn(mockPerson);

      staticResponseFactory
          .when(
              () -> ResponseFactory.createGetUserSharingResponse(request, mockBaseUrl, mockSharing))
          .thenReturn(mockGetUserSharingResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response = usersController.getUserSharings(mockUserId);

      // Assert
      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  @Tag("Unit")
  public void patchUserSharing() {
    // Arrange
    final var mockUserId = "mock-user-id";
    final var mockSharingId = mock(UUID.class);
    final var mockPatchUserSharingRequest = mock(PatchUserSharingRequest.class);
    final var mockSharing = mock(Sharing.class);
    final var mockAssignedSharing = mock(Sharing.class);
    final var mockUpdatedSharing = mock(Sharing.class);
    final var mockPatchUserSharingResponse = mock(PatchUserSharingResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockSharingId.toString()).thenReturn("mock-sharing-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockPatchUserSharingResponse))
        .thenReturn(mockResponseEntity);

    when(sharingService.getSharingById(mockSharingId)).thenReturn(Optional.of(mockSharing));
    when(sharingService.update(mockAssignedSharing)).thenReturn(mockUpdatedSharing);

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockSharing)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle PATCH user sharing request - Could not find sharing"
                              + " with id: mock-sharing-id")))
          .thenReturn(mockSharing);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.assignNonNull(
                      mockPatchUserSharingRequest, mockSharing, List.of("revoked")))
          .thenReturn(mockAssignedSharing);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createPatchUserSharingResponse(
                      request, mockBaseUrl, mockUpdatedSharing))
          .thenReturn(mockPatchUserSharingResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response =
          usersController.patchUserSharing(mockUserId, mockSharingId, mockPatchUserSharingRequest);

      // Assert
      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }
}
