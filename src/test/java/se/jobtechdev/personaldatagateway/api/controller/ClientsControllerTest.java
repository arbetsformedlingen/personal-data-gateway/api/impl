package se.jobtechdev.personaldatagateway.api.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import se.jobtechdev.personaldatagateway.api.generated.entities.Client;
import se.jobtechdev.personaldatagateway.api.generated.entities.ClientKeyReset;
import se.jobtechdev.personaldatagateway.api.generated.entities.Organization;
import se.jobtechdev.personaldatagateway.api.generated.entities.Redirect;
import se.jobtechdev.personaldatagateway.api.generated.model.*;
import se.jobtechdev.personaldatagateway.api.service.ClientService;
import se.jobtechdev.personaldatagateway.api.service.OrganizationService;
import se.jobtechdev.personaldatagateway.api.service.RedirectService;
import se.jobtechdev.personaldatagateway.api.util.*;

import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ClientsControllerTest {
  private final String mockBaseUrl = "mock-base-url";
  private final HttpHeaders mockLinkHeader = mock(HttpHeaders.class);
  private ClientsController clientsController;
  @Mock
  private ClientService clientService;

  @Mock
  private RedirectService redirectService;

  @Mock
  private OrganizationService organizationService;

  @Mock
  private NativeWebRequest request;

  @BeforeEach
  void setUp() {
    clientsController =
        new ClientsController(
            mockBaseUrl, request, clientService, redirectService, organizationService);
  }

  @Test
  @Tag("Unit")
  void postClient() {
    // Arrange
    final var mockOrganization = mock(Organization.class);
    final var mockOrganizationId = mock(UUID.class);
    final var mockKey = "mock-key".getBytes(StandardCharsets.UTF_8);
    final var mockBase64EncodedKey = "mock-base64-encoded-key";
    final var mockCreatedClient = mock(Client.class);
    final var mockPostClientRequest = mock(PostClientRequest.class);
    final var mockPostClientRequestName = "mock-post-client-request-name";
    final var mockPostClientResponse = mock(PostClientResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockOrganizationId.toString()).thenReturn("mock-organization-id");
    when(mockPostClientRequest.getOrganizationId()).thenReturn(mockOrganizationId);
    when(mockPostClientRequest.getName()).thenReturn(mockPostClientRequestName);
    when(mockPostClientRequest.getRole()).thenReturn(Role.CONSUMER);

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockPostClientResponse)).thenReturn(mockResponseEntity);

    when(organizationService.getOrganizationById(mockOrganizationId))
        .thenReturn(Optional.of(mockOrganization));
    when(clientService.createClient(eq(mockOrganization), anyString(), anyString(), any()))
        .thenReturn(mockCreatedClient);

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticKeyProvider = mockStatic(KeyProvider.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.CREATED))
          .thenReturn(mockResponseEntityBodyBuilder);

      staticKeyProvider.when(() -> KeyProvider.randomBytes(32)).thenReturn(mockKey);

      staticKeyProvider
          .when(() -> KeyProvider.b64EncodedKey(mockKey))
          .thenReturn(mockBase64EncodedKey);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockOrganization)),
                      eq(HttpStatus.BAD_REQUEST),
                      eq(
                          "Failed to handle POST client request - Could not find organization with"
                              + " id: mock-organization-id")))
          .thenReturn(mockOrganization);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createPostClientResponse(
                      request, mockBaseUrl, mockCreatedClient, mockBase64EncodedKey))
          .thenReturn(mockPostClientResponse);

      // Act
      final var result = clientsController.postClient(mockPostClientRequest);

      // Assert
      assertEquals(mockResponseEntity, result);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  @Tag("Unit")
  void getClient() {
    // Arrange
    final var mockClient = mock(Client.class);
    final var mockClientId = mock(UUID.class);
    final var mockGetClientResponse = mock(GetClientResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockClientId.toString()).thenReturn("mock-client-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockGetClientResponse)).thenReturn(mockResponseEntity);

    when(clientService.getClientById(mockClientId)).thenReturn(Optional.of(mockClient));

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockClient)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle GET client request - Could not find client with id:"
                              + " mock-client-id")))
          .thenReturn(mockClient);

      staticResponseFactory
          .when(() -> ResponseFactory.createGetClientResponse(request, mockBaseUrl, mockClient))
          .thenReturn(mockGetClientResponse);

      // Act
      final var result = clientsController.getClient(mockClientId);

      // Assert
      assertEquals(mockResponseEntity, result);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  @Tag("Unit")
  void getClients() {
    // Arrange
    final var mockClient = mock(Client.class);
    final var mockGetClientResponse = mock(GetClientResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(List.of(mockGetClientResponse)))
        .thenReturn(mockResponseEntity);

    when(clientService.getClients()).thenReturn(List.of(mockClient));

    try (final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      staticResponseFactory
          .when(() -> ResponseFactory.createGetClientResponse(request, mockBaseUrl, mockClient))
          .thenReturn(mockGetClientResponse);

      // Act
      final var result = clientsController.getClients();

      // Assert
      assertEquals(mockResponseEntity, result);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  @Tag("Unit")
  void patchClient() {
    // Arrange
    final var mockClient = mock(Client.class);
    final var mockClientId = mock(UUID.class);
    final var mockAssignedClient = mock(Client.class);
    final var mockSavedClient = mock(Client.class);
    final var mockPatchClientRequest = mock(PatchClientRequest.class);
    final var mockPatchClientResponse = mock(PatchClientResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockClientId.toString()).thenReturn("mock-client-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockPatchClientResponse))
        .thenReturn(mockResponseEntity);

    when(clientService.getClientById(mockClientId)).thenReturn(Optional.of(mockClient));
    when(clientService.saveClient(mockAssignedClient)).thenReturn(mockSavedClient);

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.assignNonNull(
                      mockPatchClientRequest, mockClient, List.of("revoked")))
          .thenReturn(mockAssignedClient);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockClient)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle PATCH client request - Could not find client with id:"
                              + " mock-client-id")))
          .thenReturn(mockClient);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createPatchClientResponse(request, mockBaseUrl, mockSavedClient))
          .thenReturn(mockPatchClientResponse);

      // Act
      final var result = clientsController.patchClient(mockClientId, mockPatchClientRequest);

      // Assert
      assertEquals(mockResponseEntity, result);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  @Tag("Unit")
  void postClientRedirect() {
    // Arrange
    final var mockClient = mock(Client.class);
    final var mockClientId = mock(UUID.class);
    final var mockRedirect = mock(Redirect.class);
    final var mockPostClientRedirectRequest = mock(PostClientRedirectRequest.class);
    final var mockPostClientRedirectRequestUrl = mock(URL.class);
    final var mockPostClientRedirectResponse = mock(PostClientRedirectResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockClientId.toString()).thenReturn("mock-client-id");

    when(mockPostClientRedirectRequest.getRedirectUrl())
        .thenReturn(mockPostClientRedirectRequestUrl);
    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockPostClientRedirectResponse))
        .thenReturn(mockResponseEntity);

    when(clientService.getClientById(mockClientId)).thenReturn(Optional.of(mockClient));
    when(redirectService.createRedirect(mockPostClientRedirectRequestUrl, mockClient))
        .thenReturn(mockRedirect);

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockClient)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle POST client redirect request - Could not find client"
                              + " with id: mock-client-id")))
          .thenReturn(mockClient);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createPostClientRedirectResponse(
                      request, mockBaseUrl, mockRedirect))
          .thenReturn(mockPostClientRedirectResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.CREATED))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response =
          clientsController.postClientRedirect(mockClientId, mockPostClientRedirectRequest);

      // Assert
      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  @Tag("Unit")
  void getClientRedirects() {
    // Arrange
    final var mockClient = mock(Client.class);
    final var mockClientId = mock(UUID.class);
    final var mockRedirect = mock(Redirect.class);
    final var mockGetClientRedirectResponse = mock(GetClientRedirectResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockClientId.toString()).thenReturn("mock-client-id");

    when(mockClient.getId()).thenReturn(mockClientId);
    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(List.of(mockGetClientRedirectResponse)))
        .thenReturn(mockResponseEntity);

    when(clientService.getClientById(mockClientId)).thenReturn(Optional.of(mockClient));
    when(redirectService.getRedirects(mockClientId)).thenReturn(List.of(mockRedirect));

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockClient)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle GET client redirects request - Could not find client"
                              + " with id: mock-client-id")))
          .thenReturn(mockClient);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createGetClientRedirectResponse(
                      request, mockBaseUrl, mockRedirect))
          .thenReturn(mockGetClientRedirectResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response = clientsController.getClientRedirects(mockClientId);

      // Assert
      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  @Tag("Unit")
  void getClientRedirect() {
    // Arrange
    final var mockClient = mock(Client.class);
    final var mockClientId = mock(UUID.class);
    final var mockRedirect = mock(Redirect.class);
    final var mockRedirectId = mock(UUID.class);
    final Function<UUID, Boolean> mockInequalityCheckingLambda = mock(Function.class);
    final var mockGetClientRedirectResponse = mock(GetClientRedirectResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockClient.getId()).thenReturn(mockClientId);
    when(mockClientId.toString()).thenReturn("mock-client-id");
    when(mockRedirect.getClient()).thenReturn(mockClient);
    when(mockRedirectId.toString()).thenReturn("mock-redirect-id");
    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockGetClientRedirectResponse))
        .thenReturn(mockResponseEntity);

    when(clientService.getClientById(mockClientId)).thenReturn(Optional.of(mockClient));
    when(redirectService.getRedirectById(mockRedirectId)).thenReturn(Optional.of(mockRedirect));

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticUuidProvider = mockStatic(UuidProvider.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockClient)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle GET client redirect request - Could not find client"
                              + " with id: mock-client-id")))
          .thenReturn(mockClient);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockRedirect)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle GET client redirect request - Could not find redirect"
                              + " with id: mock-redirect-id")))
          .thenReturn(mockRedirect);

      staticUuidProvider
          .when(() -> UuidProvider.generateInequalityCheckingLambda(mockClientId))
          .thenReturn(mockInequalityCheckingLambda);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createGetClientRedirectResponse(
                      request, mockBaseUrl, mockRedirect))
          .thenReturn(mockGetClientRedirectResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response = clientsController.getClientRedirect(mockClientId, mockRedirectId);

      // Assert
      staticControllerUtil.verify(
          () ->
              ControllerUtil.earlyExit(
                  eq(mockClientId),
                  eq(mockInequalityCheckingLambda),
                  eq(HttpStatus.NOT_FOUND),
                  any()),
          times(1));

      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  @Tag("Unit")
  void patchClientRedirect() {
    // Arrange
    final var mockClient = mock(Client.class);
    final var mockClientId = mock(UUID.class);
    final var mockRedirect = mock(Redirect.class);
    final var mockRedirectId = mock(UUID.class);
    final var mockRedirectClient = mock(Client.class);
    final var mockRedirectClientId = mock(UUID.class);
    final var mockAssignedRedirect = mock(Redirect.class);
    final var mockUpdatedRedirect = mock(Redirect.class);
    final Function<UUID, Boolean> mockInequalityCheckingLambda = mock(Function.class);
    final var mockPatchClientRedirectRequest = mock(PatchClientRedirectRequest.class);
    final var mockPatchClientRedirectResponse = mock(PatchClientRedirectResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockClient.getId()).thenReturn(mockClientId);
    when(mockClientId.toString()).thenReturn("mock-client-id");
    when(mockRedirect.getClient()).thenReturn(mockRedirectClient);
    when(mockRedirectId.toString()).thenReturn("mock-redirect-id");
    when(mockRedirectClient.getId()).thenReturn(mockRedirectClientId);
    when(mockRedirectClientId.toString()).thenReturn("mock-redirect-client-id");
    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockPatchClientRedirectResponse))
        .thenReturn(mockResponseEntity);

    when(clientService.getClientById(mockClientId)).thenReturn(Optional.of(mockClient));
    when(redirectService.getRedirectById(mockRedirectId)).thenReturn(Optional.of(mockRedirect));
    when(redirectService.update(mockAssignedRedirect)).thenReturn(mockUpdatedRedirect);

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticUuidProvider = mockStatic(UuidProvider.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockClient)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle PATCH client redirect request - Could not find client"
                              + " with id: mock-client-id")))
          .thenReturn(mockClient);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockRedirect)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle PATCH client redirect request - Could not find redirect"
                              + " with id: mock-redirect-id")))
          .thenReturn(mockRedirect);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.assignNonNull(
                      mockPatchClientRedirectRequest, mockRedirect, List.of("deleted")))
          .thenReturn(mockAssignedRedirect);

      staticUuidProvider
          .when(() -> UuidProvider.generateInequalityCheckingLambda(mockClientId))
          .thenReturn(mockInequalityCheckingLambda);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createPatchClientRedirectResponse(
                      request, mockBaseUrl, mockUpdatedRedirect))
          .thenReturn(mockPatchClientRedirectResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response =
          clientsController.patchClientRedirect(
              mockClientId, mockRedirectId, mockPatchClientRedirectRequest);

      // Assert
      staticControllerUtil.verify(
          () ->
              ControllerUtil.earlyExit(
                  eq(mockRedirectClientId),
                  eq(mockInequalityCheckingLambda),
                  eq(HttpStatus.NOT_FOUND),
                  eq(
                      "Failed to handle PATCH client redirect request - The redirect's"
                          + " [clientId:mock-redirect-client-id] does not match provided"
                          + " [clientId:mock-client-id]")),
          times(1));

      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  @Tag("Unit")
  public void getClientKeyResets() {
    // Arrange
    final var mockClientId = mock(UUID.class);
    final var mockClient = mock(Client.class);
    final var mockClientKeyReset = mock(ClientKeyReset.class);
    final var mockClientKeyResets = List.of(mockClientKeyReset);
    final var mockGetClientKeyResetResponse = mock(GetClientKeyResetResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockClientId.toString()).thenReturn("mock-client-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(List.of(mockGetClientKeyResetResponse)))
        .thenReturn(mockResponseEntity);

    when(clientService.getClientById(mockClientId)).thenReturn(Optional.of(mockClient));
    when(clientService.getAllClientKeyResetsByClientId(mockClient)).thenReturn(mockClientKeyResets);

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockClient)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle GET client key resets request - Could not find client"
                              + " with id: mock-client-id")))
          .thenReturn(mockClient);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createGetClientKeyResetResponse(
                      request, mockBaseUrl, mockClientKeyReset))
          .thenReturn(mockGetClientKeyResetResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response = clientsController.getClientKeyResets(mockClientId);

      // Assert
      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  @Tag("Unit")
  public void postClientKeyReset() {
    // Arrange
    final var mockClientId = mock(UUID.class);
    final var mockClient = mock(Client.class);
    final var mockClientKeyReset = mock(ClientKeyReset.class);
    final var mockClientKeyResetAndKey = mock(ClientService.ClientKeyResetAndKey.class);
    final var mockKey = new byte[]{1, 2, 3};
    final var mockEncodedKey = "mock-encoded-key";
    final var mockPostClientKeyResetResponse = mock(PostClientKeyResetResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockClientId.toString()).thenReturn("mock-client-id");
    when(mockClientKeyResetAndKey.clientKeyReset()).thenReturn(mockClientKeyReset);
    when(mockClientKeyResetAndKey.key()).thenReturn(mockKey);

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockPostClientKeyResetResponse))
        .thenReturn(mockResponseEntity);

    when(clientService.getClientById(mockClientId)).thenReturn(Optional.of(mockClient));
    when(clientService.keyReset(mockClient)).thenReturn(mockClientKeyResetAndKey);

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticKeyProvider = mockStatic(KeyProvider.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockClient)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle POST client key reset request - Could not find client"
                              + " with id: mock-client-id")))
          .thenReturn(mockClient);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createPostClientKeyResetResponse(
                      request, mockBaseUrl, mockClientKeyReset, mockEncodedKey))
          .thenReturn(mockPostClientKeyResetResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      staticKeyProvider.when(() -> KeyProvider.b64EncodedKey(mockKey)).thenReturn(mockEncodedKey);

      // Act
      final var response = clientsController.postClientKeyReset(mockClientId);

      // Assert
      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }
}
