package se.jobtechdev.personaldatagateway.api.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import se.jobtechdev.personaldatagateway.api.generated.entities.Faq;
import se.jobtechdev.personaldatagateway.api.generated.entities.Organization;
import se.jobtechdev.personaldatagateway.api.generated.model.*;
import se.jobtechdev.personaldatagateway.api.service.FaqEntity;
import se.jobtechdev.personaldatagateway.api.service.FaqService;
import se.jobtechdev.personaldatagateway.api.service.OrganizationService;
import se.jobtechdev.personaldatagateway.api.util.ControllerUtil;
import se.jobtechdev.personaldatagateway.api.util.Hateoas;
import se.jobtechdev.personaldatagateway.api.util.ResponseFactory;
import se.jobtechdev.personaldatagateway.api.util.UuidProvider;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static se.jobtechdev.personaldatagateway.api.util.ControllerUtil.throwApiExceptionOnAbsentValue;

@ExtendWith(MockitoExtension.class)
public class OrganizationsControllerTest {
  private final String mockBaseUrl = "mock-base-url";
  private final HttpHeaders mockLinkHeader = mock(HttpHeaders.class);
  private OrganizationsController organizationsController;
  @Mock
  private OrganizationService organizationService;
  @Mock
  private FaqService faqService;
  @Mock
  private NativeWebRequest request;

  @BeforeEach
  void setUp() {
    organizationsController =
        new OrganizationsController(mockBaseUrl, request, organizationService, faqService);
  }

  @Test
  void postOrganization() {
    // Arrange
    final var mockOrganization = mock(Organization.class);
    final var mockPostOrganizationRequest = mock(PostOrganizationRequest.class);
    final var mockPostOrganizationRequestName = "mock-post-organization-request-name";
    final var mockPostOrganizationRequestOrgNr = "mock-post-organization-request-org-nr";
    final var mockPostOrganizationResponse = mock(PostOrganizationResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockPostOrganizationRequest.getName()).thenReturn(mockPostOrganizationRequestName);
    when(mockPostOrganizationRequest.getOrgNr()).thenReturn(mockPostOrganizationRequestOrgNr);

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockPostOrganizationResponse))
        .thenReturn(mockResponseEntity);

    when(organizationService.createOrganization(
        mockPostOrganizationRequestOrgNr, mockPostOrganizationRequestName))
        .thenReturn(Optional.of(mockOrganization));

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockOrganization)),
                      eq(HttpStatus.BAD_REQUEST),
                      eq(
                          "Failed to handle POST organization request - Attempt to create"
                              + " organization resulted in an empty result")))
          .thenReturn(mockOrganization);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createPostOrganizationResponse(
                      request, mockBaseUrl, mockOrganization))
          .thenReturn(mockPostOrganizationResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response = organizationsController.postOrganization(mockPostOrganizationRequest);

      // Assert
      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  public void getOrganizations() {
    // Arrange
    final var mockOrganization = mock(Organization.class);
    final var mockGetOrganizationResponse = mock(GetOrganizationResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(List.of(mockGetOrganizationResponse)))
        .thenReturn(mockResponseEntity);

    when(organizationService.getOrganizations()).thenReturn(List.of(mockOrganization));

    try (final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createGetOrganizationResponse(
                      request, mockBaseUrl, mockOrganization))
          .thenReturn(mockGetOrganizationResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response = organizationsController.getOrganizations();

      // Assert
      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  void getOrganization() {
    // Arrange
    final var mockOrganization = mock(Organization.class);
    final var mockOrganizationId = mock(UUID.class);
    final var mockGetOrganizationResponse = mock(GetOrganizationResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockOrganizationId.toString()).thenReturn("mock-organization-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockGetOrganizationResponse))
        .thenReturn(mockResponseEntity);

    when(organizationService.getOrganizationById(mockOrganizationId))
        .thenReturn(Optional.of(mockOrganization));

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockOrganization)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle GET organization request - Could not find organization"
                              + " with id: mock-organization-id")))
          .thenReturn(mockOrganization);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createGetOrganizationResponse(
                      request, mockBaseUrl, mockOrganization))
          .thenReturn(mockGetOrganizationResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response = organizationsController.getOrganization(mockOrganizationId);

      // Assert
      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  void patchOrganization() {
    // Arrange
    final var mockOrganizationId = mock(UUID.class);
    final var mockOrganization = mock(Organization.class);
    final var mockPatchOrganizationRequest = mock(PatchOrganizationRequest.class);
    final var mockAssignedOrganization = mock(Organization.class);
    final var mockPatchOrganizationResponse = mock(PatchOrganizationResponse.class);
    final var mockUpdatedOrganization = mock(Organization.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockOrganizationId.toString()).thenReturn("mock-organization-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockPatchOrganizationResponse))
        .thenReturn(mockResponseEntity);

    when(organizationService.getOrganizationById(mockOrganizationId))
        .thenReturn(Optional.of(mockOrganization));
    when(organizationService.update(mockAssignedOrganization)).thenReturn(mockUpdatedOrganization);

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticOrganizationsController = mockStatic(OrganizationsController.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockOrganization)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle PATCH organization request - Could not find"
                              + " organization with id: mock-organization-id")))
          .thenReturn(mockOrganization);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.assignNonNull(
                      mockPatchOrganizationRequest,
                      mockOrganization,
                      List.of("name", "deleted"),
                      List.of("deleted")))
          .thenReturn(mockAssignedOrganization);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createPatchOrganizationResponse(
                      request, mockBaseUrl, mockUpdatedOrganization))
          .thenReturn(mockPatchOrganizationResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response =
          organizationsController.patchOrganization(
              mockOrganizationId, mockPatchOrganizationRequest);

      // Assert
      verify(organizationService, times(1)).update(mockAssignedOrganization);

      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  public void postOrganizationFaq() {
    // Arrange
    final var mockOrganizationId = mock(UUID.class);
    final var mockOrganization = mock(Organization.class);
    final var mockPostOrganizationFaqRequest = mock(PostOrganizationFaqRequest.class);
    final var mockPostOrganizationFaqRequestLang = "mock-post-organization-faq-request-lang";
    final var mockPostOrganizationFaqRequestTitle = "mock-post-organization-faq-request-title";
    final var mockPostOrganizationFaqRequestContent = "mock-post-organization-faq-request-content";
    final var mockFaq = mock(Faq.class);
    final var mockPostOrganizationFaqResponse = mock(PostOrganizationFaqResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockOrganizationId.toString()).thenReturn("mock-organization-id");
    when(mockPostOrganizationFaqRequest.getLang()).thenReturn(mockPostOrganizationFaqRequestLang);
    when(mockPostOrganizationFaqRequest.getTitle()).thenReturn(mockPostOrganizationFaqRequestTitle);
    when(mockPostOrganizationFaqRequest.getContent())
        .thenReturn(mockPostOrganizationFaqRequestContent);

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockPostOrganizationFaqResponse))
        .thenReturn(mockResponseEntity);

    when(organizationService.getOrganizationById(mockOrganizationId))
        .thenReturn(Optional.of(mockOrganization));

    when(faqService.createFaq(
        mockPostOrganizationFaqRequestLang,
        mockPostOrganizationFaqRequestTitle,
        mockPostOrganizationFaqRequestContent))
        .thenReturn(mockFaq);

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockOrganization)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle POST organization faq request - Could not find"
                              + " organization with id: mock-organization-id")))
          .thenReturn(mockOrganization);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createPostOrganizationFaqResponse(
                      request, mockBaseUrl, mockOrganizationId, mockFaq))
          .thenReturn(mockPostOrganizationFaqResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var responseEntity =
          organizationsController.postOrganizationFaq(
              mockOrganizationId, mockPostOrganizationFaqRequest);

      // Assert
      verify(organizationService, times(1)).getOrganizationById(mockOrganizationId);

      assertEquals(mockResponseEntity, responseEntity);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  public void getOrganizationFaqs_when_acceptLanguage_is_null_return_all_faqs() {
    // Arrange
    final var mockOrganizationId = mock(UUID.class);
    final var mockOrganization = mock(Organization.class);
    final var mockFaq = mock(Faq.class);
    final var mockFaqs = Set.of(mockFaq);
    final var mockGetOrganizationFaqResponse = mock(GetOrganizationFaqResponse.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);

    when(mockOrganizationId.toString()).thenReturn("mock-organization-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(List.of(mockGetOrganizationFaqResponse)))
        .thenReturn(mockResponseEntity);

    when(organizationService.getOrganizationById(mockOrganizationId))
        .thenReturn(Optional.of(mockOrganization));
    when(faqService.getFaqs((FaqEntity) mockOrganization, null)).thenReturn(mockFaqs);

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockOrganization)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle GET organization faqs request - Could not find"
                              + " organization with id: mock-organization-id")))
          .thenReturn(mockOrganization);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createGetOrganizationFaqResponse(
                      request, mockBaseUrl, mockOrganizationId, mockFaq))
          .thenReturn(mockGetOrganizationFaqResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var responseEntity =
          organizationsController.getOrganizationFaqs(mockOrganizationId, null);

      // Assert
      assertEquals(mockResponseEntity, responseEntity);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  public void getOrganizationFaq() {
    // Arrange
    final var mockOrganizationId = mock(UUID.class);
    final var mockFaq = mock(Faq.class);
    final var mockGetOrganizationFaqResponse = mock(GetOrganizationFaqResponse.class);
    final var mockFaqId = mock(UUID.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);
    final Function<UUID, Boolean> mockInequalityCheckingLambda = mock(Function.class);

    when(mockFaqId.toString()).thenReturn("mock-faq-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockGetOrganizationFaqResponse))
        .thenReturn(mockResponseEntity);

    when(faqService.getFaqById(mockFaqId)).thenReturn(Optional.of(mockFaq));

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticUuidProvider = mockStatic(UuidProvider.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockFaq)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle GET organization faq request - Could not find faq with"
                              + " id: mock-faq-id")))
          .thenReturn(mockFaq);

      staticUuidProvider
          .when(() -> UuidProvider.generateInequalityCheckingLambda(mockOrganizationId))
          .thenReturn(mockInequalityCheckingLambda);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createGetOrganizationFaqResponse(
                      request, mockBaseUrl, mockOrganizationId, mockFaq))
          .thenReturn(mockGetOrganizationFaqResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var responseEntity =
          organizationsController.getOrganizationFaq(mockOrganizationId, mockFaqId);

      assertEquals(mockResponseEntity, responseEntity);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }

  @Test
  public void patchOrganizationFaq() {
    // Arrange
    final var mockOrganizationId = mock(UUID.class);
    final var mockFaq = mock(Faq.class);
    final var mockPatchOrganizationFaqResponse = mock(PatchOrganizationFaqResponse.class);
    final var mockFaqId = mock(UUID.class);
    final var mockPatchOrganizationFaqRequest = mock(PatchOrganizationFaqRequest.class);
    final var mockAssignedFaq = mock(Faq.class);
    final var mockUpdatedFaq = mock(Faq.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    final var mockResponseEntity = mock(ResponseEntity.class);
    final var mockInequalityCheckingLambda = mock(Function.class);

    when(mockFaqId.toString()).thenReturn("mock-faq-id");

    when(mockResponseEntityBodyBuilder.contentType(MediaType.APPLICATION_JSON))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.headers(mockLinkHeader))
        .thenReturn(mockResponseEntityBodyBuilder);
    when(mockResponseEntityBodyBuilder.body(mockPatchOrganizationFaqResponse))
        .thenReturn(mockResponseEntity);

    when(faqService.getFaqById(mockFaqId)).thenReturn(Optional.of(mockFaq));
    when(faqService.update(mockAssignedFaq)).thenReturn(mockUpdatedFaq);

    try (final var staticControllerUtil = mockStatic(ControllerUtil.class);
         final var staticResponseFactory = mockStatic(ResponseFactory.class);
         final var staticResponseEntity = mockStatic(ResponseEntity.class);
         final var staticUuidProvider = mockStatic(UuidProvider.class);
         final var staticHateoas = mockStatic(Hateoas.class)) {

      staticHateoas.when(() -> Hateoas.linkHeader(request, mockBaseUrl)).thenReturn(mockLinkHeader);

      staticControllerUtil
          .when(
              () ->
                  throwApiExceptionOnAbsentValue(
                      eq(Optional.of(mockFaq)),
                      eq(HttpStatus.NOT_FOUND),
                      eq(
                          "Failed to handle PATCH organization faq request - Could not find faq"
                              + " with id: mock-faq-id")))
          .thenReturn(mockFaq);

      staticUuidProvider
          .when(() -> UuidProvider.generateInequalityCheckingLambda(mockOrganizationId))
          .thenReturn(mockInequalityCheckingLambda);

      staticControllerUtil
          .when(
              () ->
                  ControllerUtil.assignNonNull(
                      mockPatchOrganizationFaqRequest, mockFaq, List.of("deleted")))
          .thenReturn(mockAssignedFaq);

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.createPatchOrganizationFaqResponse(
                      request, mockBaseUrl, mockOrganizationId, mockUpdatedFaq))
          .thenReturn(mockPatchOrganizationFaqResponse);

      staticResponseEntity
          .when(() -> ResponseEntity.status(HttpStatus.OK))
          .thenReturn(mockResponseEntityBodyBuilder);

      // Act
      final var response =
          organizationsController.patchOrganizationFaq(
              mockOrganizationId, mockFaqId, mockPatchOrganizationFaqRequest);

      // Assert
      assertEquals(mockResponseEntity, response);
      staticHateoas.verify(() -> Hateoas.linkHeader(request, mockBaseUrl), times(1));
    }
  }
}
