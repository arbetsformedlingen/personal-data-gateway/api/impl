package se.jobtechdev.personaldatagateway.api.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;
import se.jobtechdev.personaldatagateway.api.generated.model.ApiStatus;
import se.jobtechdev.personaldatagateway.api.generated.model.GetApiInfoResponse;
import se.jobtechdev.personaldatagateway.api.util.ResponseFactory;
import se.jobtechdev.personaldatagateway.api.util.UrlFactory;

public class ApiInfoControllerTest {

  private final String mockApiName = "api-name";
  private final String apiVersion = "api-version";
  private final String buildVersion = "build-version";
  private final String apiDocumentation = "http://example.com";
  private final String released = "2024-10-31";
  private final String status = "beta";

  @Test
  @Tag("Unit")
  void getApiInfo() throws MalformedURLException {
    // Arrange
    final var mockGetApiInfoResponse = mock(GetApiInfoResponse.class);
    final var mockApiReleased = mock(LocalDate.class);
    final var mockUrl = mock(URL.class);
    final var mockApiStatus = mock(ApiStatus.class);
    final var mockResponseEntity = mock(ResponseEntity.class);
    final var mockResponseEntityBodyBuilder = mock(ResponseEntity.BodyBuilder.class);
    try (final var staticResponseFactory = mockStatic(ResponseFactory.class);
        final var staticLocalDate = mockStatic(LocalDate.class);
        final var staticApiStatus = mockStatic(ApiStatus.class);
        final var staticResponseEntity = mockStatic(ResponseEntity.class);
        final var staticUrlFactory = mockStatic(UrlFactory.class)) {

      staticResponseFactory
          .when(
              () ->
                  ResponseFactory.getApiInfoResponse(
                      mockApiName,
                      apiVersion,
                      buildVersion,
                      mockApiReleased,
                      mockUrl,
                      mockApiStatus))
          .thenReturn(mockGetApiInfoResponse);
      staticUrlFactory.when(() -> UrlFactory.createURL(apiDocumentation)).thenReturn(mockUrl);
      staticLocalDate.when(() -> LocalDate.parse(any())).thenReturn(mockApiReleased);
      staticApiStatus.when(() -> ApiStatus.fromValue(anyString())).thenReturn(mockApiStatus);
      staticResponseEntity
          .when(() -> ResponseEntity.status(any()))
          .thenReturn(mockResponseEntityBodyBuilder);
      when(mockResponseEntityBodyBuilder.contentType(any()))
          .thenReturn(mockResponseEntityBodyBuilder);
      when(mockResponseEntityBodyBuilder.body(any())).thenReturn(mockResponseEntity);
      // Act
      final var result =
          new ApiInfoController(
                  mockApiName, apiVersion, buildVersion, apiDocumentation, released, status)
              .apiInfo();

      // Assert
      assertEquals(mockResponseEntity, result);
    }
  }
}
