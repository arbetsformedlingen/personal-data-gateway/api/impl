package se.jobtechdev.personaldatagateway.api.exception;

import lombok.Getter;
import se.jobtechdev.personaldatagateway.api.generated.model.ErrorResponse;

@Getter
public class ApiException extends RuntimeException {
  private final ErrorResponse errorResponse;

  public ApiException(ErrorResponse errorResponse) {
    this.errorResponse = errorResponse;
  }

  @Override
  public String toString() {
    return "ApiException{" + "errorResponse=" + errorResponse + '}';
  }
}
