package se.jobtechdev.personaldatagateway.api.ratelimit;

import io.github.bucket4j.Bucket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RateLimitProfiles {
  private final Map<String, RateLimitProfile> rateLimitProfileMap;
  private static final Map<String, Bucket> quotaByClient = new ConcurrentHashMap<>();

  public RateLimitProfiles(
      @Value("${rate.limit.consumer.bucket.size:#{'1800'}}") int consumerBucketSize,
      @Value("${rate.limit.consumer.quota:#{'1800'}}") int consumerQuota,
      @Value("${rate.limit.consumer.window:#{'1800'}}") int consumerWindow,
      @Value("${rate.limit.frontend.bucket.size:#{'10000'}}") int frontendBucketSize,
      @Value("${rate.limit.frontend.quota:#{'10000'}}") int frontendQuota,
      @Value("${rate.limit.frontend.window:#{'60'}}") int frontendWindow,
      @Value("${rate.limit.admin.bucket.size:#{'1000'}}") int adminBucketSize,
      @Value("${rate.limit.admin.quota:#{'1000'}}") int adminQuota,
      @Value("${rate.limit.admin.window:#{'60'}}") int adminWindow,
      @Value("${rate.limit.default.bucket.size:#{'60'}}") int defaultBucketSize,
      @Value("${rate.limit.default.quota:#{'60'}}") int defaultQuota,
      @Value("${rate.limit.default.window:#{'60'}}") int defaultWindow) {
    this.rateLimitProfileMap = new HashMap<>(4);
    rateLimitProfileMap.put(
        "consumer",
        new RateLimitProfile(
            quotaByClient, "consumer", consumerBucketSize, consumerQuota, consumerWindow));
    rateLimitProfileMap.put(
        "frontend",
        new RateLimitProfile(
            quotaByClient, "frontend", frontendBucketSize, frontendQuota, frontendWindow));
    rateLimitProfileMap.put(
        "admin",
        new RateLimitProfile(quotaByClient, "admin", adminBucketSize, adminQuota, adminWindow));
    rateLimitProfileMap.put(
        "default",
        new RateLimitProfile(
            quotaByClient, "default", defaultBucketSize, defaultQuota, defaultWindow));
  }

  public RateLimitProfile getRateLimitProfile(String profileName) {
    return rateLimitProfileMap.get(profileName);
  }
}
