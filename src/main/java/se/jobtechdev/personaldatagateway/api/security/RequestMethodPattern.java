package se.jobtechdev.personaldatagateway.api.security;

import java.util.Arrays;
import java.util.Objects;
import org.springframework.http.HttpMethod;

public record RequestMethodPattern(HttpMethod method, String pattern, String[] roles) {
  public static RequestMethodPattern create(HttpMethod method, String pattern, String[] roles) {
    return new RequestMethodPattern(method, pattern, roles);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    RequestMethodPattern that = (RequestMethodPattern) o;
    return Objects.equals(method, that.method)
        && Objects.equals(pattern, that.pattern)
        && Arrays.equals(roles, that.roles);
  }
}
