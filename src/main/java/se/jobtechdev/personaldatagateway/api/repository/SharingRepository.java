package se.jobtechdev.personaldatagateway.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import se.jobtechdev.personaldatagateway.api.generated.entities.Sharing;

import java.util.List;
import java.util.UUID;

public interface SharingRepository extends JpaRepository<Sharing, UUID> {
  @Query(
      value =
          "SELECT * FROM Sharing s WHERE s.person_id = :#{#personId} AND s.datasource_id ="
              + " :#{#datasourceId} AND s.client_id = :#{#clientId} AND (s.revoked IS NULL OR"
              + " s.revoked > CURRENT_TIMESTAMP) AND (s.expires IS NULL OR s.expires > CURRENT_TIMESTAMP)",
      nativeQuery = true)
  List<Sharing> findAllActiveSharings(
      @Param("personId") String personId,
      @Param("datasourceId") UUID datasourceId,
      @Param("clientId") UUID clientId);
}
