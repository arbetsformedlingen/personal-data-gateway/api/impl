package se.jobtechdev.personaldatagateway.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import se.jobtechdev.personaldatagateway.api.generated.entities.Person;

public interface PersonRepository extends JpaRepository<Person, String> {
}
