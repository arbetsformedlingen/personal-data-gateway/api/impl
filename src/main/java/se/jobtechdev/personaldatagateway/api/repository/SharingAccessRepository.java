package se.jobtechdev.personaldatagateway.api.repository;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import se.jobtechdev.personaldatagateway.api.generated.entities.SharingAccess;

public interface SharingAccessRepository extends JpaRepository<SharingAccess, UUID> {}
