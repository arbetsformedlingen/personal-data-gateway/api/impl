package se.jobtechdev.personaldatagateway.api.repository;

import jakarta.annotation.Nonnull;
import java.util.List;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import se.jobtechdev.personaldatagateway.api.generated.entities.Datasource;

public interface DatasourceRepository extends CrudRepository<Datasource, UUID> {
  @Override
  @Nonnull
  List<Datasource> findAll();
}
