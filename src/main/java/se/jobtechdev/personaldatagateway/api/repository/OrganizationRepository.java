package se.jobtechdev.personaldatagateway.api.repository;

import jakarta.annotation.Nonnull;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import se.jobtechdev.personaldatagateway.api.generated.entities.Organization;

public interface OrganizationRepository extends CrudRepository<Organization, UUID> {

  @Override
  @Nonnull
  List<Organization> findAll();

  Optional<Organization> findByOrgNr(String orgNr);
}
