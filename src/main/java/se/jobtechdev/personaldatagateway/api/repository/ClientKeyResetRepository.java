package se.jobtechdev.personaldatagateway.api.repository;

import java.util.List;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import se.jobtechdev.personaldatagateway.api.generated.entities.ClientKeyReset;

public interface ClientKeyResetRepository extends CrudRepository<ClientKeyReset, UUID> {
  List<ClientKeyReset> findAllByClientId(UUID clientId);
}
