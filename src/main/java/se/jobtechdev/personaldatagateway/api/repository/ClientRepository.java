package se.jobtechdev.personaldatagateway.api.repository;

import jakarta.annotation.Nonnull;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.repository.CrudRepository;
import se.jobtechdev.personaldatagateway.api.generated.entities.Client;

public interface ClientRepository extends CrudRepository<Client, UUID> {
  Optional<Client> findByKeyHash(byte[] keyHash);

  @Override
  @Nonnull
  List<Client> findAll();
}
