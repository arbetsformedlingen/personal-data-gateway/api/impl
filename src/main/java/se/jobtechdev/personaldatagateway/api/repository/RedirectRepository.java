package se.jobtechdev.personaldatagateway.api.repository;

import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import se.jobtechdev.personaldatagateway.api.generated.entities.Redirect;

public interface RedirectRepository extends JpaRepository<Redirect, UUID> {
  List<Redirect> findByClientId(UUID clientId);
}
