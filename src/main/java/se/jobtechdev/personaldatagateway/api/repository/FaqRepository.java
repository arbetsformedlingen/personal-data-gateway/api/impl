package se.jobtechdev.personaldatagateway.api.repository;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import se.jobtechdev.personaldatagateway.api.generated.entities.Faq;

public interface FaqRepository extends JpaRepository<Faq, UUID> {}
