package se.jobtechdev.personaldatagateway.api.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se.jobtechdev.personaldatagateway.api.generated.entities.Client;
import se.jobtechdev.personaldatagateway.api.generated.entities.ClientKeyReset;
import se.jobtechdev.personaldatagateway.api.generated.entities.Organization;
import se.jobtechdev.personaldatagateway.api.repository.ClientKeyResetRepository;
import se.jobtechdev.personaldatagateway.api.repository.ClientRepository;
import se.jobtechdev.personaldatagateway.api.util.KeyProvider;
import se.jobtechdev.personaldatagateway.api.util.TimeProvider;
import se.jobtechdev.personaldatagateway.api.util.UuidProvider;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static se.jobtechdev.personaldatagateway.api.util.MessageDigestProvider.sha256;

@Component
public class ClientService {
  private static final Logger logger = LoggerFactory.getLogger(ClientService.class);

  private final ClientRepository clientRepository;

  private final ClientKeyResetRepository clientKeyResetRepository;

  @SuppressWarnings("unused")
  @Autowired
  public ClientService(
      ClientRepository clientRepository, ClientKeyResetRepository clientKeyResetRepository) {
    this.clientRepository = clientRepository;
    this.clientKeyResetRepository = clientKeyResetRepository;
  }

  public List<Client> getClients() {
    return clientRepository.findAll();
  }

  public Optional<Client> getClientById(UUID clientId) {
    return clientRepository.findById(clientId);
  }

  public Optional<Client> getClientByKeyHash(byte[] apiKeyHash) {
    return clientRepository.findByKeyHash(apiKeyHash);
  }

  @Transactional
  public Client createClient(
      Organization organization, String name, String role, byte[] clientKey) {
    final var clientId = UuidProvider.uuid();
    final var now = TimeProvider.now();
    final var client = new Client(clientId, organization, sha256(clientKey), name, role, now, now);
    return clientRepository.save(client);
  }

  @Transactional
  public Client saveClient(Client client) {
    return clientRepository.save(client);
  }

  @Transactional
  public void delete(Client client) {
    clientRepository.delete(client);
  }

  public List<ClientKeyReset> getAllClientKeyResetsByClientId(Client client) {
    return clientKeyResetRepository.findAllByClientId(client.getId());
  }

  @Transactional
  public ClientKeyResetAndKey keyReset(Client client) {
    final var key = KeyProvider.randomBytes(32);
    final var hash = sha256(key);
    client.setKeyHash(hash);
    clientRepository.save(client);

    final var now = ZonedDateTime.now();
    final var clientKeyReset = new ClientKeyReset(UUID.randomUUID(), client, now, now);

    final var savedClientKeyReset = clientKeyResetRepository.save(clientKeyReset);

    return new ClientKeyResetAndKey(savedClientKeyReset, key);
  }

  public record ClientKeyResetAndKey(ClientKeyReset clientKeyReset, byte[] key) {
  }
}
