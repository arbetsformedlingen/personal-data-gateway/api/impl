package se.jobtechdev.personaldatagateway.api.service;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se.jobtechdev.personaldatagateway.api.generated.entities.Faq;
import se.jobtechdev.personaldatagateway.api.repository.FaqRepository;
import se.jobtechdev.personaldatagateway.api.util.TimeProvider;
import se.jobtechdev.personaldatagateway.api.util.UuidProvider;

@Component
public class FaqService {
  private final FaqRepository faqRepository;

  @SuppressWarnings("unused")
  @Autowired
  public FaqService(FaqRepository faqRepository) {
    this.faqRepository = faqRepository;
  }

  public Optional<Faq> getFaqById(UUID faqId) {
    return faqRepository.findById(faqId);
  }

  @Transactional
  public Faq update(Faq faq) {
    final var updated = TimeProvider.now();
    faq.setUpdated(updated);
    return faqRepository.save(faq);
  }

  @Transactional
  public Faq createFaq(String lang, String title, String content) {
    final var faqId = UuidProvider.uuid();
    final var now = TimeProvider.now();

    final var faq = new Faq(faqId, lang, title, content, now, now);
    return faqRepository.save(faq);
  }

  public Set<Faq> getFaqs(FaqEntity faqEntity, String acceptLanguage) {
    final var faqs = faqEntity.getFaqs();
    return (acceptLanguage != null && !acceptLanguage.equals("*"))
        ? faqs.stream()
            .filter(faq -> faq.getLang().equals(acceptLanguage))
            .collect(Collectors.toSet())
        : faqs;
  }
}
