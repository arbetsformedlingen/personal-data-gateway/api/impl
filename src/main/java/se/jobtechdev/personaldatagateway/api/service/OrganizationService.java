package se.jobtechdev.personaldatagateway.api.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se.jobtechdev.personaldatagateway.api.generated.entities.Organization;
import se.jobtechdev.personaldatagateway.api.repository.OrganizationRepository;
import se.jobtechdev.personaldatagateway.api.util.ApiExceptionFactory;
import se.jobtechdev.personaldatagateway.api.util.ErrorResponseFactory;
import se.jobtechdev.personaldatagateway.api.util.TimeProvider;
import se.jobtechdev.personaldatagateway.api.util.UuidProvider;

@Component
public class OrganizationService {

  private static final Logger logger = LoggerFactory.getLogger(OrganizationService.class);

  private final OrganizationRepository organizationRepository;

  @Autowired
  public OrganizationService(OrganizationRepository organizationRepository) {
    this.organizationRepository = organizationRepository;
  }

  public List<Organization> getOrganizations() {
    return organizationRepository.findAll();
  }

  public Optional<Organization> getOrganizationById(UUID id) {
    return organizationRepository.findById(id);
  }

  public Optional<Organization> getOrganizationByOrgNr(String orgNr) {
    return organizationRepository.findByOrgNr(orgNr);
  }

  @Transactional
  public Optional<Organization> createOrganization(String orgNr, String name) {
    final var organizationId = UuidProvider.uuid();
    final var now = TimeProvider.now();
    final var organization = new Organization(organizationId, orgNr, name, now, now);

    Organization saved;
    try {
      saved = organizationRepository.save(organization);
      return Optional.of(saved);
    } catch (DataIntegrityViolationException e) {
      logger.error(e.getClass().getName() + " " + e.getMessage());
      throw ApiExceptionFactory.createApiException(
          ErrorResponseFactory.createErrorResponse(HttpStatus.CONFLICT, e.getMessage()));
    }
  }

  @Transactional
  public Organization save(Organization organization) {
    return organizationRepository.save(organization);
  }

  @Transactional
  public Organization update(Organization organization) {
    final var updated = TimeProvider.now();
    organization.setUpdated(updated);
    return organizationRepository.save(organization);
  }
}
