package se.jobtechdev.personaldatagateway.api.service;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se.jobtechdev.personaldatagateway.api.generated.entities.*;
import se.jobtechdev.personaldatagateway.api.repository.DatasourceRepository;
import se.jobtechdev.personaldatagateway.api.util.TimeProvider;
import se.jobtechdev.personaldatagateway.api.util.UuidProvider;

@Component
public class DatasourceService {
  private final DatasourceRepository datasourceRepository;

  @SuppressWarnings("unused")
  @Autowired
  public DatasourceService(DatasourceRepository datasourceRepository) {
    this.datasourceRepository = datasourceRepository;
  }

  public List<Datasource> getDatasources() {
    return datasourceRepository.findAll();
  }

  public Optional<Datasource> getDatasourceById(UUID id) {
    return datasourceRepository.findById(id);
  }

  @Transactional
  public Optional<Datasource> createDatasource(
      Organization organization, String name, String path, String headers, String method) {
    final URL datatypeUrl;
    try {
      datatypeUrl =
          URI.create("http://example.com/datatypes/6871e92f-5374-4f83-b743-e9966913cbbb").toURL();
    } catch (MalformedURLException e) {
      throw new RuntimeException(e);
    }

    final var datasourceId = UuidProvider.uuid();
    final var now = TimeProvider.now();

    final var datasource =
        new Datasource(datasourceId, organization, name, datatypeUrl, path, now, now);
    datasource.setHeaders(headers);
    datasource.setMethod(method);
    final var saved = datasourceRepository.save(datasource);
    return Optional.of(saved);
  }

  @Transactional
  public Datasource save(Datasource datasource) {
    return datasourceRepository.save(datasource);
  }

  @Transactional
  public Datasource update(Datasource datasource) {
    final var updated = TimeProvider.now();
    datasource.setUpdated(updated);
    return datasourceRepository.save(datasource);
  }
}
