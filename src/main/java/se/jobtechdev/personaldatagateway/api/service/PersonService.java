package se.jobtechdev.personaldatagateway.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se.jobtechdev.personaldatagateway.api.generated.entities.Person;
import se.jobtechdev.personaldatagateway.api.repository.PersonRepository;
import se.jobtechdev.personaldatagateway.api.util.TimeProvider;

import java.util.Optional;

@Component
public class PersonService {

  private final PersonRepository personRepository;

  @SuppressWarnings("unused")
  @Autowired
  public PersonService(PersonRepository personRepository) {
    this.personRepository = personRepository;
  }

  @Transactional
  public Person createPerson(String id) {
    final var person = new Person(id, TimeProvider.now());
    return personRepository.save(person);
  }

  public Optional<Person> getPersonById(String id) {
    return personRepository.findById(id);
  }
}
