package se.jobtechdev.personaldatagateway.api.service;

import java.util.Set;
import se.jobtechdev.personaldatagateway.api.generated.entities.Faq;

public interface FaqEntity {
  Set<Faq> getFaqs();
}
