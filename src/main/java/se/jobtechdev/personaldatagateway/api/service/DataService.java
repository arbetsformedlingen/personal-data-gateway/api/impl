package se.jobtechdev.personaldatagateway.api.service;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import se.jobtechdev.personaldatagateway.api.exception.ApiException;
import se.jobtechdev.personaldatagateway.api.generated.entities.Datasource;
import se.jobtechdev.personaldatagateway.api.util.CustomRestClient;
import se.jobtechdev.personaldatagateway.api.util.DataWrapper;
import se.jobtechdev.personaldatagateway.api.util.ErrorResponseFactory;
import se.jobtechdev.personaldatagateway.api.util.TokenReplacer;

@Component
public class DataService {
  private static final Logger logger = LoggerFactory.getLogger(DataService.class);

  public DataWrapper retrieveData(Datasource datasource, String userId, String acceptHeader) {
    try {
      return CustomRestClient.retrieveData(
          TokenReplacer.replace(
              datasource.getPath(), Map.of("personnummer", userId, "user-id", userId)),
          datasource.getMethod(),
          datasource.getHeaders(),
          acceptHeader);
    } catch (HttpClientErrorException exception) {
      logger.warn("Could not retrieve data, exception message: {}", exception.getMessage());
      if (exception.getStatusCode().equals(HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS)) {
        throw new ApiException(
            ErrorResponseFactory.createErrorResponse(
                HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS, exception.getMessage()));
      }
      throw new ApiException(ErrorResponseFactory.createErrorResponse(HttpStatus.BAD_GATEWAY));
    } catch (RuntimeException exception) {
      logger.error("Could not retrieve data, exception message: {}", exception.getMessage());
      throw new ApiException(ErrorResponseFactory.createErrorResponse(HttpStatus.BAD_GATEWAY));
    }
  }
}
