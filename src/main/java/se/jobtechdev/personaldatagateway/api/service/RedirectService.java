package se.jobtechdev.personaldatagateway.api.service;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se.jobtechdev.personaldatagateway.api.generated.entities.Client;
import se.jobtechdev.personaldatagateway.api.generated.entities.Redirect;
import se.jobtechdev.personaldatagateway.api.repository.RedirectRepository;
import se.jobtechdev.personaldatagateway.api.util.TimeProvider;
import se.jobtechdev.personaldatagateway.api.util.UuidProvider;

@Component
public class RedirectService {
  private final RedirectRepository redirectRepository;

  @SuppressWarnings("unused")
  @Autowired
  public RedirectService(RedirectRepository redirectRepository) {
    this.redirectRepository = redirectRepository;
  }

  @Transactional
  public Redirect createRedirect(URL url, Client client) {
    final var redirectId = UuidProvider.uuid();
    final var now = TimeProvider.now();
    final var redirect = new Redirect(redirectId, client, url, now, now);
    return redirectRepository.save(redirect);
  }

  public List<Redirect> getRedirects(UUID clientId) {
    return redirectRepository.findByClientId(clientId);
  }

  public Optional<Redirect> getRedirectById(UUID redirectId) {
    return redirectRepository.findById(redirectId);
  }

  @Transactional
  public Redirect update(Redirect redirect) {
    final var updated = TimeProvider.now();
    redirect.setUpdated(updated);
    return redirectRepository.save(redirect);
  }
}
