package se.jobtechdev.personaldatagateway.api.service;

import jakarta.annotation.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se.jobtechdev.personaldatagateway.api.generated.entities.*;
import se.jobtechdev.personaldatagateway.api.repository.SharingAccessRepository;
import se.jobtechdev.personaldatagateway.api.repository.SharingRepository;
import se.jobtechdev.personaldatagateway.api.util.TimeProvider;
import se.jobtechdev.personaldatagateway.api.util.UuidProvider;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Component
public class SharingService {
  private final SharingRepository sharingRepository;

  private final SharingAccessRepository sharingAccessRepository;

  @SuppressWarnings("unused")
  @Autowired
  public SharingService(
      SharingRepository sharingRepository, SharingAccessRepository sharingAccessRepository) {
    this.sharingRepository = sharingRepository;
    this.sharingAccessRepository = sharingAccessRepository;
  }

  @Transactional
  public Sharing createSharing(
      Person person,
      Datasource datasource,
      boolean publicSharing,
      ZonedDateTime expires,
      Client client) {
    // Assert mutually exclusive parameters
    assert publicSharing ^ (client != null);

    final var sharingId = UuidProvider.uuid();
    final var created = TimeProvider.now();
    final var sharing =
        new Sharing(
            sharingId,
            client,
            datasource,
            person,
            created,
            null,
            expires,
            created,
            Set.of());
    return sharingRepository.save(sharing);
  }

  public Optional<Sharing> getSharingById(UUID sharingId) {
    return sharingRepository.findById(sharingId);
  }

  @Transactional
  public Sharing update(Sharing sharing) {
    return sharingRepository.save(sharing);
  }

  public Set<SharingAccess> getAllSharingAccessesBySharing(Sharing sharing) {
    return sharing.getSharingAccesses();
  }

  public Optional<SharingAccess> getSharingAccessById(UUID sharingAccessId) {
    return sharingAccessRepository.findById(sharingAccessId);
  }

  @Transactional
  public Optional<SharingAccess> createSharingAccess(Sharing sharing) {
    final var sharingAccessId = UuidProvider.uuid();
    final var now = TimeProvider.now();
    final var sharingAccess = new SharingAccess(sharingAccessId, sharing, now, now, now);
    final var saved = sharingAccessRepository.save(sharingAccess);
    return Optional.of(saved);
  }

  public List<Sharing> getActiveSharings(
      Person person, Datasource datasource, @Nullable Client client) {
    final var clientId = Optional.ofNullable(client).map(Client::getId).orElse(null);

    return sharingRepository.findAllActiveSharings(person.getId(), datasource.getId(), clientId);
  }
}
