package se.jobtechdev.personaldatagateway.api.logging;

import jakarta.annotation.Nonnull;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;
import se.jobtechdev.personaldatagateway.api.util.LoggerSupplier;

import java.io.IOException;

@Component
public class LogFilter extends OncePerRequestFilter {
  private final Logger logger;

  public LogFilter() {
    logger = LoggerSupplier.forClass(LogFilter.class).get();
  }

  public void doLog(ContentCachingRequestWrapper request, ContentCachingResponseWrapper response) {
    try {
      // TODO: MDC.put("transaction.id", the unique transaction id)
      Slf4jMdcUtil.addHttpFields(request, response);
      Slf4jMdcUtil.addUrlFields(request);
      Slf4jMdcUtil.addEventFields(request, response, logger);
      Slf4jMdcUtil.log(response, logger); // Application log

      if (Slf4jMdcUtil.activateAuditField()) {
        Slf4jMdcUtil.addObjectFields(request, logger);
        Slf4jMdcUtil.log(response, logger); // Audit log
      }
    } catch (Exception e) {
      Slf4jMdcUtil.clearMdc();
      logger.error("Failed to log!");
    } finally {
      Slf4jMdcUtil.clearMdc();
    }
  }

  @Override
  protected void doFilterInternal(
      @Nonnull HttpServletRequest request,
      @Nonnull HttpServletResponse response,
      FilterChain filterChain)
      throws ServletException, IOException {
    final var requestWrapper = new ContentCachingRequestWrapper(request);
    final var responseWrapper = new ContentCachingResponseWrapper(response);

    filterChain.doFilter(requestWrapper, responseWrapper);
    doLog(requestWrapper, responseWrapper);
    responseWrapper.copyBodyToResponse();
  }
}
