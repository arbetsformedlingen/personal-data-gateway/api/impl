package se.jobtechdev.personaldatagateway.api.logging;

import java.util.regex.Pattern;

public class LogMasker {
  private LogMasker() {}

  private static final String[] masks = {
    "(\\d{12})", "(\\d{10})", "(\\d{8}-\\d{4})", "(\\d{6}-\\d{4})"
  };

  public static String mask(String original) {
    var current = original;

    for (var mask : masks) {
      final var pattern = Pattern.compile(mask);
      final var matcher = pattern.matcher(current);

      var offset = 0;

      final var sb = new StringBuilder();
      while (matcher.find()) {
        final var groupCount = matcher.groupCount();
        for (int i = 1; i <= groupCount; i++) {
          sb.append(current, offset, matcher.start(i)).append("REDACTED");
          offset = matcher.end(i);
        }
      }

      sb.append(current, offset, current.length());

      current = sb.toString();
    }
    return current;
  }
}
