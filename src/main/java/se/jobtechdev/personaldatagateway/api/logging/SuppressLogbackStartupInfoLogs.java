package se.jobtechdev.personaldatagateway.api.logging;

import ch.qos.logback.core.status.OnConsoleStatusListener;
import ch.qos.logback.core.status.Status;

public class SuppressLogbackStartupInfoLogs extends OnConsoleStatusListener {
  protected void hookPostAddStatusEvent(Status ignoredStatus) {}

  protected void hookPostStart(Status status) {}

  @Override
  public void addStatusEvent(Status status) {
    if (status.getLevel() == Status.INFO) return;
    super.addStatusEvent(status);
    hookPostAddStatusEvent(status);
  }

  @Override
  public void start() {
    final var statuses = context.getStatusManager().getCopyOfStatusList();
    for (final var status : statuses) {
      if (status.getLevel() != Status.INFO) {
        super.start();
        hookPostStart(status);
      }
    }
  }
}
