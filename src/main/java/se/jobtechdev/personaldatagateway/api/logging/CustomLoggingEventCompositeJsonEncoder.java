package se.jobtechdev.personaldatagateway.api.logging;

import ch.qos.logback.classic.spi.ILoggingEvent;
import java.nio.charset.StandardCharsets;
import net.logstash.logback.encoder.LoggingEventCompositeJsonEncoder;

public class CustomLoggingEventCompositeJsonEncoder extends LoggingEventCompositeJsonEncoder {
  @Override
  public byte[] encode(ILoggingEvent event) {
    final var bytes = super.encode(event);
    final var preMaskedBytes = hookPreMasking(bytes);
    final var rawMessage = new String(preMaskedBytes);
    final var possiblyMaskedMessage =
        rawMessage.contains("\"audit\":") ? rawMessage : LogMasker.mask(rawMessage);
    return possiblyMaskedMessage.getBytes(StandardCharsets.UTF_8);
  }

  protected byte[] hookPreMasking(byte[] bytes) {
    return bytes;
  }
}
