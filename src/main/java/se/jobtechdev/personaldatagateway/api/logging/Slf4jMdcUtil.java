package se.jobtechdev.personaldatagateway.api.logging;

import org.slf4j.Logger;
import org.slf4j.MDC;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Iterator;

public class Slf4jMdcUtil {
  public static boolean activateAuditField() {
    final var tempLog = MDC.get("temp.log");

    if (null != tempLog) {
      MDC.put("audit.log", tempLog);
      final var clientId = MDC.get("temp.custom.pdg-api.clientid");
      if (null != clientId) MDC.put("audit.custom.pdg-api.clientid", clientId);
      return true;
    }
    return false;
  }

  public static void addHttpFields(ContentCachingRequestWrapper request, ContentCachingResponseWrapper response) {
    MDC.put("http.request.method", request.getMethod());
    MDC.put("http.response.mime_type", response.getContentType());
    MDC.put("http.response.status_code", Integer.toString(response.getStatus()));
  }

  public static void addUrlFields(ContentCachingRequestWrapper request) {
    MDC.put("url.path", UrlFieldExtractor.extractUrlPath(request));
    MDC.put("url.query", UrlFieldExtractor.extractUrlQuery(request));
    MDC.put("url.full", UrlFieldExtractor.extractUrlFull(request));
    MDC.put("url.port", UrlFieldExtractor.extractUrlPort(request));
    MDC.put("url.scheme", UrlFieldExtractor.extractUrlScheme(request));
  }

  public static void addEventFields(
      ContentCachingRequestWrapper request, ContentCachingResponseWrapper response, Logger logger) {
    final var paramStringBuilder = new StringBuilder();
    final var paramNames = request.getParameterNames();
    for (Iterator<String> it = paramNames.asIterator(); it.hasNext(); ) {
      var paramName = it.next();
      var paramValues = String.join(",", request.getParameterValues(paramName));
      paramStringBuilder.append(String.format("%s=%s", paramName, paramValues));
      if (it.hasNext()) {
        paramStringBuilder.append(";");
      }
    }

    switch (request.getMethod()) {
      case "GET": {
        MDC.put("event.action", "read");
        // TODO: MDC.put("event.search.hits", the number of matching resources in collection)
        if (!paramStringBuilder.isEmpty()) {
          MDC.put("event.search.parameters", paramStringBuilder.toString());
        }
        break;
      }
      case "POST": {
        MDC.put("event.action", "create");
        if (!paramStringBuilder.isEmpty()) {
          MDC.put("event.create_parameters", paramStringBuilder.toString());
        }

        // TODO: MDC.put("object.id", the id of the created resource)

        break;
      }
      case "PATCH": {
        MDC.put("event.action", "update");
        if (!paramStringBuilder.isEmpty()) {
          MDC.put("event.update_parameters", paramStringBuilder.toString());
        }

        MDC.put("object.id", request.getRequestURI());

        break;
      }
    }
  }

  public static void log(ContentCachingResponseWrapper response, Logger logger) {
    if (response.getStatus() < 400) {
      MDC.put("event.outcome", "success");
      logger.info(ApplicationFieldExtractor.extractMessage(response));
    } else {
      MDC.put("event.outcome", "failure");
      logger.error(ApplicationFieldExtractor.extractMessage(response));
    }
  }

  public static void addObjectFields(ContentCachingRequestWrapper request, Logger logger) {
    switch (request.getMethod()) {
      case "POST": {
        Slf4jMdcUtil.addObjectBodyField(request, logger);
        break;
      }
      case "PATCH": {
        MDC.put("object.id", request.getRequestURI());
        Slf4jMdcUtil.addObjectBodyField(request, logger);
        break;
      }
    }
  }

  public static void addObjectBodyField(ContentCachingRequestWrapper request, Logger logger) {
    final var encoded = request.getContentAsString();
    if (!encoded.isEmpty()) {
      try {
        final var decoded = URLDecoder.decode(encoded, request.getCharacterEncoding());
        if (!decoded.isEmpty()) {
          MDC.put("object.body", decoded);
        }
      } catch (UnsupportedEncodingException exception) {
        logger.warn(
            "Failed to decode request content, writing content as-is to 'object.body' field...");
        MDC.put("object.body", encoded);
      }
    }
  }

  public static void clearMdc() {
    MDC.clear();
  }
}
