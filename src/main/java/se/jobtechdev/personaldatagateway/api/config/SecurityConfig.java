package se.jobtechdev.personaldatagateway.api.config;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import se.jobtechdev.personaldatagateway.api.security.RequestMethodPattern;

import java.util.List;

@Component
public class SecurityConfig {
  private static final String ADMIN = "admin";
  private static final String FRONTEND = "frontend";
  private static final String CONSUMER = "consumer";

  public List<RequestMethodPattern> getMethodPatterns() {
    return List.of(
        RequestMethodPattern.create(HttpMethod.GET, "/clients", new String[]{ADMIN}),
        RequestMethodPattern.create(HttpMethod.POST, "/clients", new String[]{ADMIN}),
        RequestMethodPattern.create(
            HttpMethod.GET, "/clients/*", new String[]{ADMIN, FRONTEND}),
        RequestMethodPattern.create(HttpMethod.PATCH, "/clients/*", new String[]{ADMIN}),
        RequestMethodPattern.create(
            HttpMethod.GET, "/clients/*/redirects", new String[]{ADMIN, FRONTEND}),
        RequestMethodPattern.create(
            HttpMethod.POST, "/clients/*/redirects", new String[]{ADMIN}),
        RequestMethodPattern.create(
            HttpMethod.GET, "/clients/*/redirects/*", new String[]{ADMIN, FRONTEND}),
        RequestMethodPattern.create(
            HttpMethod.PATCH, "/clients/*/redirects/*", new String[]{ADMIN}),
        RequestMethodPattern.create(
            HttpMethod.GET, "/clients/*/key-resets", new String[]{ADMIN}),
        RequestMethodPattern.create(
            HttpMethod.POST, "/clients/*/key-resets", new String[]{ADMIN}),
        RequestMethodPattern.create(HttpMethod.GET, "/data/*", new String[]{CONSUMER}),
        RequestMethodPattern.create(HttpMethod.GET, "/data/*/accesses", new String[]{FRONTEND}),
        RequestMethodPattern.create(
            HttpMethod.GET, "/data/*/accesses/*", new String[]{FRONTEND}),
        RequestMethodPattern.create(HttpMethod.GET, "/datasources", new String[]{}),
        RequestMethodPattern.create(HttpMethod.POST, "/datasources", new String[]{ADMIN}),
        RequestMethodPattern.create(HttpMethod.GET, "/datasources/*", new String[]{}),
        RequestMethodPattern.create(HttpMethod.PATCH, "/datasources/*", new String[]{ADMIN}),
        RequestMethodPattern.create(HttpMethod.GET, "/datasources/*/faqs", new String[]{}),
        RequestMethodPattern.create(HttpMethod.POST, "/datasources/*/faqs", new String[]{ADMIN}),
        RequestMethodPattern.create(HttpMethod.GET, "/datasources/*/faqs/*", new String[]{}),
        RequestMethodPattern.create(
            HttpMethod.PATCH, "/datasources/*/faqs/*", new String[]{ADMIN}),
        RequestMethodPattern.create(
            HttpMethod.GET, "/datasources/*/config", new String[]{ADMIN}),
        RequestMethodPattern.create(
            HttpMethod.PATCH, "/datasources/*/config", new String[]{ADMIN}),
        RequestMethodPattern.create(
            HttpMethod.GET, "/datasources/*/users/*/data", new String[]{FRONTEND}),
        RequestMethodPattern.create(HttpMethod.GET, "/datatypes", new String[]{}),
        RequestMethodPattern.create(HttpMethod.POST, "/datatypes", new String[]{ADMIN}),
        RequestMethodPattern.create(HttpMethod.GET, "/datatypes/*", new String[]{}),
        RequestMethodPattern.create(HttpMethod.PATCH, "/datatypes/*", new String[]{ADMIN}),
        RequestMethodPattern.create(HttpMethod.GET, "/datatypes/*/faqs", new String[]{}),
        RequestMethodPattern.create(HttpMethod.POST, "/datatypes/*/faqs", new String[]{ADMIN}),
        RequestMethodPattern.create(HttpMethod.GET, "/datatypes/*/faqs/*", new String[]{}),
        RequestMethodPattern.create(
            HttpMethod.PATCH, "/datatypes/*/faqs/*", new String[]{ADMIN}),
        RequestMethodPattern.create(HttpMethod.GET, "/organizations", new String[]{}),
        RequestMethodPattern.create(HttpMethod.POST, "/organizations", new String[]{ADMIN}),
        RequestMethodPattern.create(HttpMethod.GET, "/organizations/*", new String[]{}),
        RequestMethodPattern.create(HttpMethod.PATCH, "/organizations/*", new String[]{ADMIN}),
        RequestMethodPattern.create(HttpMethod.GET, "/organizations/*/faqs", new String[]{}),
        RequestMethodPattern.create(
            HttpMethod.POST, "/organizations/*/faqs", new String[]{ADMIN}),
        RequestMethodPattern.create(HttpMethod.GET, "/organizations/*/faqs/*", new String[]{}),
        RequestMethodPattern.create(
            HttpMethod.PATCH, "/organizations/*/faqs/*", new String[]{ADMIN}),
        RequestMethodPattern.create(HttpMethod.GET, "/error", new String[]{}),
        RequestMethodPattern.create(HttpMethod.GET, "/users/*", new String[]{FRONTEND}),
        RequestMethodPattern.create(HttpMethod.GET, "/users/*/sharings", new String[]{FRONTEND}),
        RequestMethodPattern.create(
            HttpMethod.POST, "/users/*/sharings", new String[]{FRONTEND}),
        RequestMethodPattern.create(
            HttpMethod.GET, "/users/*/sharings/*", new String[]{FRONTEND}),
        RequestMethodPattern.create(
            HttpMethod.PATCH, "/users/*/sharings/*", new String[]{FRONTEND}),
        RequestMethodPattern.create(HttpMethod.GET, "/api-info", new String[]{}),
        RequestMethodPattern.create(HttpMethod.GET, "/health", new String[]{}));
  }
}
