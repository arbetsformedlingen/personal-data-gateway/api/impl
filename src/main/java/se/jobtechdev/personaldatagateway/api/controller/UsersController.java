package se.jobtechdev.personaldatagateway.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.NativeWebRequest;
import se.jobtechdev.personaldatagateway.api.generated.api.UsersApi;
import se.jobtechdev.personaldatagateway.api.generated.entities.Client;
import se.jobtechdev.personaldatagateway.api.generated.entities.Sharing;
import se.jobtechdev.personaldatagateway.api.generated.model.*;
import se.jobtechdev.personaldatagateway.api.service.ClientService;
import se.jobtechdev.personaldatagateway.api.service.DatasourceService;
import se.jobtechdev.personaldatagateway.api.service.PersonService;
import se.jobtechdev.personaldatagateway.api.service.SharingService;
import se.jobtechdev.personaldatagateway.api.util.ResponseFactory;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static se.jobtechdev.personaldatagateway.api.util.ControllerUtil.*;
import static se.jobtechdev.personaldatagateway.api.util.Hateoas.linkHeader;

@Controller
public class UsersController implements UsersApi {
  private final String baseUrl;

  private final NativeWebRequest request;

  private final SharingService sharingService;

  private final PersonService personService;

  private final DatasourceService datasourceService;

  private final ClientService clientService;

  @Autowired
  public UsersController(
      @Value("${pdg-api.base-url}") String baseUrl,
      NativeWebRequest request,
      SharingService sharingService,
      PersonService personService,
      DatasourceService datasourceService,
      ClientService clientService) {
    this.baseUrl = baseUrl;
    this.request = request;
    this.sharingService = sharingService;
    this.personService = personService;
    this.datasourceService = datasourceService;
    this.clientService = clientService;
  }

  @Override
  public ResponseEntity<PostUserSharingResponse> postUserSharing(
      String userId, PostUserSharingRequest sharingRequest) {
    final var publicSharing = sharingRequest.getPublic();
    final var clientId = sharingRequest.getClientId();
    final var expires = sharingRequest.getExpires();
    final var datasourceId = sharingRequest.getDatasourceId();

    final var user =
        personService.getPersonById(userId).orElseGet(() -> personService.createPerson(userId));

    final var datasource =
        throwApiExceptionOnAbsentValue(
            datasourceService.getDatasourceById(datasourceId),
            HttpStatus.BAD_REQUEST,
            "Could not find datasource by datasourceId: " + datasourceId);

    final Sharing sharing;
    Client client = null;

    if (publicSharing) {
      earlyExit(
          clientId,
          Objects::nonNull,
          HttpStatus.UNPROCESSABLE_ENTITY,
          "The \"public\" and \"clientId\" properties are mutually exclusive");
    } else {
      earlyExit(
          clientId,
          Objects::isNull,
          HttpStatus.UNPROCESSABLE_ENTITY,
          "One of \"public\" or \"clientId\" properties must be defined");

      client =
          throwApiExceptionOnAbsentValue(
              clientService.getClientById(clientId),
              HttpStatus.BAD_REQUEST,
              "Could not find client by clientId: " + clientId);
    }

    final var activeSharings = sharingService.getActiveSharings(user, datasource, client);

    final HttpStatus httpStatus;
    if (!activeSharings.isEmpty()) {
      sharing = activeSharings.getFirst();
      httpStatus = HttpStatus.OK;
    } else {
      sharing = sharingService.createSharing(user, datasource, publicSharing, expires, client);
      httpStatus = HttpStatus.CREATED;
    }

    final var response = ResponseFactory.createPostUserSharingResponse(request, baseUrl, sharing);
    return ResponseEntity.status(httpStatus)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<GetUserSharingResponse> getUserSharing(String userId, UUID sharingId) {
    final var sharing =
        throwApiExceptionOnAbsentValue(
            sharingService.getSharingById(sharingId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle GET user sharing request - Could not find sharing with id:"
                    + " %s",
                sharingId));

    final var response = ResponseFactory.createGetUserSharingResponse(request, baseUrl, sharing);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<List<GetUserSharingResponse>> getUserSharings(String userId) {
    final var user =
        throwApiExceptionOnAbsentValue(
            personService.getPersonById(userId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle GET user sharings request - Could not find user with id: %s",
                userId));

    final var response =
        user.getSharings().stream()
            .map(sharing -> ResponseFactory.createGetUserSharingResponse(request, baseUrl, sharing))
            .toList();

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<PatchUserSharingResponse> patchUserSharing(
      String userId, UUID sharingId, PatchUserSharingRequest sharingRequest) {
    final var sharing =
        throwApiExceptionOnAbsentValue(
            sharingService.getSharingById(sharingId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle PATCH user sharing request - Could not find sharing with id:"
                    + " %s",
                sharingId));

    final var assigned = assignNonNull(sharingRequest, sharing, List.of("revoked"));

    final var updated = sharingService.update(assigned);

    final var response = ResponseFactory.createPatchUserSharingResponse(request, baseUrl, updated);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }
}
