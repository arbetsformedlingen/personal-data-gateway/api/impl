package se.jobtechdev.personaldatagateway.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.NativeWebRequest;
import se.jobtechdev.personaldatagateway.api.generated.api.DataApi;
import se.jobtechdev.personaldatagateway.api.generated.entities.Client;
import se.jobtechdev.personaldatagateway.api.generated.model.GetDataAccessResponse;
import se.jobtechdev.personaldatagateway.api.service.ClientService;
import se.jobtechdev.personaldatagateway.api.service.DataService;
import se.jobtechdev.personaldatagateway.api.service.SharingService;
import se.jobtechdev.personaldatagateway.api.util.AuthUtil;
import se.jobtechdev.personaldatagateway.api.util.ResponseFactory;
import se.jobtechdev.personaldatagateway.api.util.TimeProvider;
import se.jobtechdev.personaldatagateway.api.util.UuidProvider;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;

import static se.jobtechdev.personaldatagateway.api.util.ControllerUtil.earlyExit;
import static se.jobtechdev.personaldatagateway.api.util.ControllerUtil.throwApiExceptionOnAbsentValue;
import static se.jobtechdev.personaldatagateway.api.util.Hateoas.linkHeader;

@Controller
public class DataController implements DataApi {

  private final String baseUrl;

  private final NativeWebRequest request;

  private final ClientService clientService;

  private final SharingService sharingService;

  private final DataService dataService;

  @Autowired
  public DataController(
      @Value("${pdg-api.base-url}") String baseUrl,
      NativeWebRequest request,
      ClientService clientService,
      SharingService sharingService,
      DataService dataService) {
    this.baseUrl = baseUrl;
    this.request = request;
    this.clientService = clientService;
    this.sharingService = sharingService;
    this.dataService = dataService;
  }

  public static Function<Client, Boolean> clientInequalityCheck(Client client1) {
    return client2 -> client2 != null && !client2.equals(client1);
  }

  public static Function<ZonedDateTime, Boolean> isTimestampNullOrPassedCheck() {
    return timestamp -> timestamp != null && timestamp.isBefore(TimeProvider.now());
  }

  @Override
  public ResponseEntity<byte[]> getData(UUID dataId) {
    final var authKey = request.getHeader("X-Auth-Key");

    final var decodedAuthKey = AuthUtil.decodeAuthKey(authKey);
    final var authKeyHash = AuthUtil.hashDecodedAuthKey(decodedAuthKey);

    final var authClient =
        throwApiExceptionOnAbsentValue(
            clientService.getClientByKeyHash(authKeyHash),
            HttpStatus.FORBIDDEN,
            "Failed to handle GET data request - Could not find any client associated with the"
                + " provided auth key");

    final var sharing =
        throwApiExceptionOnAbsentValue(
            sharingService.getSharingById(dataId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle GET data request - Could not find sharing by id: %s", dataId));

    final var sharingClient = sharing.getClient();

    earlyExit(
        sharingClient,
        clientInequalityCheck(authClient),
        HttpStatus.FORBIDDEN,
        String.format(
            "Failed to handle GET data request - Authenticated client [clientId:%s] does not match"
                + " sharing's client [clientId:%s]",
            authClient.getId().toString(),
            Optional.ofNullable(sharingClient)
                .map(client -> client.getId().toString())
                .orElse(null)));

    final var sharingId = sharing.getId().toString();

    earlyExit(
        sharing.getRevoked(),
        Objects::nonNull,
        HttpStatus.FORBIDDEN,
        String.format(
            "Failed to handle GET data request - The requested sharing [%s] has been revoked",
            sharingId));

    earlyExit(
        sharing.getExpires(),
        isTimestampNullOrPassedCheck(),
        HttpStatus.FORBIDDEN,
        String.format(
            "Failed to handle GET data request - The requested sharing [%s] has expired",
            sharingId));

    final var dataWrapper =
        dataService.retrieveData(
            sharing.getDatasource(),
            sharing.getPerson().getId(),
            request.getHeader(HttpHeaders.ACCEPT));

    sharingService.createSharingAccess(sharing);

    return ResponseEntity.status(dataWrapper.status())
        .contentType(dataWrapper.contentType())
        .headers(linkHeader(request, baseUrl))
        .body(dataWrapper.body());
  }

  @Override
  public ResponseEntity<GetDataAccessResponse> getDataAccess(UUID dataId, UUID accessId) {
    final var sharingAccess =
        throwApiExceptionOnAbsentValue(
            sharingService.getSharingAccessById(accessId),
            HttpStatus.NOT_FOUND,
            String.format("Could not find data access by id: %s", accessId));

    earlyExit(
        sharingAccess.getSharing().getId(),
        UuidProvider.generateInequalityCheckingLambda(dataId),
        HttpStatus.NOT_FOUND,
        String.format(
            "Could not find data access by dataId: %s and accessId: %s",
            dataId.toString(), accessId.toString()));

    final var response =
        ResponseFactory.createGetDataAccessResponse(request, baseUrl, sharingAccess);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<List<GetDataAccessResponse>> getDataAccesses(UUID dataId) {
    final var sharing =
        throwApiExceptionOnAbsentValue(
            sharingService.getSharingById(dataId),
            HttpStatus.NOT_FOUND,
            String.format("Could not find sharing by id: %s", dataId));

    final var sharingAccesses = sharingService.getAllSharingAccessesBySharing(sharing);

    final var response =
        sharingAccesses.stream()
            .map(
                sharingAccess ->
                    ResponseFactory.createGetDataAccessResponse(request, baseUrl, sharingAccess))
            .toList();

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }
}
