package se.jobtechdev.personaldatagateway.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.NativeWebRequest;
import se.jobtechdev.personaldatagateway.api.generated.api.DatasourcesApi;
import se.jobtechdev.personaldatagateway.api.generated.model.*;
import se.jobtechdev.personaldatagateway.api.service.DataService;
import se.jobtechdev.personaldatagateway.api.service.DatasourceService;
import se.jobtechdev.personaldatagateway.api.service.FaqService;
import se.jobtechdev.personaldatagateway.api.service.OrganizationService;
import se.jobtechdev.personaldatagateway.api.util.ResponseFactory;

import java.util.List;
import java.util.UUID;

import static se.jobtechdev.personaldatagateway.api.util.ControllerUtil.assignNonNull;
import static se.jobtechdev.personaldatagateway.api.util.ControllerUtil.throwApiExceptionOnAbsentValue;
import static se.jobtechdev.personaldatagateway.api.util.Hateoas.linkHeader;

@Controller
public class DatasourcesController implements DatasourcesApi {
  private final DatasourceService datasourceService;

  private final FaqService faqService;

  private final OrganizationService organizationService;

  private final DataService dataService;

  private final NativeWebRequest request;

  private final String baseUrl;

  @Autowired
  public DatasourcesController(
      @Value("${pdg-api.base-url}") String baseUrl,
      NativeWebRequest request,
      DatasourceService datasourceService,
      FaqService faqService,
      OrganizationService organizationService,
      DataService dataService) {
    this.baseUrl = baseUrl;
    this.request = request;
    this.datasourceService = datasourceService;
    this.faqService = faqService;
    this.organizationService = organizationService;
    this.dataService = dataService;
  }

  @Override
  public ResponseEntity<PostDatasourceResponse> postDatasource(
      PostDatasourceRequest postDatasourceRequest) {
    final var organizationId = postDatasourceRequest.getOrganizationId();

    final var organization =
        throwApiExceptionOnAbsentValue(
            organizationService.getOrganizationById(organizationId),
            HttpStatus.BAD_REQUEST,
            String.format(
                "Failed to handle POST datasource request - Could not find organization with"
                    + " id: %s",
                organizationId));

    final var datasource =
        throwApiExceptionOnAbsentValue(
            datasourceService.createDatasource(
                organization,
                postDatasourceRequest.getName(),
                postDatasourceRequest.getPath(),
                postDatasourceRequest.getHeaders(),
                postDatasourceRequest.getMethod()),
            HttpStatus.BAD_REQUEST,
            "Failed to handle POST datasource request - Attempt to create datasource resulted"
                + " in an empty result");

    final var response = ResponseFactory.createPostDatasourceResponse(request, baseUrl, datasource);

    return ResponseEntity.status(HttpStatus.CREATED)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<List<GetDatasourceResponse>> getDatasources() {
    final var datasources = datasourceService.getDatasources();

    final var response =
        datasources.stream()
            .map(
                datasource ->
                    ResponseFactory.createGetDatasourceResponse(request, baseUrl, datasource))
            .toList();

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<GetDatasourceResponse> getDatasource(UUID datasourceId) {
    final var datasource =
        throwApiExceptionOnAbsentValue(
            datasourceService.getDatasourceById(datasourceId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle GET datasource - Could not find datasource with id: %s",
                datasourceId));

    final var response = ResponseFactory.createGetDatasourceResponse(request, baseUrl, datasource);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<PatchDatasourceResponse> patchDatasource(
      UUID datasourceId, PatchDatasourceRequest patchDatasourceRequest) {
    final var datasource =
        throwApiExceptionOnAbsentValue(
            datasourceService.getDatasourceById(datasourceId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle PATCH datasource - Could not find datasource with id: %s",
                datasourceId));

    final var assigned = assignNonNull(patchDatasourceRequest, datasource, List.of("deleted"));

    final var updated = datasourceService.update(assigned);

    final var response = ResponseFactory.createPatchDatasourceResponse(request, baseUrl, updated);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<byte[]> getDatasourceUserData(UUID datasourceId, String userId) {
    final var datasource =
        throwApiExceptionOnAbsentValue(
            datasourceService.getDatasourceById(datasourceId),
            HttpStatus.NOT_FOUND,
            String.format("Could not find datasource by id: %s", datasourceId));

    final var dataWrapper =
        dataService.retrieveData(datasource, userId, request.getHeader(HttpHeaders.ACCEPT));

    return ResponseEntity.status(dataWrapper.status())
        .contentType(dataWrapper.contentType())
        .headers(linkHeader(request, baseUrl))
        .body(dataWrapper.body());
  }

  @Override
  public ResponseEntity<GetDatasourceConfigResponse> getDatasourceConfig(UUID datasourceId) {
    final var datasource =
        throwApiExceptionOnAbsentValue(
            datasourceService.getDatasourceById(datasourceId),
            HttpStatus.NOT_FOUND,
            String.format("Could not find datasource by id: %s", datasourceId));

    final var responseBody =
        ResponseFactory.createGetDatasourceConfigResponse(request, baseUrl, datasource);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(responseBody);
  }

  @Override
  public ResponseEntity<PatchDatasourceConfigResponse> patchDatasourceConfig(
      UUID datasourceId, PatchDatasourceConfigRequest patchDatasourceConfigRequest) {
    final var datasource =
        throwApiExceptionOnAbsentValue(
            datasourceService.getDatasourceById(datasourceId),
            HttpStatus.NOT_FOUND,
            String.format("Could not find datasource by id: %s", datasourceId));

    final var assigned =
        assignNonNull(patchDatasourceConfigRequest, datasource, List.of("deleted"));

    final var updated = datasourceService.update(assigned);

    final var responseBody =
        ResponseFactory.createPatchDatasourceConfigResponse(request, baseUrl, updated);

    responseBody.setHeaders(updated.getHeaders());

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(responseBody);
  }

  @Override
  public ResponseEntity<PostDatasourceFaqResponse> postDatasourceFaq(
      UUID datasourceId, PostDatasourceFaqRequest postDatasourceFaqRequest) {
    final var datasource =
        throwApiExceptionOnAbsentValue(
            datasourceService.getDatasourceById(datasourceId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle POST datasource faq request - Could not find datasource with"
                    + " id: %s",
                datasourceId));

    final var faqs = datasource.getFaqs();

    final var faq =
        faqService.createFaq(
            postDatasourceFaqRequest.getLang(),
            postDatasourceFaqRequest.getTitle(),
            postDatasourceFaqRequest.getContent());
    faqs.add(faq);
    datasourceService.save(datasource);

    final var response =
        ResponseFactory.createPostDatasourceFaqResponse(request, baseUrl, datasource, faq);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .body(response);
  }

  @Override
  public ResponseEntity<List<GetDatasourceFaqResponse>> getDatasourceFaqs(
      UUID datasourceId, String acceptLanguage) {
    final var datasource =
        throwApiExceptionOnAbsentValue(
            datasourceService.getDatasourceById(datasourceId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle GET datasource faqs request - Could not find datasource with"
                    + " id: %s",
                datasourceId));

    final var faqs = faqService.getFaqs(datasource, acceptLanguage);

    final var response =
        faqs.stream()
            .map(
                faq ->
                    ResponseFactory.createGetDatasourceFaqResponse(
                        request, baseUrl, datasourceId, faq))
            .toList();

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .body(response);
  }

  @Override
  public ResponseEntity<GetDatasourceFaqResponse> getDatasourceFaq(UUID datasourceId, UUID faqId) {
    final var faq =
        throwApiExceptionOnAbsentValue(
            faqService.getFaqById(faqId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle GET datasource faq request - Could not find faq with id: %s",
                faqId));

    final var response =
        ResponseFactory.createGetDatasourceFaqResponse(request, baseUrl, datasourceId, faq);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .body(response);
  }

  @Override
  public ResponseEntity<PatchDatasourceFaqResponse> patchDatasourceFaq(
      UUID datasourceId, UUID faqId, PatchDatasourceFaqRequest patchDatasourceFaqRequest) {

    final var faq =
        throwApiExceptionOnAbsentValue(
            faqService.getFaqById(faqId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle PATCH datasource faq request - Could not find faq with id:"
                    + " %s",
                faqId));

    final var patchedFaq = assignNonNull(patchDatasourceFaqRequest, faq, List.of("deleted"));

    final var updated = faqService.update(patchedFaq);

    final var response =
        ResponseFactory.createPatchDatasourceFaqResponse(request, baseUrl, datasourceId, updated);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .body(response);
  }
}
