package se.jobtechdev.personaldatagateway.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.NativeWebRequest;
import se.jobtechdev.personaldatagateway.api.generated.api.ClientsApi;
import se.jobtechdev.personaldatagateway.api.generated.model.*;
import se.jobtechdev.personaldatagateway.api.service.ClientService;
import se.jobtechdev.personaldatagateway.api.service.OrganizationService;
import se.jobtechdev.personaldatagateway.api.service.RedirectService;
import se.jobtechdev.personaldatagateway.api.util.KeyProvider;
import se.jobtechdev.personaldatagateway.api.util.ResponseFactory;
import se.jobtechdev.personaldatagateway.api.util.UuidProvider;

import java.util.List;
import java.util.UUID;

import static se.jobtechdev.personaldatagateway.api.util.ControllerUtil.*;
import static se.jobtechdev.personaldatagateway.api.util.Hateoas.linkHeader;

@Controller
public class ClientsController implements ClientsApi {

  private final ClientService clientService;

  private final RedirectService redirectService;

  private final OrganizationService organizationService;

  private final NativeWebRequest request;

  private final String baseUrl;

  @Autowired
  public ClientsController(
      @Value("${pdg-api.base-url}") String baseUrl,
      NativeWebRequest request,
      ClientService clientService,
      RedirectService redirectService,
      OrganizationService organizationService) {
    this.baseUrl = baseUrl;
    this.request = request;
    this.clientService = clientService;
    this.redirectService = redirectService;
    this.organizationService = organizationService;
  }

  @Override
  public ResponseEntity<PostClientResponse> postClient(PostClientRequest clientRequest) {
    final var organizationId = clientRequest.getOrganizationId();
    final var organization =
        throwApiExceptionOnAbsentValue(
            organizationService.getOrganizationById(organizationId),
            HttpStatus.BAD_REQUEST,
            String.format(
                "Failed to handle POST client request - Could not find organization with id:"
                    + " %s",
                organizationId));

    final var clientKey = KeyProvider.randomBytes(32);

    final var createdClient =
        clientService.createClient(
            organization, clientRequest.getName(), clientRequest.getRole().getValue(), clientKey);

    final var encodedApiKey = KeyProvider.b64EncodedKey(clientKey);

    final var savedClient =
        ResponseFactory.createPostClientResponse(request, baseUrl, createdClient, encodedApiKey);

    return ResponseEntity.status(HttpStatus.CREATED)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(savedClient);
  }

  @Override
  public ResponseEntity<List<GetClientResponse>> getClients() {
    final var clients = clientService.getClients();

    final var response =
        clients.stream()
            .map(client -> ResponseFactory.createGetClientResponse(request, baseUrl, client))
            .toList();

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<GetClientResponse> getClient(UUID clientId) {
    final var client =
        throwApiExceptionOnAbsentValue(
            clientService.getClientById(clientId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle GET client request - Could not find client with id: %s",
                clientId));

    final var response = ResponseFactory.createGetClientResponse(request, baseUrl, client);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<PatchClientResponse> patchClient(
      UUID clientId, PatchClientRequest patchClientRequest) {
    final var client =
        throwApiExceptionOnAbsentValue(
            clientService.getClientById(clientId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle PATCH client request - Could not find client with id: %s",
                clientId));

    final var assigned = assignNonNull(patchClientRequest, client, List.of("revoked"));

    final var savedClient = clientService.saveClient(assigned);

    final var response = ResponseFactory.createPatchClientResponse(request, baseUrl, savedClient);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<PostClientRedirectResponse> postClientRedirect(
      UUID clientId, PostClientRedirectRequest postClientRedirectRequest) {
    final var client =
        throwApiExceptionOnAbsentValue(
            clientService.getClientById(clientId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle POST client redirect request - Could not find client with id:"
                    + " %s",
                clientId));

    final var createdRedirect =
        redirectService.createRedirect(postClientRedirectRequest.getRedirectUrl(), client);

    final var response =
        ResponseFactory.createPostClientRedirectResponse(request, baseUrl, createdRedirect);

    return ResponseEntity.status(HttpStatus.CREATED)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<List<GetClientRedirectResponse>> getClientRedirects(UUID clientId) {
    final var client =
        throwApiExceptionOnAbsentValue(
            clientService.getClientById(clientId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle GET client redirects request - Could not find client with id:"
                    + " %s",
                clientId));

    final var redirects = redirectService.getRedirects(client.getId());

    final var response =
        redirects.stream()
            .map(
                redirect ->
                    ResponseFactory.createGetClientRedirectResponse(request, baseUrl, redirect))
            .toList();

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<GetClientRedirectResponse> getClientRedirect(
      UUID clientId, UUID redirectId) {
    final var client =
        throwApiExceptionOnAbsentValue(
            clientService.getClientById(clientId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle GET client redirect request - Could not find client with id:"
                    + " %s",
                clientId));

    final var redirect =
        throwApiExceptionOnAbsentValue(
            redirectService.getRedirectById(redirectId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle GET client redirect request - Could not find redirect with"
                    + " id: %s",
                redirectId));

    final var redirectClientId = redirect.getClient().getId();

    earlyExit(
        redirectClientId,
        UuidProvider.generateInequalityCheckingLambda(client.getId()),
        HttpStatus.NOT_FOUND,
        String.format(
            "Failed to handle GET client redirect request - The redirect's [clientId:%s] does not"
                + " match provided [clientId:%s]",
            redirectClientId, clientId));

    final var response =
        ResponseFactory.createGetClientRedirectResponse(request, baseUrl, redirect);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<PatchClientRedirectResponse> patchClientRedirect(
      UUID clientId, UUID redirectId, PatchClientRedirectRequest patchClientRedirectRequest) {
    final var client =
        throwApiExceptionOnAbsentValue(
            clientService.getClientById(clientId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle PATCH client redirect request - Could not find client with"
                    + " id: %s",
                clientId));

    final var redirect =
        throwApiExceptionOnAbsentValue(
            redirectService.getRedirectById(redirectId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle PATCH client redirect request - Could not find redirect with"
                    + " id: %s",
                redirectId));

    final var redirectClientId = redirect.getClient().getId();

    earlyExit(
        redirectClientId,
        UuidProvider.generateInequalityCheckingLambda(client.getId()),
        HttpStatus.NOT_FOUND,
        String.format(
            "Failed to handle PATCH client redirect request - The redirect's [clientId:%s] does not"
                + " match provided [clientId:%s]",
            redirectClientId, clientId));

    final var assigned = assignNonNull(patchClientRedirectRequest, redirect, List.of("deleted"));

    final var updatedRedirect = redirectService.update(assigned);

    final var response =
        ResponseFactory.createPatchClientRedirectResponse(request, baseUrl, updatedRedirect);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<List<GetClientKeyResetResponse>> getClientKeyResets(UUID clientId) {
    final var client =
        throwApiExceptionOnAbsentValue(
            clientService.getClientById(clientId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle GET client key resets request - Could not find client with"
                    + " id: %s",
                clientId));

    final var response =
        clientService.getAllClientKeyResetsByClientId(client).stream()
            .map(
                clientKeyReset ->
                    ResponseFactory.createGetClientKeyResetResponse(
                        request, baseUrl, clientKeyReset))
            .toList();

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<PostClientKeyResetResponse> postClientKeyReset(UUID clientId) {
    final var client =
        throwApiExceptionOnAbsentValue(
            clientService.getClientById(clientId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle POST client key reset request - Could not find client with"
                    + " id: %s",
                clientId));

    final var clientKeyResetAndKey = clientService.keyReset(client);

    final var clientKeyReset = clientKeyResetAndKey.clientKeyReset();
    final var key = clientKeyResetAndKey.key();

    final var base64EncodedKey = KeyProvider.b64EncodedKey(key);

    final var response =
        ResponseFactory.createPostClientKeyResetResponse(
            request, baseUrl, clientKeyReset, base64EncodedKey);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }
}
