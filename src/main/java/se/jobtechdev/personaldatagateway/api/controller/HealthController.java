package se.jobtechdev.personaldatagateway.api.controller;

import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import se.jobtechdev.personaldatagateway.api.Main;
import se.jobtechdev.personaldatagateway.api.generated.api.HealthApi;
import se.jobtechdev.personaldatagateway.api.generated.model.CheckStatus;
import se.jobtechdev.personaldatagateway.api.generated.model.GetHealthResponse;
import se.jobtechdev.personaldatagateway.api.generated.model.HealthChecks;
import se.jobtechdev.personaldatagateway.api.generated.model.Status;
import se.jobtechdev.personaldatagateway.api.util.ResponseFactory;

@Primary
@Controller
public class HealthController implements HealthApi {
  @Override
  public ResponseEntity<GetHealthResponse> health() {
    final var liveness = new CheckStatus(Status.UP);
    final var readiness = new CheckStatus(Main.isStarted() ? Status.UP : Status.DOWN);

    final var checks = new HealthChecks(liveness, readiness);
    final var response = ResponseFactory.getHealthResponse(Status.UP, checks);

    final var readinessStatus = (Main.isStarted()) ? HttpStatus.OK : HttpStatus.SERVICE_UNAVAILABLE;

    return ResponseEntity.status(readinessStatus)
        .contentType(MediaType.APPLICATION_JSON)
        .body(response);
  }
}
