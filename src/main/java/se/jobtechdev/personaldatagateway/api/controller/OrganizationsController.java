package se.jobtechdev.personaldatagateway.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.NativeWebRequest;
import se.jobtechdev.personaldatagateway.api.generated.api.OrganizationsApi;
import se.jobtechdev.personaldatagateway.api.generated.model.*;
import se.jobtechdev.personaldatagateway.api.service.FaqService;
import se.jobtechdev.personaldatagateway.api.service.OrganizationService;
import se.jobtechdev.personaldatagateway.api.util.ControllerUtil;
import se.jobtechdev.personaldatagateway.api.util.ResponseFactory;

import java.util.List;
import java.util.UUID;

import static se.jobtechdev.personaldatagateway.api.util.ControllerUtil.assignNonNull;
import static se.jobtechdev.personaldatagateway.api.util.ControllerUtil.throwApiExceptionOnAbsentValue;
import static se.jobtechdev.personaldatagateway.api.util.Hateoas.linkHeader;

@Controller
public class OrganizationsController implements OrganizationsApi {

  private final OrganizationService organizationService;

  private final FaqService faqService;

  private final NativeWebRequest request;

  private final String baseUrl;

  @Autowired
  public OrganizationsController(
      @Value("${pdg-api.base-url}") String baseUrl,
      NativeWebRequest request,
      OrganizationService organizationService,
      FaqService faqService) {
    this.baseUrl = baseUrl;
    this.request = request;
    this.organizationService = organizationService;
    this.faqService = faqService;
  }

  @Override
  public ResponseEntity<PostOrganizationResponse> postOrganization(
      PostOrganizationRequest postOrganizationRequest) {
    final var organization =
        throwApiExceptionOnAbsentValue(
            organizationService.createOrganization(
                postOrganizationRequest.getOrgNr(), postOrganizationRequest.getName()),
            HttpStatus.BAD_REQUEST,
            "Failed to handle POST organization request - Attempt to create organization"
                + " resulted in an empty result");

    final var response =
        ResponseFactory.createPostOrganizationResponse(request, baseUrl, organization);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<List<GetOrganizationResponse>> getOrganizations() {
    final var response =
        organizationService.getOrganizations().stream()
            .map(
                organization ->
                    ResponseFactory.createGetOrganizationResponse(request, baseUrl, organization))
            .toList();

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<GetOrganizationResponse> getOrganization(UUID organizationId) {
    final var organization =
        throwApiExceptionOnAbsentValue(
            organizationService.getOrganizationById(organizationId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle GET organization request - Could not find organization with"
                    + " id: %s",
                organizationId));

    final var response =
        ResponseFactory.createGetOrganizationResponse(request, baseUrl, organization);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<PatchOrganizationResponse> patchOrganization(
      UUID organizationId, PatchOrganizationRequest patchOrganizationRequest) {
    final var organization =
        throwApiExceptionOnAbsentValue(
            organizationService.getOrganizationById(organizationId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle PATCH organization request - Could not find organization with"
                    + " id: %s",
                organizationId));

    final var assigned =
        ControllerUtil.assignNonNull(
            patchOrganizationRequest, organization, List.of("name", "deleted"), List.of("deleted"));

    final var updated = organizationService.update(assigned);

    final var response = ResponseFactory.createPatchOrganizationResponse(request, baseUrl, updated);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<PostOrganizationFaqResponse> postOrganizationFaq(
      UUID organizationId, PostOrganizationFaqRequest postOrganizationFaqRequest) {
    final var organization =
        throwApiExceptionOnAbsentValue(
            organizationService.getOrganizationById(organizationId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle POST organization faq request - Could not find organization"
                    + " with id: %s",
                organizationId));

    final var faqs = faqService.getFaqs(organization, null);

    final var faq =
        faqService.createFaq(
            postOrganizationFaqRequest.getLang(),
            postOrganizationFaqRequest.getTitle(),
            postOrganizationFaqRequest.getContent());
    faqs.add(faq);
    organizationService.save(organization);

    final var response =
        ResponseFactory.createPostOrganizationFaqResponse(request, baseUrl, organizationId, faq);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<List<GetOrganizationFaqResponse>> getOrganizationFaqs(
      UUID organizationId, String acceptLanguage) {
    final var organization =
        throwApiExceptionOnAbsentValue(
            organizationService.getOrganizationById(organizationId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle GET organization faqs request - Could not find organization"
                    + " with id: %s",
                organizationId));

    final var faqs = faqService.getFaqs(organization, acceptLanguage);

    final var response =
        faqs.stream()
            .map(
                faq ->
                    ResponseFactory.createGetOrganizationFaqResponse(
                        request, baseUrl, organizationId, faq))
            .toList();

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<GetOrganizationFaqResponse> getOrganizationFaq(
      UUID organizationId, UUID faqId) {
    final var faq =
        throwApiExceptionOnAbsentValue(
            faqService.getFaqById(faqId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle GET organization faq request - Could not find faq with id:"
                    + " %s",
                faqId));

    final var response =
        ResponseFactory.createGetOrganizationFaqResponse(request, baseUrl, organizationId, faq);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }

  @Override
  public ResponseEntity<PatchOrganizationFaqResponse> patchOrganizationFaq(
      UUID organizationId, UUID faqId, PatchOrganizationFaqRequest patchOrganizationFaqRequest) {

    final var faq =
        throwApiExceptionOnAbsentValue(
            faqService.getFaqById(faqId),
            HttpStatus.NOT_FOUND,
            String.format(
                "Failed to handle PATCH organization faq request - Could not find faq with id:"
                    + " %s",
                faqId));

    final var patchedFaq = assignNonNull(patchOrganizationFaqRequest, faq, List.of("deleted"));

    final var updated = faqService.update(patchedFaq);

    final var response =
        ResponseFactory.createPatchOrganizationFaqResponse(
            request, baseUrl, organizationId, updated);

    return ResponseEntity.status(HttpStatus.OK)
        .contentType(MediaType.APPLICATION_JSON)
        .headers(linkHeader(request, baseUrl))
        .body(response);
  }
}
