package se.jobtechdev.personaldatagateway.api.util;

import org.springframework.http.ResponseEntity;
import se.jobtechdev.personaldatagateway.api.generated.model.ErrorResponse;

public final class ResponseEntityFactory {
  private ResponseEntityFactory() {
  }

  public static ResponseEntity<?> create(ErrorResponse errorResponse) {
    return ResponseEntity.status(errorResponse.getStatus()).body(errorResponse);
  }
}
