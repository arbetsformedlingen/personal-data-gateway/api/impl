package se.jobtechdev.personaldatagateway.api.util;

import java.util.UUID;
import java.util.function.Function;

public final class UuidProvider {
  private UuidProvider() {
  }

  public static UUID uuid() {
    return UUID.randomUUID();
  }

  public static Function<UUID, Boolean> generateInequalityCheckingLambda(UUID uuid) {
    return id -> !id.equals(uuid);
  }
}
