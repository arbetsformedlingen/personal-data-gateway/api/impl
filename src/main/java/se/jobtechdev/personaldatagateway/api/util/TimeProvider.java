package se.jobtechdev.personaldatagateway.api.util;

import java.time.ZonedDateTime;

public final class TimeProvider {
  private TimeProvider() {
  }

  public static Long currentTimeMillis() {
    return System.currentTimeMillis();
  }

  public static ZonedDateTime now() {
    return ZonedDateTime.now();
  }
}
