package se.jobtechdev.personaldatagateway.api.util;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestClient;

import java.util.Map;
import java.util.function.Consumer;

public final class CustomRestClient {
  private CustomRestClient() {
  }

  public static DataWrapper retrieveData(
      String path, String method, String headers, String acceptHeader) {
    final var datasourceHeaders = HeaderExtractor.extractHeader(headers);
    if (acceptHeader != null) {
      datasourceHeaders.put(HttpHeaders.ACCEPT, acceptHeader);
    }
    final var headerConsumer = HeaderConsumer.createFromHeaders(datasourceHeaders);
    final var customClient =
        RestClient.builder().baseUrl(path).defaultHeaders(headerConsumer).build();

    final var responseEntity =
        customClient.method(HttpMethod.valueOf(method)).retrieve().toEntity(byte[].class);

    var responseContentType = responseEntity.getHeaders().getContentType();
    if (responseContentType != null) {
      if (responseContentType.toString().contains("*")) {
        // Wildcards are not allowed in Content-Type response header
        responseContentType = null;
      }
    }

    final var code = responseEntity.getStatusCode();
    final var value = code.value();
    final var httpStatus = HttpStatus.valueOf(value);

    return new DataWrapper(responseEntity.getBody(), responseContentType, httpStatus);
  }

  public static class HeaderConsumer {
    private HeaderConsumer() {
    }

    public static Consumer<HttpHeaders> createFromHeaders(Map<String, String> headers) {
      return header -> headers.forEach(header::add);
    }
  }
}
