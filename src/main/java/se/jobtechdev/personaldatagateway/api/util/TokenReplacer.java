package se.jobtechdev.personaldatagateway.api.util;

import java.util.Map;
import java.util.regex.Pattern;

public final class TokenReplacer {
  private TokenReplacer() {
  }

  public static String replace(String text, Map<String, String> tokenReplacement) {
    for (final var entry : tokenReplacement.entrySet()) {
      final var key = entry.getKey();
      final var val = entry.getValue();
      final var regex = "(\\{" + key + "\\})";
      final var pattern = Pattern.compile(regex);
      final var matcher = pattern.matcher(text);
      int lastIndex = 0;
      final var result = new StringBuilder();
      while (matcher.find()) {
        result.append(text, lastIndex, matcher.start()).append(val);
        lastIndex = matcher.end();
      }
      if (lastIndex < text.length()) {
        result.append(text, lastIndex, text.length());
      }
      text = result.toString();
    }
    return text;
  }
}
