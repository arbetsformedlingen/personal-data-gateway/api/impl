package se.jobtechdev.personaldatagateway.api.util;

import java.security.SecureRandom;
import java.util.Base64;

public final class KeyProvider {
  private static final SecureRandom random = new SecureRandom();

  private KeyProvider() {
  }

  public static String b64EncodedKey(byte[] bytes) {
    return Base64.getEncoder().withoutPadding().encodeToString(bytes);
  }

  public static byte[] randomBytes(int size) {
    final var bytes = new byte[size];
    KeyProvider.random.nextBytes(bytes);
    return bytes;
  }
}
