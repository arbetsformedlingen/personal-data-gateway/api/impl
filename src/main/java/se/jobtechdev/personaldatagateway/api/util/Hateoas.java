package se.jobtechdev.personaldatagateway.api.util;

import org.springframework.http.HttpHeaders;
import org.springframework.web.context.request.NativeWebRequest;
import se.jobtechdev.personaldatagateway.api.generated.model.Link;

import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class Hateoas {
  private static final Pattern PATTERN = Pattern.compile("uri=(.*)");

  private Hateoas() {
  }

  public static Optional<String> extractRequestUri(NativeWebRequest request, String baseUrl) {
    final var matcher = PATTERN.matcher(request.getDescription(false));

    if (matcher.find()) {
      final var uri = matcher.group(1);
      return Optional.of(baseUrl.concat(uri));
    }

    return Optional.empty();
  }

  public static String extractQueryParametersFromParameterMap(Map<String, String[]> parameterMap) {
    final var params =
        parameterMap.entrySet().stream()
            .map(
                set -> {
                  final var key = set.getKey();
                  final var values = String.join(",", set.getValue());
                  return key.concat("=").concat(values);
                })
            .collect(Collectors.joining("&"));

    return params.isEmpty() ? "" : "?".concat(params);
  }

  public static Map<String, Link> linkMap(NativeWebRequest request, String baseUrl) {
    final var uri = extractRequestUri(request, baseUrl);
    final var params = extractQueryParametersFromParameterMap(request.getParameterMap());

    if (uri.isPresent()) {
      final var selfUrl = uri.get() + params;
      return Map.of("self", new Link(UrlFactory.createURL(selfUrl)));
    }

    return Map.of();
  }

  public static HttpHeaders linkHeader(NativeWebRequest request, String baseUrl) {
    final var uri = extractRequestUri(request, baseUrl);
    final var params = extractQueryParametersFromParameterMap(request.getParameterMap());
    final var httpHeaders = new HttpHeaders();
    if (uri.isPresent()) {
      final var selfUrl = uri.get() + params;
      httpHeaders.add(HttpHeaders.LINK, "<" + selfUrl + ">; rel=\"self\"");
    }
    return httpHeaders;
  }
}
