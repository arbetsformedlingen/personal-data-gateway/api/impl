package se.jobtechdev.personaldatagateway.api.util;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

public final class UrlFactory {
  private UrlFactory() {
  }

  public static URL createURL(String url) {
    if (url == null) return null;
    try {
      return URI.create(url).toURL();
    } catch (MalformedURLException exception) {
      throw new RuntimeException(exception);
    }
  }
}
