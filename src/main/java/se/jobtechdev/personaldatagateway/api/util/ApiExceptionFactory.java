package se.jobtechdev.personaldatagateway.api.util;

import se.jobtechdev.personaldatagateway.api.exception.ApiException;
import se.jobtechdev.personaldatagateway.api.generated.model.ErrorResponse;

public final class ApiExceptionFactory {
  private ApiExceptionFactory() {
  }

  public static ApiException createApiException(ErrorResponse errorResponse) {
    return new ApiException(errorResponse);
  }
}
