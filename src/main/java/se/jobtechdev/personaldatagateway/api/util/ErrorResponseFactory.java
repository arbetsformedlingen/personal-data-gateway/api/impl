package se.jobtechdev.personaldatagateway.api.util;

import org.springframework.http.HttpStatus;
import se.jobtechdev.personaldatagateway.api.generated.model.ErrorResponse;

public final class ErrorResponseFactory {
  private ErrorResponseFactory() {
  }

  public static ErrorResponse createErrorResponse(HttpStatus httpStatus) {
    return new ErrorResponse(httpStatus.value(), httpStatus.getReasonPhrase());
  }

  public static ErrorResponse createErrorResponse(HttpStatus httpStatus, String message) {
    return createErrorResponse(httpStatus).message(message);
  }
}
