package se.jobtechdev.personaldatagateway.api.util;

import org.springframework.web.context.request.NativeWebRequest;
import se.jobtechdev.personaldatagateway.api.generated.entities.*;
import se.jobtechdev.personaldatagateway.api.generated.model.*;

import java.time.LocalDate;
import java.util.Optional;
import java.util.UUID;

import static se.jobtechdev.personaldatagateway.api.util.Hateoas.linkMap;

public final class ResponseFactory {
  private ResponseFactory() {
  }

  public static GetDataAccessResponse createGetDataAccessResponse(
      NativeWebRequest request, String baseUrl, SharingAccess sharingAccess) {
    return new GetDataAccessResponse(
        linkMap(request, baseUrl),
        sharingAccess.getId(),
        sharingAccess.getSharing().getId(),
        sharingAccess.getCreated(),
        sharingAccess.getUpdated(),
        sharingAccess.getAccessed());
  }

  public static PostOrganizationResponse createPostOrganizationResponse(
      NativeWebRequest request, String baseUrl, Organization organization) {
    final var response =
        new PostOrganizationResponse(
            linkMap(request, baseUrl),
            organization.getId(),
            organization.getOrgNr(),
            organization.getName(),
            organization.getCreated(),
            organization.getUpdated());

    response.setDeleted(organization.getDeleted());

    return response;
  }

  public static GetOrganizationResponse createGetOrganizationResponse(
      NativeWebRequest request, String baseUrl, Organization organization) {
    final var response =
        new GetOrganizationResponse(
            linkMap(request, baseUrl),
            organization.getId(),
            organization.getOrgNr(),
            organization.getName(),
            organization.getCreated(),
            organization.getUpdated());

    response.setDeleted(organization.getDeleted());

    return response;
  }

  public static PatchOrganizationResponse createPatchOrganizationResponse(
      NativeWebRequest request, String baseUrl, Organization organization) {
    final var response =
        new PatchOrganizationResponse(
            linkMap(request, baseUrl),
            organization.getId(),
            organization.getOrgNr(),
            organization.getName(),
            organization.getCreated(),
            organization.getUpdated());

    response.setDeleted(organization.getDeleted());

    return response;
  }

  public static PostOrganizationFaqResponse createPostOrganizationFaqResponse(
      NativeWebRequest request, String baseUrl, UUID organizationId, Faq faq) {
    final var response =
        new PostOrganizationFaqResponse(
            linkMap(request, baseUrl),
            faq.getId(),
            organizationId,
            faq.getLang(),
            faq.getTitle(),
            faq.getContent(),
            faq.getCreated(),
            faq.getUpdated());

    response.setDeleted(faq.getDeleted());

    return response;
  }

  public static GetOrganizationFaqResponse createGetOrganizationFaqResponse(
      NativeWebRequest request, String baseUrl, UUID organizationId, Faq faq) {
    final var response =
        new GetOrganizationFaqResponse(
            linkMap(request, baseUrl),
            faq.getId(),
            organizationId,
            faq.getLang(),
            faq.getTitle(),
            faq.getContent(),
            faq.getCreated(),
            faq.getUpdated());

    response.setDeleted(faq.getDeleted());

    return response;
  }

  public static PatchOrganizationFaqResponse createPatchOrganizationFaqResponse(
      NativeWebRequest request, String baseUrl, UUID organizationId, Faq faq) {
    final var response =
        new PatchOrganizationFaqResponse(
            linkMap(request, baseUrl),
            faq.getId(),
            organizationId,
            faq.getLang(),
            faq.getTitle(),
            faq.getContent(),
            faq.getCreated(),
            faq.getUpdated());

    response.setDeleted(faq.getDeleted());

    return response;
  }

  public static PostClientResponse createPostClientResponse(
      NativeWebRequest request, String baseUrl, Client client, String apiKey) {
    final var response =
        new PostClientResponse(
            linkMap(request, baseUrl),
            client.getId(),
            client.getOrganization().getId(),
            client.getName(),
            Role.fromValue(client.getRole()),
            client.getCreated(),
            client.getUpdated(),
            apiKey);

    response.setRevoked(client.getRevoked());

    return response;
  }

  public static GetClientResponse createGetClientResponse(
      NativeWebRequest request, String baseUrl, Client client) {
    final var response =
        new GetClientResponse(
            linkMap(request, baseUrl),
            client.getId(),
            client.getOrganization().getId(),
            client.getName(),
            Role.fromValue(client.getRole()),
            client.getCreated(),
            client.getUpdated());

    response.setRevoked(client.getRevoked());

    return response;
  }

  public static PatchClientResponse createPatchClientResponse(
      NativeWebRequest request, String baseUrl, Client client) {
    final var response =
        new PatchClientResponse(
            linkMap(request, baseUrl),
            client.getId(),
            client.getOrganization().getId(),
            client.getName(),
            client.getCreated(),
            client.getUpdated());

    response.setRevoked(client.getRevoked());

    return response;
  }

  public static PostClientRedirectResponse createPostClientRedirectResponse(
      NativeWebRequest request, String baseUrl, Redirect redirect) {
    final var response =
        new PostClientRedirectResponse(
            linkMap(request, baseUrl),
            redirect.getId(),
            redirect.getClient().getId(),
            redirect.getRedirectUrl(),
            redirect.getCreated(),
            redirect.getUpdated());

    response.setDeleted(redirect.getDeleted());

    return response;
  }

  public static GetClientRedirectResponse createGetClientRedirectResponse(
      NativeWebRequest request, String baseUrl, Redirect redirect) {
    final var response =
        new GetClientRedirectResponse(
            linkMap(request, baseUrl),
            redirect.getId(),
            redirect.getClient().getId(),
            redirect.getRedirectUrl(),
            redirect.getCreated(),
            redirect.getUpdated());

    response.setDeleted(redirect.getDeleted());

    return response;
  }

  public static PatchClientRedirectResponse createPatchClientRedirectResponse(
      NativeWebRequest request, String baseUrl, Redirect redirect) {
    final var response =
        new PatchClientRedirectResponse(
            linkMap(request, baseUrl),
            redirect.getId(),
            redirect.getClient().getId(),
            redirect.getRedirectUrl(),
            redirect.getCreated(),
            redirect.getUpdated());

    response.setDeleted(redirect.getDeleted());

    return response;
  }

  public static GetClientKeyResetResponse createGetClientKeyResetResponse(
      NativeWebRequest request, String baseUrl, ClientKeyReset clientKeyReset) {
    return new GetClientKeyResetResponse(
        linkMap(request, baseUrl),
        clientKeyReset.getId(),
        clientKeyReset.getClient().getId(),
        clientKeyReset.getCreated(),
        clientKeyReset.getUpdated());
  }

  public static PostClientKeyResetResponse createPostClientKeyResetResponse(
      NativeWebRequest request, String baseUrl, ClientKeyReset clientKeyReset, String apiKey) {
    return new PostClientKeyResetResponse(
        linkMap(request, baseUrl),
        clientKeyReset.getId(),
        clientKeyReset.getClient().getId(),
        clientKeyReset.getCreated(),
        clientKeyReset.getUpdated(),
        apiKey);
  }

  public static PostDatasourceResponse createPostDatasourceResponse(
      NativeWebRequest request, String baseUrl, Datasource datasource) {
    final var response =
        new PostDatasourceResponse(
            linkMap(request, baseUrl),
            datasource.getId(),
            datasource.getOrganization().getId(),
            datasource.getName(),
            datasource.getCreated(),
            datasource.getUpdated());

    response.setDeleted(datasource.getDeleted());

    return response;
  }

  public static GetDatasourceResponse createGetDatasourceResponse(
      NativeWebRequest request, String baseUrl, Datasource datasource) {
    final var response =
        new GetDatasourceResponse(
            linkMap(request, baseUrl),
            datasource.getId(),
            datasource.getOrganization().getId(),
            datasource.getName(),
            datasource.getCreated(),
            datasource.getUpdated());

    response.setDeleted(datasource.getDeleted());

    return response;
  }

  public static PatchDatasourceResponse createPatchDatasourceResponse(
      NativeWebRequest request, String baseUrl, Datasource datasource) {
    final var response =
        new PatchDatasourceResponse(
            linkMap(request, baseUrl),
            datasource.getId(),
            datasource.getOrganization().getId(),
            datasource.getName(),
            datasource.getCreated(),
            datasource.getUpdated());

    response.setDeleted(datasource.getDeleted());

    return response;
  }

  public static GetDatasourceConfigResponse createGetDatasourceConfigResponse(
      NativeWebRequest request, String baseUrl, Datasource datasource) {
    final var response =
        new GetDatasourceConfigResponse(
            linkMap(request, baseUrl),
            datasource.getId(),
            datasource.getPath(),
            datasource.getMethod(),
            datasource.getCreated(),
            datasource.getUpdated());

    response.setDeleted(datasource.getDeleted());

    return response;
  }

  public static PatchDatasourceConfigResponse createPatchDatasourceConfigResponse(
      NativeWebRequest request, String baseUrl, Datasource datasource) {
    final var response =
        new PatchDatasourceConfigResponse(
            linkMap(request, baseUrl),
            datasource.getId(),
            datasource.getPath(),
            datasource.getMethod(),
            datasource.getCreated(),
            datasource.getUpdated());

    response.setDeleted(datasource.getDeleted());

    return response;
  }

  public static PostDatasourceFaqResponse createPostDatasourceFaqResponse(
      NativeWebRequest request, String baseUrl, Datasource datasource, Faq faq) {
    final var response =
        new PostDatasourceFaqResponse(
            linkMap(request, baseUrl),
            faq.getId(),
            datasource.getId(),
            faq.getLang(),
            faq.getTitle(),
            faq.getContent(),
            faq.getCreated(),
            faq.getUpdated());

    response.setDeleted(faq.getDeleted());

    return response;
  }

  public static GetDatasourceFaqResponse createGetDatasourceFaqResponse(
      NativeWebRequest request, String baseUrl, UUID datasourceId, Faq faq) {
    final var response =
        new GetDatasourceFaqResponse(
            linkMap(request, baseUrl),
            faq.getId(),
            datasourceId,
            faq.getLang(),
            faq.getTitle(),
            faq.getContent(),
            faq.getCreated(),
            faq.getUpdated());

    response.setDeleted(faq.getDeleted());

    return response;
  }

  public static PatchDatasourceFaqResponse createPatchDatasourceFaqResponse(
      NativeWebRequest request, String baseUrl, UUID datasourceId, Faq faq) {
    final var response =
        new PatchDatasourceFaqResponse(
            linkMap(request, baseUrl),
            faq.getId(),
            datasourceId,
            faq.getLang(),
            faq.getTitle(),
            faq.getContent(),
            faq.getCreated(),
            faq.getUpdated());

    response.setDeleted(faq.getDeleted());

    return response;
  }

  public static PostUserSharingResponse createPostUserSharingResponse(
      NativeWebRequest request, String baseUrl, Sharing sharing) {
    final var response =
        new PostUserSharingResponse(
            linkMap(request, baseUrl),
            sharing.getId(),
            sharing.getDatasource().getId(),
            sharing.getPerson().getId(),
            null == sharing.getClient(),
            sharing.getCreated(),
            sharing.getUpdated());

    response.clientId(Optional.ofNullable(sharing.getClient()).map(Client::getId).orElse(null));
    response.setRevoked(sharing.getRevoked());
    response.setExpires(sharing.getExpires());

    return response;
  }

  public static GetUserSharingResponse createGetUserSharingResponse(
      NativeWebRequest request, String baseUrl, Sharing sharing) {
    final var response =
        new GetUserSharingResponse(
            linkMap(request, baseUrl),
            sharing.getId(),
            sharing.getDatasource().getId(),
            sharing.getPerson().getId(),
            null == sharing.getClient(),
            sharing.getCreated(),
            sharing.getUpdated());

    response.clientId(Optional.ofNullable(sharing.getClient()).map(Client::getId).orElse(null));
    response.setRevoked(sharing.getRevoked());
    response.setExpires(sharing.getExpires());

    return response;
  }

  public static PatchUserSharingResponse createPatchUserSharingResponse(
      NativeWebRequest request, String baseUrl, Sharing sharing) {
    final var response =
        new PatchUserSharingResponse(
            linkMap(request, baseUrl),
            sharing.getId(),
            sharing.getDatasource().getId(),
            sharing.getPerson().getId(),
            null == sharing.getClient(),
            sharing.getCreated(),
            sharing.getUpdated());

    response.clientId(Optional.ofNullable(sharing.getClient()).map(Client::getId).orElse(null));
    response.setRevoked(sharing.getRevoked());
    response.setExpires(sharing.getExpires());

    return response;
  }

  public static GetApiInfoResponse getApiInfoResponse(
      String apiName,
      String apiVersion,
      String buildVersion,
      LocalDate apiReleased,
      java.net.URL apiDocumentation,
      ApiStatus apiStatus) {
    return new GetApiInfoResponse(
        apiName, apiVersion, buildVersion, apiReleased, apiDocumentation, apiStatus);
  }

  public static GetHealthResponse getHealthResponse(Status status, HealthChecks checks) {
    return new GetHealthResponse(status, checks);
  }
}
