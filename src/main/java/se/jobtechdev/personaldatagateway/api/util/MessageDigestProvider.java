package se.jobtechdev.personaldatagateway.api.util;

import java.security.NoSuchAlgorithmException;

import static java.security.MessageDigest.getInstance;

public final class MessageDigestProvider {
  private MessageDigestProvider() {
  }

  public static byte[] sha256(byte[] bytes) {
    try {
      return getInstance("SHA-256").digest(bytes);
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
  }
}
