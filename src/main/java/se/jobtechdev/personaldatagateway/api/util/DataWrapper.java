package se.jobtechdev.personaldatagateway.api.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

public record DataWrapper(byte[] body, MediaType contentType, HttpStatus status) {}
