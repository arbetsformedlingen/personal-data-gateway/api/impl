package se.jobtechdev.personaldatagateway.api.util;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

public final class HeaderExtractor {
  private HeaderExtractor() {
  }

  public static Map<String, String> extractHeader(String headerString) {
    if (headerString == null) {
      return Map.of();
    }
    final var headerValues = headerString.split("&");
    final var entries =
        Arrays.stream(headerValues)
            .filter(h -> h.contains("="))
            .map(
                h -> {
                  final var kv = h.split("=");
                  return new AbstractMap.SimpleEntry<>(kv[0], kv[1]);
                })
            .toList();
    final var map = new LinkedHashMap<String, String>();
    for (Map.Entry<String, String> entry : entries) {
      map.put(entry.getKey(), entry.getValue());
    }
    return map;
  }
}
