package se.jobtechdev.personaldatagateway.api.util;

import java.util.function.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class LoggerSupplier implements Supplier<Logger> {
  private final Class<?> clazz;

  private LoggerSupplier(Class<?> clazz) {
    this.clazz = clazz;
  }

  public static LoggerSupplier forClass(Class<?> clazz) {
    return new LoggerSupplier(clazz);
  }

  @Override
  public Logger get() {
    return LoggerFactory.getLogger(clazz.getName());
  }
}
