# Semantic versioning

Releases of this software follows [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html).

MAJOR releases MAY require the user of this software to make adjustments (see changelog and release notes for more detail).

MINOR and PATCH releases of this software MUST not require changes in:

- Deployments and configurations of this software.
- Client integration with endpoints of this software.
- External platforms or services which this software depends on.

Changes to any of the following parts MUST be carefully evaluated when determining the semantic version number of the software release.

- Endpoints, including path, headers, parameters, body, and side effects.
- Configuration properties passed via file, cli, or environment variables.
- Integrations with external platforms or services.
